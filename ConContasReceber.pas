unit ConContasReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, JvBaseEdits, Vcl.Mask, JvExMask, JvToolEdit;

type
  TF_ConContasReceber = class(TF_ConPadrao)
    rgFiltrar: TRadioGroup;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    edDataFim: TJvDateEdit;
    edDataIni: TJvDateEdit;
    rgTipoPgto: TRadioGroup;
    GroupBox2: TGroupBox;
    edDescricao: TEdit;
    cbPesquisar: TComboBox;
    edValor: TJvCalcEdit;
    FDContasReceber: TFDQuery;
    ProviderContasReceber: TDataSetProvider;
    CdsContasReceber: TClientDataSet;
    DsContasReceber: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConContasReceber              : TF_ConContasReceber;
  xAux_ConContasReceber_Tela      : String;
  xAux_ConContasReceber_Venda_ID  : Integer;
  xAux_ConContasReceber_CR_ID     : Integer;
  xAux_ConContasReceber_Valor     : Double;
  xAux_ConContasReceber_Tipo_pgto : Integer;

implementation

{$R *.dfm}

uses DMC, X;

end.
