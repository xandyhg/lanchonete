inherited F_CadCredito: TF_CadCredito
  Caption = 'Cadastro de Cr'#233'dito'
  ClientHeight = 201
  ClientWidth = 684
  OnActivate = FormActivate
  ExplicitWidth = 700
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 124
    Width = 684
    ExplicitTop = 124
    ExplicitWidth = 684
    inherited icoFechar: TImage
      Left = 594
      ExplicitLeft = 594
    end
    inherited icoAlterar: TImage
      Left = 414
      OnClick = icoAlterarClick
      ExplicitLeft = 414
    end
    inherited icoExcluir: TImage
      Left = 504
      OnClick = icoExcluirClick
      ExplicitLeft = 504
    end
    inherited icoSalvar: TImage
      Left = 234
      OnClick = icoSalvarClick
      ExplicitLeft = 234
    end
    inherited icoCancelar: TImage
      Left = 324
      OnClick = icoCancelarClick
      ExplicitLeft = 324
    end
  end
  inherited Panel3: TPanel
    Width = 684
    Height = 83
    ExplicitWidth = 684
    ExplicitHeight = 83
    object Label2: TLabel
      Left = 193
      Top = 49
      Width = 30
      Height = 17
      Alignment = taRightJustify
      Caption = 'Valor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 26
      Top = 49
      Width = 53
      Height = 17
      Alignment = taRightJustify
      Caption = 'Cadastro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 9
      Top = 10
      Width = 70
      Height = 17
      Caption = 'C'#243'd. Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object sbEntidade: TSpeedButton
      Left = 147
      Top = 6
      Width = 25
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000585751747069
        E1DFDFE4E3E2EBEAE9FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFA5A39B9090878D8A81FFFFFFFFFFFFEFEEEEEBEAE9F1
        F1F0F7F7F6F8F7F7F9F9F9FBFBFBFDFDFDFEFEFEFFFFFFFFFFFFFEFEFEA9A7A1
        8B8B8189867DFFFFFFFFFFFFF7F7F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFAA9A7A18B8B8189867DFFFFFFFFFFFFFE
        FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
        FBFBFAA9A7A18A8B8189867DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABA9A389897F6E6C66FF
        FFFFF9F9FBC2C4C7C3C6CAC2C4C6D4D4D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF9F8F8D2D2CE5F5F579192949D988CCBA865CEA558D4B781C9C1
        B0A8AAABF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D3D6B0
        A382DCAA34D9AE43D7AB3FD7A02CDFA73EDDC599ABACACFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFCFCFE999588E2C366D7C46BD6C66FD6C36BD6BA5CD5A9
        3FDCA233D5C5A4C7C9CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D9BEB286DB
        CE78E1E2ACD5DA8FD5D78AD6CC7AD7BD61D5A537E2B057B4B6B7FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFDBDCDEBDB481E1E4A6E4E9BAEFF2D7E0E6B1D5DA8FD6C9
        74D6B34FE0A638ACA69AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFE1BCB78BEE
        F2CCECEFCFFCFDFAF6F7E8DFE5AED4CC76D5B654DEA42FABA69AFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFECECEDB1AE95F4F7D2F4F6E3F6F7E7F5F7E6EDF1D3E9E5
        B9E3CD86D7A949B8BCC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBEBEC8
        C8A0F9FCE2ECF0CFDAE1A2EAECC7EFE9C3F8E4B2A19A87EEEFF0FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFDFEFEB6B4B1B8B694F7F8D7E9E9AFDBCA66DECD
        999C9581D3D4D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFDCDCDDA6A49BA39F91A19C8EB6B7B7F0F1F2FFFFFFFFFFFF}
      ParentFont = False
      OnClick = sbEntidadeClick
    end
    object edCred_Valor: TJvCalcEdit
      Left = 229
      Top = 46
      Width = 68
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      MinValue = 1.000000000000000000
      ParentFont = False
      ShowButton = False
      TabOrder = 2
      Value = 1.000000000000000000
      DecimalPlacesAlwaysShown = False
    end
    object edCred_Data: TJvDateEdit
      Left = 85
      Top = 46
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 1
    end
    object rgCred_Tipo: TRadioGroup
      Left = 303
      Top = 31
      Width = 152
      Height = 45
      Caption = 'Tipo'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Cr'#233'dito'
        'D'#233'bito')
      TabOrder = 3
    end
    object GroupBox1: TGroupBox
      Left = 461
      Top = 31
      Width = 219
      Height = 45
      Caption = 'C'#243'd. Pagamento'
      Enabled = False
      TabOrder = 4
      object Label5: TLabel
        Left = 6
        Top = 18
        Width = 50
        Height = 17
        Alignment = taRightJustify
        Caption = 'C'#243'd. CR.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 127
        Top = 19
        Width = 25
        Height = 17
        Alignment = taRightJustify
        Caption = 'Seq.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object edCr_id: TJvCalcEdit
        Left = 62
        Top = 15
        Width = 56
        Height = 25
        TabStop = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ShowButton = False
        TabOrder = 0
        DecimalPlacesAlwaysShown = False
      end
      object edCrp_seq: TJvCalcEdit
        Left = 158
        Top = 15
        Width = 56
        Height = 25
        TabStop = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ShowButton = False
        TabOrder = 1
        DecimalPlacesAlwaysShown = False
      end
    end
    object edCodigo: TJvCalcEdit
      Left = 85
      Top = 7
      Width = 56
      Height = 25
      DecimalPlaces = 0
      DisplayFormat = ',0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
      OnExit = edCodigoExit
      OnKeyDown = edCodigoKeyDown
    end
    object edEnt_Nome: TEdit
      Left = 176
      Top = 7
      Width = 499
      Height = 25
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
    end
  end
  inherited Panel4: TPanel
    Top = 169
    Width = 684
    ExplicitTop = 169
    ExplicitWidth = 684
    inherited pnCancelar: TPanel
      Left = 324
      ExplicitLeft = 324
    end
    inherited pnAlterar: TPanel
      Left = 414
      ExplicitLeft = 414
    end
    inherited pnExcluir: TPanel
      Left = 504
      ExplicitLeft = 504
    end
    inherited pnSalvar: TPanel
      Left = 234
      ExplicitLeft = 234
    end
    inherited pnFechar: TPanel
      Left = 594
      ExplicitLeft = 594
    end
  end
  inherited Panel5: TPanel
    Width = 684
    ExplicitWidth = 684
    object Label3: TLabel
      Left = 5
      Top = 12
      Width = 74
      Height = 17
      Caption = 'C'#243'd. Cr'#233'dito'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCred_ID: TJvCalcEdit
      Left = 85
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      DecimalPlaces = 0
      DisplayFormat = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Timer1: TTimer
    Left = 240
    Top = 0
  end
end
