unit AuxCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AuxPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits;

type
  TF_AuxCompra = class(TF_AuxPadrao)
    edIC_valor_total: TJvCalcEdit;
    Label4: TLabel;
    GroupBox1: TGroupBox;
    mCom_obs: TMemo;
    procedure icoCancelarClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_AuxCompra                : TF_AuxCompra;
  xAux_xAuxCompra_ValorTotal : Double;
  xAux_xAuxCompra_Obs        : String;

implementation

{$R *.dfm}

procedure TF_AuxCompra.icoCancelarClick(Sender: TObject);
begin
  inherited;
  xAux_xAuxCompra_ValorTotal := 0;
  xAux_xAuxCompra_Obs        := '';
  Close;
end;

procedure TF_AuxCompra.icoSalvarClick(Sender: TObject);
begin
  inherited;
  xAux_xAuxCompra_ValorTotal := edIC_valor_total.Value;
  xAux_xAuxCompra_Obs        := mCom_obs.Text;
  Close;
end;

end.
