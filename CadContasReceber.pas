unit CadContasReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, JvToolEdit, Vcl.Mask, JvExMask,
  JvBaseEdits, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.Buttons,
  Data.DB, Datasnap.DBClient, FireDAC.Comp.Client;

type
  TF_CadContasReceber = class(TF_CadPadrao)
    Label1: TLabel;
    edCodigo: TJvCalcEdit;
    Label7: TLabel;
    edVenda_id: TJvCalcEdit;
    edCr_datacadastro: TJvDateEdit;
    Label3: TLabel;
    edCr_valor: TJvCalcEdit;
    Label4: TLabel;
    edCr_datavencimento: TJvDateEdit;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    mCr_obs: TMemo;
    Button1: TButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    GroupBox3: TGroupBox;
    JvDBGrid1: TJvDBGrid;
    mCrp_obs: TMemo;
    edCrp_valorpago: TJvCalcEdit;
    edCrp_datapagamento: TJvDateEdit;
    Label5: TLabel;
    Label6: TLabel;
    rgCrp_tipo_pgto: TRadioGroup;
    CdsPagamentos: TClientDataSet;
    DsPagamentos: TDataSource;
    CdsPagamentoscr_id: TIntegerField;
    CdsPagamentoscrp_seq: TIntegerField;
    CdsPagamentoscrp_valorpago: TFloatField;
    CdsPagamentoscrp_datapagamento: TDateField;
    CdsPagamentoscrp_tipo_pgto: TIntegerField;
    CdsPagamentoscrp_obs: TBlobField;
    Label8: TLabel;
    edCrp_seq: TJvCalcEdit;
    Label10: TLabel;
    cbCr_Status: TComboBox;
    CdsPagamentoscrp_databaixa: TDateField;
    edCrp_dataBaixa: TJvDateEdit;
    Label9: TLabel;
    CdsPagamentostotPago: TAggregateField;
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadContasReceber: TF_CadContasReceber;

implementation

{$R *.dfm}

uses ConContasReceber, X, DMC, clsContasReceber, clsContasReceber_Pgto,
  U_Principal, clsItens_Venda;

var
  ContasReceber     : TContasReceber;
  ContasReceberPgto : TContasReceber_Pgto;
  tq                : TFDQuery;
  Itens_venda       : TItens_Venda;

procedure TF_CadContasReceber.Button1Click(Sender: TObject);
begin
  inherited;
  if CdsPagamentos.Locate('crp_seq',edCrp_seq.AsInteger,[]) then begin
    CdsPagamentos.Edit;
    CdsPagamentos.FieldByName('crp_seq').AsInteger          := edCrp_seq.AsInteger;
  end else begin
    CdsPagamentos.Append;
    CdsPagamentos.FieldByName('crp_seq').AsInteger          := CdsPagamentos.RecordCount+1;
  end;

  CdsPagamentos.FieldByName('cr_id').AsInteger              := edCodigo.AsInteger;
  CdsPagamentos.FieldByName('crp_valorpago').AsFloat        := edCrp_valorpago.Value;
  CdsPagamentos.FieldByName('crp_datapagamento').AsDateTime := edCrp_datapagamento.Date;
  CdsPagamentos.FieldByName('crp_tipo_pgto').AsInteger      := rgCrp_tipo_pgto.ItemIndex;
  CdsPagamentos.FieldByName('crp_obs').AsString             := mCrp_obs.Text;
  CdsPagamentos.FieldByName('crp_databaixa').AsDateTime     := Date;
  CdsPagamentos.Post;
  if SpeedButton2.Enabled = False then begin
    SpeedButton2.Enabled := True;
    SpeedButton3.Enabled := True;
  end;
  edCrp_seq.Clear;
end;

procedure TF_CadContasReceber.FormActivate(Sender: TObject);
begin
  inherited;
  CdsPagamentos.Close;
  CdsPagamentos.CreateDataSet;
  CdsPagamentos.Open;

  if xAux_ConContasReceber_Tela = 'VENDA' then begin
    edCodigo.AsInteger        := xAux_ConContasReceber_CR_ID;
    edVenda_id.AsInteger      := xAux_ConContasReceber_Venda_ID;
    edCr_datacadastro.Date    := Date;
    edCr_datavencimento.Date  := Date+30;
    edCr_valor.Value          := xAux_ConContasReceber_Valor;
    rgCrp_tipo_pgto.ItemIndex := 0;
    edCrp_datapagamento.Date  := Date;
    mCr_obs.Text              := '';
    mCrp_obs.Text             := '';
    edCrp_valorpago.Value     := 0;
    edCrp_dataBaixa.Date      := 0;

    Panel3.Enabled := True;
    ControleBotoes(2);

    edCrp_valorpago.SetFocus;
  end;
end;

procedure TF_CadContasReceber.icoSalvarClick(Sender: TObject);
var
  xValorPago : Double;
  xCr_id     : Integer;
  xCrp_seq   : Integer;
begin
  inherited;
  if edCr_datavencimento.Date = 0 then begin
    msgPerson('Informe uma data de vencimento!',[mbOK], 'Contas a Receber', mtError);
    Exit;
  end;
  Try
    DM.FDCon.StartTransaction;
    ContasReceber                   := TContasReceber.Create;
    ContasReceberPgto               := TContasReceber_Pgto.Create;

    ContasReceber.venda_id          := edVenda_id.AsInteger;
    ContasReceber.cr_valor          := edCr_valor.Value;
    ContasReceber.cr_datacadastro   := edCr_datacadastro.Date;
    ContasReceber.cr_datavencimento := edCr_datavencimento.Date;
    ContasReceber.cr_obs            := mCr_obs.Text;
    ContasReceber.cr_status         := cbCr_Status.ItemIndex;
    xAux_ConContasReceber_Tipo_pgto := cbCr_Status.ItemIndex;;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConContasReceber_CR_ID := ContasReceber.Ultima;
      ContasReceber.Gravar(xAux_ConContasReceber_CR_ID);
    end else begin
      ContasReceber.Gravar(xAux_ConContasReceber_CR_ID);
    end;

//    ContasReceberPgto.EliminarTodosItens(xAux_ConContasReceber_CR_ID);
    CdsPagamentos.First;
    while not CdsPagamentos.Eof do begin
      ContasReceberPgto.crp_valorpago     := CdsPagamentos.FieldByName('crp_valorpago').AsFloat;
      ContasReceberPgto.crp_datapagamento := CdsPagamentos.FieldByName('crp_datapagamento').AsDateTime;
      ContasReceberPgto.crp_tipo_pgto     := CdsPagamentos.FieldByName('crp_tipo_pgto').AsInteger;
      ContasReceberPgto.crp_obs           := CdsPagamentos.FieldByName('crp_obs').AsString;
      ContasReceberPgto.crp_databaixa     := CdsPagamentos.FieldByName('crp_databaixa').AsDateTime;
      ContasReceberPgto.Gravar(CdsPagamentos.FieldByName('cr_id').AsInteger,
                               CdsPagamentos.FieldByName('crp_seq').AsInteger);
      CdsPagamentos.Next;
    end;

    FreeAndNil(ContasReceber);
    FreeAndNil(ContasReceberPgto);

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;

  Try
    DM.FDCon.StartTransaction;
    ContasReceber                   := TContasReceber.Create;
    ContasReceberPgto               := TContasReceber_Pgto.Create;
    Itens_venda                     := TItens_Venda.Create;

    tq := TFDQuery.Create(nil);
    tq.Connection := DM.FDCon;
    tq.Close;
    tq.Params.Clear;
    tq.SQL.Clear;
    tq.SQL.Add('select iv.*, v.venda_status, v.ent_id                '+
               'from itens_venda iv                                  '+
               'inner join vendas v on iv.venda_id=v.venda_id        '+
               'where v.venda_id = :k_venda_id and iv.iv_status <> 1 '+
               'order by iv.iv_status desc                           ');
    tq.ParamByName('k_venda_id').AsInteger := edVenda_id.AsInteger;
    tq.Open;

    CdsPagamentos.Filter   := 'crp_databaixa='+QuotedStr(DateToStr(Date));
    CdsPagamentos.Filtered := True;
    CdsPagamentos.Last;
    xValorPago := CdsPagamentostotPago.Value;
    xCr_id     := CdsPagamentos.FieldByName('cr_id').AsInteger;
    xCrp_seq   := CdsPagamentos.FieldByName('crp_seq').AsInteger;
    while not tq.Eof do begin
      if xValorPago >= tq.FieldByName('iv_valor_restante').AsFloat then begin
        Itens_venda.AtualizaValorRestante(tq.FieldByName('venda_id').AsInteger,
                                          tq.FieldByName('pro_id').AsInteger,
                                          1,
                                          0);
        xValorPago := xValorPago - tq.FieldByName('iv_valor_restante').AsFloat;
      end else begin
        xValorPago := tq.FieldByName('iv_valor_restante').AsFloat - xValorPago;
        Itens_venda.AtualizaValorRestante(tq.FieldByName('venda_id').AsInteger,
                                          tq.FieldByName('pro_id').AsInteger,
                                          3,
                                          xValorPago);
        xValorPago := 0;
      end;
      tq.Next;
    end;

    FreeAndNil(ContasReceber);
    FreeAndNil(ContasReceberPgto);
    FreeAndNil(Itens_venda);

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Conta '+IntToStr(xAux_ConContasReceber_CR_ID)+' cadastrada com sucesso!',[mbOK], 'Contas a Receber', mtConfirmation)
    else
      msgPerson('Conta '+IntToStr(xAux_ConContasReceber_CR_ID)+' alterada com sucesso!',[mbOK], 'Contas a Receber', mtConfirmation);

    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

procedure TF_CadContasReceber.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  if CdsPagamentos.RecordCount > 0 then begin
    CdsPagamentos.Delete;
  end else begin
    SpeedButton2.Enabled := False;
    SpeedButton3.Enabled := False;
  end;
end;

procedure TF_CadContasReceber.SpeedButton3Click(Sender: TObject);
begin
  inherited;
  if CdsPagamentos.RecordCount > 0 then begin
    rgCrp_tipo_pgto.ItemIndex := CdsPagamentos.FieldByName('crp_tipo_pgto').AsInteger;
    edCrp_datapagamento.Date  := CdsPagamentos.FieldByName('crp_datapagamento').AsDateTime;
    edCrp_valorpago.Value     := CdsPagamentos.FieldByName('crp_valorpago').AsFloat;
    mCrp_obs.Text             := CdsPagamentos.FieldByName('crp_obs').AsString;
    edCrp_seq.AsInteger       := CdsPagamentos.FieldByName('crp_seq').AsInteger;
    SpeedButton2.Enabled      := False;
    SpeedButton3.Enabled      := False;
    SpeedButton4.Enabled      := True;
  end;
end;

end.
