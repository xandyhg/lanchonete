unit DMC;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  Vcl.Dialogs, Vcl.ExtDlgs, FireDAC.Comp.UI, FireDAC.Phys.IBBase,
  FireDAC.Phys.FB, Data.DB, FireDAC.Comp.Client, IniFiles, Vcl.Forms;

type
  TDM = class(TDataModule)
    FDCon: TFDConnection;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    OpenTextFileDialog1: TOpenTextFileDialog;
    FDTransaction1: TFDTransaction;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ConectarBanco(Arquivo:String);
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses X, U_Principal;

var
  Conexao : String;

{$R *.dfm}

{ TDM }

procedure TDM.ConectarBanco(Arquivo: String);
var
  vArqIni                     : TIniFile;
  Aplicativo, Caminho, ArqGdb : String;
begin
  Aplicativo := Arquivo;

  ArqGdb     := Aplicativo + '.fdb';
  Caminho    := ExtractFilePath(Application.ExeName);
  try
    if not(fileexists( Caminho + Aplicativo + '.Ini' ) ) then begin
      Conexao := Caminho + ArqGdb;
      vArqIni := TIniFile.Create( Caminho + Aplicativo +  '.Ini' );
      try
        vArqIni.WriteString('conexao', 'DataBase', Conexao);
      finally
        vArqIni.Free;
      end;
    end else begin
      vArqIni   := TIniFile.Create( Caminho + Aplicativo +  '.Ini' );
      try
        if vArqIni.ReadString('conexao', 'DataBase', '')<>'' then begin
          Conexao := vArqIni.ReadString('conexao', 'DataBase', '');
        end else begin
          Conexao := Caminho + ArqGdb;
          vArqIni.WriteString('conexao', 'DataBase', Conexao);
        end;
      finally
        vArqIni.Free;
      end;
    end;
  except
    msgPerson('Erro ao tentar conectar com o banco de dados!',[mbok],'Aten��o',mtError);
    Application.Terminate;
  end;

  try
    if FDCon.Connected then
      FDCon.Connected := False;

    FDCon.Params.Clear;
    FDCon.Params.Add('Database='+Conexao);
    FDCon.Params.Add('SQLDialect=1');
    FDCon.Params.Add('User_Name=sysdba');
    FDCon.Params.Add('Password=masterkey');
    FDCon.Params.Add('DriverID=FB');
    FDCon.Connected := True;

  except
    On e: Exception do begin
      ShowMessage('Erro na conex�o: '+e.Message);
      Application.Terminate;
      Raise;
    end;
  end;
end;

end.
