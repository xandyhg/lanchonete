unit CadContasPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, JvToolEdit, Vcl.StdCtrls, Vcl.Buttons, Vcl.Mask,
  JvExMask, JvBaseEdits, FireDAC.Comp.Client;

type
  TF_CadContasPagar = class(TF_CadPadrao)
    edCodigo: TJvCalcEdit;
    Label1: TLabel;
    edCp_datacompra: TJvDateEdit;
    Label3: TLabel;
    rgCp_tipo_pgto: TRadioGroup;
    edCp_valor: TJvCalcEdit;
    Label4: TLabel;
    edCp_datavencimento: TJvDateEdit;
    Label2: TLabel;
    edCp_datapagamento: TJvDateEdit;
    Label5: TLabel;
    edCP_valorpago: TJvCalcEdit;
    Label6: TLabel;
    GroupBox1: TGroupBox;
    mCP_obs: TMemo;
    edCom_id: TJvCalcEdit;
    Label7: TLabel;
    icoEstornarNota: TImage;
    pnEstornarNota: TPanel;
    Label11: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
    procedure rgCp_tipo_pgtoClick(Sender: TObject);
    procedure icoEstornarNotaClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadContasPagar: TF_CadContasPagar;

implementation

{$R *.dfm}

uses clsEntidade, X, ConEntidade, U_Principal, DMC, clsContasPagar,
  ConContasPagar;

var
  Entidade    : TEntidade;
  ContasPagar : TContasPagar;

procedure TF_CadContasPagar.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_ConContasPagar_CP_ID > 0 then begin
    ContasPagar := TContasPagar.Create;
    if ContasPagar.Recuperar(xAux_ConContasPagar_CP_ID) then begin
      edCodigo.AsInteger       := xAux_ConContasPagar_CP_ID;
      edCom_id.AsInteger       := ContasPagar.com_id;
      edCp_datacompra.Date     := ContasPagar.cp_datacompra;
      rgCp_tipo_pgto.ItemIndex := ContasPagar.cp_tipo_pgto;
      edCp_valor.Value         := ContasPagar.cp_valor;
      edCP_valorpago.Value     := ContasPagar.cp_valorpago;
      edCp_datavencimento.Date := ContasPagar.cp_datavencimento;
      edCp_datapagamento.Date  := ContasPagar.cp_datapagamento;
      mCP_obs.Text             := ContasPagar.cp_obs;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(ContasPagar);

    if edCom_id.AsInteger > 0 then begin
      if edCP_valorpago.Value <> 0 then begin
        msgPerson('Conta j� est� baixada! N�o poder� ser alterada!',[mbok],'Contas a Pagar',mtError);
        icoAlterar.Hide;
        pnAlterar.Hide;
        icoExcluir.Hide;
        pnExcluir.Hide;
        icoEstornarNota.Show;
        pnEstornarNota.Show;
      end else begin
        msgPerson('Conta n�o poder� ser alterada a data nem valor,'+#13+'devido a estar amarrado a uma nota de compra!',[mbok],'Contas a Pagar',mtConfirmation);
        edCp_datacompra.Enabled := False;
        edCp_valor.Enabled      := False;
      end;
    end else begin
      if edCP_valorpago.Value <> 0 then begin
        msgPerson('Conta j� est� baixada! N�o poder� ser alterada!',[mbok],'Contas a Pagar',mtError);
        icoAlterar.Hide;
        pnAlterar.Hide;
        icoExcluir.Hide;
        pnExcluir.Hide;
      end;
    end;
  end else begin
    edCom_id.AsInteger       := xAux_ConContasPagar_COM_ID; //C�digo da Compra
    edCp_datacompra.Date     := Date;
    rgCp_tipo_pgto.ItemIndex := 1;
    edCp_valor.Clear;
    edCP_valorpago.Clear;
    edCp_datavencimento.Date := 0;
    edCp_datapagamento.Date  := 0;
    mCP_obs.Clear;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);

    if xAux_ConContasPagar_Tela = 'COMPRA' then begin
      edCp_valor.Enabled       := False;
      edCp_valor.Value         := xAux_ConContasPagar_Valor;
      edCP_valorpago.Value     := xAux_ConContasPagar_Valor;
      edCp_datavencimento.Date := edCp_datacompra.Date;
      edCp_datapagamento.Date  := edCp_datacompra.Date;
    end;
  end;
end;

procedure TF_CadContasPagar.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_F7) and (icoEstornarNota.Visible = True) then
    icoEstornarNota.OnClick(Self);
end;

procedure TF_CadContasPagar.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadContasPagar.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConContasPagar_CP_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadContasPagar.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Contas a Pagar',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    ContasPagar := TContasPagar.Create;

    ContasPagar.Eliminar(edCodigo.AsInteger);

//    xAux_AuxJustificativa_Codigo   := edCodigo.AsInteger;
//    xAux_AuxJustificativa_NomeForm := UpperCase(Copy(Name,6,Length(Name)));
//    xAux_AuxJustificativa_Descricao:= edCid_Cidade.Text;
//    AbreForm(TF_AuxJustificativa,Self);
//    xAux_AuxJustificativa_Codigo   := 0;
//    xAux_AuxJustificativa_NomeForm := '';
//    xAux_AuxJustificativa_Descricao:= '';

    msgPerson('Registro eliminado com sucesso!',[mbok],'Contas a Pagar',mtConfirmation);

    FreeAndNil(ContasPagar);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadContasPagar.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    ContasPagar := TContasPagar.Create;

    ContasPagar.cp_datacompra     := edCp_datacompra.Date;
    ContasPagar.com_id            := edCom_id.AsInteger;
    ContasPagar.cp_tipo_pgto      := rgCp_tipo_pgto.ItemIndex;
    ContasPagar.cp_valor          := edCp_valor.Value;
    ContasPagar.cp_valorpago      := edCP_valorpago.Value;
    ContasPagar.cp_datavencimento := edCp_datavencimento.Date;
    ContasPagar.cp_datapagamento  := edCp_datapagamento.Date;
    ContasPagar.cp_obs            := mCP_obs.Text;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConContasPagar_CP_ID     := ContasPagar.Ultima;
      ContasPagar.Gravar(xAux_ConContasPagar_CP_ID);
    end else begin
      ContasPagar.Gravar(xAux_ConContasPagar_CP_ID);
    end;

    FreeAndNil(ContasPagar);

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Conta a pagar '+IntToStr(xAux_ConContasPagar_CP_ID)+' cadastrada com sucesso!',[mbOK], 'Contas a Pagar', mtConfirmation)
    else
      msgPerson('Conta a pagar '+IntToStr(xAux_ConContasPagar_CP_ID)+' alterada com sucesso!',[mbOK], 'Contas a Pagar', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

procedure TF_CadContasPagar.icoEstornarNotaClick(Sender: TObject);
begin
  inherited;
  if icoEstornarNota.Visible = True then begin
    Try
      DM.FDCon.StartTransaction;
      ContasPagar := TContasPagar.Create;
      ContasPagar.EstornarContaPagar(edCodigo.AsInteger);
      FreeAndNil(ContasPagar);

      DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

      msgPerson('Conta a pagar '+IntToStr(xAux_ConContasPagar_CP_ID)+' estornada com sucesso!',[mbOK], 'Contas a Pagar', mtConfirmation);

      Close;
    Except
      on e:Exception do begin
        EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
        DM.FDCon.Rollback;
        ShowMessage('Erro na grava��o! ('+e.message+' )');
      end;
    End;
  end
end;

procedure TF_CadContasPagar.rgCp_tipo_pgtoClick(Sender: TObject);
begin
  inherited;
  if (rgCp_tipo_pgto.ItemIndex = 1) or (rgCp_tipo_pgto.ItemIndex = 2) then begin
    edCP_valorpago.Value     := edCp_valor.Value;
    edCp_datavencimento.Date := edCp_datacompra.Date;
    edCp_datapagamento.Date  := Date;
  end else begin
    edCP_valorpago.Value     := 0;
    edCp_datavencimento.Date := edCp_datacompra.Date + 30;
    edCp_datapagamento.Date  := 0;
  end;
end;

end.
