inherited F_AuxFormaPgto: TF_AuxFormaPgto
  Caption = ' Forma de Pagamento'
  ClientHeight = 326
  ClientWidth = 387
  OnActivate = FormActivate
  ExplicitWidth = 403
  ExplicitHeight = 365
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    Top = 249
    Width = 387
    ExplicitTop = 249
    ExplicitWidth = 387
    inherited icoFechar: TImage
      Left = 297
      Visible = False
      ExplicitLeft = 298
    end
    inherited icoSalvar: TImage
      Left = 117
      OnClick = icoSalvarClick
      ExplicitLeft = 118
    end
    inherited icoCancelar: TImage
      Left = 207
      OnClick = icoCancelarClick
      ExplicitLeft = 208
    end
  end
  inherited Panel3: TPanel
    Width = 387
    Height = 249
    ExplicitWidth = 387
    ExplicitHeight = 249
    object Label3: TLabel
      Left = 63
      Top = 44
      Width = 49
      Height = 17
      Alignment = taRightJustify
      Caption = 'Dinheiro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 96
      Top = 79
      Width = 16
      Height = 17
      Alignment = taRightJustify
      Caption = 'Pix'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 26
      Top = 115
      Width = 86
      Height = 17
      Alignment = taRightJustify
      Caption = 'Cart'#227'o Cr'#233'dito'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 30
      Top = 150
      Width = 82
      Height = 17
      Alignment = taRightJustify
      Caption = 'Cart'#227'o D'#233'bito'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 50
      Top = 9
      Width = 62
      Height = 17
      Alignment = taRightJustify
      Caption = 'Valor Total'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 32
      Top = 221
      Width = 80
      Height = 17
      Alignment = taRightJustify
      Caption = 'Saldo Cr'#233'dito'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edDinheiro: TJvCalcEdit
      Left = 118
      Top = 41
      Width = 77
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
      OnExit = edDinheiroExit
    end
    object edPix: TJvCalcEdit
      Left = 118
      Top = 76
      Width = 77
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 1
      DecimalPlacesAlwaysShown = False
      OnExit = edPixExit
    end
    object edCartaoCredito: TJvCalcEdit
      Left = 118
      Top = 112
      Width = 77
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 2
      DecimalPlacesAlwaysShown = False
      OnExit = edCartaoCreditoExit
    end
    object edCartaoDebito: TJvCalcEdit
      Left = 118
      Top = 147
      Width = 77
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
      OnExit = edCartaoDebitoExit
    end
    object edCreditoUtilizado: TJvCalcEdit
      Left = 118
      Top = 182
      Width = 77
      Height = 25
      DisplayFormat = '#,##0.00'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = False
      TabOrder = 4
      DecimalPlacesAlwaysShown = False
    end
    object GroupBox1: TGroupBox
      Left = 256
      Top = 68
      Width = 121
      Height = 72
      Caption = 'A Acertar'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      object edValorAcertar: TJvCalcEdit
        Left = 2
        Top = 30
        Width = 117
        Height = 40
        Align = alClient
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        ShowButton = False
        TabOrder = 0
        DecimalPlacesAlwaysShown = False
        ExplicitHeight = 36
      end
    end
    object GroupBox2: TGroupBox
      Left = 256
      Top = 0
      Width = 121
      Height = 72
      Caption = 'Valor Pago'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      object edValorPago: TJvCalcEdit
        Left = 2
        Top = 30
        Width = 117
        Height = 40
        Align = alClient
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        ShowButton = False
        TabOrder = 0
        DecimalPlacesAlwaysShown = False
        ExplicitHeight = 36
      end
    end
    object edValorTotal: TJvCalcEdit
      Left = 118
      Top = 6
      Width = 77
      Height = 25
      DisplayFormat = '#,##0.00'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = False
      TabOrder = 7
      DecimalPlacesAlwaysShown = False
    end
    object GroupBox3: TGroupBox
      Left = 258
      Top = 138
      Width = 121
      Height = 72
      Caption = 'Troco'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      object edTroco: TJvCalcEdit
        Left = 2
        Top = 30
        Width = 117
        Height = 40
        Align = alClient
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        ShowButton = False
        TabOrder = 0
        DecimalPlacesAlwaysShown = False
        ExplicitHeight = 36
      end
    end
    object edCredito: TJvCalcEdit
      Left = 118
      Top = 218
      Width = 77
      Height = 25
      DisplayFormat = '#,##0.00'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = False
      TabOrder = 9
      DecimalPlacesAlwaysShown = False
    end
    object ckUtilizarCredito: TCheckBox
      Left = 7
      Top = 186
      Width = 105
      Height = 17
      TabStop = False
      Caption = 'Utilizar Cr'#233'dito'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      OnClick = ckUtilizarCreditoClick
    end
    object ckLancarTrocoCredito: TCheckBox
      Left = 260
      Top = 208
      Width = 117
      Height = 36
      TabStop = False
      Caption = 'Lan'#231'ar Troco como Cr'#233'dito'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      WordWrap = True
    end
  end
  inherited Panel4: TPanel
    Top = 294
    Width = 387
    ExplicitTop = 294
    ExplicitWidth = 387
    inherited pnCancelar: TPanel
      Left = 207
      ExplicitLeft = 207
    end
    inherited pnSalvar: TPanel
      Left = 117
      ExplicitLeft = 117
    end
    inherited pnFechar: TPanel
      Left = 297
      Visible = False
      ExplicitLeft = 297
    end
  end
  inherited Timer1: TTimer
    Left = 11
    Top = 65520
  end
end
