unit clsItens_Compra;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TItens_Compra = class
  public
    com_id         : Integer;
    pro_id         : Integer;
    ic_qtde        : Double;
    ic_valor_unit  : Double;
    ic_valor_total : Double;

    function  Recuperar(Pcom_id,Ppro_id:Integer):Boolean;
    procedure Gravar(Pcom_id,Ppro_id:Integer);
    procedure Eliminar(Pcom_id,Ppro_id:Integer);
    procedure EliminarTodosItens(Pcom_id:Integer);
  end;

implementation

{ TItens_Compra }

uses DMC;

procedure TItens_Compra.Eliminar(Pcom_id,Ppro_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Itens_Compra where com_id = :K_com_id and pro_id = :k_pro_id');
  tq.ParamByName('K_com_id').AsInteger := Pcom_id;
  tq.ParamByName('K_pro_id').AsInteger := Ppro_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TItens_Compra.EliminarTodosItens(Pcom_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Itens_Compra where com_id = :K_com_id');
  tq.ParamByName('K_com_id').AsInteger := Pcom_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TItens_Compra.Gravar(Pcom_id,Ppro_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Itens_Compra where com_id = :k_com_id and pro_id = :k_pro_id');
  tq.ParamByName('k_com_id').AsInteger := Pcom_id;
  tq.ParamByName('k_pro_id').AsInteger := Ppro_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Itens_Compra values('+
    ':k_com_id,'+
    ':k_pro_id,'+
    ':k_ic_qtde,'+
    ':k_ic_valor_unit,'+
    ':k_ic_valor_total)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Itens_Compra set '+
    'ic_qtde        = :k_ic_qtde,'+
    'ic_valor_unit  = :k_ic_valor_unit,'+
    'ic_valor_total = :k_ic_valor_total ');
    tq.SQL.Add('where com_id = :K_com_id and pro_id = :k_pro_id ');
  end;

  tq.ParamByName('k_com_id').AsInteger       := Pcom_id;
  tq.ParamByName('k_pro_id').AsInteger       := Ppro_id;
  tq.ParamByName('k_ic_qtde').AsFloat        := ic_qtde;
  tq.ParamByName('k_ic_valor_unit').AsFloat  := ic_valor_unit;
  tq.ParamByName('k_ic_valor_total').AsFloat := ic_valor_total;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TItens_Compra.Recuperar(Pcom_id,Ppro_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Itens_Compra where com_id = :K_com_id and pro_id = :k_pro_id');
  tq.ParamByName('k_com_id').AsInteger := Pcom_id;
  tq.ParamByName('k_pro_id').AsInteger := Ppro_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  com_id         := tq.FieldByName('com_id').AsInteger;
  pro_id         := tq.FieldByName('pro_id').AsInteger;
  ic_qtde        := tq.FieldByName('ic_qtde').AsFloat;
  ic_valor_unit  := tq.FieldByName('ic_valor_unit').AsFloat;
  ic_valor_total := tq.FieldByName('ic_valor_total').AsFloat;

  tq.Close;
  tq.Free;
end;

end.
