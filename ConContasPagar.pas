unit ConContasPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, JvBaseEdits, Vcl.Mask, JvExMask, JvToolEdit, DateUtils;

type
  TF_ConContasPagar = class(TF_ConPadrao)
    FDContasPagar: TFDQuery;
    ProviderContasPagar: TDataSetProvider;
    CdsContasPagar: TClientDataSet;
    DsContasPagar: TDataSource;
    FDContasPagarCP_ID: TIntegerField;
    FDContasPagarCP_DATACOMPRA: TSQLTimeStampField;
    FDContasPagarENT_NOME_FANTASIA: TStringField;
    FDContasPagarCP_DATAVENCIMENTO: TSQLTimeStampField;
    FDContasPagarCP_DATAPAGAMENTO: TSQLTimeStampField;
    FDContasPagarCP_VALOR: TFloatField;
    FDContasPagarCP_VALORPAGO: TFloatField;
    FDContasPagarCP_TIPO_PGTO: TIntegerField;
    CdsContasPagarCP_ID: TIntegerField;
    CdsContasPagarCP_DATACOMPRA: TSQLTimeStampField;
    CdsContasPagarENT_NOME_FANTASIA: TStringField;
    CdsContasPagarCP_DATAVENCIMENTO: TSQLTimeStampField;
    CdsContasPagarCP_DATAPAGAMENTO: TSQLTimeStampField;
    CdsContasPagarCP_VALOR: TFloatField;
    CdsContasPagarCP_VALORPAGO: TFloatField;
    CdsContasPagarCP_TIPO_PGTO: TIntegerField;
    rgFiltrar: TRadioGroup;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    edDataFim: TJvDateEdit;
    edDataIni: TJvDateEdit;
    rgTipoPgto: TRadioGroup;
    GroupBox2: TGroupBox;
    edDescricao: TEdit;
    cbPesquisar: TComboBox;
    edValor: TJvCalcEdit;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure CdsContasPagarCP_DATAPAGAMENTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure CdsContasPagarCP_DATAVENCIMENTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure CdsContasPagarCP_DATACOMPRAGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure CdsContasPagarCP_TIPO_PGTOGetText(Sender: TField;
      var Text: string; DisplayText: Boolean);
    procedure cbPesquisarChange(Sender: TObject);
    procedure edDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edValorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConContasPagar               : TF_ConContasPagar;
  xAux_ConContasPagar_CP_ID      : Integer;
  xAux_ConContasPagar_COM_ID     : Integer;
  xAux_ConContasPagar_Tela       : String;
  xAux_ConContasPagar_Valor      : Double;

implementation

{$R *.dfm}

uses DMC, X, CadContasPagar;

procedure TF_ConContasPagar.cbPesquisarChange(Sender: TObject);
begin
  inherited;
  if (cbPesquisar.Text = 'Valor da Conta') or (cbPesquisar.Text = 'Valor Pago') then begin
    edDescricao.Clear;
    edValor.Show;
    edDescricao.Hide;
  end else begin
    edValor.Clear;
    edValor.Hide;
    edDescricao.Show;
  end;
end;

procedure TF_ConContasPagar.CdsContasPagarCP_DATACOMPRAGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '30/12/1899' then
    Text := '';
end;

procedure TF_ConContasPagar.CdsContasPagarCP_DATAPAGAMENTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '30/12/1899' then
    Text := '';
end;

procedure TF_ConContasPagar.CdsContasPagarCP_DATAVENCIMENTOGetText(
  Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '30/12/1899' then
    Text := '';
end;

procedure TF_ConContasPagar.CdsContasPagarCP_TIPO_PGTOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '0' then
    Text := 'BOLETO'
  else if Sender.AsString = '1' then
    Text := 'DINHEIRO'
  else if Sender.AsString = '2' then
    Text := 'PIX';
end;

procedure TF_ConContasPagar.edDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key=13 then
    icoPesquisarClick(Sender);
end;

procedure TF_ConContasPagar.edValorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key=13 then
    icoPesquisarClick(Sender);
end;

procedure TF_ConContasPagar.FormActivate(Sender: TObject);
begin
  inherited;
  edDataIni.Date := StartOfTheMonth(now);
  edDataFim.Date := EndOfTheMonth(now);
  icoPesquisarClick(Sender);
  edDescricao.SetFocus;
end;

procedure TF_ConContasPagar.icoFecharClick(Sender: TObject);
begin
  xAux_ConContasPagar_CP_ID := CdsContasPagar.FieldByName('cp_id').AsInteger;
  inherited;

end;

procedure TF_ConContasPagar.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConContasPagar_CP_ID := 0;
  AbreForm(TF_CadContasPagar,Self);
  icoPesquisarClick(Sender);
  if xAux_ConContasPagar_CP_ID <> 0 then
    CdsContasPagar.Locate('cp_id',xAux_ConContasPagar_CP_ID,[]);
end;

procedure TF_ConContasPagar.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsContasPagar.Close;

  FDContasPagar.Close;
  FDContasPagar.Params.Clear;
  FDContasPagar.SQL.Clear;
  FDContasPagar.SQL.Add('select cp.cp_id, cp.cp_datacompra, e.ent_nome_fantasia, cp.cp_datavencimento, cp.cp_datapagamento, '+
                        'cp.cp_valor, cp.cp_valorpago, cp.cp_tipo_pgto                                                      '+
                        'from contaspagar cp                                                                                '+
                        'left outer join compra c on cp.com_id=c.com_id                                                     '+
                        'left outer join entidade e on c.ent_id=e.ent_id                                                    ');
  if rgFiltrar.ItemIndex = 0 then begin
    FDContasPagar.SQL.Add('where cp.cp_datacompra >= :k_dataIni and cp.cp_datacompra <= :k_dataFim ');
    FDContasPagar.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
    FDContasPagar.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  end else if rgFiltrar.ItemIndex = 1 then begin
    FDContasPagar.SQL.Add('where cp.cp_datavencimento >= :k_dataIni and cp.cp_datavencimento <= :k_dataFim ');
    FDContasPagar.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
    FDContasPagar.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  end else if rgFiltrar.ItemIndex = 2 then begin
    FDContasPagar.SQL.Add('where cp.cp_datapagamento >= :k_dataIni and cp.cp_datapagamento <= :k_dataFim ');
    FDContasPagar.ParamByName('k_dataIni').AsDateTime := edDataIni.Date;
    FDContasPagar.ParamByName('k_dataFim').AsDateTime := edDataFim.Date;
  end;
  if rgTipoPgto.ItemIndex <> 3 then begin
    if rgTipoPgto.ItemIndex = 0 then
      FDContasPagar.SQL.Add('and cp.cp_tipo_pgto = 0 ')
    else if rgTipoPgto.ItemIndex = 1 then
      FDContasPagar.SQL.Add('and cp.cp_tipo_pgto = 1 ')
    else if rgTipoPgto.ItemIndex = 2 then
      FDContasPagar.SQL.Add('and cp.cp_tipo_pgto = 2 ');
  end;
  if cbPesquisar.Text = 'Raz�o Social' then
    FDContasPagar.SQL.Add('and e.ent_nome like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else if cbPesquisar.Text = 'Nome Fantasia' then
    FDContasPagar.SQL.Add('and e.ent_nome_fantasia like ' + QuotedStr('%'+Trim(edDescricao.Text)+'%') + ' ')
  else if cbPesquisar.Text = 'Valor da Conta' then begin
    FDContasPagar.SQL.Add('and cp.cp_valor = :k_cp_valor ');
    FDContasPagar.ParamByName('k_cp_valor').AsFloat := edValor.Value;
  end else if cbPesquisar.Text = 'Valor Pago' then begin
    FDContasPagar.SQL.Add('and cp.cp_valorpago = :k_cp_valorpago ');
    FDContasPagar.ParamByName('k_cp_valorpago').AsFloat := edValor.Value;
  end;
  FDContasPagar.SQL.Add('order by cp.cp_datacompra, e.ent_nome_fantasia                                                     ');
  CdsContasPagar.Open;

  if not CdsContasPagar.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsContasPagar.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConContasPagar.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConContasPagar_CP_ID := CdsContasPagar.FieldByName('cp_id').AsInteger;
  AbreForm(TF_CadContasPagar, Self);
  icoPesquisarClick(Sender);
  if xAux_ConContasPagar_CP_ID <> 0 then
    CdsContasPagar.Locate('cp_id',xAux_ConContasPagar_CP_ID,[]);
end;

end.
