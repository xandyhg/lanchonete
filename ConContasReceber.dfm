inherited F_ConContasReceber: TF_ConContasReceber
  Caption = ' Consulta de Contas a Receber'
  ClientHeight = 527
  ClientWidth = 1007
  ExplicitWidth = 1023
  ExplicitHeight = 566
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 414
    Width = 1007
    inherited icoPesquisar: TImage
      Left = 587
    end
    inherited icoNovo: TImage
      Left = 692
    end
    inherited icoImprimir: TImage
      Left = 797
    end
    inherited icoFechar: TImage
      Left = 902
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 491
    Width = 1007
  end
  inherited Panel3: TPanel
    Top = 97
    Width = 1007
    Height = 317
    inherited JvDBGrid1: TJvDBGrid
      Width = 1007
      Height = 317
      DataSource = DsContasReceber
    end
  end
  inherited Panel4: TPanel
    Top = 459
    Width = 1007
    inherited pnPesquisar: TPanel
      Left = 587
    end
    inherited pnNovo: TPanel
      Left = 692
    end
    inherited pnImprimir: TPanel
      Left = 797
    end
    inherited pnFechar: TPanel
      Left = 902
    end
  end
  inherited Panel1: TPanel
    Width = 1007
    Height = 97
    ExplicitWidth = 1007
    ExplicitHeight = 97
    object rgFiltrar: TRadioGroup
      Left = 0
      Top = 0
      Width = 129
      Height = 97
      Align = alLeft
      Caption = ' Filtrar por Data de '
      ItemIndex = 1
      Items.Strings = (
        'Cadastro'
        'Vencimento'
        'Pagamento')
      TabOrder = 0
      ExplicitLeft = 8
      ExplicitHeight = 137
    end
    object GroupBox1: TGroupBox
      Left = 129
      Top = 0
      Width = 116
      Height = 97
      Align = alLeft
      Caption = ' Per'#237'odo '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      ExplicitHeight = 99
      object Label8: TLabel
        Left = 54
        Top = 45
        Width = 7
        Height = 17
        Alignment = taRightJustify
        Caption = 'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object edDataFim: TJvDateEdit
        Left = 6
        Top = 65
        Width = 105
        Height = 25
        ShowNullDate = False
        TabOrder = 1
      end
      object edDataIni: TJvDateEdit
        Left = 6
        Top = 20
        Width = 105
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 0
      end
    end
    object rgTipoPgto: TRadioGroup
      Left = 245
      Top = 0
      Width = 104
      Height = 97
      Align = alLeft
      Caption = ' Tipo Pgto.'
      ItemIndex = 3
      Items.Strings = (
        'Boleto'
        'Dinheiro'
        'PIX'
        'Todos')
      TabOrder = 2
      ExplicitHeight = 99
    end
    object GroupBox2: TGroupBox
      Left = 349
      Top = 0
      Width = 180
      Height = 97
      Align = alLeft
      Caption = 'Pesquisar por'
      TabOrder = 3
      ExplicitHeight = 99
      object edDescricao: TEdit
        Left = 10
        Top = 53
        Width = 160
        Height = 25
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object cbPesquisar: TComboBox
        Left = 10
        Top = 22
        Width = 160
        Height = 25
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ItemIndex = 0
        ParentFont = False
        TabOrder = 1
        Text = 'Nome Fantasia'
        Items.Strings = (
          'Nome Fantasia'
          'Raz'#227'o Social'
          'Valor da Conta'
          'Valor Pago')
      end
      object edValor: TJvCalcEdit
        Left = 10
        Top = 53
        Width = 83
        Height = 25
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 2
        Visible = False
        DecimalPlacesAlwaysShown = False
      end
    end
  end
  object FDContasReceber: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select cr.cr_id, cr.venda_id, e.ent_id, e.ent_nome, cr.cr_dataca' +
        'dastro,'
      
        'cr.cr_datavencimento, cr.cr_valor, sum(crpgto.crp_valorpago) crp' +
        '_valorpago,'
      'cr.cr_status, crpgto.crp_tipo_pgto'
      'from contasreceber cr'
      'left outer join vendas v on cr.venda_id=v.venda_id'
      
        'left outer join contasreceber_pgto crpgto on cr.cr_id=crpgto.cr_' +
        'id'
      'left outer join entidade e on v.ent_id=e.ent_id'
      'group by 1,2,3,4,5,6,7,9,10')
    Left = 696
    Top = 8
  end
  object ProviderContasReceber: TDataSetProvider
    DataSet = FDContasReceber
    Left = 728
    Top = 8
  end
  object CdsContasReceber: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderContasReceber'
    Left = 760
    Top = 8
  end
  object DsContasReceber: TDataSource
    DataSet = CdsContasReceber
    Left = 792
    Top = 8
  end
end
