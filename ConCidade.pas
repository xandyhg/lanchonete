unit ConCidade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TF_ConCidade = class(TF_ConPadrao)
    FDCidade: TFDQuery;
    ProviderCidade: TDataSetProvider;
    CdsCidade: TClientDataSet;
    DsCidade: TDataSource;
    FDCidadeCID_ID: TIntegerField;
    FDCidadeCID_CIDADE: TStringField;
    FDCidadeCID_UF: TStringField;
    CdsCidadeCID_ID: TIntegerField;
    CdsCidadeCID_CIDADE: TStringField;
    CdsCidadeCID_UF: TStringField;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConCidade           : TF_ConCidade;
  xAux_ConCidade_Cid_ID : Integer;

implementation

{$R *.dfm}

uses DMC, CadCidade, X;

procedure TF_ConCidade.FormActivate(Sender: TObject);
begin
  inherited;
  icoPesquisarClick(Sender);
end;

procedure TF_ConCidade.icoFecharClick(Sender: TObject);
begin
  xAux_ConCidade_Cid_ID := CdsCidade.FieldByName('cid_id').AsInteger;
  inherited;

end;

procedure TF_ConCidade.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConCidade_Cid_ID := 0;
  AbreForm(TF_CadCidade,Self);
  icoPesquisarClick(Sender);
  if xAux_ConCidade_Cid_ID <> 0 then
    CdsCidade.Locate('cid_id',xAux_ConCidade_Cid_ID,[]);
end;

procedure TF_ConCidade.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsCidade.Close;

  FDCidade.Close;
  FDCidade.Params.Clear;
  FDCidade.SQL.Clear;
  FDCidade.SQL.Add('select *            '+
                   'from cidade         '+
                   'order by cid_cidade ');
  CdsCidade.Open;

  if not CdsCidade.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsCidade.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConCidade.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConCidade_Cid_ID := CdsCidade.FieldByName('cid_id').AsInteger;
  AbreForm(TF_CadCidade, Self);
  icoPesquisarClick(Sender);
  if xAux_ConCidade_Cid_ID <> 0 then
    CdsCidade.Locate('cid_id',xAux_ConCidade_Cid_ID,[]);
end;

end.
