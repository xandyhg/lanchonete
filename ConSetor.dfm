inherited F_ConSetor: TF_ConSetor
  Caption = 'Consulta de Setor'
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel3: TPanel
    inherited JvDBGrid1: TJvDBGrid
      DataSource = DsSetor
      OnDblClick = JvDBGrid1DblClick
      OnKeyDown = JvDBGrid1KeyDown
      TitleRowHeight = 21
      Columns = <
        item
          Expanded = False
          FieldName = 'SET_ID'
          Title.Caption = 'C'#243'digo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SET_DESCRICAO'
          Title.Caption = 'Descri'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 483
          Visible = True
        end>
    end
  end
  object FDSetor: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      'select *'
      'from setor'
      'order by set_descricao')
    Left = 352
    Top = 8
    object FDSetorSET_ID: TIntegerField
      FieldName = 'SET_ID'
      Origin = 'SET_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDSetorSET_DESCRICAO: TStringField
      FieldName = 'SET_DESCRICAO'
      Origin = 'SET_DESCRICAO'
      Size = 200
    end
  end
  object ProviderSetor: TDataSetProvider
    DataSet = FDSetor
    Left = 384
    Top = 8
  end
  object CdsSetor: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderSetor'
    Left = 416
    Top = 8
    object CdsSetorSET_ID: TIntegerField
      FieldName = 'SET_ID'
      Required = True
    end
    object CdsSetorSET_DESCRICAO: TStringField
      FieldName = 'SET_DESCRICAO'
      Size = 200
    end
  end
  object DsSetor: TDataSource
    DataSet = CdsSetor
    Left = 448
    Top = 8
  end
end
