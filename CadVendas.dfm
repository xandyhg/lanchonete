inherited F_CadVendas: TF_CadVendas
  Caption = ' Lan'#231'amento de Venda '
  ClientHeight = 531
  ClientWidth = 926
  OnActivate = FormActivate
  ExplicitWidth = 942
  ExplicitHeight = 570
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 454
    Width = 926
    Color = clGreen
    ExplicitTop = 454
    ExplicitWidth = 926
    inherited icoFechar: TImage
      Left = 836
      ExplicitLeft = 833
    end
    inherited icoAlterar: TImage
      Left = 656
      OnClick = icoAlterarClick
      ExplicitLeft = 653
    end
    inherited icoExcluir: TImage
      Left = 746
      OnClick = icoExcluirClick
      ExplicitLeft = 743
    end
    inherited icoSalvar: TImage
      Left = 476
      OnClick = icoSalvarClick
      ExplicitLeft = 467
      ExplicitTop = 5
      ExplicitHeight = 45
    end
    inherited icoCancelar: TImage
      Left = 566
      OnClick = icoCancelarClick
      ExplicitLeft = 563
    end
    object icoEstornarVenda: TImage
      Left = 0
      Top = 0
      Width = 97
      Height = 45
      Cursor = crHandPoint
      Align = alLeft
      Center = True
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000200000
        00200806000000737A7AF40000000467414D410000B18F0BFC61050000000662
        4B474400FF00FF00FFA0BDA7930000000774494D4507E6020D0F0D3B8B9A09D6
        00000A6A4944415478DA7D977B7054F515C7CF7DEFDECD3EC93B0B2198B04980
        250454401879444C3B8494AA953782C53AE3A8ED0CFA07AD68456D758A824EFF
        701CABFFE0B4952A8A121E5AB15A2BA8544508281AF27EEF6EF6BD7B77F7F69C
        DFBDCB925ABB93DFDCCDDEBDBFF3F97DCFF79CDF6F39F8AFD7DEA79F065114C1
        ED76812088374FAFAD3D2C88A294CD662197CB4206AFD94C063238F2D7FCFB74
        5A03D0F58C55B53E71FAF4277BEA6A6B33BC20C0F62D5BE0875EDCF700F6EE05
        5192149CD0EE9D3A7575DDAC592FF0A22066B3390C6E044BA7D3904EA5D855C3
        9132DF27E271B04832D4FB7CD1EEDE9EC72E9CEF7CA6A2B2322D2004073A6CDB
        B2F587019ED9B70F57988369D5D5746D7178DCFB438160A9AFB1A1585414D0F1
        5E96460655C868902208023083D38823808CEADDB864290C0C0E26BBBABA7EDF
        F55DD793A86692E6962509EEDCB6ED7F03ECDBBF1FD5D3158FC73347B1583638
        4A8A7FD5D7DB0BF39BE7836C51E81EAE012097CD3215341A9AC6564F10F43E11
        4F206816962F5B063A2AD6DDD39DEEEABAFCF477972EEDF11417C7798E035263
        C7F6EDDF0778F6B9E788B0BADCEB3D321E0C36969496C2F0C83034CF9B870016
        A0E8692D8DC10B5EC89002189880E8954C24586A56B6B400058B452208D193E9
        BEDCFDDC405FFFEE22873D2AA14283FDFDB0FBA1872603FC01739F4926D5997E
        FFA1E1B1D19BCACACA2010084073531303A0D503A9608E5C4E370DA8218471A5
        1424712C5BBE1C144C5B2A9984C844187A7A7AB2BDBDBDCF0F0F0EEE526DB609
        4996A1AFBB1B1E79F8E102C043BB77C3830FEF866387DFDA138E457F63B7DB21
        822B68F2FB0D05AE92CC8001C3137963224422118758340A4B972E050B3E4379
        2788503044107A7F5FDF4BC343430FDA6CB67152E8173B765C058092CC9BDF8C
        394DAFCE707050122525914CC0DCD9B341C6D5D08B02D1A45720C080609FE184
        494C0901CC9D3B97198EBE43E64D606A82C120F4F5F6C1C8D0D081503078BF24
        49E3F7DE734F01E0C5975F8278244AE4D38BDCAEBFE3C33569CCAFBFBE01E594
        D964D40BF49C5E9082331E671FE15BF20501E42B24DF232845E40D5A10563499
        6635021CDF76C71D93FBC06F1F7D14C2E3E3F282254B5EC539D790D367D7D582
        45C62A00238A1193BB02C0713CBB6675A34CC9A429342BC1A74D93E6CB9486C8
        0B5A2212699765B963F3A64D9301C807B76FDA089F7F76669720498FD3C3B36A
        6A409125A6794E3756CFE5574FC17984E0B1D1F03CE4F04B0C0295D068507052
        216D02E1E0745D0B8D8EB5A3493B366DDC3819E02F3B7742665E132453E9169B
        C3F1064EA0364CF5822288EC3E9521E51BCC5573026F04178C01F83F21669839
        4D0804C8F70F4A8D9ECD6AA30383DF07086CDC0C59D50A9FE24A3AEB7D55955E
        EF3B3849BD0FFB81829F41DE781498FEC7C1634D732205260011FF17D8E76453
        4A09DB3710266D362DE60BDC30FA2F774F06B8D0BE161C562B54FEF91538BBED
        CEDBDFBFF4CD1C75D3C6F956B7ABB5D6E902C50CCE19DAB33F5738027DC343A0
        CC9C09AADB8D2022034297315572F89DAC7E554AD088CC1BC99476F9EBAF0B00
        275B56C194A22278F1D06BB073ED2DEB8369EDD90FFB7ADDB1BADA4079DBEA12
        DF946290B0D629FD995C96A5A06C64144AFA07E1F4858B3054560CD3DB5683EA
        721910587E4C1584D0C99C884E46257F30886442BB78F6AB76C582001B10E0D3
        ED3F07CB175F42CCE3BECD02FC1FD592F2924022069F7CF70D649AFCB060D52A
        B08912AB02EA7E0EDC1FA6745E8458911D72A5E5D079FA5F3031CD0B75B7FC14
        54A71338990024430DF20442D0C8994AC46331EDEC997FB763B923C006E03A9A
        AF25994A5DAAADC3E32969B6A0E41C76B1A1B111F8C7A50B2034D4C38215CBC1
        612B02A5A717ECE7CF435A5141AAAD63FD5EC356FBF5B92F81FBF12AA85EB408
        24DCB8088093282592694C8EA947C68C4763DA67A73E4600AB0170E41A1F3A34
        37AFDC3BF598DDEE28E1B0EB710A6A61B540FFE808BC79E12CC8F53E686D9C05
        EE4BDF022F5BC13ABDC6A87F34178723140AC2398F03EADBDBA0C8E5C434C808
        205DF1058F3060AA108D44B5531F7C6028401E78B9B80202E36352DD6CFF3D15
        6ECF6356C562E3B0F31188809B46372AF1EAC5F370A3D70B8DC5A560ABF01A55
        40A6A015A592702A1903689A034DD75D07763C4971F81CF3820921E382F090C3
        7C809B93F6CFF7DE6BB7582D8607FE34A51C9278E3C0F8B0B07356D3BD954E37
        42283660AB407ACC697F30003C06ACA8A862B5CF82E34860697D9AC5D29ADD00
        FEE66628AFAA02515581C705F0724105D627789E8D6838AC9D3C7E0201CC14B0
        B380A70CE9727028302ADCD7E8BFB7DCE1DA6311C52260756E96174AAE53D733
        FB7F12577F86D78DE0739B30782548361BF0E81F1ED5330044134064E03C0144
        22DABB478FB65BC9035737A2FD9E12E6F203A131FE01DFECFB4B6D454F5805C1
        428D854D80C1D97BDAF530979F2B22688D3E68686C84F28A8A42701C82452928
        80CFCAE82789BC85CF475081778E74208065F25EF0ED2DB7C110E673F1EF1EE7
        1E58B7E16E4F22B577A145B5F2B401519BE578B3FDF3F05174027ACA4AA075ED
        4FA0A666060657415011003BA94010E401A5900281811869200F1C7FFBED762B
        A6604B1EE04CDB1AECF70234D63772A7CE9DDDD13932F2140C8F39CBB5ECA4BD
        9F335BF1287E725EE4A066F142B8B6AD0D1CD8AE79952054437E330586118D14
        D0A64547F47068423B76F830F3C0D6CD9B813B819DD089B4D4BB6C8A65472A91
        7872E0728F33130AB303666113281C876825099C78C06601F7C2EB61CE9A3560
        2708AB917F81E46600D49A45F4335594C43C30110A691D6FBCC914B8037F2F70
        2757B542251EBF46E2F1AD7236B33FD63FE48C8C05D8C193EDFFB4BB21391950
        42D75F61A1268481FA11C2B9643134AF5B07368FE74A0AA823B246846D99F22F
        5255A111C30870E4F54306C0D6ADC01D5EB1127282E0B024531DFC5860B11608
        B263149887500D57D035A31A648703AACE5F00299E842BB220441A271F28F140
        C32FEF83BAEBAE0591002413C0F40098254829980886B4B75F7F9D016C238037
        EB1BA955FAD54CF65D21122DC6BE6C4AAD431A5772E99A1A10972C8269959590
        38F509A8EF7F080A9EF19823593A38882384B6793DF8376E00D55E34A909B1C1
        1BDB348F0A4C0483DA5B7F7B8D016CA723D96BA595D801A0C62DC97F7568DA02
        4E37569FE2F84C674D75A772EBDA8619555562316E3491680CFA8F1E03D7BBEF
        812D6E40E00907061C45105CFF3358B87E1DB8300D6C2FC8071785C2F9811718
        C0E18307D7E00F94A377DF759761ECD3BE597031189A3F43145F28CE64E62579
        4E3F97CBBDF8C5DAB68F7ED4DAFA7C89DD21918168470C85C33076E428541E3B
        014E84E873DAA1B36505786F6A81993E1F58B11F80503825B1CE69F60F52221C
        0A660E1E78E5D65FEFDAF5C6CDADADC6F14EA79D0557B26F4AD9F57315E5D951
        3DF7D5A3C3838F70FE39DE1537DCB052C5C30A4B8AAEF3B873F2E18909AEFEE4
        07D3FC89E4A2234EFB3BC11B168DBB1C0E1D37982C82EA46C3E0F2756B286596
        30FE4EC8BD7FF2E489531F7FFCA5AAAAC97C239271501471A56AABEECB68E18B
        E9F484792F419D974EE53824F37BB28AD725B6A2CAE3B1680F1D17296BE6F732
        F0FF5F5448BCC3E9D4F1D7B4FE1FB90B146D77B41C4600000025744558746461
        74653A63726561746500323032322D30322D31335431353A31333A35312B3030
        3A3030BC18E5440000002574455874646174653A6D6F6469667900323032322D
        30322D31335431353A31333A35312B30303A3030CD455DF80000000049454E44
        AE426082}
      Visible = False
      OnClick = icoEstornarVendaClick
      ExplicitLeft = 8
    end
  end
  inherited Panel3: TPanel
    Width = 926
    Height = 413
    ExplicitWidth = 926
    ExplicitHeight = 413
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 924
      Height = 41
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label59: TLabel
        Left = 10
        Top = 12
        Width = 39
        Height = 17
        Alignment = taRightJustify
        Caption = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object sbEntidade: TSpeedButton
        Left = 120
        Top = 8
        Width = 25
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000585751747069
          E1DFDFE4E3E2EBEAE9FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFA5A39B9090878D8A81FFFFFFFFFFFFEFEEEEEBEAE9F1
          F1F0F7F7F6F8F7F7F9F9F9FBFBFBFDFDFDFEFEFEFFFFFFFFFFFFFEFEFEA9A7A1
          8B8B8189867DFFFFFFFFFFFFF7F7F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFAA9A7A18B8B8189867DFFFFFFFFFFFFFE
          FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
          FBFBFAA9A7A18A8B8189867DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABA9A389897F6E6C66FF
          FFFFF9F9FBC2C4C7C3C6CAC2C4C6D4D4D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF9F8F8D2D2CE5F5F579192949D988CCBA865CEA558D4B781C9C1
          B0A8AAABF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D3D6B0
          A382DCAA34D9AE43D7AB3FD7A02CDFA73EDDC599ABACACFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFCFCFE999588E2C366D7C46BD6C66FD6C36BD6BA5CD5A9
          3FDCA233D5C5A4C7C9CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D9BEB286DB
          CE78E1E2ACD5DA8FD5D78AD6CC7AD7BD61D5A537E2B057B4B6B7FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFDBDCDEBDB481E1E4A6E4E9BAEFF2D7E0E6B1D5DA8FD6C9
          74D6B34FE0A638ACA69AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFE1BCB78BEE
          F2CCECEFCFFCFDFAF6F7E8DFE5AED4CC76D5B654DEA42FABA69AFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFECECEDB1AE95F4F7D2F4F6E3F6F7E7F5F7E6EDF1D3E9E5
          B9E3CD86D7A949B8BCC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBEBEC8
          C8A0F9FCE2ECF0CFDAE1A2EAECC7EFE9C3F8E4B2A19A87EEEFF0FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFDFEFEB6B4B1B8B694F7F8D7E9E9AFDBCA66DECD
          999C9581D3D4D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDCDCDDA6A49BA39F91A19C8EB6B7B7F0F1F2FFFFFFFFFFFF}
        ParentFont = False
        OnClick = sbEntidadeClick
      end
      object Label10: TLabel
        Left = 688
        Top = 11
        Width = 35
        Height = 17
        Alignment = taRightJustify
        Caption = 'Status'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object edEnt_id: TJvCalcEdit
        Left = 55
        Top = 8
        Width = 59
        Height = 25
        TabStop = False
        DecimalPlaces = 0
        DisplayFormat = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 0
        DecimalPlacesAlwaysShown = False
        OnExit = edEnt_idExit
        OnKeyDown = edEnt_idKeyDown
      end
      object edEnt_Nome: TEdit
        Left = 151
        Top = 8
        Width = 531
        Height = 25
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object edVenda_status: TEdit
        Left = 729
        Top = 8
        Width = 185
        Height = 25
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 13
        ParentFont = False
        TabOrder = 2
      end
    end
    object Panel9: TPanel
      Left = 1
      Top = 42
      Width = 924
      Height = 23
      Align = alTop
      BevelOuter = bvNone
      Caption = 'ITENS DA VENDA'
      Color = clGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
    end
    object Panel10: TPanel
      Left = 1
      Top = 65
      Width = 924
      Height = 224
      Align = alTop
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object Label5: TLabel
        Left = 112
        Top = 9
        Width = 68
        Height = 17
        Alignment = taRightJustify
        Caption = 'C'#243'd. Barras'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 20
        Top = 10
        Width = 29
        Height = 17
        Alignment = taRightJustify
        Caption = 'Qtde'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object SpeedButton3: TSpeedButton
        Left = 219
        Top = 38
        Width = 85
        Height = 28
        Caption = 'Alterar'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCFCFA
          F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFC
          FCFBF8F7F5F3F0EEF4F2EDF5EFE5E5DED7C1B7C1DC8E48D7C3B8FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFEFDFDFCFAF9F9F9F8FCFBF8E0D7CE949CD0416B
          D70082FFBFAB8DFAF4EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFE8C7A2781B0D003CDE0067FF5FC7FFB8F2FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9AE987B0000980A00A226017AA8
          BF9AFEFFF8FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDA
          A5978B0C00AB2900B52600B71000E7A000B7FBF6FCFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFDDAC97912300C24000D44F00C96400D78F00F091
          00E6BC6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE9E97882600CC
          4700E66700E29700EDC200F0B800F5D659FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFDDC9BA570000CB4700ED6100E79A00F5DA00FADC00FCEB90FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD0D5D6330000C42200E84400E3
          7C00F6CD00FBE305FEF4BEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          D8D7D6001B3174402BDE1000DD4500EBA200F6CE00FEF2ACFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFDDDCD80210204E63778A928AAA5A1CEC6E00F6
          A800FAE856FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E9F30B1A2F
          59686E7F7A789E9F9EF9FFFFB49E5BD7BC58FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF7C84CD2B3E7890845FA09B96FFFFFFDADDDDBAC0CDFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E4F90000CB
          91A0CBFFFFF3D0CEC7BCBABAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFE1E4FFC4E6FFCBD7FFCCCBD1FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        ParentFont = False
        OnClick = SpeedButton3Click
      end
      object SpeedButton4: TSpeedButton
        Left = 310
        Top = 37
        Width = 85
        Height = 28
        Caption = 'Cancelar'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000120B0000120B00000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFB00D9D9E5009D9DD5008585D3008A8AD500AAAA
          D800E8E8E400FFFFF900FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00DDDDEB005353C8000404B9000000B9000000C0000000C9000000
          D1002929D7007E7ED500F5F5E500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00C7C7EB001010BD000000B7000000C0000B0BC9002E2ED8002F2FD9001515
          E0001616F2001111F2004949DB00E7E7E400FFFFFF00FFFFFF00FFFFFF00E6E6
          F4000A0AC2000000AE000000BA006767D900C7C7E800F1F1F400EAEAF100D0D0
          ED005858DF002929F2002020FF004B4BDB00F7F7EA00FFFFFF00FFFFFD005555
          D0000000AE000000B6009595DC00FFFFFC00FFFFFF00FFFFFF00FFFFFF00EEEE
          F1003C3CDC001313EF002828F1001010EE008F8FDA00FFFFFB00D9D9EF000000
          B2000000AA006666D100FFFFFF00FFFFFF00FFFFFF00FFFFFF00CECEEF001414
          DB000000E1004E4EDA005E5EE0001111F2003030D600F2F2E900A7A7E3000000
          B5000000AD00E1E1ED00FFFFFF00FFFFFF00FFFFFF00CCCCEF001010D0000000
          CF002323DA00ECECED00CECEED000D0DE0000404CF00B8B8DA008080D8001F1F
          CF004141C300FFFFF600FFFFFF00FFFFFF00CECEEF000D0DC8000000C1001919
          D100DCDCED00FFFFFF00E0E0EF002424D5000000C7009A9AD7007B7BD6002222
          CE006262CA00FFFFF600FFFFFF00C8C8EC000A0ABF000000B6001515C900DBDB
          EE00FFFFFF00FFFFFF00E6E6EF001F1FD1000000BD009696D7009595DF002121
          C4003B3BC600FFFFF300E3E3F9001D1DC7000000B3001515C300DADAEE00FFFF
          FF00FFFFFF00FFFFFF00C0C0E6000101C4000000B900B0B0D800CACAF0003131
          C7003131C1008080CF005252D2000D0DC4003232CD00D9D9EE00FFFFFF00FFFF
          FF00FFFFFF00FFFFFC006565D7000404C8002424C300EAEAED00FCFCFB006969
          DE002B2BC0002A2AC4002121C0005151D100EBEBF600FFFFFF00FFFFFF00FFFF
          FF00FFFFFB009898DE001B1BC7000808C6007B7BD500FFFFFD00FFFFFF00D3D3
          F5003939D3002D2DBB002A2AC4008080CB00F6F6EE00FCFCF100FAFAF300D2D2
          E8007979D8001919C4000C0CC2003434CA00EAEAEF00FFFFFF00FFFFFF00FFFF
          FC00B9B9F0003939D3002A2ABE002F2FC2004040C8005858C5005252C7002F2F
          C5001010C0001414C3003737D000D5D5EE00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00CFCFF6006767DD003232C5002222C4002424D5002121D3001C1C
          C1003030C8006D6DDA00E5E5F100FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFD00C2C2EE009292DF007B7BD3007E7ED4009C9C
          E300D0D0EC00FFFFFC00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
        ParentFont = False
        OnClick = SpeedButton4Click
      end
      object SpeedButton2: TSpeedButton
        Left = 140
        Top = 37
        Width = 85
        Height = 28
        Caption = 'Remover'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFE9EAF8A9AFE48890DC868EDBA4AAE3E1E2F5FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB4E55560D23544D638
          47DC3747DD3544D84D59D1A4A9E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF8088D83848DE3F4FEE3F50EF3F4FED3E4FED3F50EF3F50EE3A49E16973
          D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAEB3E63B4AE44051F03C4CE73C4CE73C
          4CE73C4CE73C4CE73C4CE74050F03D4DE89199DFFFFFFFFFFFFFFFFFFFFAFAFD
          3645DA4051F13D4DE93D4DEA3D4DEA3D4DEA3D4DEA3D4DEA3D4EEA3D4DE94051
          F03848DFEBECF9FFFFFFFFFFFFBCC1EC3F50EB3C4CE91B2DDC1629DB192BDC19
          2BDC192BDC192BDC172ADB172ADB3848E64252EEA0A7E6FFFFFFFFFFFF8F97E5
          3C4DF13E4EE67880D38188CF7C84D17C84D17C84D17C84D17E86D17E85D14453
          E43A4BF17B85E4FFFFFFFFFFFF8690EC2F41EA4453E7FFFFF2FFFFF3FFFFF3FF
          FFF3FFFFF3FFFFF3FFFFF3FFFFF35A67E92639E87883EDFFFFFFFFFFFF919AF3
          3646E63F4FE79AA1EAA7ADEAA0A7EAA0A7EAA0A7EAA0A7EAA3AAEAA3A9EA4857
          E83243E67E88F0FFFFFFFFFFFFB4BAF63E4EE73B4BE71F31E61B2EE61D30E61D
          30E61D30E61D30E61C2FE61C2FE63849E73F4FE79AA2F3FFFFFFFFFFFFF3F4FD
          3C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4C
          E73C4CE7E0E3FBFFFFFFFFFFFFFFFFFF949DF23C4CE73C4CE73C4CE73C4CE73C
          4CE73C4CE73C4CE73C4CE73C4CE73C4CE77883EEFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF6A76ED3C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE75765
          EAFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C96F13D4DE73C4CE73C
          4CE73C4CE73C4CE73C4CE7828CF0F7F8FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFEFF0FDB6BCF6919AF18F99F1B1B7F5E7E9FCFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        ParentFont = False
        OnClick = SpeedButton2Click
      end
      object Label3: TLabel
        Left = 761
        Top = 9
        Width = 70
        Height = 17
        Alignment = taRightJustify
        Caption = 'Valor Venda'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 774
        Top = 43
        Width = 57
        Height = 17
        Alignment = taRightJustify
        Caption = 'Total Item'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object sbProduto: TSpeedButton
        Left = 292
        Top = 6
        Width = 25
        Height = 26
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000585751747069
          E1DFDFE4E3E2EBEAE9FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFA5A39B9090878D8A81FFFFFFFFFFFFEFEEEEEBEAE9F1
          F1F0F7F7F6F8F7F7F9F9F9FBFBFBFDFDFDFEFEFEFFFFFFFFFFFFFEFEFEA9A7A1
          8B8B8189867DFFFFFFFFFFFFF7F7F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFAA9A7A18B8B8189867DFFFFFFFFFFFFFE
          FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
          FBFBFAA9A7A18A8B8189867DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABA9A389897F6E6C66FF
          FFFFF9F9FBC2C4C7C3C6CAC2C4C6D4D4D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF9F8F8D2D2CE5F5F579192949D988CCBA865CEA558D4B781C9C1
          B0A8AAABF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D3D6B0
          A382DCAA34D9AE43D7AB3FD7A02CDFA73EDDC599ABACACFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFCFCFE999588E2C366D7C46BD6C66FD6C36BD6BA5CD5A9
          3FDCA233D5C5A4C7C9CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D9BEB286DB
          CE78E1E2ACD5DA8FD5D78AD6CC7AD7BD61D5A537E2B057B4B6B7FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFDBDCDEBDB481E1E4A6E4E9BAEFF2D7E0E6B1D5DA8FD6C9
          74D6B34FE0A638ACA69AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFE1BCB78BEE
          F2CCECEFCFFCFDFAF6F7E8DFE5AED4CC76D5B654DEA42FABA69AFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFECECEDB1AE95F4F7D2F4F6E3F6F7E7F5F7E6EDF1D3E9E5
          B9E3CD86D7A949B8BCC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBEBEC8
          C8A0F9FCE2ECF0CFDAE1A2EAECC7EFE9C3F8E4B2A19A87EEEFF0FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFDFEFEB6B4B1B8B694F7F8D7E9E9AFDBCA66DECD
          999C9581D3D4D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDCDCDDA6A49BA39F91A19C8EB6B7B7F0F1F2FFFFFFFFFFFF}
        ParentFont = False
        OnClick = sbProdutoClick
      end
      object Label11: TLabel
        Left = 492
        Top = 40
        Width = 64
        Height = 17
        Caption = 'Status Item'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object edPro_CodigoBarras: TEdit
        Left = 186
        Top = 6
        Width = 100
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MaxLength = 13
        ParentFont = False
        TabOrder = 1
        OnExit = edPro_CodigoBarrasExit
        OnKeyDown = edPro_CodigoBarrasKeyDown
      end
      object edIV_Qtde: TJvCalcEdit
        Left = 55
        Top = 6
        Width = 51
        Height = 25
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MinValue = 1.000000000000000000
        ParentFont = False
        ShowButton = False
        TabOrder = 0
        Value = 1.000000000000000000
        DecimalPlacesAlwaysShown = False
        OnExit = edIV_QtdeExit
      end
      object JvDBGrid1: TJvDBGrid
        Left = 55
        Top = 71
        Width = 859
        Height = 147
        TabStop = False
        DataSource = DsItensVenda
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ParentFont = False
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        OnDrawColumnCell = JvDBGrid1DrawColumnCell
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 21
        TitleRowHeight = 21
        Columns = <
          item
            Expanded = False
            FieldName = 'iv_qtde'
            Title.Alignment = taCenter
            Title.Caption = 'Qtde'
            Width = 45
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'pro_codigobarras'
            Title.Alignment = taCenter
            Title.Caption = 'C'#243'd. Barras'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pro_id'
            Title.Caption = 'ID Produto'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pro_descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 299
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'pro_valorvenda'
            Title.Alignment = taCenter
            Title.Caption = 'Valor Venda'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'iv_totalitem'
            Title.Alignment = taCenter
            Title.Caption = 'Total'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'iv_status'
            Title.Alignment = taCenter
            Title.Caption = 'Status'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'iv_valor_restante'
            Title.Caption = 'Vlr. Restante'
            Visible = True
          end>
      end
      object Button1: TButton
        Left = 55
        Top = 37
        Width = 85
        Height = 28
        Caption = 'Adicionar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ImageIndex = 4
        ImageMargins.Left = 2
        Images = Principal.img16
        ParentFont = False
        TabOrder = 3
        OnClick = Button1Click
      end
      object edIV_totalItem: TJvCalcEdit
        Left = 837
        Top = 40
        Width = 77
        Height = 25
        TabStop = False
        DisplayFormat = '#,##0.00'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ShowButton = False
        TabOrder = 5
        DecimalPlacesAlwaysShown = False
      end
      object edPro_Descricao: TEdit
        Left = 388
        Top = 6
        Width = 365
        Height = 25
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object edPro_ValorVenda: TJvCalcEdit
        Left = 837
        Top = 6
        Width = 77
        Height = 25
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 2
        DecimalPlacesAlwaysShown = False
        OnExit = edPro_ValorVendaExit
      end
      object edPro_id: TJvCalcEdit
        Left = 323
        Top = 6
        Width = 59
        Height = 25
        DecimalPlaces = 0
        DisplayFormat = '0'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 7
        DecimalPlacesAlwaysShown = False
      end
      object edIV_Status: TJvCalcEdit
        Left = 562
        Top = 37
        Width = 31
        Height = 25
        TabStop = False
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ShowButton = False
        TabOrder = 8
        Visible = False
        DecimalPlacesAlwaysShown = False
      end
    end
    object Panel15: TPanel
      Left = 1
      Top = 289
      Width = 924
      Height = 23
      Align = alTop
      BevelOuter = bvNone
      Caption = 'OBSERVA'#199#213'ES DA VENDA'
      Color = clGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 3
    end
    object mCom_obs: TMemo
      Left = 1
      Top = 312
      Width = 924
      Height = 41
      Align = alTop
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 4
    end
    object Panel13: TPanel
      Left = 1
      Top = 353
      Width = 924
      Height = 23
      Align = alTop
      BevelOuter = bvNone
      Caption = 'TOTAL DA VENDA'
      Color = clGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 5
    end
    object Panel14: TPanel
      Left = 1
      Top = 376
      Width = 924
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      object edTotalVenda: TDBEdit
        Left = 0
        Top = 0
        Width = 924
        Height = 36
        Align = alClient
        CharCase = ecUpperCase
        DataField = 'totalVenda'
        DataSource = DsItensVenda
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -27
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        ExplicitHeight = 45
      end
    end
  end
  inherited Panel4: TPanel
    Top = 499
    Width = 926
    Color = 16384
    ParentFont = False
    ExplicitTop = 499
    ExplicitWidth = 926
    inherited pnCancelar: TPanel
      Left = 566
      ExplicitLeft = 566
    end
    inherited pnAlterar: TPanel
      Left = 656
      ExplicitLeft = 656
    end
    inherited pnExcluir: TPanel
      Left = 746
      ExplicitLeft = 746
    end
    inherited pnSalvar: TPanel
      Left = 476
      ExplicitLeft = 476
    end
    inherited pnFechar: TPanel
      Left = 836
      ExplicitLeft = 836
    end
    object pnEstornarVenda: TPanel
      Left = 0
      Top = 0
      Width = 97
      Height = 32
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      Visible = False
      object Label13: TLabel
        Left = 0
        Top = 0
        Width = 97
        Height = 32
        Align = alClient
        Alignment = taCenter
        Caption = 'F7 - ESTORNAR VENDA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
        ExplicitWidth = 89
        ExplicitHeight = 30
      end
    end
  end
  inherited Panel5: TPanel
    Width = 926
    ExplicitWidth = 926
    object Label1: TLabel
      Left = 7
      Top = 12
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 671
      Top = 12
      Width = 53
      Height = 17
      Alignment = taRightJustify
      Caption = 'Cadastro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 518
      Top = 12
      Width = 48
      Height = 17
      Alignment = taRightJustify
      Caption = 'Usu'#225'rio:'
    end
    object lbUsuario: TLabel
      Left = 572
      Top = 12
      Width = 56
      Height = 17
      Caption = 'lbUsuario'
    end
    object Label6: TLabel
      Left = 120
      Top = 12
      Width = 26
      Height = 17
      Alignment = taRightJustify
      Caption = 'Tipo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 244
      Top = 12
      Width = 31
      Height = 17
      Alignment = taRightJustify
      Caption = 'Setor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 375
      Top = 12
      Width = 41
      Height = 17
      Alignment = taRightJustify
      Caption = 'Fun'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 56
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object edVenda_Data: TJvDateEdit
      Left = 730
      Top = 9
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 1
    end
    object edVenda_Hora: TJvDateTimePicker
      Left = 836
      Top = 9
      Width = 79
      Height = 25
      Date = 43519.000000000000000000
      Time = 43519.000000000000000000
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Kind = dtkTime
      ParentFont = False
      TabOrder = 2
      DropDownDate = 43519.000000000000000000
    end
    object edEnt_tipo_cliente: TEdit
      Left = 152
      Top = 9
      Width = 84
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      MaxLength = 13
      ParentFont = False
      TabOrder = 3
    end
    object edSet_Descricao: TEdit
      Left = 281
      Top = 9
      Width = 84
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      MaxLength = 13
      ParentFont = False
      TabOrder = 4
    end
    object edFun_Descricao: TEdit
      Left = 422
      Top = 9
      Width = 91
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      MaxLength = 13
      ParentFont = False
      TabOrder = 5
    end
  end
  inherited Timer1: TTimer
    Left = 824
    Top = 80
  end
  object CdsItensVenda: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    Params = <>
    Left = 760
    Top = 80
    object CdsItensVendaiv_qtde: TFloatField
      FieldName = 'iv_qtde'
      DisplayFormat = '#,##0.##'
    end
    object CdsItensVendapro_codigobarras: TStringField
      FieldName = 'pro_codigobarras'
      Size = 13
    end
    object CdsItensVendapro_descricao: TStringField
      FieldName = 'pro_descricao'
      Size = 255
    end
    object CdsItensVendapro_id: TIntegerField
      FieldName = 'pro_id'
    end
    object CdsItensVendapro_valorvenda: TFloatField
      FieldName = 'pro_valorvenda'
      DisplayFormat = '#,##0.00'
    end
    object CdsItensVendaiv_totalitem: TFloatField
      FieldName = 'iv_totalitem'
      DisplayFormat = '#,##0.00'
    end
    object CdsItensVendaiv_status: TIntegerField
      FieldName = 'iv_status'
      OnGetText = CdsItensVendaiv_statusGetText
    end
    object CdsItensVendaiv_valor_restante: TFloatField
      FieldName = 'iv_valor_restante'
      DisplayFormat = '#,##0.00'
    end
    object CdsItensVendatotalVenda: TAggregateField
      Alignment = taCenter
      FieldName = 'totalVenda'
      Active = True
      DisplayName = ''
      DisplayFormat = '#,##0.00'
      Expression = 'sum(iv_totalItem)'
    end
  end
  object DsItensVenda: TDataSource
    DataSet = CdsItensVenda
    OnDataChange = DsItensVendaDataChange
    Left = 792
    Top = 80
  end
end
