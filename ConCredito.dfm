inherited F_ConCredito: TF_ConCredito
  Caption = 'Consulta de Cr'#233'dito'
  ClientHeight = 463
  ClientWidth = 1029
  ExplicitWidth = 1045
  ExplicitHeight = 502
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 350
    Width = 1029
    ExplicitTop = 350
    ExplicitWidth = 1029
    inherited icoPesquisar: TImage
      Left = 609
      ExplicitLeft = 609
    end
    inherited icoNovo: TImage
      Left = 714
      ExplicitLeft = 714
    end
    inherited icoImprimir: TImage
      Left = 819
      ExplicitLeft = 819
    end
    inherited icoFechar: TImage
      Left = 924
      ExplicitLeft = 924
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 427
    Width = 1029
    ExplicitTop = 427
    ExplicitWidth = 1029
  end
  inherited Panel3: TPanel
    Width = 1029
    Height = 309
    ExplicitWidth = 1029
    ExplicitHeight = 309
    inherited JvDBGrid1: TJvDBGrid
      Width = 1029
      Height = 309
      DataSource = DsCredito
      OnDrawColumnCell = JvDBGrid1DrawColumnCell
      OnDblClick = JvDBGrid1DblClick
      TitleRowHeight = 21
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CRED_TIPO'
          Title.Alignment = taCenter
          Title.Caption = 'Tipo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_ID'
          Title.Alignment = taCenter
          Title.Caption = 'C'#243'd. Cliente'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CRED_ID'
          Title.Alignment = taCenter
          Title.Caption = 'C'#243'd. Cr'#233'dito'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_NOME'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          Title.Caption = 'Cliente'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 478
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CRED_DATA'
          Title.Alignment = taCenter
          Title.Caption = 'Data'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CRED_VALOR'
          Title.Caption = 'Valor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CR_ID'
          Title.Alignment = taCenter
          Title.Caption = 'C'#243'd. CR.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CRP_SEQ'
          Title.Alignment = taCenter
          Title.Caption = 'Seq. Pgto'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end>
    end
  end
  inherited Panel4: TPanel
    Top = 395
    Width = 1029
    ExplicitTop = 395
    ExplicitWidth = 1029
    inherited pnPesquisar: TPanel
      Left = 609
      ExplicitLeft = 609
      ExplicitHeight = 32
    end
    inherited pnNovo: TPanel
      Left = 714
      ExplicitLeft = 714
      ExplicitHeight = 32
    end
    inherited pnImprimir: TPanel
      Left = 819
      ExplicitLeft = 819
      ExplicitHeight = 32
    end
    inherited pnFechar: TPanel
      Left = 924
      ExplicitLeft = 924
      ExplicitHeight = 32
    end
  end
  inherited Panel1: TPanel
    Width = 1029
    ExplicitWidth = 1029
  end
  object FDCredito: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      'select e.ent_nome, ec.*                       '
      'from entidade_credito ec                      '
      'inner join entidade e on ec.ent_id = e.ent_id '
      'order by e.ent_nome, ec.cred_id')
    Left = 144
    Top = 8
    object FDCreditoENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      Origin = 'ENT_NOME'
      Size = 250
    end
    object FDCreditoENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDCreditoCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
      Origin = 'CRED_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDCreditoCRED_VALOR: TFloatField
      FieldName = 'CRED_VALOR'
      Origin = 'CRED_VALOR'
    end
    object FDCreditoCRED_DATA: TSQLTimeStampField
      FieldName = 'CRED_DATA'
      Origin = 'CRED_DATA'
    end
    object FDCreditoCRED_TIPO: TStringField
      FieldName = 'CRED_TIPO'
      Origin = 'CRED_TIPO'
      FixedChar = True
      Size = 1
    end
    object FDCreditoCR_ID: TIntegerField
      FieldName = 'CR_ID'
      Origin = 'CR_ID'
    end
    object FDCreditoCRP_SEQ: TIntegerField
      FieldName = 'CRP_SEQ'
      Origin = 'CRP_SEQ'
    end
  end
  object ProviderCredito: TDataSetProvider
    DataSet = FDCredito
    Left = 176
    Top = 8
  end
  object CdsCredito: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderCredito'
    Left = 208
    Top = 8
    object CdsCreditoENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      Size = 250
    end
    object CdsCreditoENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Required = True
    end
    object CdsCreditoCRED_ID: TIntegerField
      FieldName = 'CRED_ID'
      Required = True
    end
    object CdsCreditoCRED_VALOR: TFloatField
      FieldName = 'CRED_VALOR'
      DisplayFormat = '#,##0.00'
    end
    object CdsCreditoCRED_DATA: TSQLTimeStampField
      FieldName = 'CRED_DATA'
    end
    object CdsCreditoCRED_TIPO: TStringField
      FieldName = 'CRED_TIPO'
      FixedChar = True
      Size = 1
    end
    object CdsCreditoCR_ID: TIntegerField
      FieldName = 'CR_ID'
    end
    object CdsCreditoCRP_SEQ: TIntegerField
      FieldName = 'CRP_SEQ'
    end
  end
  object DsCredito: TDataSource
    DataSet = CdsCredito
    Left = 240
    Top = 8
  end
end
