unit clsEntidade_Credito;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TEntidade_Credito = class
  public
    ent_id     : Integer;
    cred_id    : Integer;

    cred_valor : Double;
    cred_data  : TDateTime;
    cred_tipo  : String; //C-Cr�dito D-D�bito
    cr_id      : Integer; //Chave prim�ria da tabela ContasReceber_Pgto
    crp_seq    : Integer; //Chave prim�ria da tabela ContasReceber_Pgto

    function  Recuperar(Pent_id,Pcred_id:Integer):Boolean;
    procedure Gravar(Pent_id,Pcred_id:Integer);
    procedure Eliminar(Pent_id,Pcred_id:Integer);
    function  Ultima(Pent_id:Integer):Integer;
    function  CreditoSaldo(Pent_id:Integer):Double;
  end;

implementation

{ TEntidade_Credito }

uses DMC;

function TEntidade_Credito.CreditoSaldo(Pent_id: Integer): Double;
var
  tq          : TFDQuery;
  xTotCredito : Double;
  xTotDebito  : Double;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('select sum(ec.cred_valor) cred_valor                 '+
             'from entidade_credito ec                             '+
             'where ec.cred_tipo = ''C'' and ec.ent_id = :k_ent_id ');
  tq.ParamByName('k_ent_id').AsInteger := Pent_id;
  tq.Open;

  xTotCredito := tq.FieldByName('cred_valor').AsFloat;

  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('select sum(ec.cred_valor) cred_valor                 '+
             'from entidade_credito ec                             '+
             'where ec.cred_tipo = ''D'' and ec.ent_id = :k_ent_id ');
  tq.ParamByName('k_ent_id').AsInteger := Pent_id;
  tq.Open;

  xTotDebito := tq.FieldByName('cred_valor').AsFloat;

  Result := xTotCredito-xTotDebito;

  tq.Close;
  tq.Free;
end;

procedure TEntidade_Credito.Eliminar(Pent_id,Pcred_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Entidade_Credito where ent_id = :K_ent_id and cred_id = :k_cred_id');
  tq.ParamByName('K_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('K_cred_id').AsInteger := Pcred_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TEntidade_Credito.Gravar(Pent_id,Pcred_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade_Credito where ent_id = :k_ent_id and cred_id = :k_cred_id');
  tq.ParamByName('k_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('k_cred_id').AsInteger := Pcred_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Entidade_Credito values('+
    ':k_ent_id,'+
    ':k_cred_id,'+
    ':k_cred_valor,'+
    ':k_cred_data,'+
    ':k_cred_tipo,'+
    ':k_cr_id,'+
    ':k_crp_seq)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Entidade_Credito set '+
    'cred_valor = :k_cred_valor,'+
    'cred_data  = :k_cred_data,'+
    'cred_tipo  = :k_cred_tipo,'+
    'cr_id      = :k_cr_id,'+
    'crp_seq    = :k_crp_seq ');
    tq.SQL.Add('where ent_id = :K_ent_id and cred_id = :k_cred_id ');
  end;

  tq.ParamByName('k_ent_id').AsInteger     := Pent_id;
  tq.ParamByName('k_cred_id').AsInteger    := Pcred_id;
  tq.ParamByName('k_cred_valor').AsFloat   := cred_valor;
  tq.ParamByName('k_cred_data').AsDateTime := cred_data;
  tq.ParamByName('k_cred_tipo').AsString   := cred_tipo;
  if cr_id > 0 then begin
    tq.ParamByName('k_cr_id').AsInteger    := cr_id;
    tq.ParamByName('k_crp_seq').AsInteger  := crp_seq;
  end else begin
    tq.ParamByName('k_cr_id').IsNull;
    tq.ParamByName('k_crp_seq').IsNull;
  end;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TEntidade_Credito.Recuperar(Pent_id,Pcred_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade_Credito where ent_id = :K_ent_id and cred_id = :k_cred_id');
  tq.ParamByName('k_ent_id').AsInteger  := Pent_id;
  tq.ParamByName('k_cred_id').AsInteger := Pcred_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  ent_id     := tq.FieldByName('ent_id').AsInteger;
  cred_id    := tq.FieldByName('cred_id').AsInteger;
  cred_valor := tq.FieldByName('cred_valor').AsFloat;
  cred_data  := tq.FieldByName('cred_data').AsDateTime;
  cred_tipo  := tq.FieldByName('cred_tipo').AsString;
  cr_id      := tq.FieldByName('cr_id').AsInteger;
  crp_seq    := tq.FieldByName('crp_seq').AsInteger;

  tq.Close;
  tq.Free;
end;

function TEntidade_Credito.Ultima(Pent_id: Integer): Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(cred_id) maior from Entidade_Credito '+
             'where ent_id = :k_ent_id                        ');
  tq.ParamByName('k_ent_id').AsInteger := Pent_id;
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
