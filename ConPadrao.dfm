object F_ConPadrao: TF_ConPadrao
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  ClientHeight = 422
  ClientWidth = 704
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 17
  object Panel2: TPanel
    Left = 0
    Top = 309
    Width = 704
    Height = 45
    Align = alBottom
    BevelOuter = bvNone
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object icoPesquisar: TImage
      Left = 284
      Top = 0
      Width = 105
      Height = 45
      Cursor = crHandPoint
      Hint = 'Pesquisar'
      Align = alRight
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000200000
        00200804000000D973B27F0000000467414D410000B18F0BFC61050000000262
        4B474400FF878FCCBF0000000774494D4507E6010816352E925143DB0000036A
        4944415478DA95947968CE711CC75F3FC663CCB1C8151E33D764A296FF5C5942
        2B6794729B39929A6436676E128A15328D8C9C85B9B3C83D470C330CC3889123
        3666F3783FCFF3EBB7DF6FCFF388EF534FFDBEC7EBF3FE9C06BEE5C15CF5694B
        4FFD3A528FCF3CE60A3778CB77EF9141B065D8002E3D1CC2003D7659E7DF79C8
        298E72975FC12186F5BC2513194F072A29E42925FC249CE682B97596C75676F3
        3528C014DF9E858C92E55BECE72A2FF9248B2E1ACBA1BE8CA08BBE3358C5C740
        841FD082D58CA58C5DA4F3C82FD65A2E7A90CC50EDA6B3C41B0D2300E0622E0B
        247D236BF9620F8D15DA562C6202DF48651BBF0301BDD941B48EE6F99F3B2F98
        90D6B29FC01D4529CF79C3F0D4673173E4FB04EE874A950FD28B3D3493AB4BF0
        3801B1EC2546D637854A9489A8CD066690C3149E390153D84291527859FC080A
        43020CE239A26C24AA2E6C660CCF7631B399CE1B2629C659C135F89C707342E9
        9EA568390039CA74A6B62BF4FF80A57F013455310D503E56516107E41227E66C
        6A7296778CA12C24A031DB19C64A1929B703CECAB7DD52502E816EA6722E9806
        534116FD49630D9576C06666728669BC62B3A2B157297D1FA21ADA297CAD753B
        C31983311256A22CE4305C99AE54356E0A2C285F161238A0D64E746A343C1DF5
        2C4E7EADD42CD8C268F55CBA7AAFA8AA8ECD554FFBE33829275F3B0175495127
        E64BC375B5CD7AE504D9C8E2B65C295753979AA8816AB508195A53BD9491FD0C
        62D5C6C914D34DD5368836AACA7C0A54FBD92AF0DFBAD759F6FB7191C99A1501
        80309258260776B25C9320523ABA6BBC18C2DD134233804EAC50437F53B233AD
        8736003454794CA316A759C7350937348D3C9A0F7EDFFB485B5FD549A9EAE5BC
        0A2E970F5508C32A9214C98B9482E31CD31C2C533EC2A4AAAB723358A75F850F
        57447EE86EB6A6C78BEA00AFA5B1722546E3A554C7C512DC405977EBBB4C3633
        159D24EAF86E567088F93CF723EC53B986CF5E6F354C3335AF77FD50DEF3B9C0
        61852E415988341DAFE0A054147A9F5B013575D4204A00B7CA365CBDF9562E15
        78F3AE3558F5D2C80ABE57459A1F51BD60FD2B4CA84AFDAA5613CDCCD1DAC7A6
        22CD395C023156C27C7B6D35D0465443A4061F40A1A0512AF89136C42FF6FD23
        C04244ABA4EC2A4AFE196041A255AF552A2EFD17C09A0B7E15063749F94F8089
        885257C6F344FFE7FF0080451EDB7D4975890000002574455874646174653A63
        726561746500323032322D30312D30385432323A35333A33372B30303A3030D6
        47EFAF0000002574455874646174653A6D6F6469667900323032322D30312D30
        385432323A35333A33372B30303A3030A71A57130000001974455874536F6674
        776172650041646F626520496D616765526561647971C9653C0000000049454E
        44AE426082}
      ShowHint = True
      Transparent = True
      OnClick = icoPesquisarClick
      ExplicitLeft = 299
    end
    object icoNovo: TImage
      Left = 389
      Top = 0
      Width = 105
      Height = 45
      Cursor = crHandPoint
      Hint = 'Novo'
      Align = alRight
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000200000
        00200804000000D973B27F0000000467414D410000B18F0BFC61050000000262
        4B474400FF878FCCBF000000097048597300000B1300000B1301009A9C180000
        000774494D4507E60108163A04CE7296C20000038C4944415478DA9595776855
        5910C67F277989DD64231AD44463C3C6A2D81189820D05E30A6AFE8865772D01
        15BBAE2516ACB1226245635B5B3436581756145CA320BA2C2BA8D8BB60C7154D
        D018AFDFBDEF25EFBEFB6E04CFFBE3CD3D67E69B3933F3CD31442D2BF817A006
        D588D7E747DEEBF7C5DE3451DAC6D718D248A71B4D4892E10B6EF0371779E607
        617CCCDBD35FC66DF8CC23DE11C30FA4F0897F28E40FEE788D8CC7733DC6F293
        823FC9651ECBF70769D42499C682ECAB48F67380FFDD2091003FB284B61C94D2
        1DDD3A7225D152E0FD292097876153374067360B7D31E71570C4FDCAE3ABC120
        A60B7CA67D15178015F4BE9BA7CCE21ADF5E3D59A6CB4DE649D0D884CCEBB285
        04C6DBE6A6CC6B9C6E5E5DE22B3B64138EA52FEB95A3D97C7403E490CDCF9C89
        08BB1E4B6927F12F851C799DB12C609A72A55DE36C74600727584449B840DA6F
        403E5D241E22D35D3A9D24B247C2AF8A2D043057FE33F8CFA396A26A7493B887
        919E13E8ADB369CA9A0390C656AE328F228F5AAAAA5E11402ABF7385197C3256
        802CD632917D51F9AEADE07BE87F17BF449DC52A2F19CADA4D6325B18A3E0CE7
        AC7310AF868973A4526AA9325D251D56D162422114F326482BB9DDC060CE182B
        5521C6319A1BC60EAE15BFD14CC676A49568A136460D7D2BE437C0395608C25E
        FD14F3688E1AAB3147C5B36C1E3A009D548FD6DF68A33F95FBE78E663A7B994A
        810D50C04B013C08016C574F56BC4ECAEB3347B3BB22986C03D497CF046D5F75
        B6D3243572AE50AA71D29D3A92EE891DC13204C4D13C51DC5E03043082E3C64A
        10033319C629C20C0AB227595548AFA00AA8022B95C473C6320C140B7384EC5D
        75D5AC15015462BEAE3B4617B7EC7A6FD4E098C2DBEFE8C4661C513EE660055B
        798A22C8E0C277000C650D13C49F10179AB093BB8CA3D843A6431A32C8D7600F
        1B53D45CB755B9E2309DB3358926C9A35BB1B67ABDB5360A591EB11F2B9A0F55
        3F9C75E81CDAACAE9CA6ABAEA75DA32346B48D975864172E5C1C99E64A7B75F9
        440A1D34649D46C8BCF272FAAF38D53E87632C2C03750FD5A6426E258AE4F3BA
        4CDFE5D7FE48518C2344E45CF1C3672ADBC999CE102EA927AE881F252ECF5574
        D695519A03DBD86417DC03500E5155E5CCA223D7F598DDD7C87AEF3C2C7568AE
        1994A842E7693FE295F47BDA92E9A559D84ECD12AB068B51828B04F8AF4C0B83
        3CF079DAA2402AAB0BEA8B6401C925CAC913BD05A57E06D1EF7538693E2B5AFD
        2B7D691B22239C06090000002574455874646174653A63726561746500323032
        322D30312D30385432323A35373A35382B30303A3030EF8B36BB000000257445
        5874646174653A6D6F6469667900323032322D30312D30385432323A35373A35
        382B30303A30309ED68E070000000049454E44AE426082}
      ShowHint = True
      Transparent = True
      OnClick = icoNovoClick
      ExplicitLeft = 404
    end
    object icoImprimir: TImage
      Left = 494
      Top = 0
      Width = 105
      Height = 45
      Cursor = crHandPoint
      Hint = 'Imprimir Lista'
      Align = alRight
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000200000
        00200804000000D973B27F0000000467414D410000B18F0BFC61050000000262
        4B474400FF878FCCBF0000000774494D4507E6010816352E925143DB00000276
        4944415478DACD955F6C8B5118C67FA7A3138BD04988F8732341B2CB656191D1
        44E2CA16891071218B0D33860B11FF420C9B4BE25F8D8AB89021124124261226
        2E6671238648B82496D0212E36BAE3F9CED7A65DDBB5562EBC4DDAEF6BCFF7F4
        39CFFBBCCF31B8B260584E27418674357A59B7622D5D587F994903A8E50C3778
        C7B83C00BF98CB6AB6722737402B0D3C277F5512E5606E8015B4B38527F97740
        0DE7D8CBDDFF13A096369AE92E00B084B3EC4BD3C0A67E0D73828DF41610B18A
        0BECE451F2D6D820F32817745CFAEEE0387D05DA58C11E4EAA5725E2FC85B7C6
        EE962D828EDD24A6D2CF0F0279008629631A9FF9EE363F44A7B131D189EAD638
        1625EE73F4F25705F4EE013410F600F64B967CCA6557505CBF8A6B33C78C1DE0
        90F634962AA19E3AEE7151966E4D008C85802DE581DCD0C51AD673A41880801C
        B392FB5CA3A5380648C429EAC34FB5FD70311AA42A03202493944AA2FC156750
        668B6503189649D7D01F00C468E4A1734B06401DA7B8CC9B024E5CA026B6703B
        17402D47E5ADC2C314E580378DD9007E1EF4B0D00D57AE8AF14C00113F0F7231
        6863331FF43E5F3BCDACA05EAFD9C50CCEFB79900488092FE29684392D064F99
        A9891B1EC1C1B3C9642632C02B162991B625F2A089766307B92AF58D1EA9D417
        9B14EB1B982D069900D7E976D735FABB88F2C09BC846D619DB215B7E72631A92
        BBEA792F87CF111C2300E25CE18503ADE6929223E69E98CE2D63CB659F905B54
        C92A31E8611613B214F04EA432BEA9C9D57470530CAC13B5CFCF447F14964A83
        263E2A192B145D9915603C2FD92E1123D2E07102362D54936DEC65B1422BBB0B
        5E0EF44BE2AA64ACA7E4E15F1D6D7F7DB81675BCFF06F0D70C58329F84450000
        002574455874646174653A63726561746500323032322D30312D30385432323A
        35333A33372B30303A3030D647EFAF0000002574455874646174653A6D6F6469
        667900323032322D30312D30385432323A35333A33372B30303A3030A71A5713
        0000001974455874536F6674776172650041646F626520496D61676552656164
        7971C9653C0000000049454E44AE426082}
      ShowHint = True
      Transparent = True
      OnClick = icoImprimirClick
      ExplicitLeft = 509
    end
    object icoFechar: TImage
      Left = 599
      Top = 0
      Width = 105
      Height = 45
      Cursor = crHandPoint
      Hint = 'Fechar'
      Align = alRight
      Center = True
      ParentShowHint = False
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000200000
        00200806000000737A7AF40000000467414D410000B18F0BFC61050000000662
        4B474400FF00FF00FFA0BDA7930000000774494D4507E6010816352E925143DB
        00000A7C4944415478DA95570954536716BE2F1B0909219085258882B2886C75
        4404AC208A4B45C1566BD17AA66AAD051746EA421DB752A96D69AB15B796DA19
        04A162115038821614D041941D0C9BEC10C21642085949327F8278D8BADDF392
        F3DE3BF7BFF7FBEFF2DDFF61308384F382A071A40AD6B04360FFEC68B8D47692
        E3407573A2E0A93E641CC5DFCCC0CADC88C060D0F0C6341C86C7E46AA952A21E
        1277295ADB44AAFE7AA55A91D32CAF2B0BAB39D975C5396AF45E6F32D8515DE0
        9BF929D37C61535F1C6FF8009AA53CB8E1560C47EB434CFD991BBD2C48D6EF99
        1970031844368B8433C08D2DC4A6ACD76AB563FF5AA1AA47C257B4150FA87A52
        9F89F2328FDA7EDF75B0762358906703BAFF6300FB791BE0C2FC0CB8D27EDA6D
        B1B17FA815D936904932E7123002FC5D1954F54BBB14CD5965E2C2B8BDBC4379
        476DF6A8B7710F822DC5713A80E89761B09AF31E5C6C3D4E0A606D5AB9D4644D
        249B68E143C1D3707FDBF304516995DA7E6577C3F3A187DF3D15E5C6875A4729
        66916DE09D1257B8ED513D06606FCD5B20550F438FB213B7C562DFDBAB589BA3
        D9244B7B3C869FC1A4163428D0E8526B750F63BBD05D784C27D383AA17A1AAAF
        BB4874FF8B8281AC38038CAC403F38EE7005B06D159EC01B2E81B2A5A37083FF
        BD4F006BF31516C9DC0507D337AED4C895328D7400E5B656A8EAAD162A7B853A
        CF2C92258B8A37B26392CC9C68783AC7004731F83D1008C0A98DD93BAEBDEFE2
        A5C1690980196603E42FC9874261A66390D98E586BCABC95048C3869A146AB86
        A151616B939497DE34F2221D45EA85ABB197A84C54A02FC6C50C7F7832986DC4
        2159CD9F67E8F49603CD7D9331C1D48184234F03215074F0B20437F6ED9A1DF9
        D0E9015A7DA46E0B140C6651CF3BA69F72365A1C81763229EE728D74942F6F2B
        2E15177C7DBF3FE59EB7F12A55D57031A06E80AFE7FFA2D7D9531DA0D3834574
        3FD89F118D5D0DFECC773933289C4BB15D85EC194EA909A81357DCBCD9793582
        4DB2E063BAE649115C59E6C35813C725DBD84F545668E49A4E79D3EDB49E9FA3
        0FD97C5B71A4FE3D8889BC09903E73C19D69088516693DC43825C3E59628ABB7
        38219FDAD15C76D00874CA94EE187E2ECC3FB0FAC2A6046C77953F7DB7F58913
        EE74EF43448C34A9D43A652D45777AAEEF5DCDDE5C7EEAE52E302572E0E282CC
        3FACFAB0CA75F0A82F0B323C6BE07ADBB7DC7FCE3974611665EEDB441C493BC6
        147AC15A46EAD26A442561582AFF273B57BA67D23CAAF3A2898610A3F53CEABF
        7338D86267C2F60A1F984B7582CFEC7FFAD3B6DBFB7C23F87156C233E16308B4
        7C1FAAC5CF7D17327C3EA2128C180880FA951A6154A36A1ED58E4663B9BD6941
        8B4C7C93E80493D7B9D2F56EA3A426F59BC64FF618111942229E04DF2E48F953
        E7E3002EBADD86B3F5E106FD0A01F587A614498A6FB28686A31365A352BD0E72
        8E5170865A23A2B11CCBE949890DE06C0A43F5F8BAEFA46A8928831F1FAED2AA
        AEA7745D862CEFC6698EB616784D7A4E5A56F41AC0C7B691D02879E1CC34E0EC
        C7305C69495FE1C388A2B36D09BEB1CA2A6131C42C4984677D8F6054AB02ACA0
        2F2B6D296BED867100886100216F4D683DBFAE57C1E77DE59638E34EC7F3F948
        9089DA5403639C84E9EF093802A021E5614373C844BBA40CAB4465BD327EF163
        C1FD7B8582EC121793C592C7DD39C02473002B133E7EE06EE2BD0201D05398CE
        408BA4BEF056C78FC195A2A7C25F7C9ECE08E064C56ECC94C459E9CCF0F0468C
        89BD42808069B472B54C8EA626DB95B9643B8A8299EEFDC8E8B046A212BFEC91
        753DB9DF793BAD69885718EB7D6B086B105795CC33725E3801801A556852DBC8
        CB0FAFB79C57C67B3D9C11405479186E2EDD292270D6D6E3048C44D6B91E1B2D
        7A1C1A1D4119E02906380C378957946AB91AA57640AC14D517F2EF65623C5179
        91A3B19BE704009AFAA1AAF4C73DD9DB1F76DF95262FFFDF8C003ECC5F83F360
        2FDBB5755ED877041C9132EE792C3FA04B01A0E2C58FDB1D17356A04855A2696
        8FCA04957DC5D958716F5E9607DB6FCD780DE852D02E69ACF8B1FECB750808FF
        F6CAB21901ACCF76C116327D16AFB30ED98C4D98AAAFF2A026130C9956349B0D
        26062C96EE957C548A3875A413EDBCB4A4A7F01E6FB03C4FA214B7610F3A6F27
        ADE0066F99D80562A5B0FD6C794450B7B4BD22DE7FE6149C2D0F8748F7F3A09B
        7F1308462F45DDB9D020AA71F534F74B9D636467259076560E2A060A79C2F2BC
        8B9551C5EF3BEE13360DD5C2B9CC24C0B23B6E1D596119F4050AE3EB5CC94647
        44090DB107551AE57F0F179E0259D87400F6C9000B59DE6067BC00A42AA9BE7B
        C6C59B13A00BB53D8BCC39C0A298D5E676DCCD8F883FD3981391AA48A8BD0889
        E90FE1C6BECB20510E0196D77567C97CC61B69E68656E613F3D43DD29EB1F3B7
        800F2CA8D62226890DE7FCFE1A11E9E456E3CFBA61456A19AAA7FF2B3B4ABCC7
        3558ED6BB38630AC1AD2A70A8F463D1E236851F4945874C90176E09CADE75C99
        9EDB261A112B0785055DD99181362171EBD29CC1D4C00C12D7E5FD2500D14587
        E1E5601DFCBCF60E5C2DFFD2D796E1B0854CA0D0509434BA48E1014FEC1DE9AA
        E914B7FCA09F864FBAEFBFEBCAF28C43A4419F68A857CA2F4F6DB87630D4FD44
        FEA60C2F70327587CF975DFD43E7C137BDE059771174856BE17461F882B08547
        2FB2A9167EC88F665C07A5565321288A3E9DFF710C16F5743F2A986ACEF1C5DF
        9FB767388760D8C4939016F892F6A2ACA694E3BBDD0EE77D90B91A4ABA0BE1C5
        47B2199DFB5F77040B232B480C7E00271EED75DFE97EE0D42CBA6D10AAAF49AD
        88765EF6A43D778739CDA20A8313C84D9416EE3625AD5DCA5D75D984CC9A33D5
        70AFB4BB0A39BE72A9F44CC646BB1DDDCD83B550D491071C2A179C5973A1A8B3
        0239B60017330FF8ADE50E6DB3D38ED501B6C107D954332F3486279DED861543
        A2527EE1BF972F5BFF83DFE7366ACC25DE184C48465050DB49FA6D675AB8A7A5
        FF311A91CE980A42AC100D0ECA079EE7B566A60A657D4F4DC89CA685E64B542A
        B50A2A04C5A40E7113976B64EDB9C276C37AA621C7CB84CCE44EB5813E58D44D
        C2BAEBD1F9119FB0A89CC17EA9608C40C2733721DA240383C4A205CCDE786601
        FB1FBB2944AAE154031A3D8B29C423CAE15E915CD435AA510A75EF11E3B18D0D
        18665492119B88231A4F0DB93EEF6AA5BA43DC92F36B75FCFE6DEE5B9BB97467
        987486BE5619036831B4085B4D036CD71F73E12CDA45234D8FC4586568C7C967
        BCB07063C6663E92CB545265DB6053563AEFC6914F977FF51202012CDFA400FF
        A86CF20A9DD1B89218681B6AA2BE397B55D822CBA5A10CB2A90D1EF7F7BF8AC6
        810E4A077A5B071B9253AB13CE7FB1F66AABD7A5D9FA8369C9BE6EBDCE34C897
        8AA3A15DD48C805C235D0AFC71C502CE1B215CE3396B4D29639CFE5705A549C1
        177716D4F556259E2B38FD6B80FD7AE9B11531E071810BCF0F74BDD69B316647
        B27702AFB702EE6E2F85776EF8B0433D2397D20D180108840F93CA996B426192
        51B426AF452341AA1C51F749047CC170D733948E7B4F5A73F33FF18B6A0D4B7D
        57EBC87186D30F4E82300AA62CFB1D718F6542A560008E2D0B8533AB2EC3AE5B
        EB191B9CB69A23E7AE62F988834C2561310D4DCD51C19186156221FA40EF2313
        0D1B0C49D4D29CDAB4EE98A06B7DFF7916AB4D2E8F030ECD0212B7E5CCE8E7FF
        2064D10F0D7D5A960000002574455874646174653A6372656174650032303232
        2D30312D30385432323A35333A33362B30303A30307030E41B00000025744558
        74646174653A6D6F6469667900323032322D30312D30385432323A35333A3336
        2B30303A3030016D5CA70000001974455874536F6674776172650041646F6265
        20496D616765526561647971C9653C0000000049454E44AE426082}
      ShowHint = True
      Transparent = True
      OnClick = icoFecharClick
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 386
    Width = 704
    Height = 36
    Panels = <
      item
        Style = psOwnerDraw
        Text = 'Data'
        Width = 450
      end
      item
        Alignment = taCenter
        Style = psOwnerDraw
        Text = 'Hora'
        Width = 115
      end
      item
        Alignment = taRightJustify
        Style = psOwnerDraw
        Text = 'Usu'#225'rio'
        Width = 100
      end>
    OnDrawPanel = StatusBar1DrawPanel
  end
  object Panel3: TPanel
    Left = 0
    Top = 41
    Width = 704
    Height = 268
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    ExplicitHeight = 257
    object JvDBGrid1: TJvDBGrid
      Left = 0
      Top = 0
      Width = 704
      Height = 268
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Segoe UI'
      TitleFont.Style = []
      OnTitleClick = JvDBGrid1TitleClick
      SelectColumnsDialogStrings.Caption = 'Select columns'
      SelectColumnsDialogStrings.OK = '&OK'
      SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
      EditControls = <>
      RowsHeight = 21
      TitleRowHeight = 17
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 354
    Width = 704
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    Color = 12673280
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    object lblRegTotal: TLabel
      Left = 13
      Top = 1
      Width = 153
      Height = 28
      Caption = 'N'#186' de Registros:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -20
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object pnPesquisar: TPanel
      Left = 284
      Top = 0
      Width = 105
      Height = 32
      Align = alRight
      BevelOuter = bvNone
      Caption = 'F6 - PESQUISAR'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      ExplicitHeight = 37
    end
    object pnNovo: TPanel
      Left = 389
      Top = 0
      Width = 105
      Height = 32
      Align = alRight
      BevelOuter = bvNone
      Caption = 'F1 - NOVO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      ExplicitHeight = 37
    end
    object pnImprimir: TPanel
      Left = 494
      Top = 0
      Width = 105
      Height = 32
      Align = alRight
      BevelOuter = bvNone
      Caption = 'F5 - IMPRIMIR'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      ExplicitHeight = 37
    end
    object pnFechar: TPanel
      Left = 599
      Top = 0
      Width = 105
      Height = 32
      Align = alRight
      BevelOuter = bvNone
      Caption = 'ESC - FECHAR'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      ExplicitHeight = 37
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 704
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Color = clGray
    UseDockManager = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 4
  end
  object Timer1: TTimer
    Interval = 500
    OnTimer = Timer1Timer
    Left = 655
    Top = 8
  end
end
