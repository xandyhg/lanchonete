unit clsContasReceber;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TContasReceber = class
  public
    cr_id             : Integer;
    venda_id          : Integer;
    cr_valor          : Double;
    cr_datacadastro   : TDateTime;
    cr_datavencimento : TDateTime;
    cr_obs            : String;
    cr_status         : Integer; //0-Aberto 1-Liquidado 2-Parcial

    function  Recuperar(Pcr_id:Integer):Boolean;
    procedure Gravar(Pcr_id:Integer);
    procedure Eliminar(Pcr_id:Integer);
    function  Ultima:Integer;
    function  RecuperarPorVenda(Pvenda_id:Integer):Boolean;
    procedure EstornarContaPagar(Pcr_id:Integer);
  end;

implementation

{ TContasReceber }

uses DMC;

procedure TContasReceber.Eliminar(Pcr_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from ContasReceber where cr_id = :K_cr_id');
  tq.ParamByName('K_cr_id').AsInteger := Pcr_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TContasReceber.EstornarContaPagar(Pcr_id: Integer);
var
  tq : TFDQuery;
begin
//  tq := TFDQuery.Create(nil);
//  tq.Connection := DM.FDCon;
//  tq.SQL.Clear;
//  tq.Params.Clear;
//  tq.SQL.Add('update ContasReceber set cr_valorpago=0, cr_datapagamento=''30.12.1899'' '+
//             'where cr_id = :K_cr_id                                                 ');
//  tq.ParamByName('K_cr_id').AsInteger := Pcr_id;
//
//  tq.ExecSQL;
//  tq.Close;
//  tq.Free;
end;

procedure TContasReceber.Gravar(Pcr_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ContasReceber where cr_id = :k_cr_id');
  tq.ParamByName('k_cr_id').AsInteger := Pcr_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into ContasReceber values('+
    ':k_cr_id,'+
    ':k_venda_id,'+
    ':k_cr_valor,'+
    ':k_cr_datacadastro,'+
    ':k_cr_datavencimento,'+
    ':k_cr_obs,'+
    ':k_cr_status)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update ContasReceber set '+
    'venda_id          = :k_venda_id,'+
    'cr_valor          = :k_cr_valor,'+
    'cr_datacadastro   = :k_cr_datacadastro,'+
    'cr_datavencimento = :k_cr_datavencimento,'+
    'cr_obs            = :k_cr_obs,'+
    'cr_status         = :k_cr_status ');
    tq.SQL.Add('where cr_id = :K_cr_id ');
  end;

  tq.ParamByName('k_cr_id').AsInteger              := Pcr_id;
  if venda_id > 0 then
    tq.ParamByName('k_venda_id').AsInteger         := venda_id
  else
    tq.ParamByName('k_venda_id').IsNull;
  tq.ParamByName('k_cr_valor').AsFloat             := cr_valor;
  tq.ParamByName('k_cr_datacadastro').AsDateTime   := cr_datacadastro;
  tq.ParamByName('k_cr_datavencimento').AsDateTime := cr_datavencimento;
  tq.ParamByName('k_cr_obs').AsString              := cr_obs;
  tq.ParamByName('k_cr_status').AsInteger          := cr_status;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TContasReceber.Recuperar(Pcr_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ContasReceber where cr_id = :K_cr_id');
  tq.ParamByName('k_cr_id').AsInteger := Pcr_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  cr_id             := tq.FieldByName('cr_id').AsInteger;
  venda_id          := tq.FieldByName('venda_id').AsInteger;
  cr_valor          := tq.FieldByName('cr_valor').AsFloat;
  cr_datacadastro   := tq.FieldByName('cr_datacadastro').AsDateTime;
  cr_datavencimento := tq.FieldByName('cr_datavencimento').AsDateTime;
  cr_obs            := tq.FieldByName('cr_obs').AsString;
  cr_status         := tq.FieldByName('cr_status').AsInteger;

  tq.Close;
  tq.Free;
end;

function TContasReceber.RecuperarPorVenda(Pvenda_id: Integer): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ContasReceber where venda_id = :K_venda_id');
  tq.ParamByName('k_venda_id').AsInteger := Pvenda_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('cr_id').AsInteger);

  tq.Close;
  tq.Free;
end;

function TContasReceber.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(cr_id) maior from ContasReceber');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
