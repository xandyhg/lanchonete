inherited F_CadFuncao: TF_CadFuncao
  Caption = ' CADASTRO DE FUN'#199#195'O'
  ClientHeight = 166
  ClientWidth = 646
  OnActivate = FormActivate
  ExplicitWidth = 662
  ExplicitHeight = 205
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 89
    Width = 646
    ExplicitTop = 89
    ExplicitWidth = 646
    inherited icoFechar: TImage
      Left = 556
      ExplicitLeft = 556
    end
    inherited icoAlterar: TImage
      Left = 376
      OnClick = icoAlterarClick
      ExplicitLeft = 376
    end
    inherited icoExcluir: TImage
      Left = 466
      OnClick = icoExcluirClick
      ExplicitLeft = 466
    end
    inherited icoSalvar: TImage
      Left = 196
      OnClick = icoSalvarClick
      ExplicitLeft = 196
    end
    inherited icoCancelar: TImage
      Left = 286
      OnClick = icoCancelarClick
      ExplicitLeft = 286
    end
  end
  inherited Panel3: TPanel
    Width = 646
    Height = 48
    ExplicitWidth = 646
    ExplicitHeight = 48
    object Label2: TLabel
      Left = 9
      Top = 15
      Width = 57
      Height = 17
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edFun_descricao: TEdit
      Left = 72
      Top = 12
      Width = 569
      Height = 25
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  inherited Panel4: TPanel
    Top = 134
    Width = 646
    ExplicitTop = 134
    ExplicitWidth = 646
    inherited pnCancelar: TPanel
      Left = 286
      ExplicitLeft = 286
    end
    inherited pnAlterar: TPanel
      Left = 376
      ExplicitLeft = 376
    end
    inherited pnExcluir: TPanel
      Left = 466
      ExplicitLeft = 466
    end
    inherited pnSalvar: TPanel
      Left = 196
      ExplicitLeft = 196
    end
    inherited pnFechar: TPanel
      Left = 556
      ExplicitLeft = 556
    end
  end
  inherited Panel5: TPanel
    Width = 646
    ExplicitWidth = 646
    object Label1: TLabel
      Left = 23
      Top = 10
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 72
      Top = 7
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Timer1: TTimer
    Left = 576
  end
end
