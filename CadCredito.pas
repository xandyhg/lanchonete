unit CadCredito;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits, FireDAC.Comp.Client, Vcl.Buttons;

type
  TF_CadCredito = class(TF_CadPadrao)
    Label2: TLabel;
    edCred_ID: TJvCalcEdit;
    Label3: TLabel;
    edCred_Valor: TJvCalcEdit;
    edCred_Data: TJvDateEdit;
    Label4: TLabel;
    rgCred_Tipo: TRadioGroup;
    GroupBox1: TGroupBox;
    edCr_id: TJvCalcEdit;
    Label5: TLabel;
    edCrp_seq: TJvCalcEdit;
    Label6: TLabel;
    Label1: TLabel;
    edCodigo: TJvCalcEdit;
    sbEntidade: TSpeedButton;
    edEnt_Nome: TEdit;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
    procedure sbEntidadeClick(Sender: TObject);
    procedure edCodigoExit(Sender: TObject);
    procedure edCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadCredito: TF_CadCredito;

implementation

{$R *.dfm}

uses ConCredito, clsEntidade_Credito, X, DMC, U_Principal, ConEntidade,
  clsEntidade;

var
  EntCred  : TEntidade_Credito;
  Entidade : TEntidade;

procedure TF_CadCredito.edCodigoExit(Sender: TObject);
begin
  inherited;
  if edCodigo.AsInteger > 0 then begin
    Entidade := TEntidade.Create;
    if Entidade.Recuperar(edCodigo.AsInteger) then begin
      if Entidade.ent_tipo <> 0 then begin
        msgPerson('C�digo informado n�o � de cliente!',[mbok],'Entidade',mtError);
        edEnt_Nome.Clear;
        edCodigo.SetFocus;
        edCodigo.SelectAll;
      end else begin
        edEnt_Nome.Text := Entidade.ent_nome;
      end;
    end else begin
      msgPerson('Cliente n�o cadastrado!',[mbok],'Entidade',mtError);
      edEnt_Nome.Clear;
      edCodigo.SetFocus;
      edCodigo.SelectAll;
    end;
    FreeAndNil(Entidade);
  end else begin
    edEnt_Nome.Clear;
  end;
end;

procedure TF_CadCredito.edCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = 117 then
    sbEntidadeClick(Sender);
end;

procedure TF_CadCredito.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_ConCredito_Cred_ID > 0 then begin
    EntCred := TEntidade_Credito.Create;
    if EntCred.Recuperar(xAux_ConCredito_Ent_ID,xAux_ConCredito_Cred_ID) then begin
      edCodigo.AsInteger      := xAux_ConCredito_Ent_ID;
      edCred_ID.AsInteger     := xAux_ConCredito_Cred_ID;
      edEnt_Nome.Text         := xAux_ConCredito_Ent_Nome;
      edCred_Valor.Value      := EntCred.cred_valor;
      edCred_Data.Date        := EntCred.cred_data;
      if EntCred.cred_tipo = 'C' then
        rgCred_Tipo.ItemIndex := 0
      else
        rgCred_Tipo.ItemIndex := 1;
      edCr_id.AsInteger       := EntCred.cr_id;
      edCrp_seq.AsInteger     := EntCred.crp_seq;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(EntCred);
    edCodigo.Enabled   := False;
    sbEntidade.Enabled := False;
  end else begin
    edCodigo.AsInteger := xAux_ConCredito_Ent_ID;
    edCred_ID.Clear;
    edEnt_Nome.Text := xAux_ConCredito_Ent_Nome;
    edCred_Valor.Clear;
    edCred_Data.Date := Date;
    rgCred_Tipo.ItemIndex := 0;
    edCr_id.Clear;
    edCrp_seq.Clear;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadCredito.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadCredito.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConCredito_Cred_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadCredito.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Cr�dito',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    EntCred := TEntidade_Credito.Create;

    EntCred.Eliminar(edCodigo.AsInteger,edCred_ID.AsInteger);

//    xAux_AuxJustificativa_Codigo   := edCodigo.AsInteger;
//    xAux_AuxJustificativa_NomeForm := UpperCase(Copy(Name,6,Length(Name)));
//    xAux_AuxJustificativa_Descricao:= edConfe_nome.Text;
//    AbreForm(TF_AuxJustificativa,Self);
//    xAux_AuxJustificativa_Codigo   := 0;
//    xAux_AuxJustificativa_NomeForm := '';
//    xAux_AuxJustificativa_Descricao:= '';

    msgPerson('Cr�dito eliminado com sucesso!',[mbok],'Cr�dito',mtConfirmation);

    FreeAndNil(EntCred);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadCredito.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    EntCred := TEntidade_Credito.Create;

    EntCred.cred_valor := edCred_Valor.Value;
    EntCred.cred_data  := edCred_Data.Date;
    EntCred.cred_tipo  := Copy(rgCred_Tipo.Items[rgCred_Tipo.ItemIndex],1,1);
    EntCred.cr_id      := edCr_id.AsInteger;
    EntCred.crp_seq    := edCrp_seq.AsInteger;

    if edCred_ID.AsInteger = 0 then begin
      xAux_ConCredito_Cred_ID :=  EntCred.Ultima(edCodigo.AsInteger);
      EntCred.Gravar(edCodigo.AsInteger,xAux_ConCredito_Cred_ID);
    end else begin
      EntCred.Gravar(edCodigo.AsInteger,xAux_ConCredito_Cred_ID);
    end;

    FreeAndNil(EntCred);

//    if edCodigo.AsInteger = 0 then begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 1;
//      log.log_tabela      := 'CONFIG EMAIL';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'GRAVADO C�DIGO: '+IntToStr(xAux_ConConfEmail_Confe_ID)+' - '+edConfe_nome.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end else begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 2;
//      log.log_tabela      := 'CONFIG EMAIL';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'ALTERADO C�DIGO: '+IntToStr(xAux_ConConfEmail_Confe_ID)+' - '+edConfe_nome.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end;

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCred_ID.AsInteger = 0 then
      msgPerson('Cr�dito '+IntToStr(xAux_ConCredito_Cred_ID)+' cadastrado com sucesso!',[mbOK], 'Cr�dito', mtConfirmation)
    else
      msgPerson('Cr�dito '+IntToStr(xAux_ConCredito_Cred_ID)+' alterado com sucesso!',[mbOK], 'Cr�dito', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

procedure TF_CadCredito.sbEntidadeClick(Sender: TObject);
begin
  inherited;
  xAux_ConEntidade_Ent_Tipo   := 0;
  xAux_ConEntidade_Tela       := 'VENDA';
  xAux_ConEntidade_AtivaEnter := True;
  AbreForm(TF_ConEntidade,Self);
  xAux_ConEntidade_Ent_Tipo   := -1;
  xAux_ConEntidade_Tela       := '';
  xAux_ConEntidade_AtivaEnter := False;
  edCodigo.AsInteger          := xAux_ConEntidade_Ent_ID;
  edCodigoExit(Sender);
end;

end.
