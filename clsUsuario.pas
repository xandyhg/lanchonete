unit clsUsuario;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TUsuario = class
  public
    usua_id                 : Integer;
    usua_nome               : String;
    usua_login              : String;
    usua_senha              : String;
    usua_dataadmissao       : TDateTime;
    usua_datadesligamento   : TDateTime;
    usua_fone               : String;
    usua_obs                : String;
    usua_ativo              : Integer; //0-Sim 1-N�o
    usua_email              : String;
    usua_email_nome         : String;
    usua_email_usuario      : String;
    usua_email_senha        : String;
    usua_caminho_imp_pdf    : String;
    usua_caminho_imp_padrao : String;
    usua_alterarSenha       : Integer; //0-N�o 1-Sim

    function  Recuperar(Pusua_id:Integer):Boolean;
    procedure Gravar(Pusua_id:Integer);
    procedure Eliminar(Pusua_id:Integer);
    function  Ultima:Integer;
    function  RecuperarPorLogin(PUsua_login:String):Boolean;
    procedure AlteraSenha(Pusua_id,Pusua_alterarSenha:Integer;Pusua_senha:String);
  end;

implementation

{ TUsuario }

uses DMC;

procedure TUsuario.AlteraSenha(Pusua_id,Pusua_alterarSenha: Integer;Pusua_senha:String);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('update usuario set usua_alterarSenha=:k_usua_alterarSenha, usua_senha=:k_usua_senha '+
             'where usua_id = :K_usua_id                                                          ');
  tq.ParamByName('K_usua_id').AsInteger           := Pusua_id;
  tq.ParamByName('K_usua_alterarSenha').AsInteger := Pusua_alterarSenha;
  tq.ParamByName('k_usua_senha').AsString         := Pusua_senha;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TUsuario.Eliminar(Pusua_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Usuario where usua_id = :K_usua_id');
  tq.ParamByName('K_usua_id').AsInteger := Pusua_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TUsuario.Gravar(Pusua_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Usuario where usua_id = :k_usua_id');
  tq.ParamByName('k_usua_id').AsInteger := Pusua_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Usuario values('+
    ':k_usua_id,                           '+
    ':k_usua_nome,                         '+
    ':k_usua_login,                        '+
    ':k_usua_senha,                        '+
    ':k_usua_dataadmissao,                 '+
    ':k_usua_datadesligamento,             '+
    ':k_usua_fone,                         '+
    ':k_usua_obs,                          '+
    ':k_usua_ativo,                        '+
    ':k_usua_email,                        '+
    ':k_usua_email_nome,                   '+
    ':k_usua_email_usuario,                '+
    ':k_usua_email_senha,                  '+
    ':k_usua_caminho_imp_pdf,              '+
    ':k_usua_caminho_imp_padrao,           '+
    ':k_usua_alterarSenha)                 ');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Usuario set                           '+
    'usua_nome                = :k_usua_nome,                '+
    'usua_login               = :k_usua_login,               '+
    'usua_senha               = :k_usua_senha,               '+
    'usua_dataadmissao        = :k_usua_dataadmissao,        '+
    'usua_datadesligamento    = :k_usua_datadesligamento,    '+
    'usua_fone                = :k_usua_fone,                '+
    'usua_obs                 = :k_usua_obs,                 '+
    'usua_ativo               = :k_usua_ativo,               '+
    'usua_email               = :k_usua_email,               '+
    'usua_email_nome          = :k_usua_email_nome,          '+
    'usua_email_usuario       = :k_usua_email_usuario,       '+
    'usua_email_senha         = :k_usua_email_senha,         '+
    'usua_caminho_imp_pdf     = :k_usua_caminho_imp_pdf,     '+
    'usua_caminho_imp_padrao  = :k_usua_caminho_imp_padrao,  '+
    'usua_alterarSenha        = :k_usua_alterarSenha         ');
    tq.SQL.Add('where usua_id = :K_usua_id                ');
  end;

  tq.ParamByName('k_usua_id').AsInteger                 := Pusua_id;
  tq.ParamByName('k_usua_nome').AsString                := usua_nome;
  tq.ParamByName('k_usua_login').AsString               := usua_login;
  tq.ParamByName('k_usua_senha').AsString               := usua_senha;
  tq.ParamByName('k_usua_dataadmissao').AsDateTime      := usua_dataadmissao;
  tq.ParamByName('k_usua_datadesligamento').AsDateTime  := usua_datadesligamento;
  tq.ParamByName('k_usua_fone').AsString                := usua_fone;
  tq.ParamByName('k_usua_obs').AsString                 := usua_obs;
  tq.ParamByName('k_usua_ativo').AsInteger              := usua_ativo;
  tq.ParamByName('k_usua_email').AsString               := usua_email;
  tq.ParamByName('k_usua_email_nome').AsString          := usua_email_nome;
  tq.ParamByName('k_usua_email_usuario').AsString       := usua_email_usuario;
  tq.ParamByName('k_usua_email_senha').AsString         := usua_email_senha;
  tq.ParamByName('k_usua_caminho_imp_pdf').AsString     := usua_caminho_imp_pdf;
  tq.ParamByName('k_usua_caminho_imp_padrao').AsString  := usua_caminho_imp_padrao;
  tq.ParamByName('k_usua_alterarSenha').AsInteger       := usua_alterarSenha;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TUsuario.Recuperar(Pusua_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Usuario where usua_id = :K_usua_id');
  tq.ParamByName('k_usua_id').AsInteger := Pusua_id;
  tq.Open;
  if tq.Eof then
    Result := False
  else
    Result := True;

  usua_id                 := tq.FieldByName('usua_id').AsInteger;
  usua_nome               := tq.FieldByName('usua_nome').AsString;
  usua_login              := tq.FieldByName('usua_login').AsString;
  usua_senha              := tq.FieldByName('usua_senha').AsString;
  usua_dataadmissao       := tq.FieldByName('usua_dataadmissao').AsDateTime;
  usua_datadesligamento   := tq.FieldByName('usua_datadesligamento').AsDateTime;
  usua_fone               := tq.FieldByName('usua_fone').AsString;
  usua_obs                := tq.FieldByName('usua_obs').AsString;
  usua_ativo              := tq.FieldByName('usua_ativo').AsInteger;
  usua_email              := tq.FieldByName('usua_email').AsString;
  usua_email_nome         := tq.FieldByName('usua_email_nome').AsString;
  usua_email_usuario      := tq.FieldByName('usua_email_usuario').AsString;
  usua_email_senha        := tq.FieldByName('usua_email_senha').AsString;
  usua_caminho_imp_pdf    := tq.FieldByName('usua_caminho_imp_pdf').AsString;
  usua_caminho_imp_padrao := tq.FieldByName('usua_caminho_imp_padrao').AsString;
  usua_alterarSenha       := tq.FieldByName('usua_alterarSenha').AsInteger;

  tq.Close;
  tq.Free;
end;

function TUsuario.RecuperarPorLogin(PUsua_login: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Usuario where usua_login = :K_usua_login');
  tq.ParamByName('k_usua_login').AsString := PUsua_login;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('usua_id').AsInteger);

  tq.Close;
  tq.Free;
end;

function TUsuario.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(usua_id) maior from Usuario');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
