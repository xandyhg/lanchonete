inherited F_ConConfigEmail: TF_ConConfigEmail
  Caption = 'Consulta de Configura'#231#227'o de Email'
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel3: TPanel
    inherited JvDBGrid1: TJvDBGrid
      DataSource = DsConfigEmail
      OnDblClick = JvDBGrid1DblClick
      TitleRowHeight = 21
      Columns = <
        item
          Expanded = False
          FieldName = 'CONFE_ID'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          Title.Caption = 'C'#243'digo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CONFE_NOME'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          Title.Caption = 'Nome'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 127
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CONFE_AUTENTICACAO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'Autentica'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CONFE_PORTA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'Porta'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CONFE_SMTP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          Title.Caption = 'SMTP'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 144
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CONFE_ENDERECO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          Title.Caption = 'Endere'#231'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 136
          Visible = True
        end>
    end
  end
  object FDConfigEmail: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      'select * from configemail')
    Left = 384
    Top = 8
    object FDConfigEmailCONFE_ID: TIntegerField
      FieldName = 'CONFE_ID'
      Origin = 'CONFE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDConfigEmailCONFE_NOME: TStringField
      FieldName = 'CONFE_NOME'
      Origin = 'CONFE_NOME'
      Size = 150
    end
    object FDConfigEmailCONFE_AUTENTICACAO: TStringField
      FieldName = 'CONFE_AUTENTICACAO'
      Origin = 'CONFE_AUTENTICACAO'
      Size = 15
    end
    object FDConfigEmailCONFE_PORTA: TIntegerField
      FieldName = 'CONFE_PORTA'
      Origin = 'CONFE_PORTA'
    end
    object FDConfigEmailCONFE_SMTP: TStringField
      FieldName = 'CONFE_SMTP'
      Origin = 'CONFE_SMTP'
      Size = 255
    end
    object FDConfigEmailCONFE_ENDERECO: TStringField
      FieldName = 'CONFE_ENDERECO'
      Origin = 'CONFE_ENDERECO'
      Size = 100
    end
  end
  object ProviderConfigEmail: TDataSetProvider
    DataSet = FDConfigEmail
    Left = 416
    Top = 8
  end
  object CdsConfigEmail: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderConfigEmail'
    Left = 448
    Top = 8
    object CdsConfigEmailCONFE_ID: TIntegerField
      FieldName = 'CONFE_ID'
      Required = True
    end
    object CdsConfigEmailCONFE_NOME: TStringField
      FieldName = 'CONFE_NOME'
      Size = 150
    end
    object CdsConfigEmailCONFE_AUTENTICACAO: TStringField
      FieldName = 'CONFE_AUTENTICACAO'
      Size = 15
    end
    object CdsConfigEmailCONFE_PORTA: TIntegerField
      FieldName = 'CONFE_PORTA'
    end
    object CdsConfigEmailCONFE_SMTP: TStringField
      FieldName = 'CONFE_SMTP'
      Size = 255
    end
    object CdsConfigEmailCONFE_ENDERECO: TStringField
      FieldName = 'CONFE_ENDERECO'
      Size = 100
    end
  end
  object DsConfigEmail: TDataSource
    DataSet = CdsConfigEmail
    Left = 480
    Top = 8
  end
end
