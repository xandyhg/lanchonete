inherited F_CadEntidade: TF_CadEntidade
  Caption = ' CADASTRO DE ENTIDADE'
  ClientHeight = 504
  ClientWidth = 993
  OnActivate = FormActivate
  ExplicitWidth = 1009
  ExplicitHeight = 543
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 427
    Width = 993
    ExplicitTop = 427
    ExplicitWidth = 993
    inherited icoFechar: TImage
      Left = 903
      ExplicitLeft = 784
    end
    inherited icoAlterar: TImage
      Left = 723
      OnClick = icoAlterarClick
      ExplicitLeft = 604
    end
    inherited icoExcluir: TImage
      Left = 813
      OnClick = icoExcluirClick
      ExplicitLeft = 707
      ExplicitHeight = 51
    end
    inherited icoSalvar: TImage
      Left = 543
      OnClick = icoSalvarClick
      ExplicitLeft = 424
    end
    inherited icoCancelar: TImage
      Left = 633
      OnClick = icoCancelarClick
      ExplicitLeft = 514
    end
  end
  inherited Panel3: TPanel
    Width = 993
    Height = 386
    ExplicitWidth = 993
    ExplicitHeight = 386
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 991
      Height = 41
      Align = alTop
      TabOrder = 0
      object rgEnt_Tipo: TRadioGroup
        Left = 1
        Top = 1
        Width = 289
        Height = 39
        Align = alLeft
        Caption = 'Tipo'
        Columns = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ItemIndex = 0
        Items.Strings = (
          'Cliente'
          'Fornecedor'
          'Parceiro')
        ParentFont = False
        TabOrder = 0
        OnClick = rgEnt_TipoClick
      end
      object rgEnt_Tipo_Pessoa: TRadioGroup
        Left = 290
        Top = 1
        Width = 139
        Height = 39
        Align = alLeft
        Caption = ' Tipo Pessoa '
        Columns = 2
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ItemIndex = 0
        Items.Strings = (
          'F'#237'sica'
          'Jur'#237'dica')
        ParentFont = False
        TabOrder = 1
        OnClick = rgEnt_Tipo_PessoaClick
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 250
      Width = 991
      Height = 135
      Align = alBottom
      TabOrder = 2
      object GroupBox2: TGroupBox
        Left = 7
        Top = 6
        Width = 994
        Height = 122
        Caption = 'Dados do Endere'#231'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label18: TLabel
          Left = 27
          Top = 24
          Width = 22
          Height = 17
          Caption = 'CEP'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label19: TLabel
          Left = 141
          Top = 24
          Width = 70
          Height = 17
          Caption = 'Logradouro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label20: TLabel
          Left = 14
          Top = 55
          Width = 35
          Height = 17
          Caption = 'Bairro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label21: TLabel
          Left = 454
          Top = 55
          Width = 41
          Height = 17
          Caption = 'Cidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label22: TLabel
          Left = 825
          Top = 24
          Width = 16
          Height = 17
          Caption = 'N'#186
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label24: TLabel
          Left = 7
          Top = 86
          Width = 41
          Height = 17
          Caption = 'Compl.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object sbCidade: TSpeedButton
          Left = 563
          Top = 51
          Width = 25
          Height = 26
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000585751747069
            E1DFDFE4E3E2EBEAE9FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFA5A39B9090878D8A81FFFFFFFFFFFFEFEEEEEBEAE9F1
            F1F0F7F7F6F8F7F7F9F9F9FBFBFBFDFDFDFEFEFEFFFFFFFFFFFFFEFEFEA9A7A1
            8B8B8189867DFFFFFFFFFFFFF7F7F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFAA9A7A18B8B8189867DFFFFFFFFFFFFFE
            FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
            FBFBFAA9A7A18A8B8189867DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABA9A389897F6E6C66FF
            FFFFF9F9FBC2C4C7C3C6CAC2C4C6D4D4D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFF9F8F8D2D2CE5F5F579192949D988CCBA865CEA558D4B781C9C1
            B0A8AAABF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D3D6B0
            A382DCAA34D9AE43D7AB3FD7A02CDFA73EDDC599ABACACFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFCFCFE999588E2C366D7C46BD6C66FD6C36BD6BA5CD5A9
            3FDCA233D5C5A4C7C9CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D9BEB286DB
            CE78E1E2ACD5DA8FD5D78AD6CC7AD7BD61D5A537E2B057B4B6B7FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFDBDCDEBDB481E1E4A6E4E9BAEFF2D7E0E6B1D5DA8FD6C9
            74D6B34FE0A638ACA69AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFE1BCB78BEE
            F2CCECEFCFFCFDFAF6F7E8DFE5AED4CC76D5B654DEA42FABA69AFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFECECEDB1AE95F4F7D2F4F6E3F6F7E7F5F7E6EDF1D3E9E5
            B9E3CD86D7A949B8BCC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBEBEC8
            C8A0F9FCE2ECF0CFDAE1A2EAECC7EFE9C3F8E4B2A19A87EEEFF0FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFDFEFEB6B4B1B8B694F7F8D7E9E9AFDBCA66DECD
            999C9581D3D4D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFDCDCDDA6A49BA39F91A19C8EB6B7B7F0F1F2FFFFFFFFFFFF}
          OnClick = sbCidadeClick
        end
        object Label70: TLabel
          Left = 434
          Top = 86
          Width = 61
          Height = 17
          Caption = 'Refer'#234'ncia'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object edEnd_Logradouro: TEdit
          Left = 217
          Top = 21
          Width = 602
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 250
          ParentFont = False
          TabOrder = 1
        end
        object edEnd_Bairro: TEdit
          Left = 54
          Top = 52
          Width = 374
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 250
          ParentFont = False
          TabOrder = 3
        end
        object edEnd_Numero: TEdit
          Left = 847
          Top = 21
          Width = 52
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 2
        end
        object edEnd_Complemento: TEdit
          Left = 54
          Top = 83
          Width = 374
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 250
          ParentFont = False
          TabOrder = 7
        end
        object edCid_ID: TJvCalcEdit
          Left = 501
          Top = 52
          Width = 56
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ShowButton = False
          TabOrder = 4
          DecimalPlacesAlwaysShown = False
          OnExit = edCid_IDExit
          OnKeyDown = edCid_IDKeyDown
        end
        object edDescCidade: TEdit
          Left = 594
          Top = 52
          Width = 348
          Height = 25
          CharCase = ecUpperCase
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 250
          ParentFont = False
          TabOrder = 5
        end
        object edDescUF: TEdit
          Left = 945
          Top = 52
          Width = 32
          Height = 25
          CharCase = ecUpperCase
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 2
          ParentFont = False
          TabOrder = 6
        end
        object edEnd_Cep: TEdit
          Left = 55
          Top = 21
          Width = 79
          Height = 25
          MaxLength = 10
          TabOrder = 0
          OnEnter = edEnd_CepEnter
          OnExit = edEnd_CepExit
        end
        object edEnd_Referencia: TEdit
          Left = 501
          Top = 83
          Width = 476
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 250
          ParentFont = False
          TabOrder = 8
        end
      end
    end
    object Panel9: TPanel
      Left = 1
      Top = 42
      Width = 991
      Height = 208
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 1
        Top = 1
        Width = 352
        Height = 206
        ActivePage = TabSheet3
        Align = alLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object TabSheet3: TTabSheet
          Caption = 'Cliente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ImageIndex = 2
          ParentFont = False
          object Label12: TLabel
            Left = 223
            Top = 118
            Width = 34
            Height = 17
            Alignment = taRightJustify
            Caption = 'Limite'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
          end
          object rgEnt_Tipo_Cliente: TRadioGroup
            Left = 0
            Top = 0
            Width = 344
            Height = 38
            Align = alTop
            Caption = 'Tipo Cliente'
            Columns = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ItemIndex = 1
            Items.Strings = (
              'Externo'
              'Funcion'#225'rio'
              'Terceirizado')
            ParentFont = False
            TabOrder = 0
          end
          object rgEnt_liberado: TRadioGroup
            Left = 50
            Top = 106
            Width = 113
            Height = 34
            Caption = ' Liberado '
            Columns = 2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ItemIndex = 0
            Items.Strings = (
              'Sim'
              'N'#227'o')
            ParentFont = False
            TabOrder = 2
          end
          object edEnt_limite: TJvCalcEdit
            Left = 263
            Top = 115
            Width = 75
            Height = 25
            DisplayFormat = '#,##0.00'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            ShowButton = False
            TabOrder = 3
            DecimalPlacesAlwaysShown = False
          end
          object Panel10: TPanel
            Left = 0
            Top = 38
            Width = 344
            Height = 62
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object sbFuncao: TSpeedButton
              Left = 100
              Top = 36
              Width = 25
              Height = 26
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000585751747069
                E1DFDFE4E3E2EBEAE9FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFA5A39B9090878D8A81FFFFFFFFFFFFEFEEEEEBEAE9F1
                F1F0F7F7F6F8F7F7F9F9F9FBFBFBFDFDFDFEFEFEFFFFFFFFFFFFFEFEFEA9A7A1
                8B8B8189867DFFFFFFFFFFFFF7F7F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFAA9A7A18B8B8189867DFFFFFFFFFFFFFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
                FBFBFAA9A7A18A8B8189867DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABA9A389897F6E6C66FF
                FFFFF9F9FBC2C4C7C3C6CAC2C4C6D4D4D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFF9F8F8D2D2CE5F5F579192949D988CCBA865CEA558D4B781C9C1
                B0A8AAABF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D3D6B0
                A382DCAA34D9AE43D7AB3FD7A02CDFA73EDDC599ABACACFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFCFCFE999588E2C366D7C46BD6C66FD6C36BD6BA5CD5A9
                3FDCA233D5C5A4C7C9CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D9BEB286DB
                CE78E1E2ACD5DA8FD5D78AD6CC7AD7BD61D5A537E2B057B4B6B7FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFDBDCDEBDB481E1E4A6E4E9BAEFF2D7E0E6B1D5DA8FD6C9
                74D6B34FE0A638ACA69AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFE1BCB78BEE
                F2CCECEFCFFCFDFAF6F7E8DFE5AED4CC76D5B654DEA42FABA69AFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFECECEDB1AE95F4F7D2F4F6E3F6F7E7F5F7E6EDF1D3E9E5
                B9E3CD86D7A949B8BCC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBEBEC8
                C8A0F9FCE2ECF0CFDAE1A2EAECC7EFE9C3F8E4B2A19A87EEEFF0FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFDFEFEB6B4B1B8B694F7F8D7E9E9AFDBCA66DECD
                999C9581D3D4D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFDCDCDDA6A49BA39F91A19C8EB6B7B7F0F1F2FFFFFFFFFFFF}
              ParentFont = False
              OnClick = sbFuncaoClick
            end
            object Label13: TLabel
              Left = 3
              Top = 40
              Width = 41
              Height = 17
              Alignment = taRightJustify
              Caption = 'Fun'#231#227'o'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              ParentFont = False
            end
            object Label59: TLabel
              Left = 13
              Top = 9
              Width = 31
              Height = 17
              Alignment = taRightJustify
              Caption = 'Setor'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              ParentFont = False
            end
            object sbSetor: TSpeedButton
              Left = 101
              Top = 6
              Width = 25
              Height = 26
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000585751747069
                E1DFDFE4E3E2EBEAE9FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFA5A39B9090878D8A81FFFFFFFFFFFFEFEEEEEBEAE9F1
                F1F0F7F7F6F8F7F7F9F9F9FBFBFBFDFDFDFEFEFEFFFFFFFFFFFFFEFEFEA9A7A1
                8B8B8189867DFFFFFFFFFFFFF7F7F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFAA9A7A18B8B8189867DFFFFFFFFFFFFFE
                FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFF
                FBFBFAA9A7A18A8B8189867DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBABA9A389897F6E6C66FF
                FFFFF9F9FBC2C4C7C3C6CAC2C4C6D4D4D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFF9F8F8D2D2CE5F5F579192949D988CCBA865CEA558D4B781C9C1
                B0A8AAABF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD2D3D6B0
                A382DCAA34D9AE43D7AB3FD7A02CDFA73EDDC599ABACACFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFCFCFE999588E2C366D7C46BD6C66FD6C36BD6BA5CD5A9
                3FDCA233D5C5A4C7C9CAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D7D9BEB286DB
                CE78E1E2ACD5DA8FD5D78AD6CC7AD7BD61D5A537E2B057B4B6B7FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFDBDCDEBDB481E1E4A6E4E9BAEFF2D7E0E6B1D5DA8FD6C9
                74D6B34FE0A638ACA69AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFE1BCB78BEE
                F2CCECEFCFFCFDFAF6F7E8DFE5AED4CC76D5B654DEA42FABA69AFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFECECEDB1AE95F4F7D2F4F6E3F6F7E7F5F7E6EDF1D3E9E5
                B9E3CD86D7A949B8BCC1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBEBEC8
                C8A0F9FCE2ECF0CFDAE1A2EAECC7EFE9C3F8E4B2A19A87EEEFF0FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFDFEFEB6B4B1B8B694F7F8D7E9E9AFDBCA66DECD
                999C9581D3D4D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFDCDCDDA6A49BA39F91A19C8EB6B7B7F0F1F2FFFFFFFFFFFF}
              ParentFont = False
              OnClick = sbSetorClick
            end
            object edFun_id: TJvCalcEdit
              Left = 50
              Top = 37
              Width = 44
              Height = 25
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              ParentFont = False
              ShowButton = False
              TabOrder = 1
              DecimalPlacesAlwaysShown = False
              OnExit = edFun_idExit
              OnKeyDown = edFun_idKeyDown
            end
            object edFun_descricao: TEdit
              Left = 130
              Top = 37
              Width = 208
              Height = 25
              CharCase = ecUpperCase
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
            end
            object edSet_id: TJvCalcEdit
              Left = 50
              Top = 6
              Width = 44
              Height = 25
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              ParentFont = False
              ShowButton = False
              TabOrder = 0
              DecimalPlacesAlwaysShown = False
              OnExit = edSet_idExit
              OnKeyDown = edSet_idKeyDown
            end
            object edSet_Descricao: TEdit
              Left = 130
              Top = 6
              Width = 208
              Height = 25
              CharCase = ecUpperCase
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
            end
          end
        end
        object TabSheet1: TTabSheet
          Caption = 'Pessoa F'#237'sica'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          object Label4: TLabel
            Left = 7
            Top = 43
            Width = 63
            Height = 17
            Alignment = taRightJustify
            Caption = 'Data Nasc.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
          end
          object Label5: TLabel
            Left = 52
            Top = 74
            Width = 17
            Height = 17
            Alignment = taRightJustify
            Caption = 'RG'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 184
            Top = 74
            Width = 21
            Height = 17
            Alignment = taRightJustify
            Caption = 'CPF'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
          end
          object Label8: TLabel
            Left = 22
            Top = 12
            Width = 47
            Height = 17
            Alignment = taRightJustify
            Caption = 'Est. Civil'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
          end
          object edEnt_DataNasc: TJvDateEdit
            Left = 75
            Top = 40
            Width = 100
            Height = 25
            ShowNullDate = False
            TabOrder = 2
          end
          object edEnt_RG: TEdit
            Left = 75
            Top = 71
            Width = 102
            Height = 25
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object edEnt_CPF: TEdit
            Left = 211
            Top = 71
            Width = 102
            Height = 25
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            MaxLength = 11
            ParentFont = False
            TabOrder = 4
            OnEnter = edEnt_CPFEnter
            OnExit = edEnt_CPFExit
          end
          object rgEnt_Sexo: TRadioGroup
            Left = 231
            Top = 0
            Width = 82
            Height = 34
            Caption = ' Sexo '
            Columns = 2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ItemIndex = 1
            Items.Strings = (
              'M'
              'F')
            ParentFont = False
            TabOrder = 1
          end
          object edEnt_EstadoCivil: TComboBox
            Left = 75
            Top = 9
            Width = 150
            Height = 25
            CharCase = ecUpperCase
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            Items.Strings = (
              'SOLTEIRO(A)'
              'CASADO(A)'
              'DIVORCIADO(A)'
              'SEPARADO(A)'
              'V'#205'UVO(A)')
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Pessoa Jur'#237'dica'
          ImageIndex = 1
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 344
            Height = 174
            Align = alClient
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object Label36: TLabel
              Left = 21
              Top = 13
              Width = 29
              Height = 17
              Caption = 'CNPJ'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              ParentFont = False
            end
            object Label42: TLabel
              Left = 4
              Top = 44
              Width = 47
              Height = 17
              Caption = 'Fantasia'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              ParentFont = False
            end
            object edEnt_cnpj: TEdit
              Left = 57
              Top = 10
              Width = 137
              Height = 25
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              MaxLength = 14
              NumbersOnly = True
              ParentFont = False
              TabOrder = 0
              OnEnter = edEnt_cnpjEnter
              OnExit = edEnt_cnpjExit
            end
            object edEnt_nome_fantasia: TEdit
              Left = 57
              Top = 41
              Width = 280
              Height = 25
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Segoe UI'
              Font.Style = []
              MaxLength = 250
              ParentFont = False
              TabOrder = 1
            end
          end
        end
      end
      object Panel7: TPanel
        Left = 353
        Top = 1
        Width = 637
        Height = 206
        Align = alClient
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object Label2: TLabel
          Left = 63
          Top = 14
          Width = 36
          Height = 17
          Alignment = taRightJustify
          Caption = 'Nome'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 347
          Top = 45
          Width = 49
          Height = 17
          Alignment = taRightJustify
          Caption = 'Telefone'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 59
          Top = 45
          Width = 40
          Height = 17
          Alignment = taRightJustify
          Caption = 'Celular'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 68
          Top = 107
          Width = 31
          Height = 17
          Alignment = taRightJustify
          Caption = 'Email'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 53
          Top = 76
          Width = 46
          Height = 17
          Alignment = taRightJustify
          Caption = 'Contato'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label26: TLabel
          Left = 306
          Top = 76
          Width = 90
          Height = 17
          Alignment = taRightJustify
          Caption = 'Celular Contato'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object Label17: TLabel
          Left = 75
          Top = 140
          Width = 24
          Height = 17
          Alignment = taRightJustify
          Caption = 'Obs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
        end
        object edEnt_nome: TEdit
          Left = 105
          Top = 11
          Width = 407
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edEnt_Telefone: TEdit
          Left = 401
          Top = 42
          Width = 111
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 15
          NumbersOnly = True
          ParentFont = False
          TabOrder = 2
          OnEnter = edEnt_TelefoneEnter
          OnExit = edEnt_TelefoneExit
        end
        object edEnt_Celular: TEdit
          Left = 105
          Top = 42
          Width = 111
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 15
          NumbersOnly = True
          ParentFont = False
          TabOrder = 1
          OnEnter = edEnt_CelularEnter
          OnExit = edEnt_CelularExit
        end
        object edEnt_email: TEdit
          Left = 105
          Top = 104
          Width = 407
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 255
          ParentFont = False
          TabOrder = 5
          OnExit = edEnt_emailExit
        end
        object edEnt_Contato: TEdit
          Left = 105
          Top = 73
          Width = 184
          Height = 25
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 255
          ParentFont = False
          TabOrder = 3
        end
        object edEnt_Celular_contato: TEdit
          Left = 401
          Top = 73
          Width = 111
          Height = 25
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          MaxLength = 15
          NumbersOnly = True
          ParentFont = False
          TabOrder = 4
          OnEnter = edEnt_Celular_contatoEnter
          OnExit = edEnt_Celular_contatoExit
        end
        object edEnt_Obs: TMemo
          Left = 105
          Top = 135
          Width = 407
          Height = 60
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 6
        end
        object rgEnt_Ativo: TRadioGroup
          Left = 518
          Top = 154
          Width = 113
          Height = 41
          Caption = ' Ativo '
          Columns = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          ItemIndex = 0
          Items.Strings = (
            'Sim'
            'N'#227'o')
          ParentFont = False
          TabOrder = 7
        end
      end
    end
  end
  inherited Panel4: TPanel
    Top = 472
    Width = 993
    ExplicitTop = 472
    ExplicitWidth = 993
    inherited pnCancelar: TPanel
      Left = 633
      ExplicitLeft = 633
    end
    inherited pnAlterar: TPanel
      Left = 723
      ExplicitLeft = 723
    end
    inherited pnExcluir: TPanel
      Left = 813
      ExplicitLeft = 813
    end
    inherited pnSalvar: TPanel
      Left = 543
      ExplicitLeft = 543
    end
    inherited pnFechar: TPanel
      Left = 903
      ExplicitLeft = 903
    end
  end
  inherited Panel5: TPanel
    Width = 993
    ExplicitWidth = 993
    object Label1: TLabel
      Left = 7
      Top = 12
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 826
      Top = 12
      Width = 53
      Height = 17
      Alignment = taRightJustify
      Caption = 'Cadastro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 56
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object edEnt_DataCadastro: TJvDateEdit
      Left = 885
      Top = 9
      Width = 100
      Height = 25
      ShowNullDate = False
      TabOrder = 1
    end
  end
  object HTTPRIO1: THTTPRIO
    HTTPWebNode.UseUTF8InHeader = True
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    HTTPWebNode.WebNodeOptions = []
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 784
    Top = 8
  end
end
