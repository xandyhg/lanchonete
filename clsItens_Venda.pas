unit clsItens_Venda;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TItens_Venda = class
  public
    venda_id          : Integer;
    pro_id            : Integer;
    iv_qtde           : Double;
    iv_valor_unit     : Double;
    iv_valor_total    : Double;
    iv_status         : Integer; //0-Aberto 1-Liquidado 2-Parcial
    iv_valor_restante : Double; //Valor restante do item que falta pagar

    function  Recuperar(Pvenda_id,Ppro_id:Integer):Boolean;
    procedure Gravar(Pvenda_id,Ppro_id:Integer);
    procedure Eliminar(Pvenda_id,Ppro_id:Integer);
    procedure EliminarTodosItens(Pvenda_id:Integer);
    procedure AtualizaValorRestante(Pvenda_id,Ppro_id,Piv_status:Integer;PIV_valor_restante:Double);
  end;

implementation

{ TItens_Venda }

uses DMC;

procedure TItens_Venda.AtualizaValorRestante(Pvenda_id, Ppro_id, Piv_status: Integer;
  PIV_valor_restante: Double);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('update Itens_Venda set iv_valor_restante = :k_iv_valor_restante, iv_status = :k_iv_status '+
             'where venda_id = :K_venda_id and pro_id = :k_pro_id                                       ');
  tq.ParamByName('K_venda_id').AsInteger        := Pvenda_id;
  tq.ParamByName('K_pro_id').AsInteger          := Ppro_id;
  tq.ParamByName('K_iv_status').AsInteger       := Piv_status;
  tq.ParamByName('K_iv_valor_restante').AsFloat := PIV_valor_restante;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TItens_Venda.Eliminar(Pvenda_id,Ppro_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Itens_Venda where venda_id = :K_venda_id and pro_id = :k_pro_id');
  tq.ParamByName('K_venda_id').AsInteger := Pvenda_id;
  tq.ParamByName('K_pro_id').AsInteger   := Ppro_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TItens_Venda.EliminarTodosItens(Pvenda_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Itens_Venda where venda_id = :K_venda_id');
  tq.ParamByName('K_venda_id').AsInteger := Pvenda_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TItens_Venda.Gravar(Pvenda_id,Ppro_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Itens_Venda where venda_id = :k_venda_id and pro_id = :k_pro_id');
  tq.ParamByName('k_venda_id').AsInteger := Pvenda_id;
  tq.ParamByName('k_pro_id').AsInteger   := Ppro_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Itens_Venda values('+
    ':k_venda_id,'+
    ':k_pro_id,'+
    ':k_iv_qtde,'+
    ':k_iv_valor_unit,'+
    ':k_iv_valor_total,'+
    ':k_iv_status,'+
    ':k_iv_valor_restante)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Itens_Venda set '+
    'iv_qtde           = :k_iv_qtde,'+
    'iv_valor_unit     = :k_iv_valor_unit,'+
    'iv_valor_total    = :k_iv_valor_total,'+
    'iv_status         = :k_iv_status,'+
    'iv_valor_restante = :k_iv_valor_restante ');
    tq.SQL.Add('where venda_id = :K_venda_id and pro_id = :k_pro_id ');
  end;

  tq.ParamByName('k_venda_id').AsInteger        := Pvenda_id;
  tq.ParamByName('k_pro_id').AsInteger          := Ppro_id;
  tq.ParamByName('k_iv_qtde').AsFloat           := iv_qtde;
  tq.ParamByName('k_iv_valor_unit').AsFloat     := iv_valor_unit;
  tq.ParamByName('k_iv_valor_total').AsFloat    := iv_valor_total;
  tq.ParamByName('k_iv_status').AsInteger       := iv_status;
  tq.ParamByName('k_iv_valor_restante').AsFloat := iv_valor_restante;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TItens_Venda.Recuperar(Pvenda_id,Ppro_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Itens_Venda where venda_id = :K_venda_id and pro_id = :k_pro_id');
  tq.ParamByName('k_venda_id').AsInteger := Pvenda_id;
  tq.ParamByName('k_pro_id').AsInteger   := Ppro_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  venda_id          := tq.FieldByName('venda_id').AsInteger;
  pro_id            := tq.FieldByName('pro_id').AsInteger;
  iv_qtde           := tq.FieldByName('iv_qtde').AsFloat;
  iv_valor_unit     := tq.FieldByName('iv_valor_unit').AsFloat;
  iv_valor_total    := tq.FieldByName('iv_valor_total').AsFloat;
  iv_status         := tq.FieldByName('iv_status').AsInteger;
  iv_valor_restante := tq.FieldByName('iv_valor_restante').AsFloat;

  tq.Close;
  tq.Free;
end;

end.
