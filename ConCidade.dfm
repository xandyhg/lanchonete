inherited F_ConCidade: TF_ConCidade
  Caption = 'Consulta de Cidade'
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel3: TPanel
    inherited JvDBGrid1: TJvDBGrid
      DataSource = DsCidade
      OnDblClick = JvDBGrid1DblClick
      TitleRowHeight = 21
      Columns = <
        item
          Expanded = False
          FieldName = 'CID_ID'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          Title.Caption = 'C'#243'digo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CID_CIDADE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          Title.Caption = 'Cidade'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 536
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CID_UF'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Segoe UI'
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'UF'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 44
          Visible = True
        end>
    end
  end
  object FDCidade: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      'select *'
      'from cidade'
      'order by cid_cidade')
    Left = 384
    Top = 8
    object FDCidadeCID_ID: TIntegerField
      FieldName = 'CID_ID'
      Origin = 'CID_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDCidadeCID_CIDADE: TStringField
      FieldName = 'CID_CIDADE'
      Origin = 'CID_CIDADE'
      Size = 250
    end
    object FDCidadeCID_UF: TStringField
      FieldName = 'CID_UF'
      Origin = 'CID_UF'
      FixedChar = True
      Size = 2
    end
  end
  object ProviderCidade: TDataSetProvider
    DataSet = FDCidade
    Left = 416
    Top = 8
  end
  object CdsCidade: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderCidade'
    Left = 448
    Top = 8
    object CdsCidadeCID_ID: TIntegerField
      FieldName = 'CID_ID'
      Required = True
    end
    object CdsCidadeCID_CIDADE: TStringField
      FieldName = 'CID_CIDADE'
      Size = 250
    end
    object CdsCidadeCID_UF: TStringField
      FieldName = 'CID_UF'
      FixedChar = True
      Size = 2
    end
  end
  object DsCidade: TDataSource
    DataSet = CdsCidade
    Left = 480
    Top = 8
  end
end
