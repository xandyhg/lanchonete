unit clsCidade;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TCidade = class
  public
    cid_id     : Integer;
    cid_cidade : String;
    cid_uf     : String;

    function  Recuperar(Pcid_id:Integer):Boolean;
    procedure Gravar(Pcid_id:Integer);
    procedure Eliminar(Pcid_id:Integer);
    function  Ultima:Integer;
    function  RecuperarPorCidadeUF(PCid_Cidade, PCid_UF:String):Boolean;
  end;

implementation

{ TCidade }

uses DMC;

procedure TCidade.Eliminar(Pcid_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Cidade where cid_id = :K_cid_id');
  tq.ParamByName('K_cid_id').AsInteger := Pcid_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TCidade.Gravar(Pcid_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Cidade where cid_id = :k_cid_id');
  tq.ParamByName('k_cid_id').AsInteger := Pcid_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Cidade values('+
    ':k_cid_id,'+
    ':k_cid_cidade,'+
    ':k_cid_uf)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Cidade set '+
    'cid_cidade = :k_cid_cidade,'+
    'cid_uf     = :k_cid_uf ');
    tq.SQL.Add('where cid_id = :K_cid_id ');
  end;

  tq.ParamByName('k_cid_id').AsInteger    := Pcid_id;
  tq.ParamByName('k_cid_cidade').AsString := cid_cidade;
  tq.ParamByName('k_cid_uf').AsString     := cid_uf;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TCidade.Recuperar(Pcid_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Cidade where cid_id = :K_cid_id');
  tq.ParamByName('k_cid_id').AsInteger := Pcid_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  cid_id     := tq.FieldByName('cid_id').AsInteger;
  cid_cidade := tq.FieldByName('cid_cidade').AsString;
  cid_uf     := tq.FieldByName('cid_uf').AsString;

  tq.Close;
  tq.Free;
end;

function TCidade.RecuperarPorCidadeUF(PCid_Cidade, PCid_UF: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Cidade where Cid_Cidade = :K_Cid_Cidade and Cid_UF = :k_Cid_UF');
  tq.ParamByName('k_Cid_Cidade').AsString := PCid_Cidade;
  tq.ParamByName('k_Cid_UF').AsString     := PCid_UF;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('Cid_ID').AsInteger);

  tq.Close;
  tq.Free;
end;

function TCidade.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(cid_id) maior from Cidade');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
