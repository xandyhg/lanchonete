inherited F_ConCompra: TF_ConCompra
  Caption = 'Consulta de Nota de Compra'
  ClientHeight = 524
  ClientWidth = 1155
  ExplicitWidth = 1171
  ExplicitHeight = 563
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 411
    Width = 1155
    ExplicitTop = 411
    ExplicitWidth = 1155
    inherited icoPesquisar: TImage
      Left = 735
      ExplicitLeft = 750
    end
    inherited icoNovo: TImage
      Left = 840
      ExplicitLeft = 855
    end
    inherited icoImprimir: TImage
      Left = 945
      ExplicitLeft = 960
    end
    inherited icoFechar: TImage
      Left = 1050
      ExplicitLeft = 1065
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 488
    Width = 1155
    ExplicitTop = 488
    ExplicitWidth = 1155
  end
  inherited Panel3: TPanel
    Width = 1155
    Height = 370
    ExplicitWidth = 1155
    ExplicitHeight = 359
    inherited JvDBGrid1: TJvDBGrid
      Width = 1155
      Height = 370
      DataSource = DsCompra
      OnDblClick = JvDBGrid1DblClick
      TitleRowHeight = 21
      Columns = <
        item
          Expanded = False
          FieldName = 'COM_ID'
          Title.Caption = 'C'#243'd. Compra'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_ID'
          Title.Caption = 'C'#243'd. Fornecedor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'ENT_CNPJ'
          Title.Alignment = taCenter
          Title.Caption = 'CNPJ'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_NOME_FANTASIA'
          Title.Caption = 'Nome Fantasia'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 273
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_NOME'
          Title.Caption = 'Raz'#227'o Social'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 294
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COM_NF'
          Title.Caption = 'N'#186' NF'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COM_VALORTOTAL'
          Title.Caption = 'Total Nota'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 85
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'COM_DATACADASTRO'
          Title.Alignment = taCenter
          Title.Caption = 'Data Cadastro'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 104
          Visible = True
        end>
    end
  end
  inherited Panel4: TPanel
    Top = 456
    Width = 1155
    ExplicitTop = 456
    ExplicitWidth = 1155
    inherited pnPesquisar: TPanel
      Left = 735
      ExplicitLeft = 735
    end
    inherited pnNovo: TPanel
      Left = 840
      ExplicitLeft = 840
    end
    inherited pnImprimir: TPanel
      Left = 945
      ExplicitLeft = 945
    end
    inherited pnFechar: TPanel
      Left = 1050
      ExplicitLeft = 1050
    end
  end
  inherited Panel1: TPanel
    Width = 1155
    ExplicitWidth = 1155
  end
  object FDCompra: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select c.com_id, c.ent_id, e.ent_cnpj, e.ent_nome_fantasia, e.en' +
        't_nome,'
      'c.com_nf, c.com_valortotal, c.com_datacadastro'
      'from compra c'
      'left outer join entidade e on c.ent_id=e.ent_id'
      'order by c.com_datacadastro, e.ent_nome_fantasia')
    Left = 264
    Top = 8
    object FDCompraCOM_ID: TIntegerField
      FieldName = 'COM_ID'
      Origin = 'COM_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDCompraENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
    end
    object FDCompraENT_CNPJ: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ENT_CNPJ'
      Origin = 'ENT_CNPJ'
      ProviderFlags = []
      ReadOnly = True
      Size = 18
    end
    object FDCompraENT_NOME_FANTASIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ENT_NOME_FANTASIA'
      Origin = 'ENT_NOME_FANTASIA'
      ProviderFlags = []
      ReadOnly = True
      Size = 255
    end
    object FDCompraENT_NOME: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ENT_NOME'
      Origin = 'ENT_NOME'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
    object FDCompraCOM_NF: TStringField
      FieldName = 'COM_NF'
      Origin = 'COM_NF'
      Size = 11
    end
    object FDCompraCOM_VALORTOTAL: TFloatField
      FieldName = 'COM_VALORTOTAL'
      Origin = 'COM_VALORTOTAL'
    end
    object FDCompraCOM_DATACADASTRO: TSQLTimeStampField
      FieldName = 'COM_DATACADASTRO'
      Origin = 'COM_DATACADASTRO'
    end
  end
  object ProviderCompra: TDataSetProvider
    DataSet = FDCompra
    Left = 296
    Top = 8
  end
  object CdsCompra: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderCompra'
    Left = 328
    Top = 8
    object CdsCompraCOM_ID: TIntegerField
      FieldName = 'COM_ID'
      Required = True
    end
    object CdsCompraENT_ID: TIntegerField
      FieldName = 'ENT_ID'
    end
    object CdsCompraENT_CNPJ: TStringField
      FieldName = 'ENT_CNPJ'
      ReadOnly = True
      Size = 18
    end
    object CdsCompraENT_NOME_FANTASIA: TStringField
      FieldName = 'ENT_NOME_FANTASIA'
      ReadOnly = True
      Size = 255
    end
    object CdsCompraENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      ReadOnly = True
      Size = 250
    end
    object CdsCompraCOM_NF: TStringField
      FieldName = 'COM_NF'
      Size = 11
    end
    object CdsCompraCOM_VALORTOTAL: TFloatField
      FieldName = 'COM_VALORTOTAL'
      DisplayFormat = '#,##0.00'
    end
    object CdsCompraCOM_DATACADASTRO: TSQLTimeStampField
      FieldName = 'COM_DATACADASTRO'
    end
  end
  object DsCompra: TDataSource
    DataSet = CdsCompra
    Left = 360
    Top = 8
  end
end
