unit Configuracoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, JvExGrids, JvStringGrid,
  Vcl.ComCtrls, Vcl.Imaging.pngimage, Vcl.ExtCtrls, FireDAC.Comp.Client,
  Vcl.StdCtrls, Vcl.CheckLst, Vcl.Buttons, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits;

type
  TF_Configuracoes = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel2: TPanel;
    icoFechar: TImage;
    icoSalvar: TImage;
    Panel4: TPanel;
    pnSalvar: TPanel;
    pnFechar: TPanel;
    StatusBar1: TStatusBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Label1: TLabel;
    Panel5: TPanel;
    GroupBox1: TGroupBox;
    ckUsuarios: TCheckListBox;
    ckTodos: TCheckBox;
    GroupBox2: TGroupBox;
    edPro_id: TJvCalcEdit;
    sbProduto: TSpeedButton;
    edPro_Descricao: TEdit;
    rgConf_lanca_total_nota: TRadioGroup;
    TabSheet4: TTabSheet;
    edPercVenda: TJvCalcEdit;
    Label9: TLabel;
    TabSheet5: TTabSheet;
    GroupBox3: TGroupBox;
    sbEntidade: TSpeedButton;
    edEnt_id: TJvCalcEdit;
    edEnt_Nome: TEdit;
    procedure icoFecharClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ckTodosClick(Sender: TObject);
    procedure edPro_idExit(Sender: TObject);
    procedure edPro_idKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbProdutoClick(Sender: TObject);
    procedure sbEntidadeClick(Sender: TObject);
    procedure edEnt_idExit(Sender: TObject);
    procedure edEnt_idKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ListarUsuarios;
  end;

var
  F_Configuracoes: TF_Configuracoes;

implementation

{$R *.dfm}

uses clsConfiguracoes, X, DMC, U_Principal, clsProduto, ConProduto, clsEntidade,
  ConEntidade;

var
  Config   : TConfiguracoes;
  tq       : TFDQuery;
  Produto  : TProduto;
  Entidade : TEntidade;

procedure TF_Configuracoes.ckTodosClick(Sender: TObject);
begin
  if ckTodos.Checked then
    ckUsuarios.CheckAll(cbChecked)
  else
    ckUsuarios.CheckAll(cbUnchecked);
end;

procedure TF_Configuracoes.edEnt_idExit(Sender: TObject);
begin
  if edEnt_id.AsInteger > 0 then begin
    Entidade := TEntidade.Create;
    if Entidade.Recuperar(edEnt_id.AsInteger) then begin
      if Entidade.ent_tipo <> 0 then begin
        msgPerson('C�digo informado n�o � de cliente!',[mbok],'Entidade',mtError);
        edEnt_id.SetFocus;
        edEnt_id.SelectAll;
        Exit;
      end;
      edEnt_Nome.Text := Entidade.ent_nome;
    end else begin
      msgPerson('Cliente n�o cadastrado!',[mbok],'Entidade',mtError);
      edEnt_id.SetFocus;
      edEnt_id.SelectAll;
    end;
    FreeAndNil(Entidade);
  end else begin
    edEnt_Nome.Clear;
  end;
end;

procedure TF_Configuracoes.edEnt_idKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 117 then
    sbEntidadeClick(Sender);
end;

procedure TF_Configuracoes.edPro_idExit(Sender: TObject);
begin
  if edPro_id.AsInteger > 0 then begin
    Produto := TProduto.Create;
    if Produto.Recuperar(edPro_id.AsInteger) then begin
      if Produto.pro_ativo = 1 then begin
        msgPerson('Produto est� inativo!',[mbok],'Produto',mtError);
        edPro_id.SetFocus;
        edPro_id.SelectAll;
        Exit;
      end;
      edPro_Descricao.Text := Produto.pro_descricao;
      edPro_id.SetFocus;
    end else begin
      msgPerson('Produto n�o cadastrado!',[mbok],'Produto',mtError);
      edPro_id.SetFocus;
      edPro_id.SelectAll;
    end;
    FreeAndNil(Produto);
  end else begin
    edPro_Descricao.Clear;
  end;
end;

procedure TF_Configuracoes.edPro_idKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 117 then
    sbProdutoClick(Sender);
end;

procedure TF_Configuracoes.FormActivate(Sender: TObject);
begin
//  ListarUsuarios;

  Config := TConfiguracoes.Create;
  Config.Recuperar;

  //Aba Geral

  //Aba Compra
  edPro_id.AsInteger                := Config.pro_id;
  edPro_idExit(Sender);
  rgConf_lanca_total_nota.ItemIndex := Config.conf_lanca_total_nota;

  //Aba Financeiro

  //Aba Produto
  edPercVenda.Value                 := Config.conf_perc_venda;

  //Aba Venda
  edEnt_id.AsInteger                := Config.ent_id;
  edEnt_idExit(Sender);

  FreeAndNil(Config);
end;

procedure TF_Configuracoes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F3) and (icoSalvar.Visible = True) then
    icoSalvar.OnClick(Self)
  else if (Key = VK_ESCAPE) and (icoFechar.Visible = True) then
    icoFechar.OnClick(Self);
end;

procedure TF_Configuracoes.icoFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TF_Configuracoes.icoSalvarClick(Sender: TObject);
begin
  Try
    Config := TConfiguracoes.Create;
    DM.FDCon.StartTransaction;

    //Aba Geral

    //Aba Compra
    Config.pro_id                := edPro_id.AsInteger;
    Config.conf_lanca_total_nota := rgConf_lanca_total_nota.ItemIndex;

    //Aba Financeiro

    //Aba Produto
    Config.conf_perc_venda       := edPercVenda.Value;

    //Aba Venda
    Config.ent_id                := edEnt_id.AsInteger;

    Config.Gravar;
    msgPerson('Configura��es gravadas com sucesso!',[mbok],'Configura��es do Sistema',mtConfirmation);

    icoFecharClick(Sender);

    FreeAndNil(Config);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

procedure TF_Configuracoes.ListarUsuarios;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.Close;
  tq.Params.Clear;
  tq.SQL.Clear;
  tq.SQL.Add('select usua_id, usua_login '+
             'from usuario               '+
             'where usua_ativo=0         '+
             'order by usua_login        ');
  tq.Open;

  ckUsuarios.Items.Clear;
  while not tq.Eof do begin
    ckUsuarios.Items.Add(ZeroEsquerda(IntToStr(tq.FieldByName('usua_id').AsInteger),2)+'-'+tq.FieldByName('usua_login').AsString);
    tq.Next;
  end;
  ckUsuarios.ItemIndex := 0;
  tq.Close;
  tq.Free;
end;

procedure TF_Configuracoes.sbProdutoClick(Sender: TObject);
begin
  AbreForm(TF_ConProduto,Self);
  edPro_id.AsInteger := xAux_ConProduto_Pro_ID;
  edPro_idExit(Sender);
  edPro_id.SetFocus;
end;

procedure TF_Configuracoes.sbEntidadeClick(Sender: TObject);
begin
  xAux_ConEntidade_Ent_Tipo := 0;
  xAux_ConEntidade_Tela     := 'VENDA';
  AbreForm(TF_ConEntidade,Self);
  xAux_ConEntidade_Ent_Tipo := -1;
  xAux_ConEntidade_Tela     := '';
  edEnt_id.AsInteger := xAux_ConEntidade_Ent_ID;
  edEnt_idExit(Sender);
end;

end.
