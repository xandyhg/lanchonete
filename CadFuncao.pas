unit CadFuncao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits, FireDAC.Comp.Client;

type
  TF_CadFuncao = class(TF_CadPadrao)
    Label1: TLabel;
    edCodigo: TJvCalcEdit;
    Label2: TLabel;
    edFun_descricao: TEdit;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadFuncao: TF_CadFuncao;

implementation

{$R *.dfm}

uses DMC, clsFuncao, X, ConFuncao, U_Principal;

var
  Funcao : TFuncao;

procedure TF_CadFuncao.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_ConFuncao_Fun_ID > 0 then begin
    Funcao := TFuncao.Create;
    if Funcao.Recuperar(xAux_ConFuncao_Fun_ID) then begin
      edCodigo.AsInteger      := xAux_ConFuncao_Fun_ID;
      edFun_descricao.Text    := Funcao.fun_descricao;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(Funcao);
  end else begin
    edFun_descricao.Clear;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadFuncao.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadFuncao.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConFuncao_Fun_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadFuncao.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Fun��o',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    Funcao := TFuncao.Create;

    Funcao.Eliminar(edCodigo.AsInteger);

//    xAux_AuxJustificativa_Codigo   := edCodigo.AsInteger;
//    xAux_AuxJustificativa_NomeForm := UpperCase(Copy(Name,6,Length(Name)));
//    xAux_AuxJustificativa_Descricao:= edConfe_nome.Text;
//    AbreForm(TF_AuxJustificativa,Self);
//    xAux_AuxJustificativa_Codigo   := 0;
//    xAux_AuxJustificativa_NomeForm := '';
//    xAux_AuxJustificativa_Descricao:= '';

    msgPerson('Fun��o eliminada com sucesso!',[mbok],'Fun��o',mtConfirmation);

    FreeAndNil(Funcao);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadFuncao.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    Funcao := TFuncao.Create;

    Funcao.fun_descricao := edFun_descricao.Text;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConFuncao_Fun_ID     := Funcao.Ultima;
      Funcao.Gravar(xAux_ConFuncao_Fun_ID);
    end else begin
      Funcao.Gravar(xAux_ConFuncao_Fun_ID);
    end;

    FreeAndNil(Funcao);

//    if edCodigo.AsInteger = 0 then begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 1;
//      log.log_tabela      := 'CONFIG EMAIL';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'GRAVADO C�DIGO: '+IntToStr(xAux_ConConfEmail_Confe_ID)+' - '+edConfe_nome.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end else begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 2;
//      log.log_tabela      := 'CONFIG EMAIL';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'ALTERADO C�DIGO: '+IntToStr(xAux_ConConfEmail_Confe_ID)+' - '+edConfe_nome.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end;

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Fun��o '+IntToStr(xAux_ConFuncao_Fun_ID)+' cadastrada com sucesso!',[mbOK], 'Fun��o', mtConfirmation)
    else
      msgPerson('Fun��o '+IntToStr(xAux_ConFuncao_Fun_ID)+' alterada com sucesso!',[mbOK], 'Fun��o', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

end.
