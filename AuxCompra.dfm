inherited F_AuxCompra: TF_AuxCompra
  Caption = ' LAN'#199'AMENTO TOTAL DA NF'
  ClientHeight = 238
  ClientWidth = 480
  ExplicitWidth = 496
  ExplicitHeight = 277
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    Top = 161
    Width = 480
    ExplicitTop = 161
    ExplicitWidth = 480
    inherited icoFechar: TImage
      Left = 390
      Visible = False
      ExplicitLeft = 390
    end
    inherited icoSalvar: TImage
      Left = 210
      OnClick = icoSalvarClick
      ExplicitLeft = 210
    end
    inherited icoCancelar: TImage
      Left = 300
      OnClick = icoCancelarClick
      ExplicitLeft = 300
    end
  end
  inherited Panel3: TPanel
    Width = 480
    Height = 161
    ExplicitWidth = 480
    ExplicitHeight = 161
    object Label4: TLabel
      Left = 11
      Top = 11
      Width = 62
      Height = 17
      Alignment = taRightJustify
      Caption = 'Valor Total'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edIC_valor_total: TJvCalcEdit
      Left = 79
      Top = 8
      Width = 74
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 39
      Width = 480
      Height = 122
      Align = alBottom
      Caption = 'Observa'#231#245'es'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      TabStop = True
      object mCom_obs: TMemo
        Left = 2
        Top = 19
        Width = 476
        Height = 101
        Align = alClient
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  inherited Panel4: TPanel
    Top = 206
    Width = 480
    ExplicitTop = 206
    ExplicitWidth = 480
    inherited pnCancelar: TPanel
      Left = 300
      ExplicitLeft = 300
    end
    inherited pnSalvar: TPanel
      Left = 210
      ExplicitLeft = 210
    end
    inherited pnFechar: TPanel
      Left = 390
      Visible = False
      ExplicitLeft = 390
    end
  end
  inherited Timer1: TTimer
    Left = 387
  end
end
