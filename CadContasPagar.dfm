inherited F_CadContasPagar: TF_CadContasPagar
  Caption = ' LAN'#199'AMENTO DE CONTAS A PAGAR'
  ClientHeight = 295
  ClientWidth = 693
  OnActivate = FormActivate
  ExplicitWidth = 709
  ExplicitHeight = 334
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 218
    Width = 693
    ExplicitTop = 218
    ExplicitWidth = 693
    inherited icoFechar: TImage
      Left = 603
      ExplicitLeft = 603
    end
    inherited icoAlterar: TImage
      Left = 423
      OnClick = icoAlterarClick
      ExplicitLeft = 423
    end
    inherited icoExcluir: TImage
      Left = 513
      OnClick = icoExcluirClick
      ExplicitLeft = 513
    end
    inherited icoSalvar: TImage
      Left = 243
      OnClick = icoSalvarClick
      ExplicitLeft = 243
    end
    inherited icoCancelar: TImage
      Left = 333
      OnClick = icoCancelarClick
      ExplicitLeft = 333
    end
    object icoEstornarNota: TImage
      Left = 0
      Top = 0
      Width = 97
      Height = 45
      Cursor = crHandPoint
      Align = alLeft
      Center = True
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000200000
        00200806000000737A7AF40000000467414D410000B18F0BFC61050000000662
        4B474400FF00FF00FFA0BDA7930000000774494D4507E6020D0F0D3B8B9A09D6
        00000BC84944415478DA8D570B705C6515FEEE63F7EEFB99C7E6D1A4AFD436A1
        2D2D8FFAA8521551470406F1010C033A3C065128054A754A5BA83C65444146A9
        8C383E5A1C54A8AD03152AA536A484D63E92749BE6D164B3BB4936FBBCBB7BEF
        DDBD2FCFDDB4B40CEAF84FFEC9CDE6FFEFF9CE39DFF9CE5906E7ADE36F5D8599
        F1DDE858F514E674DD87B7B67FC153D5035D9393D92B92C9EC17754D5FCAF3AC
        CFD055389CBCC87346D4E960FFE672EA7FB9E5C181815F3FB104DFD910C5CBCF
        76E25B779FC0FFB398B30FC3BDEBA0560B58FCA91791883ED3E1F2B65C5B5172
        9797C4A965C542A64E96AB6CB9AC223E914132914651CC43B033B0DB600802C6
        9C0E73BBD78B5F954A6C2C10A842554DDCB2FED4FF0760B0FB0E703637D2E3BB
        6C1DAB1EBDA95A553664A7DEEB900B0330F572ED14CB98603932E8F0C0401063
        311DC78E4D422E97E17262168C601CA1E74DCDCD27FE9648CC370DC3C41D0F9D
        FEDF008EEEB902362184CCF85FF8CE35DBEF9F494D6C8A0FED747ABD6E34B47E
        1AEEE06270BC8B805420972650CC1E855CE887DDAE025C1D0E1F2963742403B7
        CB84C3C1C0C61B598AC896AA9AFF25C15219427FE7E6FF0E82F9FD0F801B1F33
        C9FB1D3724E2A96D89A157DC1D175C8EE605D7C1E6A83B13247376D38F611010
        7114A9D86E8833FBE1F638713AC6E3D0A1143C04C2E56408B029133F1E2929C5
        A76D9CA09974F1AECD63FF1940ACEF710802DF9E2F37EC8E1EDE7EC1052B3E83
        9645D7D3BF5898860AD334E814F9C1B004853D738B81A1575148F52039F45BF2
        5AC44CD685EEEE14027ED440F09C59364CFDBE6B3E7EF10BAFBCDB0B595671EF
        A3B18F02304D13D3A3DBEFEB3FD2F3B4D75546E7A5DF035D84AE960840751680
        7590B581E39CC4150F7141F88042B23884F8E0AF003D8EA919070E1E4CA3A991
        85838EB09C39AD54B41B59067B594AE1B71F8A7F14C0D4C03A7F4159B473B8FF
        B5CB965E7C15DCBE79D0B472CDE373CB0ABF7906084F9C0980B7FBE90C477FB3
        A896C73011FD050C750AA746399C8C66D1D6CA134FAC94193DF962E53ABA9A94
        640D0F3C99F83080D4F0F3AB2626A67695B3C7EB172DFF3AE5CF8E72210AC1DD
        411EF01F940A45FDCCE6A14829B8BC4DB03B1B6B91A9812845313EF002A54646
        F74105155942738403CB1A90146DEB2D0F9EDEF4E30DAD108B1AB63E3F755E04
        865EB865F4D4A16D82DDB435B75F4AE55684543801C1F769F036810C32B54D36
        C8909D2A611AA3FD7FC282A557C3179C0BD6169C05412F2BA5F7E1F4C00E5435
        27DEDC9B464BC480D3A143D38D78AEA05D49AF399615753CFCDCF4F9009EDB32
        12EDDE1C08CD415D430394E211E280935EDC0A976F7E0D84F57643ABA0981B45
        36B91FE999192CFFE4ED04A09992C383E18879940E5397903CF51BE4D327706A
        C486C1C114DA5B8943C429A98267EEDACFACDB2810BF068127C653B300A6877E
        F2CCE9C1036BEB224BE10B0850C4A3F4B1405E0450D7F271A815910C8FA02A17
        A050AE55258D4C4EC7C7967D0B5E5F23040F458975D2769F21651423C75FA247
        01BBDFC822E029C143FF52558CA58F1ADFDCBC3BDC7BCFDC34E8069E2410CCF4
        A9A77E161BE9B9BBB17525914687A57E2CE726B5F3A3BE7515558344394F5344
        02C8257B881F03880D5510E1BE0CBFA70EEE4E3B7CAD56045C042E5323677CF0
        8F746E042747EC3831904273A309CF900D917E737C58AE3C728F917D692D1730
        093A98C9E896C75393031BEA9BBAA8CC6C28CC1CA9E59AE14288B45D52F36A96
        FF36129F3DC88C45614C5E8485EDABC1A855A4D331D8970B08B485A096FA61F2
        0B2117C7113BB913E00378EDAF692C91395C38E185BBCA2059CA178E55CA0FDC
        674CBFB896AB3799E48987D66667869E0ED5CFE50C93FA41B2970867238F1BD1
        3A6F458DE196163014D244F41D48436ECC9B731991A2025D22AD2896912D4EC1
        BE92BC6CCCA3520DD15D3F4EF7EF20423318D9CD60D1B00B3E6A4E0CF51346D7
        3021E6F2C714E9813F33CA4BCCC4F1F55797C4F87687D3EBE28536C4467AA914
        1D358F172C59059EE76A000CC38EC481145AC39D1413B566DC90CA3064DA6219
        E922A9DC8A02FCED11681499A9D849140EE4D132D804A15021A1D2C0594DCD92
        175DC750369B3A52295FCB0CBDF7DD2E5D2BBEAE6BD29C60C34A4C8C1E262122
        2E28762C5CB2021E6A4AA6A1D34D17A60FC7E0AFB6C1E5721380620D802E9F05
        2163BA3C05E79A2AE0E591DE5786FF5FD41B445253459E354E9BA3A828D50A06
        F3B99EBEAA7433F3FEEBDFF4F803CE570BB9C4E50D2D17213E364ABA5D41A552
        A5145C88A6A61079AF9304FB512D0E60FAFD04C2C627E114ECD0CE803048744C
        45855152912235943D0A82232E38C40A8C420E1CCE334EEF1D2CE47AFBAAE55B
        E780EF63F4EC1318198C6ECCA5E35BC30D6D649C456A2A49B96420B81AB0A4AB
        A31601D6E6056F123852C9E89B3CDAD98BE073F1B311502C2F359815AAF1220D
        2325093C39A14F4F83A5F49D6F3C2AE68EF457CA3737317CDF6DFC0C98BE7D37
        103E6315D53BA5A11A74FBE7613A99A0BAD550921CE85AB6044E273522DE0381
        8FC3A88EE21F7BA7E0CECCC562B61D0E8E253E52D754740260C0ACD2168B5027
        26C01217CE1AB7227A1879F43756B6368AFCA6B59309ACD74360F6EE580DCDB0
        3B9B9A3C2FE732A9AB1A22F3E8B089582C07ADAA606E47177C3E5246CE85802F
        0D538BE3C0812904431DE0C73904E26EB8492D8DCA2C00483234326E968AB5BC
        9F357E48CF21BA5887E2D6B678ECC2C3F2EB653C6509D1F07BB7136B739024E9
        DAAA22FE41537547634B27B2191189781AF3172D2681B2220034358814AC3CDE
        EF4D4070B623100822DD9D415DCC0F8F9DC8AA50D8272761A453B5C9C132AE92
        041E94B2387901A531C4A02C29F7B89DF6677FF4C2C4AC14BFB6AD0B2EB71FAA
        C67A220DF61DF95CEE4A9F3F0C4F600181D2E074D94151A60143A71E6FCD7F2A
        0DA51924923419862228F6CAA88F05E1B53B6164D2D0E271B01667C8B81501B5
        AAA25BCA6170A90A5BD854C452E51A8E63F63CFD52EADC507AECAD6B280A252A
        BDCA6759A8AFE473A57063731B9CEE665883A5A503D6E0626D8ED5698232512C
        7298DA97406BBA1D7E3B0DAAC502B4F131308AF281F1D9FC5356A8420E9A0462
        817C72D2297F9E959964F03DDB6C2FB000EC787631BD9CC16F5F1E611E5ABFF2
        918AAC6C5488C5F591361A3EEA483C34D2866A4D8CACE184D204B9CF407DB211
        3EDE09934A518BC560E673B5B0B3E719AF3DD3AD028D6487CBF9E1A8247D3D0C
        FBD18DB671DCA9369CFB5EF0EAB66534DB59A5A285230D8EDF8805F94AEB0B48
        30DC04AF3F442990502586974B2CE40101919936F86C348F93C7FA64B2B6AD7A
        978970325550A3D74977980F0C58BF7304E248AED81BAD946E0D41E87BDC75FA
        1C006BED7FE51232A2A12CAB8B833EDBEF0A79E96295542BD2DC4091B046730D
        C53E0722994E32EEAA1937B294F7B131B09A4A29D470CA9345B14EC3FC511F16
        783CD44BCE99B09EB234961DCE167B072BC55B838CA3EF4300B63D3A1FB7FD70
        043DAF5E82998C7271C0CBBF288AF2720B94DBCD411BADC35CE91204042F959C
        4C8D48A4CF46C1944BD6D885214F0EF2E728557E2FFCF13684F74D61AED339AB
        FFE781C81088DE6CB167B02ADEFC2100D67AFEE1B9D8F3761C1BD7ADC0F84469
        793868FFA95AD5D7E47224B7FDF558E95D4D25C7C32405B43C37D3E91AC9A22E
        324E7D80ADF3A073F917208933D8FFDC41E313D35EF3630137773610D6AFB26A
        E0DDB4381955C4AF7D0480B57EBAB10D8F3D1BC3EF7EBE0CF164B9A5A5C9BDC1
        21F03717A6752F8E87B1885D084F49844EC42B490A8ED94847D630882C69C3BC
        852B21517BDEB5BB67E29F07733B574E3A7D5F0DD55FBF38E8B259E55C261E1D
        CA96D22764F19E376CF11DFF1180B59E5CDF8A2F7DB6197DD13CF676276D5FBA
        ACE58A50D8759729D9574B6FC3DB366CC246353ED6C0C1F58D3908CDABAF9138
        363E58D8F5C6C9EE9D6F66F62467D4611BC3C80F36B57EE5F250E8CE1697E03A
        962FA5FA65F1DECDEAFDDBD7093FC27F0560AD4DDF8FA021EC8035CF5FD815C4
        3B3D495FE792E01A25615C6DEED23FE7B073CDFE9BC27C707E4029E4C5E9C1E1
        54DF1BEFCCECFDFB81D261BA6E298D68EDD561C1BCCEDDB2C5C3F1B74FEAD286
        8D95ADDBD6391E809D0AF4DF8EAB15D1C6ED5ACA000000257445587464617465
        3A63726561746500323032322D30322D31335431353A31333A35322B30303A30
        308DF0FFD90000002574455874646174653A6D6F6469667900323032322D3032
        2D31335431353A31333A35322B30303A3030FCAD47650000000049454E44AE42
        6082}
      Visible = False
      OnClick = icoEstornarNotaClick
      ExplicitLeft = 8
    end
  end
  inherited Panel3: TPanel
    Width = 693
    Height = 177
    ExplicitWidth = 693
    ExplicitHeight = 177
    object Label3: TLabel
      Left = 23
      Top = 11
      Width = 27
      Height = 17
      Alignment = taRightJustify
      Caption = 'Data'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 20
      Top = 42
      Width = 30
      Height = 17
      Alignment = taRightJustify
      Caption = 'Valor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 324
      Top = 44
      Width = 67
      Height = 17
      Alignment = taRightJustify
      Caption = 'Vencimento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 511
      Top = 43
      Width = 66
      Height = 17
      Alignment = taRightJustify
      Caption = 'Pagamento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 154
      Top = 43
      Width = 64
      Height = 17
      Alignment = taRightJustify
      Caption = 'Valor Pago'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCp_datacompra: TJvDateEdit
      Left = 56
      Top = 8
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 0
    end
    object rgCp_tipo_pgto: TRadioGroup
      Left = 162
      Top = 0
      Width = 223
      Height = 34
      Caption = 'Tipo Pagamento'
      Columns = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ItemIndex = 1
      Items.Strings = (
        'Boleto'
        'Dinheiro'
        'Pix')
      ParentFont = False
      TabOrder = 1
      OnClick = rgCp_tipo_pgtoClick
    end
    object edCp_valor: TJvCalcEdit
      Left = 56
      Top = 39
      Width = 88
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 2
      DecimalPlacesAlwaysShown = False
    end
    object edCp_datavencimento: TJvDateEdit
      Left = 397
      Top = 41
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 4
    end
    object edCp_datapagamento: TJvDateEdit
      Left = 583
      Top = 40
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 5
    end
    object edCP_valorpago: TJvCalcEdit
      Left = 224
      Top = 40
      Width = 88
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
    end
    object GroupBox1: TGroupBox
      Left = 56
      Top = 66
      Width = 627
      Height = 102
      Caption = 'Observa'#231#245'es'
      TabOrder = 6
      object mCP_obs: TMemo
        Left = 2
        Top = 19
        Width = 623
        Height = 81
        Align = alClient
        TabOrder = 0
      end
    end
  end
  inherited Panel4: TPanel
    Top = 263
    Width = 693
    ExplicitTop = 263
    ExplicitWidth = 693
    inherited pnCancelar: TPanel
      Left = 333
      ExplicitLeft = 333
    end
    inherited pnAlterar: TPanel
      Left = 423
      ExplicitLeft = 423
    end
    inherited pnExcluir: TPanel
      Left = 513
      ExplicitLeft = 513
    end
    inherited pnSalvar: TPanel
      Left = 243
      ExplicitLeft = 243
    end
    inherited pnFechar: TPanel
      Left = 603
      ExplicitLeft = 603
    end
    object pnEstornarNota: TPanel
      Left = 0
      Top = 0
      Width = 97
      Height = 32
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      Visible = False
      object Label11: TLabel
        Left = 0
        Top = 0
        Width = 97
        Height = 32
        Align = alClient
        Alignment = taCenter
        Caption = 'F7 - ESTORNAR PAGAMENTO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
        ExplicitWidth = 89
        ExplicitHeight = 30
      end
    end
  end
  inherited Panel5: TPanel
    Width = 693
    ExplicitWidth = 693
    object Label1: TLabel
      Left = 7
      Top = 12
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 527
      Top = 12
      Width = 94
      Height = 17
      Caption = 'C'#243'digo Compra'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 56
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object edCom_id: TJvCalcEdit
      Left = 627
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 1
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Timer1: TTimer
    Left = 448
    Top = 16
  end
end
