unit clsVendas;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TVendas = class
  public
    venda_id       : Integer;
    ent_id         : Integer;
    venda_data     : TDateTime;
    venda_hora     : String;
    venda_desconto : Double;
    venda_total    : Double;
    venda_obs      : String;
    usua_id        : Integer; //Usu�rio que fez a venda
    venda_status   : Integer; //0-Aberto 1-Liquidado 2-Parcial

    function  Recuperar(Pvenda_id:Integer):Boolean;
    procedure Gravar(Pvenda_id:Integer);
    procedure Eliminar(Pvenda_id:Integer);
    function  Ultima:Integer;
    procedure AlteraStatus(PVenda_id,PVenda_status:Integer);
  end;

implementation

{ TVendas }

uses DMC;

procedure TVendas.AlteraStatus(PVenda_id, PVenda_status: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('update Vendas set venda_status = :k_venda_status '+
             'where venda_id = :K_venda_id                     ');
  tq.ParamByName('K_venda_id').AsInteger     := Pvenda_id;
  tq.ParamByName('K_venda_status').AsInteger := PVenda_status;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TVendas.Eliminar(Pvenda_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Vendas where venda_id = :K_venda_id');
  tq.ParamByName('K_venda_id').AsInteger := Pvenda_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TVendas.Gravar(Pvenda_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Vendas where venda_id = :k_venda_id');
  tq.ParamByName('k_venda_id').AsInteger := Pvenda_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Vendas values('+
    ':k_venda_id,'+
    ':k_ent_id,'+
    ':k_venda_data,'+
    ':k_venda_hora,'+
    ':k_venda_desconto,'+
    ':k_venda_total,'+
    ':k_venda_obs,'+
    ':k_usua_id,'+
    ':k_venda_status)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Vendas set '+
    'ent_id         = :k_ent_id,'+
    'venda_data     = :k_venda_data,'+
    'venda_hora     = :k_venda_hora,'+
    'venda_desconto = :k_venda_desconto,'+
    'venda_total    = :k_venda_total,'+
    'venda_obs      = :k_venda_obs,'+
    'usua_id        = :k_usua_id,'+
    'venda_status   = :k_venda_status ');
    tq.SQL.Add('where venda_id = :K_venda_id ');
  end;

  tq.ParamByName('k_venda_id').AsInteger     := Pvenda_id;
  tq.ParamByName('k_ent_id').AsInteger       := ent_id;
  tq.ParamByName('k_venda_data').AsDateTime  := venda_data;
  tq.ParamByName('k_venda_hora').AsString    := venda_hora;
  tq.ParamByName('k_venda_desconto').AsFloat := venda_desconto;
  tq.ParamByName('k_venda_total').AsFloat    := venda_total;
  tq.ParamByName('k_venda_obs').AsString     := venda_obs;
  if usua_id > 0 then
    tq.ParamByName('k_usua_id').AsInteger    := usua_id
  else
    tq.ParamByName('k_usua_id').IsNull;
  tq.ParamByName('k_venda_status').AsInteger := venda_status;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TVendas.Recuperar(Pvenda_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Vendas where venda_id = :K_venda_id');
  tq.ParamByName('k_venda_id').AsInteger := Pvenda_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  venda_id       := tq.FieldByName('venda_id').AsInteger;
  ent_id         := tq.FieldByName('ent_id').AsInteger;
  venda_data     := tq.FieldByName('venda_data').AsDateTime;
  venda_hora     := tq.FieldByName('venda_hora').AsString;
  venda_desconto := tq.FieldByName('venda_desconto').AsFloat;
  venda_total    := tq.FieldByName('venda_total').AsFloat;
  venda_obs      := tq.FieldByName('venda_obs').AsString;
  usua_id        := tq.FieldByName('usua_id').AsInteger;
  venda_status   := tq.FieldByName('venda_status').AsInteger;

  tq.Close;
  tq.Free;
end;

function TVendas.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(venda_id) maior from Vendas');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
