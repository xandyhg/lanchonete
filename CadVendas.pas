unit CadVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.ComCtrls, JvExComCtrls, JvDateTimePicker,
  JvToolEdit, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvBaseEdits, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Vcl.DBCtrls, Data.DB, Datasnap.DBClient, Datasnap.Provider,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Buttons, Vcl.Grids,
  Vcl.DBGrids, JvExDBGrids, JvDBGrid;

type
  TF_CadVendas = class(TF_CadPadrao)
    edCodigo: TJvCalcEdit;
    Label1: TLabel;
    Label2: TLabel;
    edVenda_Data: TJvDateEdit;
    edVenda_Hora: TJvDateTimePicker;
    Panel1: TPanel;
    Label59: TLabel;
    sbEntidade: TSpeedButton;
    Panel9: TPanel;
    Panel10: TPanel;
    Label5: TLabel;
    Label12: TLabel;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton2: TSpeedButton;
    edPro_CodigoBarras: TEdit;
    edIV_Qtde: TJvCalcEdit;
    JvDBGrid1: TJvDBGrid;
    Button1: TButton;
    Panel15: TPanel;
    mCom_obs: TMemo;
    Panel13: TPanel;
    Panel14: TPanel;
    Label3: TLabel;
    CdsItensVenda: TClientDataSet;
    CdsItensVendaiv_qtde: TFloatField;
    CdsItensVendapro_codigobarras: TStringField;
    CdsItensVendapro_descricao: TStringField;
    CdsItensVendapro_id: TIntegerField;
    CdsItensVendapro_valorvenda: TFloatField;
    CdsItensVendaiv_totalitem: TFloatField;
    CdsItensVendatotalVenda: TAggregateField;
    edTotalVenda: TDBEdit;
    Label4: TLabel;
    lbUsuario: TLabel;
    edIV_totalItem: TJvCalcEdit;
    Label7: TLabel;
    edEnt_id: TJvCalcEdit;
    edEnt_Nome: TEdit;
    sbProduto: TSpeedButton;
    edPro_Descricao: TEdit;
    edPro_ValorVenda: TJvCalcEdit;
    DsItensVenda: TDataSource;
    edPro_id: TJvCalcEdit;
    Label6: TLabel;
    edEnt_tipo_cliente: TEdit;
    edSet_Descricao: TEdit;
    Label8: TLabel;
    edFun_Descricao: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    edVenda_status: TEdit;
    CdsItensVendaiv_status: TIntegerField;
    edIV_Status: TJvCalcEdit;
    Label11: TLabel;
    CdsItensVendaiv_valor_restante: TFloatField;
    icoEstornarVenda: TImage;
    pnEstornarVenda: TPanel;
    Label13: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
    procedure edPro_CodigoBarrasExit(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure edIV_QtdeExit(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure DsItensVendaDataChange(Sender: TObject; Field: TField);
    procedure edEnt_idExit(Sender: TObject);
    procedure sbEntidadeClick(Sender: TObject);
    procedure edEnt_idKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edPro_CodigoBarrasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbProdutoClick(Sender: TObject);
    procedure edPro_ValorVendaExit(Sender: TObject);
    procedure icoEstornarVendaClick(Sender: TObject);
    procedure CdsItensVendaiv_statusGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure LimparCamposItem;
    procedure CalcularTotalItem;
  end;

var
  F_CadVendas: TF_CadVendas;

implementation

{$R *.dfm}

uses DMC, clsVendas, clsItens_Venda, X, U_Principal, ConVendas, ConEntidade,
  clsUsuario, clsEntidade, clsProduto, ConProduto, clsConfiguracoes,
  CadEntidade, CadContasReceber, ConContasReceber, clsFuncao, clsSetor,
  AuxFormaPgto, clsContasReceber, clsContasReceber_Pgto, clsItens_Venda_Pgto,
  clsEntidade_Credito;

var
  Vendas        : TVendas;
  ItenVenda     : TItens_Venda;
  Usuario       : TUsuario;
  tq            : TFDQuery;
  Entidade      : TEntidade;
  Produto       : TProduto;
  Config        : TConfiguracoes;
  Funcao        : TFuncao;
  Setor         : TSetor;
  ConReceb      : TContasReceber;
  ConRecebPgto  : TContasReceber_Pgto;
  Itens_VenPgto : TItens_Venda_Pgto;
  EntCred       : TEntidade_Credito;

procedure TF_CadVendas.Button1Click(Sender: TObject);
begin
  inherited;
  if edPro_id.AsInteger = 0 then begin
    msgPerson('N�o h� produto informado para ser adicionado!',[mbok],'Vendas',mtError);
    edIV_Qtde.SetFocus;
    Exit;
  end;

  if not CdsItensVenda.Locate('pro_id',edPro_id.AsInteger,[]) then begin
    CdsItensVenda.Append;
  end else begin
    CdsItensVenda.Edit;
  end;

  CdsItensVenda.FieldByName('iv_qtde').AsFloat           := edIV_Qtde.Value;
  CdsItensVenda.FieldByName('pro_codigobarras').AsString := edPro_CodigoBarras.Text;
  CdsItensVenda.FieldByName('pro_descricao').AsString    := edPro_Descricao.Text;
  CdsItensVenda.FieldByName('pro_id').AsInteger          := edPro_id.AsInteger;
  CdsItensVenda.FieldByName('pro_valorvenda').AsFloat    := edPro_ValorVenda.Value;
  CdsItensVenda.FieldByName('iv_totalitem').AsFloat      := edIV_totalItem.Value;
  CdsItensVenda.FieldByName('iv_status').AsInteger       := edIV_Status.AsInteger;
  CdsItensVenda.FieldByName('iv_valor_restante').AsFloat := edIV_totalItem.Value;
  CdsItensVenda.Post;

  LimparCamposItem;
  edIV_Qtde.SetFocus;
end;

procedure TF_CadVendas.CalcularTotalItem;
begin
  if edPro_ValorVenda.Value <> 0 then
    edIV_totalItem.Value := edIV_Qtde.Value * edPro_ValorVenda.Value
  else
    edIV_totalItem.Value := 0;
end;

procedure TF_CadVendas.CdsItensVendaiv_statusGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '0' then
    Text := 'ABERTO'
  else if Sender.AsString = '1' then
    Text := 'LIQUIDADO'
  else if Sender.AsString = '2' then
    Text := 'PARCIAL';
end;

procedure TF_CadVendas.DsItensVendaDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if CdsItensVenda.RecordCount > 0 then begin
    SpeedButton2.Enabled := True;
    SpeedButton3.Enabled := True;
  end;
end;

procedure TF_CadVendas.edEnt_idExit(Sender: TObject);
begin
  inherited;
  if edEnt_id.AsInteger > 0 then begin
    Entidade := TEntidade.Create;
    Funcao   := TFuncao.Create;
    Setor    := TSetor.Create;
    if Entidade.Recuperar(edEnt_id.AsInteger) then begin
      if Entidade.ent_tipo <> 0 then begin
        msgPerson('C�digo informado n�o � de cliente!',[mbok],'Entidade',mtError);
        edEnt_id.SetFocus;
        edEnt_id.SelectAll;
      end;
      edEnt_Nome.Text := Entidade.ent_nome;
      if Entidade.ent_tipo_cliente = 0 then
        edEnt_tipo_cliente.Text := 'EXTERNO'
      else if Entidade.ent_tipo_cliente = 1 then
        edEnt_tipo_cliente.Text := 'FUNCION�RIO'
      else if Entidade.ent_tipo_cliente = 2 then
        edEnt_tipo_cliente.Text := 'TERCEIRIZADO'
      else
        edEnt_tipo_cliente.Text := '';
      if Funcao.Recuperar(Entidade.fun_id) then
        edFun_Descricao.Text := Funcao.fun_descricao
      else
        edFun_Descricao.Text := '';
      if Setor.Recuperar(Entidade.set_id) then
        edSet_Descricao.Text := Setor.set_descricao
      else
        edSet_Descricao.Text := '';
    end else begin
      msgPerson('Cliente n�o cadastrado!',[mbok],'Entidade',mtError);
      edEnt_id.SetFocus;
      edEnt_id.SelectAll;
    end;
    FreeAndNil(Entidade);
    FreeAndNil(Funcao);
    FreeAndNil(Setor);
  end else begin
    edEnt_Nome.Clear;
    edEnt_tipo_cliente.Clear;
    edFun_Descricao.Clear;
    edSet_Descricao.Clear;
  end;
end;

procedure TF_CadVendas.edEnt_idKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = 117 then
    sbEntidadeClick(Sender);
end;

procedure TF_CadVendas.edIV_QtdeExit(Sender: TObject);
begin
  inherited;
  CalcularTotalItem;
end;

procedure TF_CadVendas.edPro_CodigoBarrasExit(Sender: TObject);
begin
  inherited;
  if Trim(edPro_CodigoBarras.Text) <> '' then begin
    Produto := TProduto.Create;
    if Produto.RecuperarPorCodBarras(edPro_CodigoBarras.Text) then begin
      edPro_id.AsInteger     := Produto.pro_id;
      edPro_Descricao.Text   := Produto.pro_descricao;
      edPro_ValorVenda.Value := Produto.pro_valorvenda;
      CalcularTotalItem;
    end else begin
      msgPerson('C�digo de barras n�o existe! Informe outro ou pesquise pelo produto!',[mbok],'Vendas',mtConfirmation);
      edPro_id.Clear;
      edPro_ValorVenda.Clear;
      edIV_totalItem.Clear;
      edPro_Descricao.Clear;
      edIV_Status.Clear;
      edPro_CodigoBarras.Clear;
      edPro_CodigoBarras.SetFocus;
    end;
    FreeAndNil(Produto);
  end else begin
    edPro_id.Clear;
    edPro_ValorVenda.Clear;
    edIV_totalItem.Clear;
    edPro_Descricao.Clear;
    edIV_Status.Clear;
  end;
end;

procedure TF_CadVendas.edPro_CodigoBarrasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = 117 then
    sbProdutoClick(Sender);
end;

procedure TF_CadVendas.edPro_ValorVendaExit(Sender: TObject);
begin
  inherited;
  CalcularTotalItem;
end;

procedure TF_CadVendas.FormActivate(Sender: TObject);
begin
  inherited;
  CdsItensVenda.Close;
  CdsItensVenda.CreateDataSet;
  CdsItensVenda.Open;

  LimparCamposItem;

  if xAux_ConVendas_Venda_ID > 0 then begin
    Vendas  := TVendas.Create;
    Usuario := TUsuario.Create;
    if Vendas.Recuperar(xAux_ConVendas_Venda_ID) then begin
      edCodigo.AsInteger    := xAux_ConVendas_Venda_ID;
      edVenda_Data.Date     := Vendas.venda_data;
      edVenda_Hora.Time     := StrToTime(Vendas.venda_hora);
      edEnt_id.AsInteger    := Vendas.ent_id;
      edEnt_idExit(Sender);
      mCom_obs.Text         := Vendas.venda_obs;
      if Usuario.Recuperar(Vendas.usua_id) then
        lbUsuario.Caption   := Usuario.usua_login
      else
        lbUsuario.Caption   := '';
      if Vendas.venda_status = 0 then
        edVenda_status.Text := 'ABERTO'
      else if Vendas.venda_status = 1 then
        edVenda_status.Text := 'LIQUIDADO'
      else if Vendas.venda_status = 2 then
        edVenda_status.Text := 'PARCIAL'
      else
        edVenda_status.Text := 'ABERTO';

      //Carrega os itens da venda
      tq := TFDQuery.Create(nil);
      tq.Connection := DM.FDCon;
      tq.Close;
      tq.Params.Clear;
      tq.SQL.Clear;
      tq.SQL.Add('select p.pro_descricao, p.pro_codigobarras, iv.* '+
                 'from itens_venda iv                              '+
                 'inner join produto p on iv.pro_id=p.pro_id       '+
                 'where iv.venda_id = :k_venda_id                  ');
      tq.ParamByName('k_venda_id').AsInteger := xAux_ConVendas_Venda_ID;
      tq.Open;

      while not tq.Eof do begin
        CdsItensVenda.Append;
        CdsItensVenda.FieldByName('pro_codigobarras').AsString   := tq.FieldByName('pro_codigobarras').AsString;
        CdsItensVenda.FieldByName('pro_id').AsInteger            := tq.FieldByName('pro_id').AsInteger;
        CdsItensVenda.FieldByName('pro_descricao').AsString      := tq.FieldByName('pro_descricao').AsString;
        CdsItensVenda.FieldByName('iv_qtde').AsFloat             := tq.FieldByName('iv_qtde').AsFloat;
        CdsItensVenda.FieldByName('pro_valorvenda').AsFloat      := tq.FieldByName('iv_valor_unit').AsFloat;
        CdsItensVenda.FieldByName('iv_totalitem').AsFloat        := tq.FieldByName('iv_valor_total').AsFloat;
        CdsItensVenda.FieldByName('iv_status').AsInteger         := tq.FieldByName('iv_status').AsInteger;
        CdsItensVenda.FieldByName('iv_valor_restante').AsFloat   := tq.FieldByName('iv_valor_restante').AsFloat;
        CdsItensVenda.Post;

        tq.Next;
      end;

      tq.Close;
      tq.Free;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(Vendas);
    FreeAndNil(Usuario);

    ConReceb := TContasReceber.Create;
    if ConReceb.RecuperarPorVenda(xAux_ConVendas_Venda_ID) then begin
      if ConReceb.cr_status <> 0 then begin
        msgPerson('J� existe um registro de Contas a Receber que j� est� pago!'+#13+'A venda n�o pode ser alterada!'+#13+'Caso precise, o registro pode ser estornado pela tela de Contas a Receber!',[mbok],'Venda',mtConfirmation);
      end else begin
        msgPerson('J� existe um registro de Contas a Receber!'+#13+'A venda n�o pode ser alterada!'+#13+'Caso precise alterar a Venda, clique no bot�o Estornar Venda!',[mbok],'Venda',mtConfirmation);
        icoEstornarVenda.Show;
        pnEstornarVenda.Show;
      end;
//      icoLancaTotalNF.Hide;
//      pnLancaTotalNF.Hide;
      icoAlterar.Hide;
      pnAlterar.Hide;
      icoExcluir.Hide;
      pnExcluir.Hide;
    end else begin
//      icoNovoProduto.Show;
//      pnNovoProduto.Show;
    end;
    FreeAndNil(ConReceb);
  end else begin
    edVenda_Data.Date   := Date;
    edVenda_Hora.Time   := Time;
    lbUsuario.Caption   := xAux_NomeUsuario;
    mCom_obs.Clear;
    edVenda_status.Text := 'ABERTO';
    edFun_Descricao.Clear;
    edSet_Descricao.Clear;

    Config := TConfiguracoes.Create;
    Config.Recuperar;
    if Config.ent_id = 0 then begin
      if msgPerson('N�o existe um cliente padr�o para as vendas!'+#13+'Deseja cadastrar um cliente padr�o para todas as vendas?'+#13+'Esse cliente ser� informado sempre que fizer uma nova venda!',[mbYes,mbNo],'Vendas',mtWarning) = IDNO then begin
        FreeAndNil(Config);
        Exit;
      end else begin
        xAux_ConEntidade_Ent_ID := 0;
        AbreForm(TF_CadEntidade,Self);
        if xAux_ConEntidade_Ent_ID = 0 then
          Exit
        else begin
          Config.ent_id := xAux_ConEntidade_Ent_ID;
          Config.Gravar;
        end;
      end;
    end else begin
      xAux_ConEntidade_Ent_ID := Config.ent_id;
    end;

    edEnt_id.AsInteger := xAux_ConEntidade_Ent_ID;
    edEnt_idExit(Sender);

    FreeAndNil(Config);

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadVendas.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_F7) and (icoSalvar.Visible = False) then
    icoEstornarVenda.OnClick(Self)
end;

procedure TF_CadVendas.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadVendas.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConVendas_Venda_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadVendas.icoEstornarVendaClick(Sender: TObject);
begin
  inherited;
  if icoEstornarVenda.Visible = True then begin
    try
      DM.FDCon.StartTransaction;
      ConReceb := TContasReceber.Create;
      if ConReceb.RecuperarPorVenda(edCodigo.AsInteger) then begin
        ConReceb.Eliminar(ConReceb.cr_id);
      end;
      FreeAndNil(ConReceb);

      DM.FDCon.ReleaseClients(rmFetchAll);
      DM.FDCon.Commit;

      msgPerson('Venda estornada com sucesso!',[mbok],'Vendas',mtConfirmation);

      icoFecharClick(Sender);
    except
      on e:Exception do begin
        EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
        DM.FDCon.Rollback;
        ShowMessage('Erro na grava��o! ('+e.message+' )');
      end;
    end;
  end;
end;

procedure TF_CadVendas.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Vendas',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    Vendas    := TVendas.Create;
    ItenVenda := TItens_Venda.Create;

    ItenVenda.EliminarTodosItens(edCodigo.AsInteger);
    Vendas.Eliminar(edCodigo.AsInteger);

    msgPerson('Registro eliminado com sucesso!',[mbok],'Vendas',mtConfirmation);

    FreeAndNil(Vendas);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadVendas.icoSalvarClick(Sender: TObject);
var
  xAuxCr_id    : Integer;
  SeqDinheiro  : Integer;
  SeqPix       : Integer;
  SeqCarCred   : Integer;
  SeqCarDeb    : Integer;
  SeqCredito   : Integer;
  xAux_VlrItem : Double;
begin
  inherited;
  if CdsItensVenda.RecordCount = 0 then begin
    msgPerson('N�o h� itens na venda para serem salvos!',[mbOK], 'Vendas', mtError);
    Exit;
  end;
  Try
    DM.FDCon.StartTransaction;
    Vendas    := TVendas.Create;
    ItenVenda := TItens_Venda.Create;
    Usuario   := TUsuario.Create;

    Vendas.venda_data     := edVenda_Data.Date;
    Vendas.venda_hora     := TimeToStr(edVenda_Hora.Time);
    Vendas.ent_id         := edEnt_id.AsInteger;
    Vendas.venda_desconto := 0;
    Vendas.venda_total    := StrToFloat(edTotalVenda.Text);
    Vendas.venda_obs      := mCom_obs.Text;
    if Usuario.RecuperarPorLogin(lbUsuario.Caption) then
      Vendas.usua_id      := Usuario.usua_id
    else
      Vendas.usua_id      := 0;
    if edVenda_status.Text = 'ABERTO' then
      Vendas.venda_status := 0
    else if edVenda_status.Text = 'LIQUIDADO' then
      Vendas.venda_status := 1
    else if edVenda_status.Text = 'PARCIAL' then
      Vendas.venda_status := 2;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConVendas_Venda_ID := Vendas.Ultima;
      Vendas.Gravar(xAux_ConVendas_Venda_ID);
    end else begin
      Vendas.Gravar(xAux_ConVendas_Venda_ID);
    end;

    ItenVenda.EliminarTodosItens(xAux_ConVendas_Venda_ID);
    CdsItensVenda.First;
    while not CdsItensVenda.Eof do begin
      ItenVenda.iv_qtde           := CdsItensVenda.FieldByName('iv_qtde').AsFloat;
      ItenVenda.iv_valor_unit     := CdsItensVenda.FieldByName('pro_valorvenda').AsFloat;
      ItenVenda.iv_valor_total    := CdsItensVenda.FieldByName('iv_totalitem').AsFloat;
      ItenVenda.iv_status         := CdsItensVenda.FieldByName('iv_status').AsInteger;
      ItenVenda.iv_valor_restante := CdsItensVenda.FieldByName('iv_totalitem').AsFloat;
      ItenVenda.Gravar(xAux_ConVendas_Venda_ID,CdsItensVenda.FieldByName('pro_id').AsInteger);
      CdsItensVenda.Next;
    end;

    FreeAndNil(Vendas);
    FreeAndNil(ItenVenda);
    FreeAndNil(Usuario);

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Venda '+IntToStr(xAux_ConVendas_Venda_ID)+' cadastrada com sucesso!',[mbOK], 'Vendas', mtConfirmation)
    else
      msgPerson('Venda '+IntToStr(xAux_ConVendas_Venda_ID)+' alterada com sucesso!',[mbOK], 'Vendas', mtConfirmation);

//    xAux_ConContasReceber_Venda_ID := xAux_ConVendas_Venda_ID;
//    xAux_ConContasReceber_CR_ID    := 0;
//    xAux_ConContasReceber_Valor    := CdsItensVendatotalVenda.Value;
//    AbreForm(TF_CadContasReceber,Self);
//
//    xAux_ConContasReceber_Tela     := '';
//    xAux_ConContasReceber_Venda_ID := 0;
//    xAux_ConContasReceber_CR_ID    := 0;
//    xAux_ConContasReceber_Valor    := 0;

    xAux_AuxFormaPgto_ValorTotal := CdsItensVendatotalVenda.Value;
    xAux_AuxFormaPgto_Ent_ID     := edEnt_id.AsInteger;
    xAux_AuxFormaPgto_Dinheiro   := 0;
    xAux_AuxFormaPgto_Pix        := 0;
    xAux_AuxFormaPgto_CarCred    := 0;
    xAux_AuxFormaPgto_CarDeb     := 0;
    xAux_AuxFormaPgto_CredUtil   := 0;
    xAux_AuxFormaPgto_CredTotal  := 0;
    xAux_AuxFormaPgto_VlrPago    := 0;
    xAux_AuxFormaPgto_Acertar    := 0;
    AbreForm(TF_AuxFormaPgto,Self);
    if xAux_AuxFormaPgto_ValorTotal > 0 then begin
      Try
        DM.FDCon.StartTransaction;
        ConReceb      := TContasReceber.Create;
        ConRecebPgto  := TContasReceber_Pgto.Create;
        Vendas        := TVendas.Create;
        Itens_VenPgto := TItens_Venda_Pgto.Create;
        ItenVenda     := TItens_Venda.Create;

        //Cria o t�tulo a receber
        ConReceb.venda_id            := xAux_ConVendas_Venda_ID;
        ConReceb.cr_valor            := xAux_AuxFormaPgto_ValorTotal;
        ConReceb.cr_datacadastro     := Date;
        ConReceb.cr_obs              := 'GERADO AUTOM�TICO PELA TELA DE VENDAS';
        if (xAux_AuxFormaPgto_Acertar > 0) and (xAux_AuxFormaPgto_VlrPago = 0) then begin
          ConReceb.cr_datavencimento := Date+30;
          ConReceb.cr_status         := 0;
          Vendas.AlteraStatus(xAux_ConVendas_Venda_ID,0);
        end else if xAux_AuxFormaPgto_VlrPago >= xAux_AuxFormaPgto_ValorTotal then begin
          ConReceb.cr_datavencimento := Date;
          ConReceb.cr_status         := 1;
          Vendas.AlteraStatus(xAux_ConVendas_Venda_ID,1);
        end else begin
          ConReceb.cr_datavencimento := Date+30;
          ConReceb.cr_status         := 2;
          Vendas.AlteraStatus(xAux_ConVendas_Venda_ID,2);
        end;

        xAuxCr_id := ConReceb.Ultima;
        ConReceb.Gravar(xAuxCr_id);

        //Cria os pagamentos da venda
//        if (xAux_AuxFormaPgto_Acertar = 0) then begin
        if xAux_AuxFormaPgto_Dinheiro > 0 then begin //Dinheiro
          ConRecebPgto.crp_valorpago     := xAux_AuxFormaPgto_Dinheiro;
          ConRecebPgto.crp_datapagamento := Date;
          ConRecebPgto.crp_tipo_pgto     := 0;
          ConRecebPgto.crp_obs           := 'BAIXADO AUTOM�TICO PELA TELA DE VENDAS';
          ConRecebPgto.crp_databaixa     := Date;
          SeqDinheiro                    := ConRecebPgto.Ultima(xAuxCr_id);
          ConRecebPgto.Gravar(xAuxCr_id,
                              SeqDinheiro);
        end;
        if xAux_AuxFormaPgto_Pix > 0 then begin //Pix
          ConRecebPgto.crp_valorpago     := xAux_AuxFormaPgto_Pix;
          ConRecebPgto.crp_datapagamento := Date;
          ConRecebPgto.crp_tipo_pgto     := 1;
          ConRecebPgto.crp_obs           := 'BAIXADO AUTOM�TICO PELA TELA DE VENDAS';
          ConRecebPgto.crp_databaixa     := Date;
          SeqPix                         := ConRecebPgto.Ultima(xAuxCr_id);
          ConRecebPgto.Gravar(xAuxCr_id,
                              SeqPix);
        end;
        if xAux_AuxFormaPgto_CarCred > 0 then begin //Cart�o Cr�dito
          ConRecebPgto.crp_valorpago     := xAux_AuxFormaPgto_CarCred;
          ConRecebPgto.crp_datapagamento := Date;
          ConRecebPgto.crp_tipo_pgto     := 2;
          ConRecebPgto.crp_obs           := 'BAIXADO AUTOM�TICO PELA TELA DE VENDAS';
          ConRecebPgto.crp_databaixa     := Date;
          SeqCarCred                     := ConRecebPgto.Ultima(xAuxCr_id);
          ConRecebPgto.Gravar(xAuxCr_id,
                              SeqCarCred);
        end;
        if xAux_AuxFormaPgto_CarDeb > 0 then begin //Cart�o D�bito
          ConRecebPgto.crp_valorpago     := xAux_AuxFormaPgto_CarDeb;
          ConRecebPgto.crp_datapagamento := Date;
          ConRecebPgto.crp_tipo_pgto     := 3;
          ConRecebPgto.crp_obs           := 'BAIXADO AUTOM�TICO PELA TELA DE VENDAS';
          ConRecebPgto.crp_databaixa     := Date;
          SeqCarDeb                      := ConRecebPgto.Ultima(xAuxCr_id);
          ConRecebPgto.Gravar(xAuxCr_id,
                              SeqCarDeb);
        end;
        if xAux_AuxFormaPgto_CredUtil > 0 then begin //Cr�dito
          ConRecebPgto.crp_valorpago     := xAux_AuxFormaPgto_CredUtil;
          ConRecebPgto.crp_datapagamento := Date;
          ConRecebPgto.crp_tipo_pgto     := 4;
          ConRecebPgto.crp_obs           := 'BAIXADO AUTOM�TICO PELA TELA DE VENDAS';
          ConRecebPgto.crp_databaixa     := Date;
          SeqCredito                     := ConRecebPgto.Ultima(xAuxCr_id);
          ConRecebPgto.Gravar(xAuxCr_id,
                              SeqCredito);

          //Lan�a o valor utilizado como d�bito
          EntCred := TEntidade_Credito.Create;
          EntCred.cred_valor := xAux_AuxFormaPgto_CredUtil;
          EntCred.cred_data  := Date;
          EntCred.cred_tipo  := 'D';
          EntCred.cr_id      := xAuxCr_id;
          EntCred.crp_seq    := SeqCredito;
          EntCred.Gravar(edEnt_id.AsInteger,
                         EntCred.Ultima(edEnt_id.AsInteger));
          FreeAndNil(EntCred);
        end;

        //Amarra os pagamentos em cada item da venda
        CdsItensVenda.First;
        while not CdsItensVenda.Eof do begin
          xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat;
          if xAux_VlrItem > 0 then begin
            if xAux_AuxFormaPgto_Dinheiro > 0 then begin
              repeat
                if xAux_AuxFormaPgto_Dinheiro > xAux_VlrItem then begin
                  Itens_VenPgto.ivp_valor    := xAux_VlrItem;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_VlrItem; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_Dinheiro := xAux_AuxFormaPgto_Dinheiro - xAux_VlrItem;
                  xAux_VlrItem               := 0;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  1,
                                                  0);

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqDinheiro,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);

                  CdsItensVenda.Next; //Como zerou o valor do item, passa para o pr�ximo item

                  xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat; //Atualiza o valor com o valor do proximo item
                end else if xAux_AuxFormaPgto_Dinheiro = xAux_VlrItem then begin
                  Itens_VenPgto.ivp_valor    := xAux_VlrItem;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_VlrItem; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_Dinheiro := 0;
                  xAux_VlrItem               := 0;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  1,
                                                  0);

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqDinheiro,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);

                  CdsItensVenda.Next; //Como zerou o valor do item, passa para o pr�ximo item

                  xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat; //Atualiza o valor com o valor do proximo item
                end else begin
                  Itens_VenPgto.ivp_valor    := xAux_AuxFormaPgto_Dinheiro;
                  xAux_VlrItem               := xAux_VlrItem - xAux_AuxFormaPgto_Dinheiro;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_AuxFormaPgto_Dinheiro; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_Dinheiro := 0; //xAux_AuxFormaPgto_Dinheiro - CdsItensVenda.FieldByName('iv_totalitem').AsFloat;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  2,
                                                  xAux_VlrItem);//xAux_AuxFormaPgto_Dinheiro*(-1));

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqDinheiro,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);
                end;
              until xAux_AuxFormaPgto_Dinheiro <= 0;
            end;
          end;
          if xAux_VlrItem > 0 then begin
            if xAux_AuxFormaPgto_Pix > 0 then begin
              repeat
                if xAux_AuxFormaPgto_Pix > xAux_VlrItem then begin
                  Itens_VenPgto.ivp_valor    := xAux_VlrItem;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_VlrItem; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_Pix      := xAux_AuxFormaPgto_Pix - xAux_VlrItem;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  1,
                                                  0);

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqPix,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);

                  CdsItensVenda.Next; //Como zerou o valor do item, passa para o pr�ximo item

                  xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat; //Atualiza o valor com o valor do proximo item
                end else if xAux_AuxFormaPgto_Pix = xAux_VlrItem then begin
                  Itens_VenPgto.ivp_valor    := xAux_VlrItem;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_VlrItem; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_Pix      := 0;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  1,
                                                  0);

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqPix,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);

                  CdsItensVenda.Next; //Como zerou o valor do item, passa para o pr�ximo item

                  xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat; //Atualiza o valor com o valor do proximo item
                end else begin
                  Itens_VenPgto.ivp_valor    := xAux_AuxFormaPgto_Pix;
                  xAux_VlrItem               := xAux_VlrItem - xAux_AuxFormaPgto_Pix;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_AuxFormaPgto_Pix; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_Pix      := 0; //xAux_AuxFormaPgto_Pix - CdsItensVenda.FieldByName('iv_totalitem').AsFloat;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  2,
                                                  xAux_VlrItem);//xAux_AuxFormaPgto_Pix*(-1));

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqPix,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);
                end;
              until xAux_AuxFormaPgto_Pix <= 0;
            end;
          end;
          if xAux_VlrItem > 0 then begin
            if xAux_AuxFormaPgto_CarCred > 0 then begin
              repeat
                if xAux_AuxFormaPgto_CarCred > xAux_VlrItem then begin
                  Itens_VenPgto.ivp_valor    := xAux_VlrItem;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_VlrItem; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_CarCred  := xAux_AuxFormaPgto_CarCred - xAux_VlrItem;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  1,
                                                  0);

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqCarCred,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);

                  CdsItensVenda.Next; //Como zerou o valor do item, passa para o pr�ximo item

                  xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat; //Atualiza o valor com o valor do proximo item
                end else if xAux_AuxFormaPgto_CarCred = xAux_VlrItem then begin
                  Itens_VenPgto.ivp_valor    := xAux_VlrItem;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_VlrItem; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_CarCred  := 0;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  1,
                                                  0);

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqCarCred,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);

                  CdsItensVenda.Next; //Como zerou o valor do item, passa para o pr�ximo item

                  xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat; //Atualiza o valor com o valor do proximo item
                end else begin
                  Itens_VenPgto.ivp_valor    := xAux_AuxFormaPgto_CarCred;
                  xAux_VlrItem               := xAux_VlrItem - xAux_AuxFormaPgto_CarCred;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_AuxFormaPgto_CarCred; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_CarCred  := 0; //xAux_AuxFormaPgto_CarCred - CdsItensVenda.FieldByName('iv_totalitem').AsFloat;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  2,
                                                  xAux_VlrItem);//xAux_AuxFormaPgto_CarCred*(-1));

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqCarCred,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);
                end;
              until xAux_AuxFormaPgto_CarCred <= 0;
            end;
          end;
          if xAux_VlrItem > 0 then begin
            if xAux_AuxFormaPgto_CarDeb > 0 then begin
              repeat
                if xAux_AuxFormaPgto_CarDeb > xAux_VlrItem then begin
                  Itens_VenPgto.ivp_valor    := xAux_VlrItem;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_VlrItem; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_CarDeb   := xAux_AuxFormaPgto_CarDeb - xAux_VlrItem;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  1,
                                                  0);

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqCarDeb,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);

                  CdsItensVenda.Next; //Como zerou o valor do item, passa para o pr�ximo item

                  xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat; //Atualiza o valor com o valor do proximo item
                end else if xAux_AuxFormaPgto_CarDeb = xAux_VlrItem then begin
                  Itens_VenPgto.ivp_valor    := xAux_VlrItem;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_VlrItem; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_CarDeb   := 0;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  1,
                                                  0);

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqCarDeb,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);

                  CdsItensVenda.Next; //Como zerou o valor do item, passa para o pr�ximo item

                  xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat; //Atualiza o valor com o valor do proximo item
                end else begin
                  Itens_VenPgto.ivp_valor    := xAux_AuxFormaPgto_CarDeb;
                  xAux_VlrItem               := xAux_VlrItem - xAux_AuxFormaPgto_CarDeb;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_AuxFormaPgto_CarDeb; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_CarDeb   := 0; //xAux_AuxFormaPgto_CarDeb - CdsItensVenda.FieldByName('iv_totalitem').AsFloat;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  2,
                                                  xAux_VlrItem);//xAux_AuxFormaPgto_CarDeb*(-1));

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqCarDeb,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);
                end;
              until xAux_AuxFormaPgto_CarDeb <= 0;
            end;
          end;
          if xAux_VlrItem > 0 then begin
            if xAux_AuxFormaPgto_CredUtil > 0 then begin
              repeat
                if xAux_AuxFormaPgto_CredUtil > xAux_VlrItem then begin
                  Itens_VenPgto.ivp_valor    := xAux_VlrItem;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_VlrItem; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_CredUtil := xAux_AuxFormaPgto_CredUtil - xAux_VlrItem;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  1,
                                                  0);

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqCredito,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);

                  CdsItensVenda.Next; //Como zerou o valor do item, passa para o pr�ximo item

                  xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat; //Atualiza o valor com o valor do proximo item
                end else if xAux_AuxFormaPgto_CredUtil = xAux_VlrItem then begin
                  Itens_VenPgto.ivp_valor    := xAux_VlrItem;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_VlrItem; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_CredUtil := 0;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  1,
                                                  0);

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqCredito,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);

                  CdsItensVenda.Next; //Como zerou o valor do item, passa para o pr�ximo item

                  xAux_VlrItem := CdsItensVenda.FieldByName('iv_totalitem').AsFloat; //Atualiza o valor com o valor do proximo item
                end else begin
                  Itens_VenPgto.ivp_valor    := xAux_AuxFormaPgto_CredUtil;
                  xAux_VlrItem               := xAux_VlrItem - xAux_AuxFormaPgto_CredUtil;
                  xAux_AuxFormaPgto_VlrPago  := xAux_AuxFormaPgto_VlrPago - xAux_AuxFormaPgto_CredUtil; //Diminui valor do item do total pago
                  xAux_AuxFormaPgto_CredUtil := 0; //xAux_AuxFormaPgto_CredUtil - CdsItensVenda.FieldByName('iv_totalitem').AsFloat;
                  ItenVenda.AtualizaValorRestante(xAux_ConVendas_Venda_ID,
                                                  CdsItensVenda.FieldByName('pro_id').AsInteger,
                                                  2,
                                                  xAux_VlrItem);//xAux_AuxFormaPgto_CredUtil*(-1));

                  Itens_VenPgto.Gravar(xAuxCr_id,
                                       SeqCredito,
                                       xAux_ConVendas_Venda_ID,
                                       CdsItensVenda.FieldByName('pro_id').AsInteger);
                end;
              until xAux_AuxFormaPgto_CredUtil <= 0;
            end;
          end;
          CdsItensVenda.Next;
        end;

        //Se o troco for lan�ar como cr�dito
        if xAux_AuxFormaPgto_LancarTroco = True then begin
          //Lan�a o valor do troco como cr�dito
          EntCred := TEntidade_Credito.Create;
          EntCred.cred_valor := xAux_AuxFormaPgto_VlrTroco;
          EntCred.cred_data  := Date;
          EntCred.cred_tipo  := 'C';
          EntCred.cr_id      := 0;
          EntCred.crp_seq    := 0;
          EntCred.Gravar(edEnt_id.AsInteger,EntCred.Ultima(edEnt_id.AsInteger));
          FreeAndNil(EntCred);
        end;

        FreeAndNil(ConReceb);
        FreeAndNil(ConRecebPgto);
        FreeAndNil(Vendas);
        FreeAndNil(Itens_VenPgto);
        FreeAndNil(ItenVenda);

        DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

        if (SeqDinheiro<>0) or (SeqPix<>0) or (SeqCarCred<>0) or (SeqCarDeb<>0) or (SeqCredito<>0) then
          msgPerson('Conta a receber '+IntToStr(xAuxCr_id)+' cadastrada e baixada com sucesso!',[mbOK], 'Vendas', mtConfirmation)
        else
          msgPerson('Conta a receber '+IntToStr(xAuxCr_id)+' cadastrada com sucesso!',[mbOK], 'Vendas', mtConfirmation);
      Except
        on e:Exception do begin
          EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
          DM.FDCon.Rollback;
          ShowMessage('Erro na grava��o! ('+e.message+' )');
        end;
      End;
    end;
    xAux_AuxFormaPgto_Dinheiro   := 0;
    xAux_AuxFormaPgto_Pix        := 0;
    xAux_AuxFormaPgto_CarCred    := 0;
    xAux_AuxFormaPgto_CarDeb     := 0;
    xAux_AuxFormaPgto_CredUtil   := 0;
    xAux_AuxFormaPgto_CredTotal  := 0;
    xAux_AuxFormaPgto_VlrPago    := 0;
    xAux_AuxFormaPgto_Acertar    := 0;
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

procedure TF_CadVendas.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if Column.FieldName = 'iv_status' then begin
    if CdsItensVenda.FieldByName('iv_status').AsInteger = 0 then begin
      JvDBGrid1.Canvas.Brush.Color := clRed;
      JvDBGrid1.Canvas.Font.Color  := clWhite;
    end else if CdsItensVenda.FieldByName('iv_status').AsInteger = 1 then begin
      JvDBGrid1.Canvas.Brush.Color := clYellow;
      JvDBGrid1.Canvas.Font.Color  := clBlack;
    end else if CdsItensVenda.FieldByName('iv_status').AsInteger = 2 then begin
      JvDBGrid1.Canvas.Brush.Color := clSkyBlue;
      JvDBGrid1.Canvas.Font.Color  := clBlack;
    end else begin
      JvDBGrid1.Canvas.Brush.Color := clWhite;
      JvDBGrid1.Canvas.Font.Color  := clBlack;
    end;
  end;
  JvDBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TF_CadVendas.LimparCamposItem;
begin
  edIV_Qtde.Clear;
  edPro_CodigoBarras.Clear;
  edPro_ValorVenda.Clear;
  edIV_totalItem.Clear;
  edIV_Status.Clear;
  edPro_id.Clear;
  edPro_Descricao.Clear;
  edPro_CodigoBarras.Enabled := True;
  edPro_ValorVenda.Enabled   := True;
  sbProduto.Enabled          := True;
  JvDBGrid1.Enabled          := True;
  SpeedButton4.Enabled       := False;
end;

procedure TF_CadVendas.sbEntidadeClick(Sender: TObject);
begin
  inherited;
  xAux_ConEntidade_Ent_Tipo   := 0;
  xAux_ConEntidade_Tela       := 'VENDA';
  xAux_ConEntidade_AtivaEnter := True;
  AbreForm(TF_ConEntidade,Self);
  xAux_ConEntidade_Ent_Tipo   := -1;
  xAux_ConEntidade_Tela       := '';
  xAux_ConEntidade_AtivaEnter := False;
  edEnt_id.AsInteger := xAux_ConEntidade_Ent_ID;
  edEnt_idExit(Sender);
  edIV_Qtde.SetFocus;
end;

procedure TF_CadVendas.sbProdutoClick(Sender: TObject);
begin
  inherited;
  xAux_ConProduto_AtivaEnter := True;
  AbreForm(TF_ConProduto,Self);
  xAux_ConProduto_AtivaEnter := False;
  edPro_id.AsInteger := xAux_ConProduto_Pro_ID;
  if edPro_id.AsInteger > 0 then begin
    Produto := TProduto.Create;
    if Produto.Recuperar(edPro_id.AsInteger) then begin
      edPro_id.AsInteger      := Produto.pro_id;
      edPro_Descricao.Text    := Produto.pro_descricao;
      edPro_ValorVenda.Value  := Produto.pro_valorvenda;
      edPro_CodigoBarras.Text := Produto.pro_codigobarras;
      CalcularTotalItem;
    end;
    FreeAndNil(Produto);
  end;
end;

procedure TF_CadVendas.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  if CdsItensVenda.RecordCount > 0 then begin
    if msgPerson('Tem certeza que deseja excluir o produto: '+#13+CdsItensVenda.FieldByName('pro_descricao').AsString+'?',[mbYes,mbNo],'Vendas',mtWarning) = IDYES then begin
      CdsItensVenda.Delete;
      if CdsItensVenda.RecordCount > 0 then begin
        if SpeedButton2.Enabled = False then
          SpeedButton2.Enabled   := True;
        if SpeedButton3.Enabled = False then
          SpeedButton3.Enabled   := True;
      end else begin
        if SpeedButton2.Enabled = True then
          SpeedButton2.Enabled   := False;
        if SpeedButton3.Enabled = True then
          SpeedButton3.Enabled   := False;
      end;
    end;
  end else
    msgPerson('N�o h� produto para remover!',[mbok],'Vendas',mtError);
end;

procedure TF_CadVendas.SpeedButton3Click(Sender: TObject);
begin
  inherited;
  if CdsItensVenda.RecordCount > 0 then begin
    edPro_CodigoBarras.Enabled := False;
    sbProduto.Enabled          := False;
    JvDBGrid1.Enabled          := False;
    SpeedButton3.Enabled       := False;
    SpeedButton4.Enabled       := True;
    edPro_CodigoBarras.Text    := CdsItensVenda.FieldByName('pro_codigobarras').AsString;
    edPro_id.AsInteger         := CdsItensVenda.FieldByName('pro_id').AsInteger;
    edPro_Descricao.Text       := CdsItensVenda.FieldByName('pro_descricao').AsString;
    edIV_Qtde.Value            := CdsItensVenda.FieldByName('iv_qtde').AsFloat;
    edPro_ValorVenda.Value     := CdsItensVenda.FieldByName('pro_valorvenda').AsFloat;
    edIV_totalItem.Value       := CdsItensVenda.FieldByName('iv_totalitem').AsFloat;
    edIV_Status.AsInteger      := CdsItensVenda.FieldByName('iv_status').AsInteger;
    CalcularTotalItem;
    edIV_Qtde.SetFocus;
  end;
end;

procedure TF_CadVendas.SpeedButton4Click(Sender: TObject);
begin
  inherited;
  LimparCamposItem;
  edIV_Qtde.SetFocus;
end;

end.
