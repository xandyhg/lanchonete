unit ConConfigEmail;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TF_ConConfigEmail = class(TF_ConPadrao)
    FDConfigEmail: TFDQuery;
    ProviderConfigEmail: TDataSetProvider;
    CdsConfigEmail: TClientDataSet;
    DsConfigEmail: TDataSource;
    FDConfigEmailCONFE_ID: TIntegerField;
    FDConfigEmailCONFE_NOME: TStringField;
    FDConfigEmailCONFE_AUTENTICACAO: TStringField;
    FDConfigEmailCONFE_PORTA: TIntegerField;
    FDConfigEmailCONFE_SMTP: TStringField;
    FDConfigEmailCONFE_ENDERECO: TStringField;
    CdsConfigEmailCONFE_ID: TIntegerField;
    CdsConfigEmailCONFE_NOME: TStringField;
    CdsConfigEmailCONFE_AUTENTICACAO: TStringField;
    CdsConfigEmailCONFE_PORTA: TIntegerField;
    CdsConfigEmailCONFE_SMTP: TStringField;
    CdsConfigEmailCONFE_ENDERECO: TStringField;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConConfigEmail           : TF_ConConfigEmail;
  xAux_ConConfEmail_Confe_ID : Integer;

implementation

{$R *.dfm}

uses DMC, CadConfigEmail, X;

procedure TF_ConConfigEmail.FormActivate(Sender: TObject);
begin
  inherited;
  icoPesquisarClick(Sender);
end;

procedure TF_ConConfigEmail.icoFecharClick(Sender: TObject);
begin
  xAux_ConConfEmail_Confe_ID := CdsConfigEmail.FieldByName('confe_id').AsInteger;
  inherited;

end;

procedure TF_ConConfigEmail.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConConfEmail_Confe_ID := 0;
  AbreForm(TF_CadConfigEmail,Self);
  icoPesquisarClick(Sender);
  if xAux_ConConfEmail_Confe_ID <> 0 then
    CdsConfigEmail.Locate('confe_id',xAux_ConConfEmail_Confe_ID,[]);
end;

procedure TF_ConConfigEmail.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsConfigEmail.Close;

  FDConfigEmail.Close;
  FDConfigEmail.Params.Clear;
  FDConfigEmail.SQL.Clear;
  FDConfigEmail.SQL.Add('select *            '+
                        'from configEmail    '+
                        'order by confe_nome ');
  CdsConfigEmail.Open;

  if not CdsConfigEmail.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsConfigEmail.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConConfigEmail.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConConfEmail_Confe_ID := CdsConfigEmail.FieldByName('confe_id').AsInteger;
  AbreForm(TF_CadConfigEmail, Self);
  icoPesquisarClick(Sender);
  if xAux_ConConfEmail_Confe_ID <> 0 then
    CdsConfigEmail.Locate('confe_id',xAux_ConConfEmail_Confe_ID,[]);
end;

end.
