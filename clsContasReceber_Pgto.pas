unit clsContasReceber_Pgto;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TContasReceber_Pgto = class
  public
    cr_id             : Integer;
    crp_seq           : Integer;
    crp_valorpago     : Double;
    crp_datapagamento : TDateTime;
    crp_tipo_pgto     : Integer; //0-Dinheiro 1-Pix 2-Cart�o Cr�dito 3-Cart�o D�bito 4-Cr�dito
    crp_obs           : String;
    crp_databaixa     : TDateTime;

    function  Recuperar(Pcr_id,Pcrp_seq:Integer):Boolean;
    procedure Gravar(Pcr_id,Pcrp_seq:Integer);
    procedure Eliminar(Pcr_id,Pcrp_seq:Integer);
    procedure EliminarTodosItens(Pcr_id:Integer);
    function  Ultima(Pcr_id:Integer):Integer;
  end;

implementation

{ TContasReceber_Pgto }

uses DMC;

procedure TContasReceber_Pgto.Eliminar(Pcr_id,Pcrp_seq:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from ContasReceber_Pgto where cr_id = :K_cr_id and crp_seq = :k_crp_seq');
  tq.ParamByName('K_cr_id').AsInteger   := Pcr_id;
  tq.ParamByName('K_crp_seq').AsInteger := Pcrp_seq;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TContasReceber_Pgto.EliminarTodosItens(Pcr_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from ContasReceber_Pgto where cr_id = :K_cr_id');
  tq.ParamByName('K_cr_id').AsInteger := Pcr_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TContasReceber_Pgto.Gravar(Pcr_id,Pcrp_seq: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ContasReceber_Pgto where cr_id = :k_cr_id and crp_seq = :k_crp_seq');
  tq.ParamByName('k_cr_id').AsInteger  := Pcr_id;
  tq.ParamByName('k_crp_seq').AsInteger := Pcrp_seq;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into ContasReceber_Pgto values('+
    ':k_cr_id,'+
    ':k_crp_seq,'+
    ':k_crp_valorpago,'+
    ':k_crp_datapagamento,'+
    ':k_crp_tipo_pgto,'+
    ':k_crp_obs,'+
    ':k_crp_databaixa)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update ContasReceber_Pgto set '+
    'crp_valorpago     = :k_crp_valorpago,'+
    'crp_datapagamento = :k_crp_datapagamento,'+
    'crp_tipo_pgto     = :k_crp_tipo_pgto,'+
    'crp_obs           = :k_crp_obs,'+
    'crp_databaixa     = :k_crp_databaixa ');
    tq.SQL.Add('where cr_id = :K_cr_id and crp_seq = :k_crp_seq ');
  end;

  tq.ParamByName('k_cr_id').AsInteger              := Pcr_id;
  tq.ParamByName('k_crp_seq').AsInteger            := Pcrp_seq;
  tq.ParamByName('k_crp_valorpago').AsFloat        := crp_valorpago;
  tq.ParamByName('k_crp_datapagamento').AsDateTime := crp_datapagamento;
  tq.ParamByName('k_crp_tipo_pgto').AsInteger      := crp_tipo_pgto;
  tq.ParamByName('k_crp_obs').AsString             := crp_obs;
  tq.ParamByName('k_crp_databaixa').AsDateTime     := crp_databaixa;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TContasReceber_Pgto.Recuperar(Pcr_id,Pcrp_seq:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ContasReceber_Pgto where cr_id = :K_cr_id and crp_seq = k_crp_seq');
  tq.ParamByName('k_cr_id').AsInteger   := Pcr_id;
  tq.ParamByName('k_crp_seq').AsInteger := Pcrp_seq;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  cr_id             := tq.FieldByName('cr_id').AsInteger;
  crp_seq           := tq.FieldByName('crp_seq').AsInteger;
  crp_valorpago     := tq.FieldByName('crp_valorpago').AsFloat;
  crp_datapagamento := tq.FieldByName('crp_datapagamento').AsDateTime;
  crp_tipo_pgto     := tq.FieldByName('crp_tipo_pgto').AsInteger;
  crp_obs           := tq.FieldByName('crp_obs').AsString;
  crp_databaixa     := tq.FieldByName('crp_databaixa').AsDateTime;

  tq.Close;
  tq.Free;
end;

function TContasReceber_Pgto.Ultima(Pcr_id:Integer): Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(crp_seq) maior from ContasReceber_Pgto where cr_id = :k_cr_id ');
  tq.ParamByName('k_cr_id').AsInteger := Pcr_id;
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
