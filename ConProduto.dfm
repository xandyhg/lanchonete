inherited F_ConProduto: TF_ConProduto
  Caption = 'Consulta de Produto'
  ClientWidth = 1215
  ExplicitWidth = 1231
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Width = 1215
    ExplicitWidth = 1215
    inherited icoPesquisar: TImage
      Left = 795
      ExplicitLeft = 795
    end
    inherited icoNovo: TImage
      Left = 900
      ExplicitLeft = 900
    end
    inherited icoImprimir: TImage
      Left = 1005
      ExplicitLeft = 1005
    end
    inherited icoFechar: TImage
      Left = 1110
      ExplicitLeft = 1110
    end
    object Label11: TLabel
      Left = 13
      Top = 6
      Width = 154
      Height = 17
      Caption = 'Media % Venda Listados:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 13
      Top = 23
      Width = 134
      Height = 17
      Caption = 'Media % Venda Total:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbPercListados: TLabel
      Left = 173
      Top = 6
      Width = 154
      Height = 17
      Caption = 'Media % Venda Listados:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbPercTotal: TLabel
      Left = 173
      Top = 23
      Width = 154
      Height = 17
      Caption = 'Media % Venda Listados:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  inherited StatusBar1: TStatusBar
    Width = 1215
    ExplicitWidth = 1215
  end
  inherited Panel3: TPanel
    Width = 1215
    ExplicitWidth = 1215
    inherited JvDBGrid1: TJvDBGrid
      Width = 1215
      DataSource = DsProduto
      OnDrawColumnCell = JvDBGrid1DrawColumnCell
      OnDblClick = JvDBGrid1DblClick
      OnKeyDown = JvDBGrid1KeyDown
      TitleRowHeight = 21
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'PRO_ATIVO'
          Title.Alignment = taCenter
          Title.Caption = 'Ativo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 'Tipo Produto'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = False
        end
        item
          Alignment = taCenter
          Expanded = False
          Title.Alignment = taCenter
          Title.Caption = 'Unid. Medida'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = False
        end
        item
          Expanded = False
          FieldName = 'PRO_ID'
          Title.Caption = 'C'#243'digo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRO_DESCRICAO'
          Title.Caption = 'Descri'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 355
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'PRO_DATACADASTRO'
          Title.Alignment = taCenter
          Title.Caption = 'Cadastro'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 94
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRO_VALORCUSTO'
          Title.Caption = 'Vlr. Custo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 91
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRO_VALORVENDA'
          Title.Caption = 'Vlr. Venda'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 89
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'PRO_CODIGOBARRAS'
          Title.Alignment = taCenter
          Title.Caption = 'C'#243'digo Barras'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PercVenda'
          Title.Alignment = taCenter
          Title.Caption = '% Venda'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end>
    end
  end
  inherited Panel4: TPanel
    Width = 1215
    ExplicitWidth = 1215
    inherited pnPesquisar: TPanel
      Left = 795
      ExplicitLeft = 795
    end
    inherited pnNovo: TPanel
      Left = 900
      ExplicitLeft = 900
    end
    inherited pnImprimir: TPanel
      Left = 1005
      ExplicitLeft = 1005
    end
    inherited pnFechar: TPanel
      Left = 1110
      ExplicitLeft = 1110
    end
  end
  inherited Panel1: TPanel
    Width = 1215
    ExplicitWidth = 1215
  end
  object FDProduto: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      'select p.pro_id, p.pro_descricao, p.pro_datacadastro,'
      
        'p.pro_valorcusto, p.pro_valorvenda, p.pro_codigobarras, p.pro_at' +
        'ivo'
      'from produto p'
      'order by p.pro_descricao')
    Left = 232
    Top = 8
    object FDProdutoPRO_ID: TIntegerField
      FieldName = 'PRO_ID'
      Origin = 'PRO_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDProdutoPRO_DESCRICAO: TStringField
      FieldName = 'PRO_DESCRICAO'
      Origin = 'PRO_DESCRICAO'
      Size = 255
    end
    object FDProdutoPRO_DATACADASTRO: TSQLTimeStampField
      FieldName = 'PRO_DATACADASTRO'
      Origin = 'PRO_DATACADASTRO'
    end
    object FDProdutoPRO_VALORCUSTO: TFloatField
      FieldName = 'PRO_VALORCUSTO'
      Origin = 'PRO_VALORCUSTO'
    end
    object FDProdutoPRO_VALORVENDA: TFloatField
      FieldName = 'PRO_VALORVENDA'
      Origin = 'PRO_VALORVENDA'
    end
    object FDProdutoPRO_CODIGOBARRAS: TStringField
      FieldName = 'PRO_CODIGOBARRAS'
      Origin = 'PRO_CODIGOBARRAS'
      Size = 13
    end
    object FDProdutoPRO_ATIVO: TIntegerField
      FieldName = 'PRO_ATIVO'
      Origin = 'PRO_ATIVO'
    end
  end
  object ProviderProduto: TDataSetProvider
    DataSet = FDProduto
    Left = 264
    Top = 8
  end
  object CdsProduto: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    Params = <>
    ProviderName = 'ProviderProduto'
    OnCalcFields = CdsProdutoCalcFields
    Left = 296
    Top = 8
    object CdsProdutoPRO_ID: TIntegerField
      FieldName = 'PRO_ID'
      Required = True
    end
    object CdsProdutoPRO_DESCRICAO: TStringField
      FieldName = 'PRO_DESCRICAO'
      Size = 255
    end
    object CdsProdutoPRO_DATACADASTRO: TSQLTimeStampField
      FieldName = 'PRO_DATACADASTRO'
    end
    object CdsProdutoPRO_VALORCUSTO: TFloatField
      FieldName = 'PRO_VALORCUSTO'
      DisplayFormat = '#,##0.00000'
    end
    object CdsProdutoPRO_VALORVENDA: TFloatField
      FieldName = 'PRO_VALORVENDA'
      DisplayFormat = '#,##0.00'
    end
    object CdsProdutoPRO_CODIGOBARRAS: TStringField
      FieldName = 'PRO_CODIGOBARRAS'
      Size = 13
    end
    object CdsProdutoPRO_ATIVO: TIntegerField
      FieldName = 'PRO_ATIVO'
      OnGetText = CdsProdutoPRO_ATIVOGetText
    end
    object CdsProdutoPercVenda: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'PercVenda'
      DisplayFormat = '#,##0.00'
    end
    object CdsProdutototPercVenda: TAggregateField
      FieldName = 'totPercVenda'
      Active = True
      DisplayName = ''
      DisplayFormat = '#,##0.00'
      Expression = 'sum(PercVenda)'
    end
  end
  object DsProduto: TDataSource
    DataSet = CdsProduto
    Left = 328
    Top = 8
  end
end
