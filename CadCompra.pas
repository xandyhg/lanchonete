unit CadCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits, FireDAC.Comp.Client, Vcl.Buttons, Vcl.Grids, Vcl.DBGrids,
  JvExDBGrids, JvDBGrid, Data.DB, Datasnap.DBClient, Vcl.DBCtrls;

type
  TF_CadCompra = class(TF_CadPadrao)
    edCodigo: TJvCalcEdit;
    Label1: TLabel;
    edCom_DataCadastro: TJvDateEdit;
    Label2: TLabel;
    Panel1: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Label5: TLabel;
    edPro_CodigoBarras: TEdit;
    edPro_id: TJvCalcEdit;
    Label6: TLabel;
    sbProduto: TSpeedButton;
    edPro_Descricao: TEdit;
    edIC_Qtde: TJvCalcEdit;
    Label12: TLabel;
    JvDBGrid1: TJvDBGrid;
    Panel13: TPanel;
    Panel14: TPanel;
    CdsItensNota: TClientDataSet;
    DsItensNota: TDataSource;
    CdsItensNotaic_valor_unit: TFloatField;
    CdsItensNotaic_valor_total_item: TFloatField;
    CdsItensNotapro_codigobarras: TStringField;
    CdsItensNotapro_id: TIntegerField;
    CdsItensNotaic_qtde: TFloatField;
    CdsItensNotapro_descricao: TStringField;
    Panel15: TPanel;
    mCom_obs: TMemo;
    edIC_valor_unit: TJvCalcEdit;
    Label7: TLabel;
    edValorTotalItem: TJvCalcEdit;
    Label4: TLabel;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton2: TSpeedButton;
    edQtdeCaixa: TJvCalcEdit;
    Label8: TLabel;
    icoLancaTotalNF: TImage;
    pnLancaTotalNF: TPanel;
    Label10: TLabel;
    edCom_NF: TEdit;
    Label3: TLabel;
    Label59: TLabel;
    edEnt_id: TJvCalcEdit;
    sbEntidade: TSpeedButton;
    edEnt_NomeFantasia: TEdit;
    Button1: TButton;
    icoNovoProduto: TImage;
    pnNovoProduto: TPanel;
    Label9: TLabel;
    icoEstornarNota: TImage;
    pnEstornarNota: TPanel;
    Label11: TLabel;
    icoAlteraValor: TImage;
    pnAlteraValor: TPanel;
    Label13: TLabel;
    CdsItensNotatotNota: TAggregateField;
    edIC_valor_total: TDBEdit;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
    procedure sbEntidadeClick(Sender: TObject);
    procedure edEnt_idExit(Sender: TObject);
    procedure edEnt_idKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edPro_idExit(Sender: TObject);
    procedure edPro_idKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbProdutoClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure edPro_CodigoBarrasExit(Sender: TObject);
    procedure edIC_QtdeExit(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure edQtdeCaixaExit(Sender: TObject);
    procedure edValorTotalItemExit(Sender: TObject);
    procedure Panel1Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure icoLancaTotalNFClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure icoNovoProdutoClick(Sender: TObject);
    procedure icoEstornarNotaClick(Sender: TObject);
    procedure icoAlteraValorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure LimparCamposItens;
    procedure CalcularValorTotalItem;
  end;

var
  F_CadCompra: TF_CadCompra;

implementation

{$R *.dfm}

uses DMC, clsCompra, clsItens_Compra, X, U_Principal, ConCompra, ConEntidade,
  clsEntidade, clsProduto, ConProduto, CadProduto, clsConfiguracoes, AuxCompra,
  clsContasPagar, ConContasPagar, CadContasPagar;

var
  Compra       : TCompra;
  Itens_Compra : TItens_Compra;
  Entidade     : TEntidade;
  Produto      : TProduto;
  tq           : TFDQuery;
  Config       : TConfiguracoes;
  ContasPagar  : TContasPagar;

procedure TF_CadCompra.Button1Click(Sender: TObject);
begin
  inherited;
  if edPro_id.AsInteger = 0 then begin
    msgPerson('N�o h� produto informado para ser adicionado!',[mbok],'Compras',mtError);
    Exit;
  end;

  if SpeedButton4.Enabled = True then
    CdsItensNota.Edit
  else
    CdsItensNota.Append;

  CdsItensNota.FieldByName('pro_codigobarras').AsString   := edPro_CodigoBarras.Text;
  CdsItensNota.FieldByName('pro_id').AsInteger            := edPro_id.AsInteger;
  CdsItensNota.FieldByName('pro_descricao').AsString      := edPro_Descricao.Text;
  if edQtdeCaixa.Value > 0 then
    CdsItensNota.FieldByName('ic_qtde').AsFloat           := edQtdeCaixa.Value * edIC_Qtde.Value
  else
    CdsItensNota.FieldByName('ic_qtde').AsFloat           := edIC_Qtde.Value;
  CdsItensNota.FieldByName('ic_valor_unit').AsFloat       := edIC_valor_unit.Value;
  CdsItensNota.FieldByName('ic_valor_total_item').AsFloat := edValorTotalItem.Value;
  CdsItensNota.Post;

//  //Soma os totais da Nota de Compra
//  edIC_valor_total.Value := edIC_valor_total.Value + edValorTotalItem.Value;

  LimparCamposItens;

  if SpeedButton2.Enabled = False then
    SpeedButton2.Enabled   := True;
  if SpeedButton3.Enabled = False then
    SpeedButton3.Enabled   := True;

  edPro_CodigoBarras.Enabled := True;
  edPro_id.Enabled           := True;
  sbProduto.Enabled          := True;
  JvDBGrid1.Enabled          := True;
  SpeedButton3.Enabled       := True;
  SpeedButton4.Enabled       := False;

  edPro_CodigoBarras.SetFocus;
end;

procedure TF_CadCompra.CalcularValorTotalItem;
begin
  if (edQtdeCaixa.Value > 0) and (edIC_Qtde.Value > 0) then
    edIC_valor_unit.Value := edValorTotalItem.Value / (edQtdeCaixa.Value*edIC_Qtde.Value)
  else if (edIC_Qtde.Value > 0) then
    edIC_valor_unit.Value := edValorTotalItem.Value / edIC_Qtde.Value
  else
    edIC_valor_unit.Value := 0;
end;

procedure TF_CadCompra.edEnt_idExit(Sender: TObject);
begin
  inherited;
  if edEnt_id.AsInteger > 0 then begin
    Entidade := TEntidade.Create;
    if Entidade.Recuperar(edEnt_id.AsInteger) then begin
      if Entidade.ent_tipo <> 1 then begin
        msgPerson('C�digo informado n�o � de fornecedor!',[mbok],'Entidade',mtError);
        edEnt_id.SetFocus;
        edEnt_id.SelectAll;
      end;
      edEnt_NomeFantasia.Text := Entidade.ent_nome_fantasia;
    end else begin
      msgPerson('Fornecedor n�o cadastrado!',[mbok],'Entidade',mtError);
      edEnt_id.SetFocus;
      edEnt_id.SelectAll;
    end;
    FreeAndNil(Entidade);
  end else begin
    edEnt_NomeFantasia.Clear;
  end;
end;

procedure TF_CadCompra.edEnt_idKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = 117 then
    sbEntidadeClick(Sender);
end;

procedure TF_CadCompra.edIC_QtdeExit(Sender: TObject);
begin
  inherited;
  CalcularValorTotalItem;
end;

procedure TF_CadCompra.edPro_CodigoBarrasExit(Sender: TObject);
begin
  inherited;
  if Trim(edPro_CodigoBarras.Text) <> '' then begin
    Produto := TProduto.Create;
    if Produto.RecuperarPorCodBarras(edPro_CodigoBarras.Text) then begin
      edPro_id.AsInteger        := Produto.pro_id;
      edPro_Descricao.Text      := Produto.pro_descricao;
      edIC_Qtde.Value           := 1;
      edIC_valor_unit.Value     := Produto.pro_valorcusto;
      CalcularValorTotalItem;
      edIC_Qtde.SetFocus;
    end else begin
      if msgPerson('C�digo de barras n�o existe! Deseja cadastrar um novo produto?',[mbYes,mbNo],'Compra',mtWarning) = IDYES then begin
        xAux_ConProduto_Pro_ID   := 0;
        xAuxCadProduto_CodBarras := edPro_CodigoBarras.Text;
        AbreForm(TF_CadProduto,Self);
        xAuxCadProduto_CodBarras := '';
        edPro_CodigoBarrasExit(Sender);
      end else begin
        edPro_CodigoBarras.Clear;
        edPro_id.SetFocus;
      end;
    end;
    FreeAndNil(Produto);
  end;
end;

procedure TF_CadCompra.edPro_idExit(Sender: TObject);
begin
  inherited;
  if edPro_id.AsInteger > 0 then begin
    Produto := TProduto.Create;
    if Produto.Recuperar(edPro_id.AsInteger) then begin
      if Produto.pro_ativo = 1 then begin
        msgPerson('Produto est� inativo!',[mbok],'Produto',mtError);
        edPro_id.SetFocus;
        edPro_id.SelectAll;
        Exit;
      end;
      edPro_id.AsInteger        := Produto.pro_id;
      edPro_Descricao.Text      := Produto.pro_descricao;
      edPro_CodigoBarras.Text   := Produto.pro_codigobarras;
      edIC_Qtde.Value           := 1;
      edIC_valor_unit.Value     := Produto.pro_valorcusto;
      CalcularValorTotalItem;
      edIC_Qtde.SetFocus;
    end else begin
      msgPerson('Produto n�o cadastrado!',[mbok],'Produto',mtError);
      edPro_id.SetFocus;
      edPro_id.SelectAll;
    end;
    FreeAndNil(Produto);
  end else begin
    edPro_Descricao.Clear;
    edPro_CodigoBarras.Clear;
  end;
end;

procedure TF_CadCompra.edPro_idKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = 117 then
    sbProdutoClick(Sender);
end;

procedure TF_CadCompra.edQtdeCaixaExit(Sender: TObject);
begin
  inherited;
  CalcularValorTotalItem;
end;

procedure TF_CadCompra.edValorTotalItemExit(Sender: TObject);
begin
  inherited;
  CalcularValorTotalItem;
end;

procedure TF_CadCompra.FormActivate(Sender: TObject);
begin
  inherited;
  CdsItensNota.Close;
  CdsItensNota.CreateDataSet;
  CdsItensNota.Open;

  LimparCamposItens;

  if xAux_ConCompra_Com_ID > 0 then begin
    Compra := TCompra.Create;
    if Compra.Recuperar(xAux_ConCompra_Com_ID) then begin
      edCodigo.AsInteger          := xAux_ConCompra_Com_ID;
      edCom_DataCadastro.Date     := Compra.com_datacadastro;
      edEnt_id.AsInteger          := Compra.ent_id;
      edEnt_idExit(Sender);
      edCom_NF.Text               := Compra.com_nf;
//      edIC_valor_total.Value      := Compra.com_valortotal;
      mCom_obs.Text               := Compra.com_obs;

      //Carrega os itens da nota
      tq := TFDQuery.Create(nil);
      tq.Connection := DM.FDCon;
      tq.Close;
      tq.Params.Clear;
      tq.SQL.Clear;
      tq.SQL.Add('select p.pro_descricao, p.pro_codigobarras, ic.* '+
                 'from itens_compra ic                             '+
                 'inner join produto p on ic.pro_id=p.pro_id       '+
                 'where com_id = :k_com_id                         ');
      tq.ParamByName('k_com_id').AsInteger := xAux_ConCompra_Com_ID;
      tq.Open;

      while not tq.Eof do begin
        CdsItensNota.Append;
        CdsItensNota.FieldByName('pro_codigobarras').AsString   := tq.FieldByName('pro_codigobarras').AsString;
        CdsItensNota.FieldByName('pro_id').AsInteger            := tq.FieldByName('pro_id').AsInteger;
        CdsItensNota.FieldByName('pro_descricao').AsString      := tq.FieldByName('pro_descricao').AsString;
        CdsItensNota.FieldByName('ic_qtde').AsFloat             := tq.FieldByName('ic_qtde').AsFloat;
        CdsItensNota.FieldByName('ic_valor_unit').AsFloat       := tq.FieldByName('ic_valor_unit').AsFloat;
        CdsItensNota.FieldByName('ic_valor_total_item').AsFloat := tq.FieldByName('ic_valor_total').AsFloat;
        CdsItensNota.Post;

        tq.Next;
      end;

      Panel3.Enabled := False;
      ControleBotoes(1);

      if CdsItensNota.RecordCount > 0 then begin
        SpeedButton2.Enabled := True;
        SpeedButton3.Enabled := True;
      end;
    end;
    FreeAndNil(Compra);

    ContasPagar := TContasPagar.Create;
    if ContasPagar.RecuperarPorCompra(xAux_ConCompra_Com_ID) then begin
      if ContasPagar.cp_datapagamento <> 0 then begin
        msgPerson('J� existe um registro de Contas a Pagar que j� est� pago!'+#13+'A nota de compra n�o pode ser alterada!'+#13+'Caso precise, o registro pode ser estornado pela tela de Contas a Pagar!',[mbok],'Compra',mtConfirmation);
      end else begin
        msgPerson('J� existe um registro de Contas a Pagar!'+#13+'A nota de compra n�o pode ser alterada!'+#13+'Caso precise alterar a Nota, clique no bot�o Estornar Nota!',[mbok],'Compra',mtConfirmation);
        icoEstornarNota.Show;
        pnEstornarNota.Show;
      end;
      icoLancaTotalNF.Hide;
      pnLancaTotalNF.Hide;
      icoAlterar.Hide;
      pnAlterar.Hide;
      icoExcluir.Hide;
      pnExcluir.Hide;
    end else begin
      icoNovoProduto.Show;
      pnNovoProduto.Show;
    end;
    FreeAndNil(ContasPagar);
  end else begin
    icoNovoProduto.Show;
    pnNovoProduto.Show;
    edCom_DataCadastro.Date := Date;
    edEnt_id.Clear;
    edEnt_NomeFantasia.Clear;
    edCom_NF.Clear;
//    edIC_valor_total.Clear;
    mCom_obs.Clear;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadCompra.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_F7) and (icoEstornarNota.Visible = True) then
    icoEstornarNota.OnClick(Self)
  else if (Key = VK_F8) then
    icoNovoProduto.OnClick(Self)
  else if (Key = VK_F9) then
    icoAlteraValor.OnClick(Self)
  else if (Key = VK_F12) and (icoLancaTotalNF.Visible = True) then
    icoLancaTotalNF.OnClick(Self);
end;

procedure TF_CadCompra.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadCompra.icoAlteraValorClick(Sender: TObject);
begin
  inherited;
  xAux_ConProduto_Pro_ID := 0;
  AbreForm(TF_ConProduto,Self);
  xAux_ConProduto_Pro_ID := 0;
end;

procedure TF_CadCompra.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConCompra_Com_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadCompra.icoEstornarNotaClick(Sender: TObject);
begin
  inherited;
  if icoEstornarNota.Visible = True then begin
    try
      DM.FDCon.StartTransaction;
      ContasPagar := TContasPagar.Create;
      if ContasPagar.RecuperarPorCompra(edCodigo.AsInteger) then begin
        ContasPagar.Eliminar(ContasPagar.cp_id);
      end;
      FreeAndNil(ContasPagar);

      DM.FDCon.ReleaseClients(rmFetchAll);
      DM.FDCon.Commit;

      msgPerson('Nota estornada com sucesso!',[mbok],'Compra',mtConfirmation);

      icoFecharClick(Sender);
    except
      on e:Exception do begin
        EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
        DM.FDCon.Rollback;
        ShowMessage('Erro na grava��o! ('+e.message+' )');
      end;
    end;
  end;
end;

procedure TF_CadCompra.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Compra',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    Compra       := TCompra.Create;
    Itens_Compra := TItens_Compra.Create;

    Itens_Compra.EliminarTodosItens(edCodigo.AsInteger);
    Compra.Eliminar(edCodigo.AsInteger);

    msgPerson('Registro eliminado com sucesso!',[mbok],'Compra',mtConfirmation);

    FreeAndNil(Compra);
    FreeAndNil(Itens_Compra);

    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadCompra.icoLancaTotalNFClick(Sender: TObject);
begin
  inherited;
  Config := TConfiguracoes.Create;
  Config.Recuperar;
  if Config.pro_id = 0 then begin
    if msgPerson('N�o existe um produto padr�o para as compras!'+#13+'Deseja cadastrar um produto padr�o para todas as compras?'+#13+'Esse produto ser� lan�ado sempre que lan�ar somente o total da nota!',[mbYes,mbNo],'Compra',mtWarning) = IDNO then begin
      FreeAndNil(Config);
      Exit;
    end else begin
      xAux_ConProduto_Pro_ID := 0;
      AbreForm(TF_CadProduto,Self);
      if xAux_ConProduto_Pro_ID = 0 then
        Exit
      else begin
        Config.pro_id := xAux_ConProduto_Pro_ID;
        Config.Gravar;
      end;
    end;
  end else begin
    xAux_ConProduto_Pro_ID := Config.pro_id;
  end;

  xAux_xAuxCompra_ValorTotal := 0;
  xAux_xAuxCompra_Obs        := '';
  AbreForm(TF_AuxCompra,Self);

  if xAux_xAuxCompra_ValorTotal > 0 then begin
    CdsItensNota.Append;
    CdsItensNota.FieldByName('pro_codigobarras').AsString   := '';
    CdsItensNota.FieldByName('pro_id').AsInteger            := xAux_ConProduto_Pro_ID;
    CdsItensNota.FieldByName('pro_descricao').AsString      := '';
    CdsItensNota.FieldByName('ic_qtde').AsFloat             := 1;
    CdsItensNota.FieldByName('ic_valor_unit').AsFloat       := xAux_xAuxCompra_ValorTotal;
    CdsItensNota.FieldByName('ic_valor_total_item').AsFloat := xAux_xAuxCompra_ValorTotal;
    CdsItensNota.Post;

    //Soma os totais da Nota de Compra
//    edIC_valor_total.Value := xAux_xAuxCompra_ValorTotal;
    mCom_obs.Text          := xAux_xAuxCompra_Obs;

    icoSalvarClick(Sender);
  end;
  FreeAndNil(Config);
end;

procedure TF_CadCompra.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    Compra       := TCompra.Create;
    Itens_Compra := TItens_Compra.Create;

    Compra.com_datacadastro   := edCom_DataCadastro.Date;
    Compra.ent_id             := edEnt_id.AsInteger;
    Compra.com_nf             := edCom_NF.Text;
    Compra.com_valortotal     := CdsItensNotatotNota.Value;
    Compra.com_obs            := mCom_obs.Text;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConCompra_Com_ID   := Compra.Ultima;
      Compra.Gravar(xAux_ConCompra_Com_ID);
    end else begin
      Compra.Gravar(xAux_ConCompra_Com_ID);
    end;

    Itens_Compra.EliminarTodosItens(xAux_ConCompra_Com_ID);
    CdsItensNota.First;
    while not CdsItensNota.Eof do begin
      Itens_Compra.ic_qtde        := CdsItensNota.FieldByName('ic_qtde').AsFloat;
      Itens_Compra.ic_valor_unit  := CdsItensNota.FieldByName('ic_valor_unit').AsFloat;
      Itens_Compra.ic_valor_total := CdsItensNota.FieldByName('ic_valor_total_item').AsFloat;
      Itens_Compra.Gravar(xAux_ConCompra_Com_ID,CdsItensNota.FieldByName('pro_id').AsInteger);
      CdsItensNota.Next;
    end;

    FreeAndNil(Compra);
    FreeAndNil(Itens_Compra);

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Nota de Compra '+IntToStr(xAux_ConCompra_Com_ID)+' cadastrada com sucesso!',[mbOK], 'Compra', mtConfirmation)
    else
      msgPerson('Nota de Compra '+IntToStr(xAux_ConCompra_Com_ID)+' alterada com sucesso!',[mbOK], 'Compra', mtConfirmation);

    xAux_ConContasPagar_Tela   := 'COMPRA';
    xAux_ConContasPagar_COM_ID := xAux_ConCompra_Com_ID;
    xAux_ConContasPagar_Valor  := CdsItensNotatotNota.Value;
    xAux_ConContasPagar_CP_ID  := 0;
    AbreForm(TF_CadContasPagar,Self);
    xAux_ConContasPagar_Tela   := '';
    xAux_ConContasPagar_COM_ID := 0;
    xAux_ConContasPagar_Valor  := 0;
    xAux_ConContasPagar_CP_ID  := 0;

    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

procedure TF_CadCompra.icoNovoProdutoClick(Sender: TObject);
begin
  inherited;
  xAux_ConProduto_Pro_ID := 0;
  AbreForm(TF_CadProduto,Self);
end;

procedure TF_CadCompra.LimparCamposItens;
begin
  edPro_CodigoBarras.Clear;
  edPro_id.Clear;
  edPro_Descricao.Clear;
  edIC_Qtde.Clear;
  edQtdeCaixa.Clear;
  edIC_valor_unit.Clear;
  edValorTotalItem.Clear;
end;

procedure TF_CadCompra.Panel1Exit(Sender: TObject);
begin
  inherited;
  if edEnt_id.AsInteger > 0 then begin
    pnLancaTotalNF.Show;
    icoLancaTotalNF.Show;
    Config := TConfiguracoes.Create;
    Config.Recuperar;
    if Config.conf_lanca_total_nota = 0 then
      icoLancaTotalNFClick(Sender);
    FreeAndNil(Config);
  end else begin
    pnLancaTotalNF.Hide;
    icoLancaTotalNF.Hide;
  end;
end;

procedure TF_CadCompra.sbEntidadeClick(Sender: TObject);
begin
  inherited;
  xAux_ConEntidade_Ent_Tipo   := 1;
  xAux_ConEntidade_Tela       := 'COMPRA';
  xAux_ConEntidade_AtivaEnter := True;
  AbreForm(TF_ConEntidade,Self);
  xAux_ConEntidade_Ent_Tipo   := -1;
  xAux_ConEntidade_Tela       := '';
  xAux_ConEntidade_AtivaEnter := False;
  edEnt_id.AsInteger := xAux_ConEntidade_Ent_ID;
  edEnt_idExit(Sender);
  edPro_CodigoBarras.SetFocus;
end;

procedure TF_CadCompra.sbProdutoClick(Sender: TObject);
begin
  inherited;
  xAux_ConProduto_AtivaEnter := True;
  AbreForm(TF_ConProduto,Self);
  xAux_ConProduto_AtivaEnter := False;
  edPro_id.AsInteger := xAux_ConProduto_Pro_ID;
  edPro_idExit(Sender);
  edIC_Qtde.SetFocus;
end;

procedure TF_CadCompra.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  if CdsItensNota.RecordCount > 0 then begin
    if msgPerson('Tem certeza que deseja excluir o produto: '+#13+CdsItensNota.FieldByName('pro_descricao').AsString+'?',[mbYes,mbNo],'Compra',mtWarning) = IDYES then begin
//      edIC_valor_total.Value := edIC_valor_total.Value - CdsItensNota.FieldByName('ic_valor_total_item').Value;
      CdsItensNota.Delete;
      if CdsItensNota.RecordCount > 0 then begin
        if SpeedButton2.Enabled = False then
          SpeedButton2.Enabled   := True;
        if SpeedButton3.Enabled = False then
          SpeedButton3.Enabled   := True;
      end else begin
        if SpeedButton2.Enabled = True then
          SpeedButton2.Enabled   := False;
        if SpeedButton3.Enabled = True then
          SpeedButton3.Enabled   := False;
      end;
    end;
  end else
    msgPerson('N�o h� produto para remover!',[mbok],'Compra',mtError);
end;

procedure TF_CadCompra.SpeedButton3Click(Sender: TObject);
begin
  inherited;
  if CdsItensNota.RecordCount > 0 then begin
    edPro_CodigoBarras.Enabled := False;
    edPro_id.Enabled           := False;
    sbProduto.Enabled          := False;
    JvDBGrid1.Enabled          := False;
    SpeedButton3.Enabled       := False;
    SpeedButton4.Enabled       := True;
    edPro_CodigoBarras.Text    := CdsItensNota.FieldByName('pro_codigobarras').AsString;
    edPro_id.AsInteger         := CdsItensNota.FieldByName('pro_id').AsInteger;
    edPro_Descricao.Text       := CdsItensNota.FieldByName('pro_descricao').AsString;
    edIC_Qtde.Value            := CdsItensNota.FieldByName('ic_qtde').AsFloat;
    edQtdeCaixa.Value          := 0;
    edIC_valor_unit.Value      := CdsItensNota.FieldByName('ic_valor_unit').AsFloat;
//    edIC_valor_total.Value     := CdsItensNota.FieldByName('ic_valor_total_item').AsFloat;
    CalcularValorTotalItem;

//    edIC_valor_total.Value     := edIC_valor_total.Value - edValorTotalItem.Value;

    edIC_Qtde.SetFocus;
  end;
end;

procedure TF_CadCompra.SpeedButton4Click(Sender: TObject);
begin
  inherited;
  edPro_CodigoBarras.Enabled := True;
  edPro_id.Enabled           := True;
  sbProduto.Enabled          := True;
  JvDBGrid1.Enabled          := True;
  SpeedButton3.Enabled       := True;
  SpeedButton4.Enabled       := False;

//  edIC_valor_total.Value     := edIC_valor_total.Value + edValorTotalItem.Value;

  LimparCamposItens;
  edPro_CodigoBarras.SetFocus;
end;

end.
