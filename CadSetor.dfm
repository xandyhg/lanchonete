inherited F_CadSetor: TF_CadSetor
  Caption = ' CADASTRO DE SETOR'
  ClientHeight = 166
  ClientWidth = 649
  OnActivate = FormActivate
  ExplicitWidth = 665
  ExplicitHeight = 205
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 89
    Width = 649
    ExplicitTop = 89
    inherited icoFechar: TImage
      Left = 559
    end
    inherited icoAlterar: TImage
      Left = 379
      OnClick = icoAlterarClick
    end
    inherited icoExcluir: TImage
      Left = 469
      OnClick = icoExcluirClick
    end
    inherited icoSalvar: TImage
      Left = 199
      OnClick = icoSalvarClick
    end
    inherited icoCancelar: TImage
      Left = 289
      OnClick = icoCancelarClick
    end
  end
  inherited Panel3: TPanel
    Width = 649
    Height = 48
    ExplicitHeight = 48
    object Label2: TLabel
      Left = 9
      Top = 15
      Width = 57
      Height = 17
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edSet_descricao: TEdit
      Left = 72
      Top = 12
      Width = 569
      Height = 25
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  inherited Panel4: TPanel
    Top = 134
    Width = 649
    ExplicitTop = 134
    inherited pnCancelar: TPanel
      Left = 289
    end
    inherited pnAlterar: TPanel
      Left = 379
    end
    inherited pnExcluir: TPanel
      Left = 469
    end
    inherited pnSalvar: TPanel
      Left = 199
    end
    inherited pnFechar: TPanel
      Left = 559
    end
  end
  inherited Panel5: TPanel
    Width = 649
    object Label1: TLabel
      Left = 23
      Top = 10
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 72
      Top = 7
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
  end
  inherited Timer1: TTimer
    Left = 528
  end
end
