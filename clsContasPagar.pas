unit clsContasPagar;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TContasPagar = class
  public
    cp_id             : Integer;
    com_id            : Integer;
    cp_valor          : Double;
    cp_valorpago      : Double;
    cp_datacompra     : TDateTime;
    cp_datavencimento : TDateTime;
    cp_datapagamento  : TDateTime;
    cp_tipo_pgto      : Integer; //0-BOLETO
                                 //1-DINHEIRO
                                 //2-PIX
    cp_obs            : String;

    function  Recuperar(Pcp_id:Integer):Boolean;
    procedure Gravar(Pcp_id:Integer);
    procedure Eliminar(Pcp_id:Integer);
    function  Ultima:Integer;
    function  RecuperarPorCompra(PCom_id:Integer):Boolean;
    procedure EstornarContaPagar(Pcp_id:Integer);
  end;

implementation

{ TContasPagar }

uses DMC;

procedure TContasPagar.Eliminar(Pcp_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from ContasPagar where cp_id = :K_cp_id');
  tq.ParamByName('K_cp_id').AsInteger := Pcp_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TContasPagar.EstornarContaPagar(Pcp_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('update ContasPagar set cp_valorpago=0, cp_datapagamento=''30.12.1899'' '+
             'where cp_id = :K_cp_id                                                 ');
  tq.ParamByName('K_cp_id').AsInteger := Pcp_id;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TContasPagar.Gravar(Pcp_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ContasPagar where cp_id = :k_cp_id');
  tq.ParamByName('k_cp_id').AsInteger := Pcp_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into ContasPagar values('+
    ':k_cp_id,'+
    ':k_com_id,'+
    ':k_cp_valor,'+
    ':k_cp_valorpago,'+
    ':k_cp_datacompra,'+
    ':k_cp_datavencimento,'+
    ':k_cp_datapagamento,'+
    ':k_cp_tipo_pgto,'+
    ':k_cp_obs)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update ContasPagar set '+
    'com_id            = :k_com_id,'+
    'cp_valor          = :k_cp_valor,'+
    'cp_valorpago      = :k_cp_valorpago,'+
    'cp_datacompra     = :k_cp_datacompra,'+
    'cp_datavencimento = :k_cp_datavencimento,'+
    'cp_datapagamento  = :k_cp_datapagamento,'+
    'cp_tipo_pgto      = :k_cp_tipo_pgto,'+
    'cp_obs            = :k_cp_obs ');
    tq.SQL.Add('where cp_id = :K_cp_id ');
  end;

  tq.ParamByName('k_cp_id').AsInteger              := Pcp_id;
  if com_id > 0 then
    tq.ParamByName('k_com_id').AsInteger           := com_id
  else
    tq.ParamByName('k_com_id').IsNull;
  tq.ParamByName('k_cp_valor').AsFloat             := cp_valor;
  tq.ParamByName('k_cp_valorpago').AsFloat         := cp_valorpago;
  tq.ParamByName('k_cp_datacompra').AsDateTime     := cp_datacompra;
  tq.ParamByName('k_cp_datavencimento').AsDateTime := cp_datavencimento;
  tq.ParamByName('k_cp_datapagamento').AsDateTime  := cp_datapagamento;
  tq.ParamByName('k_cp_tipo_pgto').AsInteger       := cp_tipo_pgto;
  tq.ParamByName('k_cp_obs').AsString              := cp_obs;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TContasPagar.Recuperar(Pcp_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ContasPagar where cp_id = :K_cp_id');
  tq.ParamByName('k_cp_id').AsInteger := Pcp_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  cp_id             := tq.FieldByName('cp_id').AsInteger;
  com_id            := tq.FieldByName('com_id').AsInteger;
  cp_valor          := tq.FieldByName('cp_valor').AsFloat;
  cp_valorpago      := tq.FieldByName('cp_valorpago').AsFloat;
  cp_datacompra     := tq.FieldByName('cp_datacompra').AsDateTime;
  cp_datavencimento := tq.FieldByName('cp_datavencimento').AsDateTime;
  cp_datapagamento  := tq.FieldByName('cp_datapagamento').AsDateTime;
  cp_tipo_pgto      := tq.FieldByName('cp_tipo_pgto').AsInteger;
  cp_obs            := tq.FieldByName('cp_obs').AsString;

  tq.Close;
  tq.Free;
end;

function TContasPagar.RecuperarPorCompra(PCom_id: Integer): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from ContasPagar where com_id = :K_com_id');
  tq.ParamByName('k_com_id').AsInteger := Pcom_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('cp_id').AsInteger);

  tq.Close;
  tq.Free;
end;

function TContasPagar.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(cp_id) maior from ContasPagar');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
