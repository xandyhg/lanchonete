unit CadSetor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits, FireDAC.Comp.Client;

type
  TF_CadSetor = class(TF_CadPadrao)
    Label1: TLabel;
    edCodigo: TJvCalcEdit;
    edSet_descricao: TEdit;
    Label2: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadSetor: TF_CadSetor;

implementation

{$R *.dfm}

uses DMC, clsSetor, X, ConSetor, U_Principal;

var
  Setor : TSetor;

procedure TF_CadSetor.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_ConSetor_Set_ID > 0 then begin
    Setor := TSetor.Create;
    if Setor.Recuperar(xAux_ConSetor_Set_ID) then begin
      edCodigo.AsInteger      := xAux_ConSetor_Set_ID;
      edSet_descricao.Text    := Setor.set_descricao;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(Setor);
  end else begin
    edSet_descricao.Clear;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadSetor.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadSetor.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConSetor_Set_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadSetor.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Setor',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    Setor := TSetor.Create;

    Setor.Eliminar(edCodigo.AsInteger);

//    xAux_AuxJustificativa_Codigo   := edCodigo.AsInteger;
//    xAux_AuxJustificativa_NomeForm := UpperCase(Copy(Name,6,Length(Name)));
//    xAux_AuxJustificativa_Descricao:= edConfe_nome.Text;
//    AbreForm(TF_AuxJustificativa,Self);
//    xAux_AuxJustificativa_Codigo   := 0;
//    xAux_AuxJustificativa_NomeForm := '';
//    xAux_AuxJustificativa_Descricao:= '';

    msgPerson('Setor eliminado com sucesso!',[mbok],'Setor',mtConfirmation);

    FreeAndNil(Setor);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadSetor.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    Setor := TSetor.Create;

    Setor.set_descricao := edSet_descricao.Text;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConSetor_Set_ID     := Setor.Ultima;
      Setor.Gravar(xAux_ConSetor_Set_ID);
    end else begin
      Setor.Gravar(xAux_ConSetor_Set_ID);
    end;

    FreeAndNil(Setor);

//    if edCodigo.AsInteger = 0 then begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 1;
//      log.log_tabela      := 'CONFIG EMAIL';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'GRAVADO C�DIGO: '+IntToStr(xAux_ConConfEmail_Confe_ID)+' - '+edConfe_nome.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end else begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 2;
//      log.log_tabela      := 'CONFIG EMAIL';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'ALTERADO C�DIGO: '+IntToStr(xAux_ConConfEmail_Confe_ID)+' - '+edConfe_nome.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end;

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Setor '+IntToStr(xAux_ConSetor_Set_ID)+' cadastrado com sucesso!',[mbOK], 'Setor', mtConfirmation)
    else
      msgPerson('Setor '+IntToStr(xAux_ConSetor_Set_ID)+' alterado com sucesso!',[mbOK], 'Setor', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

end.
