inherited F_ConContasPagar: TF_ConContasPagar
  Caption = ' CONSULTA DE CONTAS A PAGAR'
  ClientHeight = 518
  ClientWidth = 1138
  ExplicitWidth = 1154
  ExplicitHeight = 557
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 405
    Width = 1138
    ExplicitTop = 405
    ExplicitWidth = 1138
    inherited icoPesquisar: TImage
      Left = 718
      ExplicitLeft = 718
    end
    inherited icoNovo: TImage
      Left = 823
      ExplicitLeft = 823
    end
    inherited icoImprimir: TImage
      Left = 928
      ExplicitLeft = 928
    end
    inherited icoFechar: TImage
      Left = 1033
      ExplicitLeft = 1033
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 482
    Width = 1138
    ExplicitTop = 482
    ExplicitWidth = 1138
  end
  inherited Panel3: TPanel
    Top = 99
    Width = 1138
    Height = 306
    ExplicitTop = 99
    ExplicitWidth = 1138
    ExplicitHeight = 306
    inherited JvDBGrid1: TJvDBGrid
      Width = 1138
      Height = 306
      DataSource = DsContasPagar
      OnDblClick = JvDBGrid1DblClick
      TitleRowHeight = 21
      Columns = <
        item
          Expanded = False
          FieldName = 'CP_ID'
          Title.Caption = 'C'#243'd. Conta'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CP_DATACOMPRA'
          Title.Alignment = taCenter
          Title.Caption = 'Data Cadastro'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 134
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_NOME_FANTASIA'
          Title.Caption = 'Nome'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 362
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CP_DATAVENCIMENTO'
          Title.Alignment = taCenter
          Title.Caption = 'Vencimento'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 134
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CP_DATAPAGAMENTO'
          Title.Alignment = taCenter
          Title.Caption = 'Pagamento'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 134
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CP_VALOR'
          Title.Caption = 'Valor'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CP_VALORPAGO'
          Title.Caption = 'Valor Pago'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CP_TIPO_PGTO'
          Title.Alignment = taCenter
          Title.Caption = 'Tipo Pgto'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 107
          Visible = True
        end>
    end
  end
  inherited Panel4: TPanel
    Top = 450
    Width = 1138
    ExplicitTop = 450
    ExplicitWidth = 1138
    inherited pnPesquisar: TPanel
      Left = 718
      ExplicitLeft = 718
      ExplicitHeight = 32
    end
    inherited pnNovo: TPanel
      Left = 823
      ExplicitLeft = 823
      ExplicitHeight = 32
    end
    inherited pnImprimir: TPanel
      Left = 928
      ExplicitLeft = 928
      ExplicitHeight = 32
    end
    inherited pnFechar: TPanel
      Left = 1033
      ExplicitLeft = 1033
      ExplicitHeight = 32
    end
  end
  inherited Panel1: TPanel
    Width = 1138
    Height = 99
    ExplicitWidth = 1138
    ExplicitHeight = 99
    object rgFiltrar: TRadioGroup
      Left = 0
      Top = 0
      Width = 129
      Height = 99
      Align = alLeft
      Caption = ' Filtrar por Data de '
      ItemIndex = 1
      Items.Strings = (
        'Cadastro'
        'Vencimento'
        'Pagamento')
      TabOrder = 0
    end
    object GroupBox1: TGroupBox
      Left = 129
      Top = 0
      Width = 116
      Height = 99
      Align = alLeft
      Caption = ' Per'#237'odo '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object Label8: TLabel
        Left = 54
        Top = 45
        Width = 7
        Height = 17
        Alignment = taRightJustify
        Caption = 'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object edDataFim: TJvDateEdit
        Left = 6
        Top = 65
        Width = 105
        Height = 25
        ShowNullDate = False
        TabOrder = 1
      end
      object edDataIni: TJvDateEdit
        Left = 6
        Top = 20
        Width = 105
        Height = 25
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowNullDate = False
        TabOrder = 0
      end
    end
    object rgTipoPgto: TRadioGroup
      Left = 245
      Top = 0
      Width = 104
      Height = 99
      Align = alLeft
      Caption = ' Tipo Pgto.'
      ItemIndex = 3
      Items.Strings = (
        'Boleto'
        'Dinheiro'
        'PIX'
        'Todos')
      TabOrder = 2
    end
    object GroupBox2: TGroupBox
      Left = 349
      Top = 0
      Width = 180
      Height = 99
      Align = alLeft
      Caption = 'Pesquisar por'
      TabOrder = 3
      object edDescricao: TEdit
        Left = 10
        Top = 53
        Width = 160
        Height = 25
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnKeyDown = edDescricaoKeyDown
      end
      object cbPesquisar: TComboBox
        Left = 10
        Top = 22
        Width = 160
        Height = 25
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ItemIndex = 0
        ParentFont = False
        TabOrder = 1
        Text = 'Nome Fantasia'
        OnChange = cbPesquisarChange
        Items.Strings = (
          'Nome Fantasia'
          'Raz'#227'o Social'
          'Valor da Conta'
          'Valor Pago')
      end
      object edValor: TJvCalcEdit
        Left = 10
        Top = 53
        Width = 83
        Height = 25
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 2
        Visible = False
        DecimalPlacesAlwaysShown = False
        OnKeyDown = edValorKeyDown
      end
    end
  end
  object FDContasPagar: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select cp.cp_id, cp.cp_datacompra, e.ent_nome_fantasia, cp.cp_da' +
        'tavencimento, cp.cp_datapagamento,'
      'cp.cp_valor, cp.cp_valorpago, cp.cp_tipo_pgto'
      'from contaspagar cp'
      'left outer join compra c on cp.com_id=c.com_id'
      'left outer join entidade e on c.ent_id=e.ent_id'
      'order by cp.cp_datacompra, e.ent_nome_fantasia')
    Left = 904
    Top = 8
    object FDContasPagarCP_ID: TIntegerField
      FieldName = 'CP_ID'
      Origin = 'CP_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDContasPagarCP_DATACOMPRA: TSQLTimeStampField
      FieldName = 'CP_DATACOMPRA'
      Origin = 'CP_DATACOMPRA'
    end
    object FDContasPagarENT_NOME_FANTASIA: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ENT_NOME_FANTASIA'
      Origin = 'ENT_NOME_FANTASIA'
      ProviderFlags = []
      ReadOnly = True
      Size = 255
    end
    object FDContasPagarCP_DATAVENCIMENTO: TSQLTimeStampField
      FieldName = 'CP_DATAVENCIMENTO'
      Origin = 'CP_DATAVENCIMENTO'
    end
    object FDContasPagarCP_DATAPAGAMENTO: TSQLTimeStampField
      FieldName = 'CP_DATAPAGAMENTO'
      Origin = 'CP_DATAPAGAMENTO'
    end
    object FDContasPagarCP_VALOR: TFloatField
      FieldName = 'CP_VALOR'
      Origin = 'CP_VALOR'
    end
    object FDContasPagarCP_VALORPAGO: TFloatField
      FieldName = 'CP_VALORPAGO'
      Origin = 'CP_VALORPAGO'
    end
    object FDContasPagarCP_TIPO_PGTO: TIntegerField
      FieldName = 'CP_TIPO_PGTO'
      Origin = 'CP_TIPO_PGTO'
    end
  end
  object ProviderContasPagar: TDataSetProvider
    DataSet = FDContasPagar
    Left = 936
    Top = 8
  end
  object CdsContasPagar: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderContasPagar'
    Left = 968
    Top = 8
    object CdsContasPagarCP_ID: TIntegerField
      FieldName = 'CP_ID'
      Required = True
    end
    object CdsContasPagarCP_DATACOMPRA: TSQLTimeStampField
      FieldName = 'CP_DATACOMPRA'
      OnGetText = CdsContasPagarCP_DATACOMPRAGetText
    end
    object CdsContasPagarENT_NOME_FANTASIA: TStringField
      FieldName = 'ENT_NOME_FANTASIA'
      ReadOnly = True
      Size = 255
    end
    object CdsContasPagarCP_DATAVENCIMENTO: TSQLTimeStampField
      FieldName = 'CP_DATAVENCIMENTO'
      OnGetText = CdsContasPagarCP_DATAVENCIMENTOGetText
    end
    object CdsContasPagarCP_DATAPAGAMENTO: TSQLTimeStampField
      FieldName = 'CP_DATAPAGAMENTO'
      OnGetText = CdsContasPagarCP_DATAPAGAMENTOGetText
    end
    object CdsContasPagarCP_VALOR: TFloatField
      FieldName = 'CP_VALOR'
      DisplayFormat = '#,##0.00'
    end
    object CdsContasPagarCP_VALORPAGO: TFloatField
      FieldName = 'CP_VALORPAGO'
      DisplayFormat = '#,##0.00'
    end
    object CdsContasPagarCP_TIPO_PGTO: TIntegerField
      FieldName = 'CP_TIPO_PGTO'
      OnGetText = CdsContasPagarCP_TIPO_PGTOGetText
    end
  end
  object DsContasPagar: TDataSource
    DataSet = CdsContasPagar
    Left = 1000
    Top = 8
  end
end
