unit U_Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ToolWin, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.ImgList;

type
  TPrincipal = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ToolBar1: TToolBar;
    TabSheet2: TTabSheet;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    img16: TImageList;
    TabSheet3: TTabSheet;
    ToolBar3: TToolBar;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
    procedure ToolButton11Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure ToolButton14Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton15Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Principal                 : TPrincipal;
  xAux_NomeUsuario          : String;
  xAux_IDUsuario            : Integer;
  xAux_AcessoPermitido      : Boolean;
  xAux_TelaCheia            : Boolean;
  xAux_SenhaAdmin           : String;

implementation

{$R *.dfm}

uses DMC, X, U_Login, ConUsuario, ConConfigEmail, ConCidade, ConFuncao,
  ConSetor, ConEntidade, ConProduto, ConCompra, ConContasPagar, Configuracoes,
  ConVendas, ConCredito;

procedure TPrincipal.FormActivate(Sender: TObject);
begin
  if xAux_AcessoPermitido = False then
    Application.Terminate;
  SelectFirst;
end;

procedure TPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TPrincipal.FormCreate(Sender: TObject);
begin
  if IsDebuggerPresent then begin
    Caption := 'BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE     BASE TESTE';
  end;

  DM.ConectarBanco('Banco'); //Conecta no banco  //Adicionei essa linha para abrir Principal primeiro

  AbreForm(TF_Login,Self);  //Adicionei essa linha para abrir Principal primeiro
end;

procedure TPrincipal.ToolButton11Click(Sender: TObject);
begin
  AbreForm(TF_ConProduto,Self);
end;

procedure TPrincipal.ToolButton12Click(Sender: TObject);
begin
  AbreForm(TF_ConContasPagar,Self);
end;

procedure TPrincipal.ToolButton14Click(Sender: TObject);
begin
  AbreForm(TF_Configuracoes,Self);
end;

procedure TPrincipal.ToolButton15Click(Sender: TObject);
begin
  AbreForm(TF_ConCredito,Self);
end;

procedure TPrincipal.ToolButton1Click(Sender: TObject);
begin
  AbreForm(TF_ConUsuario,Self);
end;

procedure TPrincipal.ToolButton2Click(Sender: TObject);
begin
  AbreForm(TF_ConCompra,Self);
end;

procedure TPrincipal.ToolButton3Click(Sender: TObject);
begin
  AbreForm(TF_ConVendas,Self);
end;

procedure TPrincipal.ToolButton4Click(Sender: TObject);
begin
  AbreForm(TF_ConConfigEmail,Self);
end;

procedure TPrincipal.ToolButton5Click(Sender: TObject);
begin
  AbreForm(TF_ConCidade,Self);
end;

procedure TPrincipal.ToolButton6Click(Sender: TObject);
begin
  AbreForm(TF_ConFuncao,Self);
end;

procedure TPrincipal.ToolButton7Click(Sender: TObject);
begin
  AbreForm(TF_ConSetor,Self);
end;

procedure TPrincipal.ToolButton8Click(Sender: TObject);
begin
  xAux_ConEntidade_Ent_Tipo := -1;
  AbreForm(TF_ConEntidade,Self);
end;

end.
