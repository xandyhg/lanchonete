unit ConFuncao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TF_ConFuncao = class(TF_ConPadrao)
    FDFuncao: TFDQuery;
    ProviderFuncao: TDataSetProvider;
    CdsFuncao: TClientDataSet;
    DsFuncao: TDataSource;
    FDFuncaoFUN_ID: TIntegerField;
    FDFuncaoFUN_DESCRICAO: TStringField;
    CdsFuncaoFUN_ID: TIntegerField;
    CdsFuncaoFUN_DESCRICAO: TStringField;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConFuncao               : TF_ConFuncao;
  xAux_ConFuncao_Fun_ID     : Integer;
  xAux_ConFuncao_AtivaEnter : Boolean;

implementation

{$R *.dfm}

uses DMC, X, CadFuncao;

procedure TF_ConFuncao.FormActivate(Sender: TObject);
begin
  inherited;
  icoPesquisarClick(Sender);
end;

procedure TF_ConFuncao.icoFecharClick(Sender: TObject);
begin
  xAux_ConFuncao_Fun_ID := CdsFuncao.FieldByName('fun_id').AsInteger;
  inherited;

end;

procedure TF_ConFuncao.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConFuncao_Fun_ID := 0;
  AbreForm(TF_CadFuncao,Self);
  icoPesquisarClick(Sender);
  if xAux_ConFuncao_Fun_ID <> 0 then
    CdsFuncao.Locate('fun_id',xAux_ConFuncao_Fun_ID,[]);
end;

procedure TF_ConFuncao.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsFuncao.Close;

  FDFuncao.Close;
  FDFuncao.Params.Clear;
  FDFuncao.SQL.Clear;
  FDFuncao.SQL.Add('select *               '+
                   'from funcao            '+
                   'order by fun_descricao ');
  CdsFuncao.Open;

  if not CdsFuncao.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsFuncao.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConFuncao.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConFuncao_Fun_ID := CdsFuncao.FieldByName('fun_id').AsInteger;
  AbreForm(TF_CadFuncao, Self);
  icoPesquisarClick(Sender);
  if xAux_ConFuncao_Fun_ID <> 0 then
    CdsFuncao.Locate('fun_id',xAux_ConFuncao_Fun_ID,[]);
end;

procedure TF_ConFuncao.JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (key=13) and (JvDBGrid1.Focused) and (xAux_ConFuncao_AtivaEnter=True) then
    icoFecharClick(Sender);
end;

end.
