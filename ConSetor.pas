unit ConSetor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TF_ConSetor = class(TF_ConPadrao)
    FDSetor: TFDQuery;
    ProviderSetor: TDataSetProvider;
    CdsSetor: TClientDataSet;
    DsSetor: TDataSource;
    FDSetorSET_ID: TIntegerField;
    FDSetorSET_DESCRICAO: TStringField;
    CdsSetorSET_ID: TIntegerField;
    CdsSetorSET_DESCRICAO: TStringField;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConSetor               : TF_ConSetor;
  xAux_ConSetor_Set_ID     : Integer;
  xAux_ConSetor_AtivaEnter : Boolean;

implementation

{$R *.dfm}

uses DMC, X, CadSetor;

procedure TF_ConSetor.FormActivate(Sender: TObject);
begin
  inherited;
  icoPesquisarClick(Sender);
end;

procedure TF_ConSetor.icoFecharClick(Sender: TObject);
begin
  xAux_ConSetor_Set_ID := CdsSetor.FieldByName('set_id').AsInteger;
  inherited;

end;

procedure TF_ConSetor.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConSetor_Set_ID := 0;
  AbreForm(TF_CadSetor,Self);
  icoPesquisarClick(Sender);
  if xAux_ConSetor_Set_ID <> 0 then
    CdsSetor.Locate('set_id',xAux_ConSetor_Set_ID,[]);
end;

procedure TF_ConSetor.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsSetor.Close;

  FDSetor.Close;
  FDSetor.Params.Clear;
  FDSetor.SQL.Clear;
  FDSetor.SQL.Add('select *               '+
                  'from setor            '+
                  'order by set_descricao ');
  CdsSetor.Open;

  if not CdsSetor.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsSetor.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConSetor.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConSetor_Set_ID := CdsSetor.FieldByName('set_id').AsInteger;
  AbreForm(TF_CadSetor, Self);
  icoPesquisarClick(Sender);
  if xAux_ConSetor_Set_ID <> 0 then
    CdsSetor.Locate('set_id',xAux_ConSetor_Set_ID,[]);
end;

procedure TF_ConSetor.JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (key=13) and (JvDBGrid1.Focused) and (xAux_ConSetor_AtivaEnter=True) then
    icoFecharClick(Sender);
end;

end.
