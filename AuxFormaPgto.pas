unit AuxFormaPgto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AuxPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits;

type
  TF_AuxFormaPgto = class(TF_AuxPadrao)
    Label3: TLabel;
    edDinheiro: TJvCalcEdit;
    Label1: TLabel;
    edPix: TJvCalcEdit;
    edCartaoCredito: TJvCalcEdit;
    Label2: TLabel;
    edCartaoDebito: TJvCalcEdit;
    Label4: TLabel;
    edCreditoUtilizado: TJvCalcEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    edValorPago: TJvCalcEdit;
    edValorTotal: TJvCalcEdit;
    Label6: TLabel;
    edValorAcertar: TJvCalcEdit;
    GroupBox3: TGroupBox;
    edTroco: TJvCalcEdit;
    edCredito: TJvCalcEdit;
    Label7: TLabel;
    ckUtilizarCredito: TCheckBox;
    ckLancarTrocoCredito: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure ckUtilizarCreditoClick(Sender: TObject);
    procedure edDinheiroExit(Sender: TObject);
    procedure edPixExit(Sender: TObject);
    procedure edCartaoCreditoExit(Sender: TObject);
    procedure edCartaoDebitoExit(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CalcularValores;
  end;

var
  F_AuxFormaPgto                : TF_AuxFormaPgto;
  xAux_AuxFormaPgto_ValorTotal  : Double;
  xAux_AuxFormaPgto_Ent_ID      : Integer;
  xAux_AuxFormaPgto_Dinheiro    : Double;
  xAux_AuxFormaPgto_Pix         : Double;
  xAux_AuxFormaPgto_CarCred     : Double;
  xAux_AuxFormaPgto_CarDeb      : Double;
  xAux_AuxFormaPgto_CredUtil    : Double;
  xAux_AuxFormaPgto_CredTotal   : Double;
  xAux_AuxFormaPgto_VlrPago     : Double;
  xAux_AuxFormaPgto_Acertar     : Double;
  xAux_AuxFormaPgto_VlrTroco    : Double;
  xAux_AuxFormaPgto_LancarTroco : Boolean;

implementation

{$R *.dfm}

uses X, clsEntidade_Credito;

var
  EntCred : TEntidade_Credito;

procedure TF_AuxFormaPgto.CalcularValores;
begin
  edValorPago.Value := edDinheiro.Value+edPix.Value+edCartaoCredito.Value+edCartaoDebito.Value+edCreditoUtilizado.Value;
  if (edValorTotal.Value = edValorPago.Value) then begin
    edValorAcertar.Value := 0;
    edTroco.Value        := 0;
  end else if (edValorPago.Value > edValorTotal.Value) then begin
    edValorAcertar.Value := 0;
    edTroco.Value        := edValorPago.Value - edValorTotal.Value;
  end else if (edValorPago.Value < edValorTotal.Value) then begin
    edValorAcertar.Value := edValorTotal.Value - edValorPago.Value;
    edTroco.Value        := 0;
  end;
end;

procedure TF_AuxFormaPgto.ckUtilizarCreditoClick(Sender: TObject);
begin
  inherited;
  if ckUtilizarCredito.Checked then begin
    if edValorTotal.Value <= edCredito.Value then begin
      edCreditoUtilizado.Value := edValorTotal.Value;
    end else begin
      edCreditoUtilizado.Value := edCredito.Value;
    end;
  end else begin
    edCreditoUtilizado.Clear;
  end;
  CalcularValores;
end;

procedure TF_AuxFormaPgto.edCartaoCreditoExit(Sender: TObject);
begin
  inherited;
  CalcularValores;
end;

procedure TF_AuxFormaPgto.edCartaoDebitoExit(Sender: TObject);
begin
  inherited;
  CalcularValores;
end;

procedure TF_AuxFormaPgto.edDinheiroExit(Sender: TObject);
begin
  inherited;
  CalcularValores;
end;

procedure TF_AuxFormaPgto.edPixExit(Sender: TObject);
begin
  inherited;
  CalcularValores;
end;

procedure TF_AuxFormaPgto.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_AuxFormaPgto_ValorTotal > 0 then begin
    EntCred := TEntidade_Credito.Create;
    edValorTotal.Value   := xAux_AuxFormaPgto_ValorTotal;
    edValorAcertar.Value := xAux_AuxFormaPgto_ValorTotal;
    edDinheiro.Clear;
    edPix.Clear;
    edCartaoCredito.Clear;
    edCartaoDebito.Clear;
    edCreditoUtilizado.Clear;
    edCredito.Value := EntCred.CreditoSaldo(xAux_AuxFormaPgto_Ent_ID);
    if edCredito.Value > 0 then begin
      ckUtilizarCredito.Checked := True;
      ckUtilizarCreditoClick(Sender);
    end else begin
      ckUtilizarCredito.Checked := False;
      ckUtilizarCreditoClick(Sender);
    end;
    FreeAndNil(EntCred);
  end;
end;

procedure TF_AuxFormaPgto.icoCancelarClick(Sender: TObject);
begin
  inherited;
  xAux_AuxFormaPgto_ValorTotal  := -1;
  xAux_AuxFormaPgto_Dinheiro    := 0;
  xAux_AuxFormaPgto_Pix         := 0;
  xAux_AuxFormaPgto_CarCred     := 0;
  xAux_AuxFormaPgto_CarDeb      := 0;
  xAux_AuxFormaPgto_CredUtil    := 0;
  xAux_AuxFormaPgto_CredTotal   := 0;
  xAux_AuxFormaPgto_VlrPago     := 0;
  xAux_AuxFormaPgto_Acertar     := 0;
  xAux_AuxFormaPgto_VlrTroco    := 0;
  xAux_AuxFormaPgto_LancarTroco := False;
  Close;
end;

procedure TF_AuxFormaPgto.icoSalvarClick(Sender: TObject);
begin
  inherited;
  xAux_AuxFormaPgto_Dinheiro      := edDinheiro.Value;
  xAux_AuxFormaPgto_Pix           := edPix.Value;
  xAux_AuxFormaPgto_CarCred       := edCartaoCredito.Value;
  xAux_AuxFormaPgto_CarDeb        := edCartaoDebito.Value;
  xAux_AuxFormaPgto_CredUtil      := edCreditoUtilizado.Value;
  xAux_AuxFormaPgto_CredTotal     := edCredito.Value;
  xAux_AuxFormaPgto_VlrPago       := edValorPago.Value;
  xAux_AuxFormaPgto_Acertar       := edValorAcertar.Value;
  xAux_AuxFormaPgto_VlrTroco      := edTroco.Value;
  if ckLancarTrocoCredito.Checked = True then
    xAux_AuxFormaPgto_LancarTroco := True
  else
    xAux_AuxFormaPgto_LancarTroco := False;
  Close;
end;

end.
