unit clsProduto;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TProduto = class
  public
    pro_id           : Integer;
    pro_descricao    : String;
    pro_datacadastro : TDateTime;
    pro_valorcusto   : Double;
    pro_valorvenda   : Double;
    pro_codigobarras : String;
    pro_ativo        : Integer;

    function  Recuperar(Ppro_id:Integer):Boolean;
    procedure Gravar(Ppro_id:Integer);
    procedure Eliminar(Ppro_id:Integer);
    function  Ultima:Integer;
    function  RecuperarPorCodBarras(PPro_CodigoBarras:String):Boolean;
  end;

implementation

{ TProduto }

uses DMC;

procedure TProduto.Eliminar(Ppro_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Produto where pro_id = :K_pro_id');
  tq.ParamByName('K_pro_id').AsInteger := Ppro_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TProduto.Gravar(Ppro_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Produto where pro_id = :k_pro_id');
  tq.ParamByName('k_pro_id').AsInteger := Ppro_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Produto values('+
    ':k_pro_id,'+
    ':k_pro_descricao,'+
    ':k_pro_datacadastro,'+
    ':k_pro_valorcusto,'+
    ':k_pro_valorvenda,'+
    ':k_pro_codigobarras,'+
    ':k_pro_ativo)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Produto set '+
    'pro_descricao    = :k_pro_descricao,'+
    'pro_datacadastro = :k_pro_datacadastro,'+
    'pro_valorcusto   = :k_pro_valorcusto,'+
    'pro_valorvenda   = :k_pro_valorvenda,'+
    'pro_codigobarras = :k_pro_codigobarras,'+
    'pro_ativo        = :k_pro_ativo ');
    tq.SQL.Add('where pro_id = :K_pro_id ');
  end;

  tq.ParamByName('k_pro_id').AsInteger            := Ppro_id;
  tq.ParamByName('k_pro_descricao').AsString      := pro_descricao;
  tq.ParamByName('k_pro_datacadastro').AsDateTime := pro_datacadastro;
  tq.ParamByName('k_pro_valorcusto').AsFloat      := pro_valorcusto;
  tq.ParamByName('k_pro_valorvenda').AsFloat      := pro_valorvenda;
  tq.ParamByName('k_pro_codigobarras').AsString   := pro_codigobarras;
  tq.ParamByName('k_pro_ativo').AsInteger         := pro_ativo;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TProduto.Recuperar(Ppro_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Produto where pro_id = :K_pro_id');
  tq.ParamByName('k_pro_id').AsInteger := Ppro_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  pro_id           := tq.FieldByName('pro_id').AsInteger;
  pro_descricao    := tq.FieldByName('pro_descricao').AsString;
  pro_datacadastro := tq.FieldByName('pro_datacadastro').AsDateTime;
  pro_valorcusto   := tq.FieldByName('pro_valorcusto').AsFloat;
  pro_valorvenda   := tq.FieldByName('pro_valorvenda').AsFloat;
  pro_codigobarras := tq.FieldByName('pro_codigobarras').AsString;
  pro_ativo        := tq.FieldByName('pro_ativo').AsInteger;

  tq.Close;
  tq.Free;
end;

function TProduto.RecuperarPorCodBarras(PPro_CodigoBarras: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Produto where pro_codigobarras = :K_pro_codigobarras');
  tq.ParamByName('k_pro_codigobarras').AsString := Ppro_codigobarras;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('pro_id').AsInteger);

  tq.Free;
  tq.Close;
end;

function TProduto.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(pro_id) maior from Produto');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
