inherited F_ConFuncao: TF_ConFuncao
  Caption = 'Consulta de Fun'#231#227'o'
  ClientWidth = 905
  ExplicitWidth = 921
  ExplicitHeight = 461
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Width = 905
    ExplicitWidth = 905
    inherited icoPesquisar: TImage
      Left = 485
      ExplicitLeft = 485
    end
    inherited icoNovo: TImage
      Left = 590
      ExplicitLeft = 590
    end
    inherited icoImprimir: TImage
      Left = 695
      ExplicitLeft = 695
    end
    inherited icoFechar: TImage
      Left = 800
      ExplicitLeft = 800
    end
  end
  inherited StatusBar1: TStatusBar
    Width = 905
    ExplicitWidth = 905
  end
  inherited Panel3: TPanel
    Width = 905
    ExplicitWidth = 905
    inherited JvDBGrid1: TJvDBGrid
      Width = 905
      DataSource = DsFuncao
      OnDblClick = JvDBGrid1DblClick
      OnKeyDown = JvDBGrid1KeyDown
      TitleRowHeight = 21
      Columns = <
        item
          Expanded = False
          FieldName = 'FUN_ID'
          Title.Caption = 'C'#243'digo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FUN_DESCRICAO'
          Title.Caption = 'Descri'#231#227'o'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 500
          Visible = True
        end>
    end
  end
  inherited Panel4: TPanel
    Width = 905
    ExplicitWidth = 905
    inherited pnPesquisar: TPanel
      Left = 485
      ExplicitLeft = 485
    end
    inherited pnNovo: TPanel
      Left = 590
      ExplicitLeft = 590
    end
    inherited pnImprimir: TPanel
      Left = 695
      ExplicitLeft = 695
    end
    inherited pnFechar: TPanel
      Left = 800
      ExplicitLeft = 800
    end
  end
  inherited Panel1: TPanel
    Width = 905
    ExplicitWidth = 905
  end
  object FDFuncao: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      'select *'
      'from funcao'
      'order by fun_descricao')
    Left = 328
    Top = 8
    object FDFuncaoFUN_ID: TIntegerField
      FieldName = 'FUN_ID'
      Origin = 'FUN_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDFuncaoFUN_DESCRICAO: TStringField
      FieldName = 'FUN_DESCRICAO'
      Origin = 'FUN_DESCRICAO'
      Size = 200
    end
  end
  object ProviderFuncao: TDataSetProvider
    DataSet = FDFuncao
    Left = 360
    Top = 8
  end
  object CdsFuncao: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderFuncao'
    Left = 392
    Top = 8
    object CdsFuncaoFUN_ID: TIntegerField
      FieldName = 'FUN_ID'
      Required = True
    end
    object CdsFuncaoFUN_DESCRICAO: TStringField
      FieldName = 'FUN_DESCRICAO'
      Size = 200
    end
  end
  object DsFuncao: TDataSource
    DataSet = CdsFuncao
    Left = 424
    Top = 8
  end
end
