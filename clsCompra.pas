unit clsCompra;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TCompra = class
  public
    com_id             : Integer;
    ent_id             : Integer;
    com_nf             : String;
    com_datacadastro   : TDateTime;
    com_valortotal     : Double;
    com_obs            : String;

    function  Recuperar(Pcom_id:Integer):Boolean;
    procedure Gravar(Pcom_id:Integer);
    procedure Eliminar(Pcom_id:Integer);
    function  Ultima:Integer;
  end;

implementation

{ TCompra }

uses DMC;

procedure TCompra.Eliminar(Pcom_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Compra where com_id = :K_com_id');
  tq.ParamByName('K_com_id').AsInteger := Pcom_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TCompra.Gravar(Pcom_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Compra where com_id = :k_com_id');
  tq.ParamByName('k_com_id').AsInteger := Pcom_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Compra values('+
    ':k_com_id,'+
    ':k_ent_id,'+
    ':k_com_nf,'+
    ':k_com_datacadastro,'+
    ':k_com_valortotal,'+
    ':k_com_obs)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Compra set '+
    'ent_id             = :k_ent_id,'+
    'com_nf             = :k_com_nf,'+
    'com_datacadastro   = :k_com_datacadastro,'+
    'com_valortotal     = :k_com_valortotal,'+
    'com_obs            = :k_com_obs ');
    tq.SQL.Add('where com_id = :K_com_id ');
  end;

  tq.ParamByName('k_com_id').AsInteger            := Pcom_id;
  tq.ParamByName('k_ent_id').AsInteger            := ent_id;
  tq.ParamByName('k_com_nf').AsString             := com_nf;
  tq.ParamByName('k_com_datacadastro').AsDateTime := com_datacadastro;
  tq.ParamByName('k_com_valortotal').AsFloat      := com_valortotal;
  tq.ParamByName('k_com_obs').AsString            := com_obs;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TCompra.Recuperar(Pcom_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Compra where com_id = :K_com_id');
  tq.ParamByName('k_com_id').AsInteger := Pcom_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  com_id             := tq.FieldByName('com_id').AsInteger;
  ent_id             := tq.FieldByName('ent_id').AsInteger;
  com_nf             := tq.FieldByName('com_nf').AsString;
  com_datacadastro   := tq.FieldByName('com_datacadastro').AsDateTime;
  com_valortotal     := tq.FieldByName('com_valortotal').AsFloat;
  com_obs            := tq.FieldByName('com_obs').AsString;

  tq.Close;
  tq.Free;
end;

function TCompra.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(com_id) maior from Compra');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
