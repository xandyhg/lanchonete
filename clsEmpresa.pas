unit clsEmpresa;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TEmpresa = class
  public
    emp_id              : Integer;
    emp_razao           : String;
    emp_fantasia        : String;
    emp_cnpj            : String;
    emp_assinatura      : String;
    emp_endLogradouro   : String;
    emp_endBairro       : String;
    emp_endCep          : String;
    emp_endNumero       : String;
    emp_endComplemento  : String;
    cid_id              : Integer;
    emp_ativo           : Integer; //0-Sim 1-N�o
    emp_telefone        : String;

    function  Recuperar(Pemp_id:Integer):Boolean;
    procedure Gravar(Pemp_id:Integer);
    procedure Eliminar(Pemp_id:Integer);
    function  Ultima:Integer;
  end;

implementation

{ TEmpresa }

uses DMC;

procedure TEmpresa.Eliminar(Pemp_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Empresa where emp_id = :K_emp_id');
  tq.ParamByName('K_emp_id').AsInteger := Pemp_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TEmpresa.Gravar(Pemp_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Empresa where emp_id = :k_emp_id');
  tq.ParamByName('k_emp_id').AsInteger := Pemp_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Empresa values('+
    ':k_emp_id,'+
    ':k_emp_razao,'+
    ':k_emp_fantasia,'+
    ':k_emp_cnpj,'+
    ':k_emp_assinatura,'+
    ':k_emp_endLogradouro,'+
    ':k_emp_endBairro,'+
    ':k_emp_endCep,'+
    ':k_emp_endNumero,'+
    ':k_emp_endComplemento,'+
    ':k_cid_id,'+
    ':k_emp_ativo,'+
    ':k_emp_telefone)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Empresa set '+
    'emp_razao          = :k_emp_razao,'+
    'emp_fantasia       = :k_emp_fantasia,'+
    'emp_cnpj           = :k_emp_cnpj,'+
    'emp_assinatura     = :k_emp_assinatura,'+
    'emp_endLogradouro  = :k_emp_endLogradouro,'+
    'emp_endBairro      = :k_emp_endBairro,'+
    'emp_endCep         = :k_emp_endCep,'+
    'emp_endNumero      = :k_emp_endNumero,'+
    'emp_endComplemento = :k_emp_endComplemento,'+
    'cid_id             = :k_cid_id,'+
    'emp_ativo          = :k_emp_ativo,'+
    'emp_telefone       = :k_emp_telefone ');
    tq.SQL.Add('where emp_id = :K_emp_id');
  end;

  tq.ParamByName('k_emp_id').AsInteger            := Pemp_id;
  tq.ParamByName('k_emp_razao').AsString          := emp_razao;
  tq.ParamByName('k_emp_fantasia').AsString       := emp_fantasia;
  tq.ParamByName('k_emp_cnpj').AsString           := emp_cnpj;
  tq.ParamByName('k_emp_assinatura').AsString     := emp_assinatura;
  tq.ParamByName('k_emp_endLogradouro').AsString  := emp_endLogradouro;
  tq.ParamByName('k_emp_endBairro').AsString      := emp_endBairro;
  tq.ParamByName('k_emp_endCep').AsString         := emp_endCep;
  tq.ParamByName('k_emp_endNumero').AsString      := emp_endNumero;
  tq.ParamByName('k_emp_endComplemento').AsString := emp_endComplemento;
  if Cid_ID > 0 then
    tq.ParamByName('k_Cid_ID').AsInteger          := Cid_ID
  else
    tq.ParamByName('k_Cid_ID').IsNull;
  tq.ParamByName('k_emp_ativo').AsInteger         := emp_ativo;
  tq.ParamByName('k_emp_telefone').AsString       := emp_telefone;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TEmpresa.Recuperar(Pemp_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Empresa where emp_id = :K_emp_id');
  tq.ParamByName('k_emp_id').AsInteger := Pemp_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  emp_id             := tq.FieldByName('emp_id').AsInteger;
  emp_razao          := tq.FieldByName('emp_razao').AsString;
  emp_fantasia       := tq.FieldByName('emp_fantasia').AsString;
  emp_cnpj           := tq.FieldByName('emp_cnpj').AsString;
  emp_assinatura     := tq.FieldByName('emp_assinatura').AsString;
  emp_endLogradouro  := tq.FieldByName('emp_endLogradouro').AsString;
  emp_endBairro      := tq.FieldByName('emp_endBairro').AsString;
  emp_endCep         := tq.FieldByName('emp_endCep').AsString;
  emp_endNumero      := tq.FieldByName('emp_endNumero').AsString;
  emp_endComplemento := tq.FieldByName('emp_endComplemento').AsString;
  cid_id             := tq.FieldByName('cid_id').AsInteger;
  emp_ativo          := tq.FieldByName('emp_ativo').AsInteger;
  emp_telefone       := tq.FieldByName('emp_telefone').AsString;

  tq.Close;
  tq.Free;
end;

function TEmpresa.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(emp_id) maior from Empresa');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
