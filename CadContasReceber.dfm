inherited F_CadContasReceber: TF_CadContasReceber
  Caption = ' Lan'#231'amento de Contas a Receber'
  ClientHeight = 422
  ClientWidth = 961
  OnActivate = FormActivate
  ExplicitWidth = 977
  ExplicitHeight = 461
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 345
    Width = 961
    ExplicitTop = 345
    ExplicitWidth = 961
    inherited icoFechar: TImage
      Left = 871
      ExplicitLeft = 711
    end
    inherited icoAlterar: TImage
      Left = 691
      ExplicitLeft = 531
    end
    inherited icoExcluir: TImage
      Left = 781
      ExplicitLeft = 621
    end
    inherited icoSalvar: TImage
      Left = 511
      OnClick = icoSalvarClick
      ExplicitLeft = 351
    end
    inherited icoCancelar: TImage
      Left = 601
      ExplicitLeft = 441
    end
  end
  inherited Panel3: TPanel
    Width = 961
    Height = 304
    Enabled = True
    ExplicitWidth = 961
    ExplicitHeight = 304
    object Label3: TLabel
      Left = 23
      Top = 11
      Width = 27
      Height = 17
      Alignment = taRightJustify
      Caption = 'Data'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 308
      Top = 11
      Width = 30
      Height = 17
      Alignment = taRightJustify
      Caption = 'Valor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 164
      Top = 11
      Width = 30
      Height = 17
      Alignment = taRightJustify
      Caption = 'Venc.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton2: TSpeedButton
      Left = 871
      Top = 43
      Width = 85
      Height = 28
      Caption = 'Remover'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFE9EAF8A9AFE48890DC868EDBA4AAE3E1E2F5FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB4E55560D23544D638
        47DC3747DD3544D84D59D1A4A9E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF8088D83848DE3F4FEE3F50EF3F4FED3E4FED3F50EF3F50EE3A49E16973
        D3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAEB3E63B4AE44051F03C4CE73C4CE73C
        4CE73C4CE73C4CE73C4CE74050F03D4DE89199DFFFFFFFFFFFFFFFFFFFFAFAFD
        3645DA4051F13D4DE93D4DEA3D4DEA3D4DEA3D4DEA3D4DEA3D4EEA3D4DE94051
        F03848DFEBECF9FFFFFFFFFFFFBCC1EC3F50EB3C4CE91B2DDC1629DB192BDC19
        2BDC192BDC192BDC172ADB172ADB3848E64252EEA0A7E6FFFFFFFFFFFF8F97E5
        3C4DF13E4EE67880D38188CF7C84D17C84D17C84D17C84D17E86D17E85D14453
        E43A4BF17B85E4FFFFFFFFFFFF8690EC2F41EA4453E7FFFFF2FFFFF3FFFFF3FF
        FFF3FFFFF3FFFFF3FFFFF3FFFFF35A67E92639E87883EDFFFFFFFFFFFF919AF3
        3646E63F4FE79AA1EAA7ADEAA0A7EAA0A7EAA0A7EAA0A7EAA3AAEAA3A9EA4857
        E83243E67E88F0FFFFFFFFFFFFB4BAF63E4EE73B4BE71F31E61B2EE61D30E61D
        30E61D30E61D30E61C2FE61C2FE63849E73F4FE79AA2F3FFFFFFFFFFFFF3F4FD
        3C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4C
        E73C4CE7E0E3FBFFFFFFFFFFFFFFFFFF949DF23C4CE73C4CE73C4CE73C4CE73C
        4CE73C4CE73C4CE73C4CE73C4CE73C4CE77883EEFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF6A76ED3C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE73C4CE75765
        EAFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8C96F13D4DE73C4CE73C
        4CE73C4CE73C4CE73C4CE7828CF0F7F8FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFEFF0FDB6BCF6919AF18F99F1B1B7F5E7E9FCFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      OnClick = SpeedButton2Click
    end
    object SpeedButton3: TSpeedButton
      Left = 871
      Top = 80
      Width = 85
      Height = 28
      Caption = 'Alterar'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCFCFA
        F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFC
        FCFBF8F7F5F3F0EEF4F2EDF5EFE5E5DED7C1B7C1DC8E48D7C3B8FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFEFDFDFCFAF9F9F9F8FCFBF8E0D7CE949CD0416B
        D70082FFBFAB8DFAF4EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFE8C7A2781B0D003CDE0067FF5FC7FFB8F2FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9AE987B0000980A00A226017AA8
        BF9AFEFFF8FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDA
        A5978B0C00AB2900B52600B71000E7A000B7FBF6FCFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFDDAC97912300C24000D44F00C96400D78F00F091
        00E6BC6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE9E97882600CC
        4700E66700E29700EDC200F0B800F5D659FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFDDC9BA570000CB4700ED6100E79A00F5DA00FADC00FCEB90FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD0D5D6330000C42200E84400E3
        7C00F6CD00FBE305FEF4BEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        D8D7D6001B3174402BDE1000DD4500EBA200F6CE00FEF2ACFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFDDDCD80210204E63778A928AAA5A1CEC6E00F6
        A800FAE856FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8E9F30B1A2F
        59686E7F7A789E9F9EF9FFFFB49E5BD7BC58FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF7C84CD2B3E7890845FA09B96FFFFFFDADDDDBAC0CDFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E4F90000CB
        91A0CBFFFFF3D0CEC7BCBABAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFE1E4FFC4E6FFCBD7FFCCCBD1FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentFont = False
      Visible = False
      OnClick = SpeedButton3Click
    end
    object SpeedButton4: TSpeedButton
      Left = 871
      Top = 117
      Width = 85
      Height = 28
      Caption = 'Cancelar'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000120B0000120B00000000000000000000FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFB00D9D9E5009D9DD5008585D3008A8AD500AAAA
        D800E8E8E400FFFFF900FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00DDDDEB005353C8000404B9000000B9000000C0000000C9000000
        D1002929D7007E7ED500F5F5E500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00C7C7EB001010BD000000B7000000C0000B0BC9002E2ED8002F2FD9001515
        E0001616F2001111F2004949DB00E7E7E400FFFFFF00FFFFFF00FFFFFF00E6E6
        F4000A0AC2000000AE000000BA006767D900C7C7E800F1F1F400EAEAF100D0D0
        ED005858DF002929F2002020FF004B4BDB00F7F7EA00FFFFFF00FFFFFD005555
        D0000000AE000000B6009595DC00FFFFFC00FFFFFF00FFFFFF00FFFFFF00EEEE
        F1003C3CDC001313EF002828F1001010EE008F8FDA00FFFFFB00D9D9EF000000
        B2000000AA006666D100FFFFFF00FFFFFF00FFFFFF00FFFFFF00CECEEF001414
        DB000000E1004E4EDA005E5EE0001111F2003030D600F2F2E900A7A7E3000000
        B5000000AD00E1E1ED00FFFFFF00FFFFFF00FFFFFF00CCCCEF001010D0000000
        CF002323DA00ECECED00CECEED000D0DE0000404CF00B8B8DA008080D8001F1F
        CF004141C300FFFFF600FFFFFF00FFFFFF00CECEEF000D0DC8000000C1001919
        D100DCDCED00FFFFFF00E0E0EF002424D5000000C7009A9AD7007B7BD6002222
        CE006262CA00FFFFF600FFFFFF00C8C8EC000A0ABF000000B6001515C900DBDB
        EE00FFFFFF00FFFFFF00E6E6EF001F1FD1000000BD009696D7009595DF002121
        C4003B3BC600FFFFF300E3E3F9001D1DC7000000B3001515C300DADAEE00FFFF
        FF00FFFFFF00FFFFFF00C0C0E6000101C4000000B900B0B0D800CACAF0003131
        C7003131C1008080CF005252D2000D0DC4003232CD00D9D9EE00FFFFFF00FFFF
        FF00FFFFFF00FFFFFC006565D7000404C8002424C300EAEAED00FCFCFB006969
        DE002B2BC0002A2AC4002121C0005151D100EBEBF600FFFFFF00FFFFFF00FFFF
        FF00FFFFFB009898DE001B1BC7000808C6007B7BD500FFFFFD00FFFFFF00D3D3
        F5003939D3002D2DBB002A2AC4008080CB00F6F6EE00FCFCF100FAFAF300D2D2
        E8007979D8001919C4000C0CC2003434CA00EAEAEF00FFFFFF00FFFFFF00FFFF
        FC00B9B9F0003939D3002A2ABE002F2FC2004040C8005858C5005252C7002F2F
        C5001010C0001414C3003737D000D5D5EE00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00CFCFF6006767DD003232C5002222C4002424D5002121D3001C1C
        C1003030C8006D6DDA00E5E5F100FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFD00C2C2EE009292DF007B7BD3007E7ED4009C9C
        E300D0D0EC00FFFFFC00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
      ParentFont = False
      Visible = False
    end
    object Label5: TLabel
      Left = 691
      Top = 11
      Width = 66
      Height = 17
      Alignment = taRightJustify
      Caption = 'Pagamento'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 693
      Top = 37
      Width = 64
      Height = 17
      Alignment = taRightJustify
      Caption = 'Valor Pago'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCr_datacadastro: TJvDateEdit
      Left = 56
      Top = 8
      Width = 100
      Height = 25
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 0
    end
    object edCr_valor: TJvCalcEdit
      Left = 344
      Top = 8
      Width = 100
      Height = 25
      DisplayFormat = '#,##0.00'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 2
      DecimalPlacesAlwaysShown = False
    end
    object edCr_datavencimento: TJvDateEdit
      Left = 200
      Top = 8
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 1
    end
    object GroupBox1: TGroupBox
      Left = 450
      Top = 59
      Width = 415
      Height = 86
      Caption = 'Observa'#231#227'o do Pagamento'
      TabOrder = 4
      object mCrp_obs: TMemo
        Left = 2
        Top = 19
        Width = 411
        Height = 65
        Align = alClient
        TabOrder = 0
      end
    end
    object GroupBox2: TGroupBox
      Left = 56
      Top = 39
      Width = 388
      Height = 106
      Caption = 'Observa'#231#227'o da Conta'
      TabOrder = 3
      object mCr_obs: TMemo
        Left = 2
        Top = 19
        Width = 384
        Height = 85
        Align = alClient
        CharCase = ecUpperCase
        TabOrder = 0
      end
    end
    object Button1: TButton
      Left = 871
      Top = 6
      Width = 85
      Height = 28
      Caption = 'Adicionar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 4
      ImageMargins.Left = 2
      Images = Principal.img16
      ParentFont = False
      TabOrder = 5
      OnClick = Button1Click
    end
    object GroupBox3: TGroupBox
      Left = 58
      Top = 151
      Width = 898
      Height = 146
      Caption = 'Detalhes do(s) Pagamento(s)'
      TabOrder = 6
      object JvDBGrid1: TJvDBGrid
        Left = 2
        Top = 19
        Width = 894
        Height = 125
        Align = alClient
        DataSource = DsPagamentos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        SelectColumnsDialogStrings.Caption = 'Select columns'
        SelectColumnsDialogStrings.OK = '&OK'
        SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
        EditControls = <>
        RowsHeight = 21
        TitleRowHeight = 21
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'crp_seq'
            Title.Alignment = taCenter
            Title.Caption = 'Seq.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'crp_valorpago'
            Title.Caption = 'Valor Pago'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'crp_datapagamento'
            Title.Alignment = taCenter
            Title.Caption = 'Pagamento'
            Width = 77
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'crp_databaixa'
            Title.Alignment = taCenter
            Title.Caption = 'Data Baixa'
            Width = 77
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'crp_tipo_pgto'
            Title.Alignment = taCenter
            Title.Caption = 'Tipo'
            Width = 146
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'crp_obs'
            Title.Caption = 'Observa'#231#227'o'
            Width = 441
            Visible = True
          end>
      end
    end
    object edCrp_valorpago: TJvCalcEdit
      Left = 763
      Top = 34
      Width = 100
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 7
      DecimalPlacesAlwaysShown = False
    end
    object edCrp_datapagamento: TJvDateEdit
      Left = 763
      Top = 8
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 8
    end
    object rgCrp_tipo_pgto: TRadioGroup
      Left = 450
      Top = 0
      Width = 221
      Height = 59
      Caption = 'Tipo Pagamento'
      Columns = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Dinheiro'
        'Pix'
        'Cart'#227'o D'#233'bito'
        'Cart'#227'o Cr'#233'dito')
      ParentFont = False
      TabOrder = 9
    end
  end
  inherited Panel4: TPanel
    Top = 390
    Width = 961
    ExplicitTop = 390
    ExplicitWidth = 961
    inherited pnCancelar: TPanel
      Left = 601
      ExplicitLeft = 601
    end
    inherited pnAlterar: TPanel
      Left = 691
      ExplicitLeft = 691
    end
    inherited pnExcluir: TPanel
      Left = 781
      ExplicitLeft = 781
    end
    inherited pnSalvar: TPanel
      Left = 511
      ExplicitLeft = 511
    end
    inherited pnFechar: TPanel
      Left = 871
      ExplicitLeft = 871
    end
  end
  inherited Panel5: TPanel
    Width = 961
    ExplicitWidth = 961
    object Label1: TLabel
      Left = 7
      Top = 12
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 210
      Top = 12
      Width = 36
      Height = 17
      Alignment = taRightJustify
      Caption = 'Venda'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 118
      Top = 12
      Width = 22
      Height = 17
      Caption = 'Seq'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 768
      Top = 12
      Width = 35
      Height = 17
      Alignment = taRightJustify
      Caption = 'Status'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 595
      Top = 12
      Width = 61
      Height = 17
      Alignment = taRightJustify
      Caption = 'Data Baixa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 56
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object edVenda_id: TJvCalcEdit
      Left = 250
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 1
      DecimalPlacesAlwaysShown = False
    end
    object edCrp_seq: TJvCalcEdit
      Left = 146
      Top = 9
      Width = 56
      Height = 25
      TabStop = False
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 2
      DecimalPlacesAlwaysShown = False
    end
    object cbCr_Status: TComboBox
      Left = 809
      Top = 9
      Width = 145
      Height = 25
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Items.Strings = (
        'ABERTO'
        'LIQUIDADO'
        'PARCIAL')
    end
    object edCrp_dataBaixa: TJvDateEdit
      Left = 662
      Top = 9
      Width = 100
      Height = 25
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 4
    end
  end
  inherited Timer1: TTimer
    Left = 272
  end
  object CdsPagamentos: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    Params = <>
    Left = 448
    Top = 8
    object CdsPagamentoscr_id: TIntegerField
      FieldName = 'cr_id'
    end
    object CdsPagamentoscrp_seq: TIntegerField
      FieldName = 'crp_seq'
    end
    object CdsPagamentoscrp_valorpago: TFloatField
      FieldName = 'crp_valorpago'
      DisplayFormat = '#,##0.00'
    end
    object CdsPagamentoscrp_datapagamento: TDateField
      FieldName = 'crp_datapagamento'
    end
    object CdsPagamentoscrp_tipo_pgto: TIntegerField
      FieldName = 'crp_tipo_pgto'
    end
    object CdsPagamentoscrp_obs: TBlobField
      FieldName = 'crp_obs'
      Size = 80
    end
    object CdsPagamentoscrp_databaixa: TDateField
      FieldName = 'crp_databaixa'
    end
    object CdsPagamentostotPago: TAggregateField
      FieldName = 'totPago'
      Active = True
      DisplayName = ''
      DisplayFormat = '#,##0.00'
      Expression = 'sum(crp_valorpago)'
    end
  end
  object DsPagamentos: TDataSource
    DataSet = CdsPagamentos
    Left = 488
    Top = 8
  end
end
