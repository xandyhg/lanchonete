inherited F_CadProduto: TF_CadProduto
  Caption = ' CADASTRO DE PRODUTO'
  ClientHeight = 251
  ClientWidth = 687
  OnActivate = FormActivate
  ExplicitWidth = 703
  ExplicitHeight = 290
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 174
    Width = 687
    ExplicitTop = 174
    ExplicitWidth = 687
    inherited icoFechar: TImage
      Left = 597
      ExplicitLeft = 358
    end
    inherited icoAlterar: TImage
      Left = 417
      OnClick = icoAlterarClick
      ExplicitLeft = 178
    end
    inherited icoExcluir: TImage
      Left = 507
      OnClick = icoExcluirClick
      ExplicitLeft = 268
    end
    inherited icoSalvar: TImage
      Left = 237
      OnClick = icoSalvarClick
      ExplicitLeft = -2
    end
    inherited icoCancelar: TImage
      Left = 327
      OnClick = icoCancelarClick
      ExplicitLeft = 88
    end
  end
  inherited Panel3: TPanel
    Width = 687
    Height = 133
    ExplicitWidth = 687
    ExplicitHeight = 133
    object Label2: TLabel
      Left = 202
      Top = 71
      Width = 57
      Height = 17
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 7
      Top = 71
      Width = 68
      Height = 17
      Alignment = taRightJustify
      Caption = 'C'#243'd. Barras'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 19
      Top = 102
      Width = 56
      Height = 17
      Alignment = taRightJustify
      Caption = 'Vlr. Custo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 200
      Top = 102
      Width = 59
      Height = 17
      Alignment = taRightJustify
      Caption = 'Vlr. Venda'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edPro_Descricao: TEdit
      Left = 265
      Top = 68
      Width = 412
      Height = 25
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object edPro_CodigoBarras: TEdit
      Left = 81
      Top = 68
      Width = 113
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      MaxLength = 13
      NumbersOnly = True
      ParentFont = False
      TabOrder = 1
      OnExit = edPro_CodigoBarrasExit
    end
    object edPro_ValorCusto: TJvCalcEdit
      Left = 81
      Top = 99
      Width = 113
      Height = 25
      DecimalPlaces = 5
      DisplayFormat = '#,##0.00000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 3
      DecimalPlacesAlwaysShown = False
      OnExit = edPro_ValorCustoExit
    end
    object edPro_ValorVenda: TJvCalcEdit
      Left = 265
      Top = 99
      Width = 109
      Height = 25
      DisplayFormat = '#,##0.00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowButton = False
      TabOrder = 4
      DecimalPlacesAlwaysShown = False
      OnExit = edPro_ValorVendaExit
    end
    object ckPro_Ativo: TCheckBox
      Left = 629
      Top = 103
      Width = 48
      Height = 17
      Caption = 'Ativo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 685
      Height = 58
      Align = alTop
      Caption = 'Dados para C'#225'lculo dos Valores de Custo e Venda'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label7: TLabel
        Left = 264
        Top = 24
        Width = 62
        Height = 17
        Alignment = taRightJustify
        Caption = 'Valor Total'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 138
        Top = 25
        Width = 63
        Height = 17
        Alignment = taRightJustify
        Caption = 'Qtde Unid.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 10
        Top = 24
        Width = 64
        Height = 17
        Alignment = taRightJustify
        Caption = 'Qtde Caixa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 408
        Top = 24
        Width = 51
        Height = 17
        Alignment = taRightJustify
        Caption = '% Venda'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object edValorTotalItem: TJvCalcEdit
        Left = 332
        Top = 21
        Width = 70
        Height = 25
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 2
        DecimalPlacesAlwaysShown = False
        OnExit = edValorTotalItemExit
      end
      object edIC_Qtde: TJvCalcEdit
        Left = 207
        Top = 21
        Width = 51
        Height = 25
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        MinValue = 1.000000000000000000
        ParentFont = False
        ShowButton = False
        TabOrder = 1
        Value = 1.000000000000000000
        DecimalPlacesAlwaysShown = False
        OnExit = edIC_QtdeExit
      end
      object edQtdeCaixa: TJvCalcEdit
        Left = 80
        Top = 21
        Width = 51
        Height = 25
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 0
        DecimalPlacesAlwaysShown = False
        OnExit = edQtdeCaixaExit
      end
      object edPercVenda: TJvCalcEdit
        Left = 465
        Top = 21
        Width = 56
        Height = 25
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 3
        DecimalPlacesAlwaysShown = False
        OnExit = edPercVendaExit
      end
    end
  end
  inherited Panel4: TPanel
    Top = 219
    Width = 687
    ExplicitTop = 219
    ExplicitWidth = 687
    inherited pnCancelar: TPanel
      Left = 327
      ExplicitLeft = 327
    end
    inherited pnAlterar: TPanel
      Left = 417
      ExplicitLeft = 417
    end
    inherited pnExcluir: TPanel
      Left = 507
      ExplicitLeft = 507
    end
    inherited pnSalvar: TPanel
      Left = 237
      ExplicitLeft = 237
    end
    inherited pnFechar: TPanel
      Left = 597
      ExplicitLeft = 597
    end
  end
  inherited Panel5: TPanel
    Width = 687
    ExplicitWidth = 687
    object Label1: TLabel
      Left = 32
      Top = 13
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 517
      Top = 13
      Width = 53
      Height = 17
      Alignment = taRightJustify
      Caption = 'Cadastro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 81
      Top = 10
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
    object edPro_DataCadastro: TJvDateEdit
      Left = 576
      Top = 10
      Width = 100
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ShowNullDate = False
      TabOrder = 1
    end
  end
  inherited Timer1: TTimer
    Left = 208
  end
end
