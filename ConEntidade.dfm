inherited F_ConEntidade: TF_ConEntidade
  Caption = 'Consulta de Entidade'
  ClientHeight = 521
  ClientWidth = 1182
  ExplicitWidth = 1198
  ExplicitHeight = 560
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 408
    Width = 1182
    ExplicitTop = 408
    ExplicitWidth = 1182
    inherited icoPesquisar: TImage
      Left = 762
      ExplicitLeft = 762
    end
    inherited icoNovo: TImage
      Left = 867
      ExplicitLeft = 867
    end
    inherited icoImprimir: TImage
      Left = 972
      ExplicitLeft = 972
    end
    inherited icoFechar: TImage
      Left = 1077
      ExplicitLeft = 1077
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 485
    Width = 1182
    ExplicitTop = 485
    ExplicitWidth = 1182
  end
  inherited Panel3: TPanel
    Top = 89
    Width = 1182
    Height = 319
    ExplicitTop = 89
    ExplicitWidth = 1182
    ExplicitHeight = 319
    inherited JvDBGrid1: TJvDBGrid
      Width = 1182
      Height = 319
      DataSource = DsEntidade
      OnDrawColumnCell = JvDBGrid1DrawColumnCell
      OnDblClick = JvDBGrid1DblClick
      OnKeyDown = JvDBGrid1KeyDown
      TitleRowHeight = 21
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'ENT_ATIVO'
          Title.Alignment = taCenter
          Title.Caption = 'Ativo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_ID'
          Title.Caption = 'C'#243'digo'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_NOME'
          Title.Caption = 'Nome'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 345
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_DATACADASTRO'
          Title.Caption = 'Cadastro'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 134
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_CELULAR'
          Title.Caption = 'Celular'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_TELEFONE'
          Title.Caption = 'Telefone'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_EMAIL'
          Title.Caption = 'Email'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 192
          Visible = True
        end>
    end
  end
  inherited Panel4: TPanel
    Top = 453
    Width = 1182
    ExplicitTop = 453
    ExplicitWidth = 1182
    inherited pnPesquisar: TPanel
      Left = 762
      ExplicitLeft = 762
      ExplicitHeight = 32
    end
    inherited pnNovo: TPanel
      Left = 867
      ExplicitLeft = 867
      ExplicitHeight = 32
    end
    inherited pnImprimir: TPanel
      Left = 972
      ExplicitLeft = 972
      ExplicitHeight = 32
    end
    inherited pnFechar: TPanel
      Left = 1077
      ExplicitLeft = 1077
      ExplicitHeight = 32
    end
  end
  inherited Panel1: TPanel
    Width = 1182
    Height = 89
    ExplicitWidth = 1182
    ExplicitHeight = 89
    object rgEnt_Ativo: TRadioGroup
      Left = 0
      Top = 0
      Width = 65
      Height = 89
      Align = alLeft
      Caption = 'Ativo'
      ItemIndex = 2
      Items.Strings = (
        'Sim'
        'N'#227'o'
        'Todos')
      TabOrder = 0
    end
    object rgEnt_Tipo: TRadioGroup
      Left = 65
      Top = 0
      Width = 96
      Height = 89
      Align = alLeft
      Caption = 'Tipo'
      ItemIndex = 3
      Items.Strings = (
        'Cliente'
        'Fornecedor'
        'Parceiro'
        'Todos')
      TabOrder = 1
    end
    object GroupBox1: TGroupBox
      Left = 161
      Top = 0
      Width = 267
      Height = 89
      Align = alLeft
      Caption = 'Pesquisar por'
      TabOrder = 2
      object edDescricao: TEdit
        Left = 10
        Top = 53
        Width = 247
        Height = 25
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnKeyDown = edDescricaoKeyDown
      end
      object cbPesquisar: TComboBox
        Left = 10
        Top = 22
        Width = 160
        Height = 25
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ItemIndex = 7
        ParentFont = False
        TabOrder = 1
        Text = 'Nome / Raz'#227'o Social'
        OnChange = cbPesquisarChange
        Items.Strings = (
          'Celular'
          'Celular Contato'
          'CNPJ'
          'C'#243'digo'
          'CPF'
          'E-mail'
          'Limite'
          'Nome / Raz'#227'o Social'
          'Nome Fantasia'
          'RG'
          'Telefone')
      end
      object edValor: TJvCalcEdit
        Left = 10
        Top = 53
        Width = 83
        Height = 25
        DisplayFormat = '#,##0.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        ShowButton = False
        TabOrder = 2
        Visible = False
        DecimalPlacesAlwaysShown = False
        OnKeyDown = edValorKeyDown
      end
    end
  end
  object FDEntidade: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select e.ent_id, e.ent_nome, e.ent_datacadastro, e.ent_celular, ' +
        'e.ent_telefone, e.ent_email, e.ent_ativo'
      'from entidade e')
    Left = 464
    Top = 8
    object FDEntidadeENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDEntidadeENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      Origin = 'ENT_NOME'
      Size = 250
    end
    object FDEntidadeENT_DATACADASTRO: TSQLTimeStampField
      FieldName = 'ENT_DATACADASTRO'
      Origin = 'ENT_DATACADASTRO'
    end
    object FDEntidadeENT_CELULAR: TStringField
      FieldName = 'ENT_CELULAR'
      Origin = 'ENT_CELULAR'
    end
    object FDEntidadeENT_TELEFONE: TStringField
      FieldName = 'ENT_TELEFONE'
      Origin = 'ENT_TELEFONE'
    end
    object FDEntidadeENT_EMAIL: TStringField
      FieldName = 'ENT_EMAIL'
      Origin = 'ENT_EMAIL'
      Size = 255
    end
    object FDEntidadeENT_ATIVO: TIntegerField
      FieldName = 'ENT_ATIVO'
      Origin = 'ENT_ATIVO'
    end
  end
  object ProviderEntidade: TDataSetProvider
    DataSet = FDEntidade
    Left = 496
    Top = 8
  end
  object CdsEntidade: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderEntidade'
    Left = 528
    Top = 8
    object CdsEntidadeENT_ATIVO: TIntegerField
      FieldName = 'ENT_ATIVO'
      OnGetText = CdsEntidadeENT_ATIVOGetText
    end
    object CdsEntidadeENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Required = True
    end
    object CdsEntidadeENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      Size = 250
    end
    object CdsEntidadeENT_DATACADASTRO: TSQLTimeStampField
      FieldName = 'ENT_DATACADASTRO'
    end
    object CdsEntidadeENT_CELULAR: TStringField
      FieldName = 'ENT_CELULAR'
    end
    object CdsEntidadeENT_TELEFONE: TStringField
      FieldName = 'ENT_TELEFONE'
    end
    object CdsEntidadeENT_EMAIL: TStringField
      FieldName = 'ENT_EMAIL'
      Size = 255
    end
  end
  object DsEntidade: TDataSource
    DataSet = CdsEntidade
    Left = 560
    Top = 8
  end
end
