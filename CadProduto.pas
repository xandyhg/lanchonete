unit CadProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.Buttons, Vcl.StdCtrls, JvToolEdit, Vcl.Mask,
  JvExMask, JvBaseEdits, FireDAC.Comp.Client;

type
  TF_CadProduto = class(TF_CadPadrao)
    edCodigo: TJvCalcEdit;
    Label1: TLabel;
    Label3: TLabel;
    edPro_DataCadastro: TJvDateEdit;
    edPro_Descricao: TEdit;
    Label2: TLabel;
    edPro_CodigoBarras: TEdit;
    Label5: TLabel;
    edPro_ValorCusto: TJvCalcEdit;
    Label12: TLabel;
    edPro_ValorVenda: TJvCalcEdit;
    Label4: TLabel;
    ckPro_Ativo: TCheckBox;
    GroupBox1: TGroupBox;
    edValorTotalItem: TJvCalcEdit;
    Label7: TLabel;
    edIC_Qtde: TJvCalcEdit;
    Label6: TLabel;
    edQtdeCaixa: TJvCalcEdit;
    Label8: TLabel;
    edPercVenda: TJvCalcEdit;
    Label9: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
    procedure edPro_CodigoBarrasExit(Sender: TObject);
    procedure edQtdeCaixaExit(Sender: TObject);
    procedure edIC_QtdeExit(Sender: TObject);
    procedure edValorTotalItemExit(Sender: TObject);
    procedure edPercVendaExit(Sender: TObject);
    procedure edPro_ValorVendaExit(Sender: TObject);
    procedure edPro_ValorCustoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CalcularValorTotalItem;
    procedure CalcularValorVenda;
    procedure CalcularPercVenda;
  end;

var
  F_CadProduto             : TF_CadProduto;
  xAuxCadProduto_CodBarras : String;

implementation

{$R *.dfm}

uses DMC, clsProduto, ConProduto, U_Principal, X, clsConfiguracoes;

var
  Produto       : TProduto;
  tq            : TFDQuery;
  Config        : TConfiguracoes;

{ TF_CadProduto }

procedure TF_CadProduto.CalcularPercVenda;
begin
  if edPro_ValorVenda.Value <> 0 then
    edPercVenda.Value := ((edPro_ValorVenda.Value-edPro_ValorCusto.Value)*100)/edPro_ValorCusto.Value
  else
    edPercVenda.Value := 0;
end;

procedure TF_CadProduto.CalcularValorTotalItem;
begin
  if (edQtdeCaixa.Value > 0) and (edValorTotalItem.Value > 0) then begin
    if (edQtdeCaixa.Value > 0) and (edIC_Qtde.Value > 0) then
      edPro_ValorCusto.Value := edValorTotalItem.Value / (edQtdeCaixa.Value*edIC_Qtde.Value)
    else if (edIC_Qtde.Value > 0) then
      edPro_ValorCusto.Value := edValorTotalItem.Value / edIC_Qtde.Value
    else
      edPro_ValorCusto.Value := 0;
  end;
end;

procedure TF_CadProduto.CalcularValorVenda;
begin
  if edPercVenda.Value <> 0 then
    edPro_ValorVenda.Value := edPro_ValorCusto.Value + ((edPercVenda.Value*edPro_ValorCusto.Value)/100)
  else
    edPro_ValorVenda.Value := edPro_ValorCusto.Value;
end;

procedure TF_CadProduto.edIC_QtdeExit(Sender: TObject);
begin
  inherited;
  CalcularValorTotalItem;
  CalcularValorVenda;
end;

procedure TF_CadProduto.edPercVendaExit(Sender: TObject);
begin
  inherited;
  CalcularValorVenda;
end;

procedure TF_CadProduto.edPro_CodigoBarrasExit(Sender: TObject);
begin
  inherited;
  Produto := TProduto.Create;
  if Produto.RecuperarPorCodBarras(edPro_CodigoBarras.Text) then begin
    if Produto.pro_id <> edCodigo.AsInteger then begin
      msgPerson('J� existe um cadastro com o mesmo c�digo de barras!'+#13+'C�digo: '+IntToStr(Produto.pro_id)+#13+'Produto: '+Produto.pro_descricao+#13+'C�digo de Barras: '+Produto.pro_codigobarras,[mbok],'Produto',mtWarning);
      Exit;
    end;
  end;
  FreeAndNil(Produto);
end;

procedure TF_CadProduto.edPro_ValorCustoExit(Sender: TObject);
begin
  inherited;
  if edPercVenda.Value > 0 then
    CalcularValorVenda;
end;

procedure TF_CadProduto.edPro_ValorVendaExit(Sender: TObject);
begin
  inherited;
  CalcularPercVenda;
end;

procedure TF_CadProduto.edQtdeCaixaExit(Sender: TObject);
begin
  inherited;
  CalcularValorTotalItem;
  CalcularValorVenda;
end;

procedure TF_CadProduto.edValorTotalItemExit(Sender: TObject);
begin
  inherited;
  CalcularValorTotalItem;
  CalcularValorVenda;
end;

procedure TF_CadProduto.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_ConProduto_Pro_ID > 0 then begin
    Produto := TProduto.Create;
    if Produto.Recuperar(xAux_ConProduto_Pro_ID) then begin
      edCodigo.AsInteger      := xAux_ConProduto_Pro_ID;
      edPro_Descricao.Text    := Produto.pro_descricao;
      edPro_DataCadastro.Date := Produto.pro_datacadastro;
      edPro_CodigoBarras.Text := Produto.pro_codigobarras;
      edPro_ValorCusto.Value  := Produto.pro_valorcusto;
      edPro_ValorVenda.Value  := Produto.pro_valorvenda;
      if Produto.pro_ativo = 0 then
        ckPro_Ativo.Checked   := True
      else
        ckPro_Ativo.Checked   := False;

      CalcularPercVenda;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(Produto);
  end else begin
    edPro_Descricao.Clear;
    edPro_DataCadastro.Date  := Date;
    edPro_CodigoBarras.Clear;
    edPro_ValorCusto.Clear;
    edPro_ValorVenda.Clear;
    ckPro_Ativo.Checked := True;

    Config := TConfiguracoes.Create;
    Config.Recuperar;
    edPercVenda.Value := Config.conf_perc_venda;
    FreeAndNil(Config);

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);

    {$REGION 'Se existir um c�digo de barras informado na vari�vel xAuxCadProduto_CodBarras'}
    if Trim(xAuxCadProduto_CodBarras) <> '' then begin
      edPro_CodigoBarras.Text    := xAuxCadProduto_CodBarras;
      edPro_CodigoBarras.Enabled := False;
      SelectFirst;
    end;
    {$ENDREGION}
  end;
end;

procedure TF_CadProduto.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadProduto.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConProduto_Pro_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadProduto.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Produto',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    Produto := TProduto.Create;

    Produto.Eliminar(edCodigo.AsInteger);

    msgPerson('Produto eliminado com sucesso!',[mbok],'Produto',mtConfirmation);

    FreeAndNil(Produto);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadProduto.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    Produto := TProduto.Create;

    Produto.pro_descricao    := edPro_Descricao.Text;
    Produto.pro_datacadastro := edPro_DataCadastro.Date;
    Produto.pro_codigobarras := edPro_CodigoBarras.Text;
    Produto.pro_valorcusto   := edPro_ValorCusto.Value;
    Produto.pro_valorvenda   := edPro_ValorVenda.Value;
    if ckPro_Ativo.Checked then
      Produto.pro_ativo      := 0
    else
      Produto.pro_ativo      := 1;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConProduto_Pro_ID := Produto.Ultima;
      Produto.Gravar(xAux_ConProduto_Pro_ID);
    end else begin
      Produto.Gravar(xAux_ConProduto_Pro_ID);
    end;

    FreeAndNil(Produto);

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Produto '+IntToStr(xAux_ConProduto_Pro_ID)+' cadastrado com sucesso!',[mbOK], 'Produto', mtConfirmation)
    else
      msgPerson('Produto '+IntToStr(xAux_ConProduto_Pro_ID)+' alterado com sucesso!',[mbOK], 'Produto', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

end.
