unit CadCidade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits, FireDAC.Comp.Client;

type
  TF_CadCidade = class(TF_CadPadrao)
    Label1: TLabel;
    edCodigo: TJvCalcEdit;
    Label2: TLabel;
    edCid_cidade: TEdit;
    cbCid_UF: TComboBox;
    Label4: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadCidade: TF_CadCidade;

implementation

{$R *.dfm}

uses DMC, clsCidade, X, ConCidade, U_Principal;

var
  Cidade : TCidade;

procedure TF_CadCidade.FormActivate(Sender: TObject);
var
  x : Integer;
begin
  inherited;
  if xAux_ConCidade_Cid_ID > 0 then begin
    Cidade := TCidade.Create;
    if Cidade.Recuperar(xAux_ConCidade_Cid_ID) then begin
      edCodigo.AsInteger := xAux_ConCidade_Cid_ID;
      edCid_Cidade.Text  := Cidade.Cid_Cidade;
      for x:=0 to cbCid_UF.Items.count -1 do begin
        if (Cidade.Cid_UF) = Copy(cbCid_UF.Items[x],1,2) then begin
          cbCid_UF.ItemIndex := x;
        end;
      end;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(Cidade);
  end else begin
    edCid_Cidade.Clear;
    cbCid_UF.ItemIndex := 15;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadCidade.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadCidade.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConCidade_Cid_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadCidade.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Cidade',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    Cidade := TCidade.Create;

    Cidade.Eliminar(edCodigo.AsInteger);

//    xAux_AuxJustificativa_Codigo   := edCodigo.AsInteger;
//    xAux_AuxJustificativa_NomeForm := UpperCase(Copy(Name,6,Length(Name)));
//    xAux_AuxJustificativa_Descricao:= edCid_Cidade.Text;
//    AbreForm(TF_AuxJustificativa,Self);
//    xAux_AuxJustificativa_Codigo   := 0;
//    xAux_AuxJustificativa_NomeForm := '';
//    xAux_AuxJustificativa_Descricao:= '';

    msgPerson('Registro eliminado com sucesso!',[mbok],'Cidade',mtConfirmation);

    FreeAndNil(Cidade);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadCidade.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    Cidade := TCidade.Create;

    Cidade.Cid_Cidade := edCid_Cidade.Text;
    Cidade.Cid_UF     := Copy(cbCid_UF.Text,1,2);

    if edCodigo.AsInteger = 0 then begin
      xAux_ConCidade_Cid_ID     := Cidade.Ultima;
      Cidade.Gravar(xAux_ConCidade_Cid_ID);
    end else begin
      Cidade.Gravar(xAux_ConCidade_Cid_ID);
    end;

    FreeAndNil(Cidade);

//    if edCodigo.AsInteger = 0 then begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 1;
//      log.log_tabela      := 'CIDADE';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'GRAVADO C�DIGO: '+IntToStr(xAux_ConCidade_Cid_ID)+' - '+edCid_Cidade.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end else begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 2;
//      log.log_tabela      := 'CIDADE';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'ALTERADO C�DIGO: '+IntToStr(xAux_ConCidade_Cid_ID)+' - '+edCid_Cidade.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end;

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Cidade '+IntToStr(xAux_ConCidade_Cid_ID)+' cadastrado com sucesso!',[mbOK], 'Cidade', mtConfirmation)
    else
      msgPerson('Cidade '+IntToStr(xAux_ConCidade_Cid_ID)+' alterado com sucesso!',[mbOK], 'Cidade', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

end.
