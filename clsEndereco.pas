unit clsEndereco;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TEndereco = class
  public
    end_id          : Integer;
    ent_id          : Integer;
    end_logradouro  : String;
    end_bairro      : String;
    end_cep         : String;
    end_numero      : String;
    end_complemento : String;
    cid_id          : Integer;
    end_referencia  : String;

    function  Recuperar(Pend_id,Pent_id:Integer):Boolean;
    procedure Gravar(Pend_id,Pent_id:Integer);
    procedure Eliminar(Pend_id,Pent_id:Integer);
  end;

implementation

{ TEndereco }

uses DMC;

procedure TEndereco.Eliminar(Pend_id,Pent_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Endereco where end_id = :K_end_id and ent_id = :k_ent_id');
  tq.ParamByName('K_end_id').AsInteger := Pend_id;
  tq.ParamByName('K_ent_id').AsInteger := Pent_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TEndereco.Gravar(Pend_id,Pent_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Endereco where end_id = :k_end_id and ent_id = :k_ent_id');
  tq.ParamByName('k_end_id').AsInteger := Pend_id;
  tq.ParamByName('k_ent_id').AsInteger := Pent_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Endereco values('+
    ':k_end_id,'+
    ':k_ent_id,'+
    ':k_end_logradouro,'+
    ':k_end_bairro,'+
    ':k_end_cep,'+
    ':k_end_numero,'+
    ':k_end_complemento,'+
    ':k_cid_id,'+
    ':k_end_referencia)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Endereco set '+
    'end_logradouro  = :k_end_logradouro,'+
    'end_bairro      = :k_end_bairro,'+
    'end_cep         = :k_end_cep,'+
    'end_numero      = :k_end_numero,'+
    'end_complemento = :k_end_complemento,'+
    'cid_id          = :k_cid_id,'+
    'end_referencia  = :k_end_referencia ');
    tq.SQL.Add('where end_id = :K_end_id and ent_id = :k_ent_id ');
  end;

  tq.ParamByName('k_end_id').AsInteger        := Pend_id;
  tq.ParamByName('k_ent_id').AsInteger        := Pent_id;
  tq.ParamByName('k_end_logradouro').AsString := end_logradouro;
  tq.ParamByName('k_end_bairro').AsString     := end_bairro;
  tq.ParamByName('k_end_cep').AsString        := end_cep;
  tq.ParamByName('k_end_numero').AsString     := end_numero;
  tq.ParamByName('k_end_complemento').AsString:= end_complemento;
  if cid_id > 0 then
    tq.ParamByName('k_cid_id').AsInteger      := cid_id
  else
    tq.ParamByName('k_cid_id').IsNull;
  tq.ParamByName('k_end_referencia').AsString := end_referencia;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TEndereco.Recuperar(Pend_id,Pent_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Endereco where end_id = :K_end_id and ent_id = :k_ent_id');
  tq.ParamByName('k_end_id').AsInteger := Pend_id;
  tq.ParamByName('k_ent_id').AsInteger := Pent_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  end_id          := tq.FieldByName('end_id').AsInteger;
  ent_id          := tq.FieldByName('ent_id').AsInteger;
  end_logradouro  := tq.FieldByName('end_logradouro').AsString;
  end_bairro      := tq.FieldByName('end_bairro').AsString;
  end_cep         := tq.FieldByName('end_cep').AsString;
  end_numero      := tq.FieldByName('end_numero').AsString;
  end_complemento := tq.FieldByName('end_complemento').AsString;
  cid_id          := tq.FieldByName('cid_id').AsInteger;
  end_referencia  := tq.FieldByName('end_referencia').AsString;

  tq.Close;
  tq.Free;
end;

end.
