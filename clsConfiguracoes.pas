unit clsConfiguracoes;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TConfiguracoes = class
  public
    pro_id                : Integer;
    conf_lanca_total_nota : Integer; //0-Sim 1-N�o
    conf_perc_venda       : Double;
    ent_id                : Integer;

    function  Recuperar:Boolean;
    procedure Gravar;
  end;

implementation

{ TConfiguracoes }

uses DMC;

procedure TConfiguracoes.Gravar;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Configuracoes');
  tq.Open;

  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Configuracoes values('+
    ':k_pro_id,'+
    ':k_conf_lanca_total_nota,'+
    ':k_conf_perc_venda,'+
    ':k_ent_id)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Configuracoes set '+
    'pro_id                = :k_pro_id,'+
    'conf_lanca_total_nota = :k_conf_lanca_total_nota,'+
    'conf_perc_venda       = :k_conf_perc_venda,'+
    'ent_id                = :k_ent_id ');
  end;

  if pro_id > 0 then
    tq.ParamByName('k_pro_id').AsInteger := pro_id
  else
    tq.ParamByName('k_pro_id').IsNull;
  tq.ParamByName('k_conf_lanca_total_nota').AsInteger := conf_lanca_total_nota;
  tq.ParamByName('k_conf_perc_venda').AsFloat         := conf_perc_venda;
  if ent_id > 0 then
    tq.ParamByName('k_ent_id').AsInteger := ent_id
  else
    tq.ParamByName('k_ent_id').IsNull;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TConfiguracoes.Recuperar:Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Configuracoes');
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  pro_id                := tq.FieldByName('pro_id').AsInteger;
  conf_lanca_total_nota := tq.FieldByName('conf_lanca_total_nota').AsInteger;
  conf_perc_venda       := tq.FieldByName('conf_perc_venda').AsFloat;
  ent_id                := tq.FieldByName('ent_id').AsInteger;

  tq.Close;
  tq.Free;
end;

end.
