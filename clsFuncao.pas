unit clsFuncao;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TFuncao = class
  public
    fun_id        : Integer;
    fun_descricao : String;

    function  Recuperar(Pfun_id:Integer):Boolean;
    procedure Gravar(Pfun_id:Integer);
    procedure Eliminar(Pfun_id:Integer);
    function  Ultima:Integer;
  end;

implementation

{ TFuncao }

uses DMC;

procedure TFuncao.Eliminar(Pfun_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Funcao where fun_id = :K_fun_id');
  tq.ParamByName('K_fun_id').AsInteger := Pfun_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TFuncao.Gravar(Pfun_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Funcao where fun_id = :k_fun_id');
  tq.ParamByName('k_fun_id').AsInteger := Pfun_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Funcao values('+
    ':k_fun_id,'+
    ':k_fun_descricao)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Funcao set '+
    'fun_descricao = :k_fun_descricao ');
    tq.SQL.Add('where fun_id = :K_fun_id ');
  end;

  tq.ParamByName('k_fun_id').AsInteger       := Pfun_id;
  tq.ParamByName('k_fun_descricao').AsString := fun_descricao;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TFuncao.Recuperar(Pfun_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Funcao where fun_id = :K_fun_id');
  tq.ParamByName('k_fun_id').AsInteger := Pfun_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  fun_id        := tq.FieldByName('fun_id').AsInteger;
  fun_descricao := tq.FieldByName('fun_descricao').AsString;

  tq.Close;
  tq.Free;
end;

function TFuncao.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(fun_id) maior from Funcao');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
