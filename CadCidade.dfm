inherited F_CadCidade: TF_CadCidade
  Caption = ' CADASTRO DE CIDADE'
  ClientHeight = 165
  OnActivate = FormActivate
  ExplicitHeight = 204
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 88
    ExplicitTop = 88
    inherited icoAlterar: TImage
      OnClick = icoAlterarClick
    end
    inherited icoExcluir: TImage
      OnClick = icoExcluirClick
    end
    inherited icoSalvar: TImage
      OnClick = icoSalvarClick
    end
    inherited icoCancelar: TImage
      OnClick = icoCancelarClick
    end
  end
  inherited Panel3: TPanel
    Height = 47
    ExplicitHeight = 47
    object Label2: TLabel
      Left = 9
      Top = 15
      Width = 57
      Height = 17
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 631
      Top = 15
      Width = 15
      Height = 17
      Caption = 'UF'
    end
    object edCid_cidade: TEdit
      Left = 72
      Top = 12
      Width = 553
      Height = 25
      CharCase = ecUpperCase
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object cbCid_UF: TComboBox
      Left = 652
      Top = 12
      Width = 49
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ItemIndex = 15
      ParentFont = False
      TabOrder = 1
      Text = 'PR'
      Items.Strings = (
        'AC'
        'AL'
        'AP'
        'AM'
        'BA'
        'CE'
        'DF'
        'ES'
        'GO'
        'MA'
        'MT'
        'MS'
        'MG'
        'PA'
        'PB'
        'PR'
        'PE'
        'PI'
        'RJ'
        'RN'
        'RS'
        'RO'
        'RR'
        'SC'
        'SP'
        'SE'
        'TO')
    end
  end
  inherited Panel4: TPanel
    Top = 133
    ExplicitTop = 133
  end
  inherited Panel5: TPanel
    object Label1: TLabel
      Left = 23
      Top = 10
      Width = 43
      Height = 17
      Caption = 'C'#243'digo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object edCodigo: TJvCalcEdit
      Left = 72
      Top = 7
      Width = 56
      Height = 25
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      DecimalPlacesAlwaysShown = False
    end
  end
end
