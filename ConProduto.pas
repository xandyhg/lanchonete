unit ConProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TF_ConProduto = class(TF_ConPadrao)
    FDProduto: TFDQuery;
    ProviderProduto: TDataSetProvider;
    CdsProduto: TClientDataSet;
    DsProduto: TDataSource;
    FDProdutoPRO_ID: TIntegerField;
    FDProdutoPRO_DESCRICAO: TStringField;
    FDProdutoPRO_DATACADASTRO: TSQLTimeStampField;
    FDProdutoPRO_VALORVENDA: TFloatField;
    FDProdutoPRO_CODIGOBARRAS: TStringField;
    FDProdutoPRO_ATIVO: TIntegerField;
    CdsProdutoPRO_ID: TIntegerField;
    CdsProdutoPRO_DESCRICAO: TStringField;
    CdsProdutoPRO_DATACADASTRO: TSQLTimeStampField;
    CdsProdutoPRO_VALORVENDA: TFloatField;
    CdsProdutoPRO_CODIGOBARRAS: TStringField;
    CdsProdutoPRO_ATIVO: TIntegerField;
    FDProdutoPRO_VALORCUSTO: TFloatField;
    CdsProdutoPRO_VALORCUSTO: TFloatField;
    CdsProdutoPercVenda: TFloatField;
    CdsProdutototPercVenda: TAggregateField;
    Label11: TLabel;
    Label12: TLabel;
    lbPercListados: TLabel;
    lbPercTotal: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure CdsProdutoPRO_ATIVOGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CdsProdutoCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConProduto               : TF_ConProduto;
  xAux_ConProduto_Pro_ID     : Integer;
  xAux_ConProduto_AtivaEnter : Boolean;

implementation

{$R *.dfm}

uses DMC, X, CadProduto;

var
  tq : TFDQuery;

procedure TF_ConProduto.CdsProdutoCalcFields(DataSet: TDataSet);
begin
  inherited;
  if CdsProdutoPRO_VALORVENDA.Value <> 0 then
    CdsProdutoPercVenda.Value := ((CdsProdutoPRO_VALORVENDA.Value-CdsProdutoPRO_VALORCUSTO.Value)*100)/CdsProdutoPRO_VALORCUSTO.Value
  else
    CdsProdutoPercVenda.Value := 0;
end;

procedure TF_ConProduto.CdsProdutoPRO_ATIVOGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
  if Sender.AsString = '0' then
    Text := 'S'
  else
    Text := 'N';
end;

procedure TF_ConProduto.FormActivate(Sender: TObject);
begin
  inherited;
  icoPesquisarClick(Sender);
end;

procedure TF_ConProduto.icoFecharClick(Sender: TObject);
begin
  xAux_ConProduto_Pro_ID := CdsProduto.FieldByName('pro_id').AsInteger;
  inherited;

end;

procedure TF_ConProduto.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConProduto_Pro_ID := 0;
  AbreForm(TF_CadProduto,Self);
  icoPesquisarClick(Sender);
  if xAux_ConProduto_Pro_ID <> 0 then
    CdsProduto.Locate('pro_id',xAux_ConProduto_Pro_ID,[]);
end;

procedure TF_ConProduto.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsProduto.Close;

  FDProduto.Close;
  FDProduto.Params.Clear;
  FDProduto.SQL.Clear;
  FDProduto.SQL.Add('select p.pro_id, p.pro_descricao, p.pro_datacadastro,                '+
                    'p.pro_valorcusto, p.pro_valorvenda, p.pro_codigobarras, p.pro_ativo  '+
                    'from produto p                                                       '+
                    'order by p.pro_descricao                                             ');
  CdsProduto.Open;

  if not CdsProduto.Eof then begin
    lblRegTotal.Caption    := 'N� de Registros: '+IntToStr(CdsProduto.RecordCount);
    lbPercListados.Caption := FormatFloat('#,##0.00', (CdsProdutototPercVenda.Value/CdsProduto.RecordCount))+'%';
  end else begin
    lblRegTotal.Caption    := 'N� de Registros: 0';
    lbPercListados.Caption := '0%';
  end;

  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;

  tq.Close;
  tq.Params.Clear;
  tq.SQL.Clear;
  tq.SQL.Add('select sum(((p.pro_valorvenda-p.pro_valorcusto)*100)/iif(p.pro_valorcusto=0,1,p.pro_valorcusto))/iif(count(p.pro_id)=0,1,count(p.pro_id)) totalPerc '+
             'from produto p                                                                                                                                      '+
             'where p.pro_ativo=0                                                                                                                                 ');
  tq.Open;

  lbPercTotal.Caption := FormatFloat('#,##0.00', tq.FieldByName('totalPerc').AsFloat)+'%';

  tq.Close;
  tq.Free;
end;

procedure TF_ConProduto.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConProduto_Pro_ID := CdsProduto.FieldByName('pro_id').AsInteger;
  AbreForm(TF_CadProduto, Self);
  icoPesquisarClick(Sender);
  if xAux_ConProduto_Pro_ID <> 0 then
    CdsProduto.Locate('pro_id',xAux_ConProduto_Pro_ID,[]);
end;

procedure TF_ConProduto.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if Column.FieldName = 'PRO_ATIVO' then begin
    if CdsProduto.FieldByName('PRO_ATIVO').AsInteger = 0 then begin
      JvDBGrid1.Canvas.Brush.Color := clGreen;
      JvDBGrid1.Canvas.Font.Color  := clwhite;
    end else begin
      JvDBGrid1.Canvas.Brush.Color := clRed;
      JvDBGrid1.Canvas.Font.Color  := clwhite;
    end;
  end;
  JvDBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TF_ConProduto.JvDBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (key=13) and (JvDBGrid1.Focused) and (xAux_ConProduto_AtivaEnter=True) then
    icoFecharClick(Sender);
end;

end.
