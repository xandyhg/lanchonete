unit clsSetor;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TSetor = class
  public
    set_id        : Integer;
    set_descricao : String;

    function  Recuperar(Pset_id:Integer):Boolean;
    procedure Gravar(Pset_id:Integer);
    procedure Eliminar(Pset_id:Integer);
    function  Ultima:Integer;
  end;

implementation

{ TSetor }

uses DMC;

procedure TSetor.Eliminar(Pset_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Setor where set_id = :K_set_id');
  tq.ParamByName('K_set_id').AsInteger := Pset_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TSetor.Gravar(Pset_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Setor where set_id = :k_set_id');
  tq.ParamByName('k_set_id').AsInteger := Pset_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Setor values('+
    ':k_set_id,'+
    ':k_set_descricao)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Setor set '+
    'set_descricao = :k_set_descricao ');
    tq.SQL.Add('where set_id = :K_set_id ');
  end;

  tq.ParamByName('k_set_id').AsInteger       := Pset_id;
  tq.ParamByName('k_set_descricao').AsString := set_descricao;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TSetor.Recuperar(Pset_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Setor where set_id = :K_set_id');
  tq.ParamByName('k_set_id').AsInteger := Pset_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  set_id        := tq.FieldByName('set_id').AsInteger;
  set_descricao := tq.FieldByName('set_descricao').AsString;

  tq.Close;
  tq.Free;
end;

function TSetor.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(set_id) maior from Setor');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
