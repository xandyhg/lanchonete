unit ConVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TF_ConVendas = class(TF_ConPadrao)
    FDVendas: TFDQuery;
    ProviderVendas: TDataSetProvider;
    CdsVendas: TClientDataSet;
    DsVendas: TDataSource;
    FDVendasVENDA_ID: TIntegerField;
    FDVendasENT_ID: TIntegerField;
    FDVendasENT_CPF: TStringField;
    FDVendasENT_NOME: TStringField;
    FDVendasVENDA_TOTAL: TFloatField;
    FDVendasVENDA_DATA: TSQLTimeStampField;
    FDVendasVENDA_HORA: TStringField;
    FDVendasVENDA_DESCONTO: TFloatField;
    FDVendasUSUA_ID: TIntegerField;
    FDVendasUSUA_LOGIN: TStringField;
    CdsVendasVENDA_ID: TIntegerField;
    CdsVendasENT_ID: TIntegerField;
    CdsVendasENT_CPF: TStringField;
    CdsVendasENT_NOME: TStringField;
    CdsVendasVENDA_TOTAL: TFloatField;
    CdsVendasVENDA_DATA: TSQLTimeStampField;
    CdsVendasVENDA_HORA: TStringField;
    CdsVendasVENDA_DESCONTO: TFloatField;
    CdsVendasUSUA_ID: TIntegerField;
    CdsVendasUSUA_LOGIN: TStringField;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConVendas             : TF_ConVendas;
  xAux_ConVendas_Venda_ID : Integer;

implementation

{$R *.dfm}

uses DMC, X, CadVendas;

procedure TF_ConVendas.FormActivate(Sender: TObject);
begin
  inherited;
  icoPesquisarClick(Sender);
end;

procedure TF_ConVendas.icoFecharClick(Sender: TObject);
begin
  xAux_ConVendas_Venda_ID := CdsVendas.FieldByName('venda_id').AsInteger;
  inherited;

end;

procedure TF_ConVendas.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConVendas_Venda_ID := 0;
  AbreForm(TF_CadVendas,Self);
  icoPesquisarClick(Sender);
  if xAux_ConVendas_Venda_ID <> 0 then
    CdsVendas.Locate('venda_id',xAux_ConVendas_Venda_ID,[]);
end;

procedure TF_ConVendas.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsVendas.Close;

  FDVendas.Close;
  FDVendas.Params.Clear;
  FDVendas.SQL.Clear;
  FDVendas.SQL.Add('select v.venda_id, v.ent_id, e.ent_cpf, e.ent_nome, v.venda_total,    '+
                   'v.venda_data, v.venda_hora, v.venda_desconto, v.usua_id, u.usua_login '+
                   'from vendas v                                                         '+
                   'left outer join entidade e on v.ent_id=e.ent_id                       '+
                   'left outer join usuario u on v.usua_id=u.usua_id                      '+
                   'order by v.venda_data, v.venda_hora, e.ent_nome                       ');
  CdsVendas.Open;

  if not CdsVendas.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsVendas.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConVendas.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConVendas_Venda_ID := CdsVendas.FieldByName('venda_id').AsInteger;
  AbreForm(TF_CadVendas, Self);
  icoPesquisarClick(Sender);
  if xAux_ConVendas_Venda_ID <> 0 then
    CdsVendas.Locate('venda_id',xAux_ConVendas_Venda_ID,[]);
end;

end.
