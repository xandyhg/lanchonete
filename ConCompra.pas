unit ConCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TF_ConCompra = class(TF_ConPadrao)
    FDCompra: TFDQuery;
    ProviderCompra: TDataSetProvider;
    CdsCompra: TClientDataSet;
    DsCompra: TDataSource;
    FDCompraCOM_ID: TIntegerField;
    FDCompraENT_ID: TIntegerField;
    FDCompraENT_CNPJ: TStringField;
    FDCompraENT_NOME_FANTASIA: TStringField;
    FDCompraENT_NOME: TStringField;
    FDCompraCOM_NF: TStringField;
    FDCompraCOM_VALORTOTAL: TFloatField;
    CdsCompraCOM_ID: TIntegerField;
    CdsCompraENT_ID: TIntegerField;
    CdsCompraENT_CNPJ: TStringField;
    CdsCompraENT_NOME_FANTASIA: TStringField;
    CdsCompraENT_NOME: TStringField;
    CdsCompraCOM_NF: TStringField;
    CdsCompraCOM_VALORTOTAL: TFloatField;
    FDCompraCOM_DATACADASTRO: TSQLTimeStampField;
    CdsCompraCOM_DATACADASTRO: TSQLTimeStampField;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConCompra           : TF_ConCompra;
  xAux_ConCompra_Com_ID : Integer;

implementation

{$R *.dfm}

uses DMC, X, CadCompra;

procedure TF_ConCompra.FormActivate(Sender: TObject);
begin
  inherited;
  icoPesquisarClick(Sender);
end;

procedure TF_ConCompra.icoFecharClick(Sender: TObject);
begin
  xAux_ConCompra_Com_ID := CdsCompra.FieldByName('com_id').AsInteger;
  inherited;

end;

procedure TF_ConCompra.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConCompra_Com_ID := 0;
  AbreForm(TF_CadCompra,Self);
  icoPesquisarClick(Sender);
  if xAux_ConCompra_Com_ID <> 0 then
    CdsCompra.Locate('com_id',xAux_ConCompra_Com_ID,[]);
end;

procedure TF_ConCompra.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsCompra.Close;

  FDCompra.Close;
  FDCompra.Params.Clear;
  FDCompra.SQL.Clear;
  FDCompra.SQL.Add('select c.com_id, c.ent_id, e.ent_cnpj, e.ent_nome_fantasia, e.ent_nome, '+
                   'c.com_nf, c.com_valortotal, c.com_datacadastro                          '+
                   'from compra c                                                           '+
                   'left outer join entidade e on c.ent_id=e.ent_id                         '+
                   'order by c.com_datacadastro, e.ent_nome_fantasia                        ');
  CdsCompra.Open;

  if not CdsCompra.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsCompra.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConCompra.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConCompra_Com_ID := CdsCompra.FieldByName('com_id').AsInteger;
  AbreForm(TF_CadCompra, Self);
  icoPesquisarClick(Sender);
  if xAux_ConCompra_Com_ID <> 0 then
    CdsCompra.Locate('com_id',xAux_ConCompra_Com_ID,[]);
end;

end.
