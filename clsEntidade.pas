unit clsEntidade;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TEntidade = class
  public
    ent_id              : Integer;
    ent_nome            : String;
    ent_datacadastro    : TDateTime;
    ent_datanasc        : TDateTime;
    ent_rg              : String;
    ent_cpf             : String;
    ent_estadocivil     : Integer; //0-Solteiro(a) 1-Casado(a) 2-Divorciado(a) 3-Separado(a) 4-V�uvo(a)
    ent_celular         : String;
    ent_telefone        : String;
    ent_email           : String;
    ent_ativo           : Integer; //0-Sim 1-N�o
    ent_obs             : String;
    ent_sexo            : Integer; //0-Masculino 1-Feminino
    ent_tipo            : Integer; //0-Cliente 1-Fornecedor 2-Parceiro
    ent_contato         : String;
    ent_celular_contato : String;
    ent_cnpj            : String;
    ent_nome_fantasia   : String;
    ent_tipo_pessoa     : Integer; //0-Pessoa F�sica 1-Pessoa Jur�dica
    ent_limite          : Double;
    ent_liberado        : Integer; //0-Sim 1-N�o
    set_id              : Integer;
    fun_id              : Integer;
    ent_tipo_cliente    : Integer; //0-Externo 1-Funcion�rio 2-Terceirizado

    function  Recuperar(Pent_id:Integer):Boolean;
    procedure Gravar(Pent_id:Integer);
    procedure Eliminar(Pent_id:Integer);
    function  Ultima:Integer;
    function  RecuperarPorEmail(Pent_email:String):Boolean;
    function  RecuperarPorCelular(Pent_Celular:String):Boolean;
    function  RecuperarPorCPF(Pent_CPF:String):Boolean;
    function  RecuperarPorTelefone(Pent_Telefone:String):Boolean;
    function  RecuperarPorCNPJ(Pent_CNPJ:String):Boolean;
  end;

implementation

{ TEntidade }

uses DMC;

procedure TEntidade.Eliminar(Pent_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Entidade where ent_id = :K_ent_id');
  tq.ParamByName('K_ent_id').AsInteger := Pent_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TEntidade.Gravar(Pent_id: Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where ent_id = :k_ent_id');
  tq.ParamByName('k_ent_id').AsInteger := Pent_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Entidade values('+
    ':k_ent_id,'+
    ':k_ent_nome,'+
    ':k_ent_datacadastro,'+
    ':k_ent_datanasc,'+
    ':k_ent_rg,'+
    ':k_ent_cpf,'+
    ':k_ent_estadocivil,'+
    ':k_ent_celular,'+
    ':k_ent_telefone,'+
    ':k_ent_email,'+
    ':k_ent_ativo,'+
    ':k_ent_obs,'+
    ':k_ent_sexo,'+
    ':k_ent_tipo,'+
    ':k_ent_contato,'+
    ':k_ent_celular_contato,'+
    ':k_ent_cnpj,'+
    ':k_ent_nome_fantasia,'+
    ':k_ent_tipo_pessoa,'+
    ':k_ent_limite,'+
    ':k_ent_liberado,'+
    ':k_set_id,'+
    ':k_fun_id,'+
    ':k_ent_tipo_cliente)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Entidade set '+
    'ent_nome            = :k_ent_nome,'+
    'ent_datacadastro    = :k_ent_datacadastro,'+
    'ent_datanasc        = :k_ent_datanasc,'+
    'ent_rg              = :k_ent_rg,'+
    'ent_cpf             = :k_ent_cpf,'+
    'ent_estadocivil     = :k_ent_estadocivil,'+
    'ent_celular         = :k_ent_celular,'+
    'ent_telefone        = :k_ent_telefone,'+
    'ent_email           = :k_ent_email,'+
    'ent_ativo           = :k_ent_ativo,'+
    'ent_obs             = :k_ent_obs,'+
    'ent_sexo            = :k_ent_sexo,'+
    'ent_tipo            = :k_ent_tipo,'+
    'ent_contato         = :k_ent_contato,'+
    'ent_celular_contato = :k_ent_celular_contato,'+
    'ent_cnpj            = :k_ent_cnpj,'+
    'ent_nome_fantasia   = :k_ent_nome_fantasia,'+
    'ent_tipo_pessoa     = :k_ent_tipo_pessoa,'+
    'ent_limite          = :k_ent_limite,'+
    'ent_liberado        = :k_ent_liberado,'+
    'set_id              = :k_set_id,'+
    'fun_id              = :k_fun_id,'+
    'ent_tipo_cliente    = :k_ent_tipo_cliente ');
    tq.SQL.Add('where ent_id = :K_ent_id ');
  end;

  tq.ParamByName('k_ent_id').AsInteger             := Pent_id;
  tq.ParamByName('k_ent_nome').AsString            := ent_nome;
  tq.ParamByName('k_ent_datacadastro').AsDateTime  := ent_datacadastro;
  tq.ParamByName('k_ent_datanasc').AsDateTime      := ent_datanasc;
  tq.ParamByName('k_ent_rg').AsString              := ent_rg;
  tq.ParamByName('k_ent_cpf').AsString             := ent_cpf;
  tq.ParamByName('k_ent_estadocivil').AsInteger    := ent_estadocivil;
  tq.ParamByName('k_ent_celular').AsString         := ent_celular;
  tq.ParamByName('k_ent_telefone').AsString        := ent_telefone;
  tq.ParamByName('k_ent_email').AsString           := ent_email;
  tq.ParamByName('k_ent_ativo').AsInteger          := ent_ativo;
  tq.ParamByName('k_ent_obs').AsString             := ent_obs;
  tq.ParamByName('k_ent_sexo').AsInteger           := ent_sexo;
  tq.ParamByName('k_ent_tipo').AsInteger           := ent_tipo;
  tq.ParamByName('k_ent_contato').AsString         := ent_contato;
  tq.ParamByName('k_ent_celular_contato').AsString := ent_celular_contato;
  tq.ParamByName('k_ent_cnpj').AsString            := ent_cnpj;
  tq.ParamByName('k_ent_nome_fantasia').AsString   := ent_nome_fantasia;
  tq.ParamByName('k_ent_tipo_pessoa').AsInteger    := ent_tipo_pessoa;
  tq.ParamByName('k_ent_limite').AsFloat           := ent_limite;
  tq.ParamByName('k_ent_liberado').AsInteger       := ent_liberado;
  if set_id > 0 then
    tq.ParamByName('k_set_id').AsInteger           := set_id
  else
    tq.ParamByName('k_set_id').IsNull;
  if fun_id > 0 then
    tq.ParamByName('k_fun_id').AsInteger           := fun_id
  else
    tq.ParamByName('k_fun_id').IsNull;
  tq.ParamByName('k_ent_tipo_cliente').AsInteger   := ent_tipo_cliente;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TEntidade.Recuperar(Pent_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where ent_id = :K_ent_id');
  tq.ParamByName('k_ent_id').AsInteger := Pent_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  ent_id              := tq.FieldByName('ent_id').AsInteger;
  ent_nome            := tq.FieldByName('ent_nome').AsString;
  ent_datacadastro    := tq.FieldByName('ent_datacadastro').AsDateTime;
  ent_datanasc        := tq.FieldByName('ent_datanasc').AsDateTime;
  ent_rg              := tq.FieldByName('ent_rg').AsString;
  ent_cpf             := tq.FieldByName('ent_cpf').AsString;
  ent_estadocivil     := tq.FieldByName('ent_estadocivil').AsInteger;
  ent_celular         := tq.FieldByName('ent_celular').AsString;
  ent_telefone        := tq.FieldByName('ent_telefone').AsString;
  ent_email           := tq.FieldByName('ent_email').AsString;
  ent_ativo           := tq.FieldByName('ent_ativo').AsInteger;
  ent_obs             := tq.FieldByName('ent_obs').AsString;
  ent_sexo            := tq.FieldByName('ent_sexo').AsInteger;
  ent_tipo            := tq.FieldByName('ent_tipo').AsInteger;
  ent_contato         := tq.FieldByName('ent_contato').AsString;
  ent_celular_contato := tq.FieldByName('ent_celular_contato').AsString;
  ent_cnpj            := tq.FieldByName('ent_cnpj').AsString;
  ent_nome_fantasia   := tq.FieldByName('ent_nome_fantasia').AsString;
  ent_tipo_pessoa     := tq.FieldByName('ent_tipo_pessoa').AsInteger;
  ent_limite          := tq.FieldByName('ent_limite').AsFloat;
  ent_liberado        := tq.FieldByName('ent_liberado').AsInteger;
  set_id              := tq.FieldByName('set_id').AsInteger;
  fun_id              := tq.FieldByName('fun_id').AsInteger;
  ent_tipo_cliente    := tq.FieldByName('ent_tipo_cliente').AsInteger;

  tq.Close;
  tq.Free;
end;

function TEntidade.RecuperarPorCelular(Pent_Celular: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where ent_celular = :K_ent_celular');
  tq.ParamByName('k_ent_celular').AsString := Pent_Celular;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('ent_id').AsInteger);

  tq.Free;
  tq.Close;
end;

function TEntidade.RecuperarPorCNPJ(Pent_CNPJ: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where (select Retorno from proc_digitos(ent_cnpj)) like :K_ent_CNPJ');
  tq.ParamByName('k_ent_CNPJ').AsString := Pent_CNPJ;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('ent_id').AsInteger);

  tq.Free;
  tq.Close;
end;

function TEntidade.RecuperarPorCPF(Pent_CPF: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where (select Retorno from proc_digitos(ent_cpf)) like :K_ent_CPF');
  tq.ParamByName('k_ent_CPF').AsString := Pent_CPF;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('ent_id').AsInteger);

  tq.Free;
  tq.Close;
end;

function TEntidade.RecuperarPorEmail(Pent_email: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where ent_email = :K_ent_email');
  tq.ParamByName('k_ent_email').AsString := Pent_email;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('ent_id').AsInteger);

  tq.Free;
  tq.Close;
end;

function TEntidade.RecuperarPorTelefone(Pent_Telefone: String): Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Entidade where ent_telefone = :K_ent_telefone');
  tq.ParamByName('k_ent_Telefone').AsString := Pent_Telefone;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := Recuperar(tq.FieldByName('ent_id').AsInteger);

  tq.Free;
  tq.Close;
end;

function TEntidade.Ultima: Integer;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select max(ent_id) maior from Entidade');
  tq.Open;
  if tq.FieldByName('maior').AsInteger <> 0 then
    Result := tq.FieldByName('maior').AsInteger + 1
  else
    Result := 1;
  tq.Close;
  tq.Free;
end;

end.
