unit CadConfigEmail;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.Mask, JvExMask,
  JvToolEdit, JvBaseEdits;

type
  TF_CadConfigEmail = class(TF_CadPadrao)
    Label1: TLabel;
    edCodigo: TJvCalcEdit;
    Label2: TLabel;
    edConfe_nome: TEdit;
    edConfe_endereco: TEdit;
    Label3: TLabel;
    edConfe_smtp: TEdit;
    Label5: TLabel;
    Label4: TLabel;
    edConfe_porta: TJvCalcEdit;
    rgConfe_autenticacao: TRadioGroup;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_CadConfigEmail: TF_CadConfigEmail;

implementation

{$R *.dfm}

uses DMC, ConConfigEmail, U_Principal, X, clsConfigEmail;

var
  ConfigEmail : TConfigEmail;

procedure TF_CadConfigEmail.FormActivate(Sender: TObject);
begin
  inherited;
  if xAux_ConConfEmail_Confe_ID > 0 then begin
    ConfigEmail := TConfigEmail.Create;
    if ConfigEmail.Recuperar(xAux_ConConfEmail_Confe_ID) then begin
      edCodigo.AsInteger      := xAux_ConConfEmail_Confe_ID;
      edConfe_nome.Text       := ConfigEmail.confe_nome;
      if ConfigEmail.confe_autenticacao = 'satDefault' then
        rgConfe_autenticacao.ItemIndex := 0
      else
        rgConfe_autenticacao.ItemIndex := 1;
      edConfe_porta.AsInteger := ConfigEmail.confe_porta;
      edConfe_smtp.Text       := ConfigEmail.confe_smtp;
      edConfe_endereco.Text   := ConfigEmail.confe_endereco;

      Panel3.Enabled := False;
      ControleBotoes(1);
    end;
    FreeAndNil(ConfigEmail);
  end else begin
    edConfe_nome.Clear;
    edConfe_endereco.Clear;
    edConfe_smtp.Clear;
    edConfe_porta.Clear;
    rgConfe_autenticacao.ItemIndex := 0;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);
  end;
end;

procedure TF_CadConfigEmail.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadConfigEmail.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConConfEmail_Confe_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadConfigEmail.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Config Email',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    ConfigEmail := TConfigEmail.Create;

    ConfigEmail.Eliminar(edCodigo.AsInteger);

//    xAux_AuxJustificativa_Codigo   := edCodigo.AsInteger;
//    xAux_AuxJustificativa_NomeForm := UpperCase(Copy(Name,6,Length(Name)));
//    xAux_AuxJustificativa_Descricao:= edConfe_nome.Text;
//    AbreForm(TF_AuxJustificativa,Self);
//    xAux_AuxJustificativa_Codigo   := 0;
//    xAux_AuxJustificativa_NomeForm := '';
//    xAux_AuxJustificativa_Descricao:= '';

    msgPerson('Configura��o eliminada com sucesso!',[mbok],'Config Email',mtConfirmation);

    FreeAndNil(ConfigEmail);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadConfigEmail.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    ConfigEmail := TConfigEmail.Create;

    ConfigEmail.confe_nome           := edConfe_nome.Text;
    if rgConfe_autenticacao.ItemIndex = 0 then
      ConfigEmail.confe_autenticacao := 'satDefault'
    else
      ConfigEmail.confe_autenticacao := 'satNone';
    ConfigEmail.confe_porta          := edConfe_porta.AsInteger;
    ConfigEmail.confe_smtp           := edConfe_smtp.Text;
    ConfigEmail.confe_endereco       := edConfe_endereco.Text;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConConfEmail_Confe_ID     := ConfigEmail.Ultima;
      ConfigEmail.Gravar(xAux_ConConfEmail_Confe_ID);
    end else begin
      ConfigEmail.Gravar(xAux_ConConfEmail_Confe_ID);
    end;

    FreeAndNil(ConfigEmail);

//    if edCodigo.AsInteger = 0 then begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 1;
//      log.log_tabela      := 'CONFIG EMAIL';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'GRAVADO C�DIGO: '+IntToStr(xAux_ConConfEmail_Confe_ID)+' - '+edConfe_nome.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end else begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 2;
//      log.log_tabela      := 'CONFIG EMAIL';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'ALTERADO C�DIGO: '+IntToStr(xAux_ConConfEmail_Confe_ID)+' - '+edConfe_nome.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end;

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Configura��o '+IntToStr(xAux_ConConfEmail_Confe_ID)+' cadastrado com sucesso!',[mbOK], 'Config Email', mtConfirmation)
    else
      msgPerson('Configura��o '+IntToStr(xAux_ConConfEmail_Confe_ID)+' alterado com sucesso!',[mbOK], 'Config Email', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

end.
