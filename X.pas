unit X;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls, ComCtrls, JvBaseEdits, JvToolEdit,
  StdCtrls, Mask, CheckLst, DbCtrls, Registry, math, ShellApi, DB, Grids, ComObj, Variants, DBGrids, Datasnap.DBClient, DateUtils, System.StrUtils,
  IniFiles, IdBaseComponent, IdComponent, IdIPWatch, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL,
  IdSSLOpenSSL, IdAntiFreezeBase, Vcl.IdAntiFreeze, IdMessage, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdMessageClient, IdSMTPBase, IdSMTP, IdAttachmentFile, IdText, IdHTTP, JPEG, FireDAC.Comp.Client, UrlMon, JvRichEdit;

procedure AbreForm(Form : TFormClass; TForm2 : TComponent);
function  Formata_Valor_decimal(valor:Double; num:Integer):String;
function  msgPerson(Texto: string; Botao: TMsgDlgButtons = [mbOk]; Rotulo:String = 'Aten��o'; Icon : TMsgDlgType = mtWarning): integer;
function  ZeroEsquerda(str : string; tam:integer): string;
function  ContarCasasDecimais(value: Extended):Integer;
function  CalculaVencimento(CampoData: TDateTime; DiaVencimento: String; CampoVencimento: TJvDateEdit):TDateTime;
function  TiraPontoNumero(frase :string):string;
function  CopyReverse(S: String; Index, Count : Integer) : String;
function  TiraLetra(numero : string): string;
function  TiraVirgulaBarraNumero(frase :string):string;
function  TiraLetraPonto(numero : string): string;
function  Formata_Inteiro(Numero:integer):string;
function  Substitui_virgula_por_ponto(numero : string): string;
function  LerIniConexao(sSecao, sVariavel: String): String;
function  LerContas(var lista:TCheckListBox):String;
function  LerItemIndex(var lista:TCheckListBox):String;
function  Criptografia(mStr, mChave: String): String;
function  Retorna_Serial_HD:String;
function  Retorna_Nome_Computador:String;
function  Retorna_IP_Computador:String;
function  Retorna_MacAddress:String;
function  EspacoEsquerda(str : string; tam:integer): string;
function  Formata_valor6(valor:Double):string;
function  Formata_valor8(valor:Double):string;
function  MesExtenso(Mes:integer ) : string;
function  TiraZeroEsquerda(numero : string): string;
function  SomenteNumero(Str:String):String;
function  ValidaCPF(num:string): boolean;
function  RemoveAcento(texto : string): string;
function  RtfToHtml(RICH: TJvRichEdit): string;
function  LerPalavras(var lista:TCheckListBox):String;
function  LerSelecionados(var lista:TClientDataSet; campo1, campo2, selecionado:String):String;
function  LerPalavrasMemo(var lista:TMemo):String;
function  Mascara_CNPJ(cnpj:string):string;
function  RetornaUsuarioLogadoRede: string;
procedure EnviaErroPorEmail(Form : TForm; Erro, Usuario: String);
procedure EnviaTarefaPorEmail(Form : TForm; PPort:Integer; PHost, PUsername, PPassword, PDestinatario, PUsuario, PTarefa, PObs: String; PDataTarefa: TDateTime);
function  Diferenca_duas_horas(Inicio,Fim:String):String;
function  SomaHoras(Hora, HoraSoma :String):String; {esta fun��o recebe duas horas no fomato hh:mm (string) soma os dois valores e retorna uma string com o resultado}
function  SegundosToTime( Segundos : Cardinal ) : String; //Nesta forma j� impede a inser��o de valores negativos, al�m de suportar um valor maior de entrada de segundos
procedure CriaFormPopup(Form : TComponent);
function  Mascara_CPF(cpf:string):string;
function  TiraSomenteEspaco(texto: string): string;
function  BrowseURL(const URL: string) : boolean;
function  RetornaDiaDaSemana(PData:TDateTime):String;
function  RetornaMes(PData:TDateTime):String;
function  PercentualValorFinal(Valor,Percentual:Double):Double;
function  AumentaMes(Data:TDateTime;NumMes:Integer):TDateTime;
function  Substitui_barra_por_ponto(data : string): string;
function  LerDatas(var lista:TCheckListBox):String;
function  LerPalavrasParteCheckList(var lista:TCheckListBox;inicio,fim:Integer):String;
function  LerQuery3Numeros(Campo:String;tq:TFDQuery):String;
function  Diferenca_duas_data(d_ini,d_fim:tdatetime):Integer;
function  RetornaTextoEntre2CaracIguais(Texto:String; Caracter:Char):String;
function  RetornaTextoEntre2CaracDiferente(Texto,Delimitador1,Delimitador2:String):String;
function  RetornaTextoDeUmCaracAteFinal(Texto,Delimitador1:String):String;
function  TiraCodigoPais(numero:String):String;
function  RetiraEnter(aText : string): string;
function  Mascara_CEP(cep:String):String;
function  RetIdxUltimaColVisivel(DBGrid: TDBGrid): Integer;
function  NaoInsereDBGrid(DBGrid: TDBGrid; DataSet: TDataSet; Key: Word): Word;
function  InverterPalavras(Texto:String):String;
procedure GetImageByUrl(URL: string; APicture: TPicture;vIdHTTP : TIdHTTP);
function  FormataNome(sNome: String): String;
function  PalavraExiste(PPalavra,PTexto:String):Boolean;
procedure GetImageByUrl2(URL: string; APicture: TPicture);

implementation

procedure AbreForm(Form : TFormClass; TForm2 : TComponent);
begin
  with Form.Create(TForm2) do begin
    try
      ShowModal;
    finally
      //Free;
      Release;
      Form := nil;
    end;
  end;
end;

function  Formata_Valor_decimal(valor:Double; num:Integer):String;
var
  aux : String;
begin
  if num = 2 then begin
    aux := FormatFloat('#,###,##0.00',valor);
    aux := stringofchar(' ',12-length(aux))+aux;
    result := aux;
  end;
  if num = 3 then begin
    aux := FormatFloat('#,###,##0.000',valor);
    aux := stringofchar(' ',13-length(aux))+aux;
    result := aux;
  end;
  if num = 4 then begin
    aux := FormatFloat('#,###,##0.0000',valor);
    aux := stringofchar(' ',14-length(aux))+aux;
    result := aux;
  end;
  if num = 5 then begin
    aux := FormatFloat('#,###,##0.00000',valor);
    aux := stringofchar(' ',15-length(aux))+aux;
    result := aux;
  end;
  if num = 6 then begin
    aux := FormatFloat('#,###,##0.000000',valor);
    aux := stringofchar(' ',16-length(aux))+aux;
    result := aux;
  end;
  if num = 7 then begin
    aux := FormatFloat('#,###,##0.0000000',valor);
    aux := stringofchar(' ',17-length(aux))+aux;
    result := aux;
  end;
end;

function msgPerson(Texto: string; Botao: TMsgDlgButtons = [mbOk]; Rotulo:String = 'Aten��o'; Icon : TMsgDlgType = mtWarning): integer;
var
  wl_mensagem: TForm;
begin

  wl_Mensagem := CreateMessageDialog(Texto, Icon, Botao);

  (wl_mensagem.FindComponent('YES') as TButton).Caption := '&Sim';
  (wl_mensagem.FindComponent('NO') as TButton).Caption  := '&N�o';
  (wl_mensagem.FindComponent('OK') as TButton).Caption  := '&Ok';
  (wl_mensagem.FindComponent('CANCEL') as TButton).Caption  := '&Cancela';

  wl_mensagem.Color := clBtnFace;
  wl_mensagem.BorderStyle := bsSingle;
  wl_mensagem.BorderIcons := [];

  wl_mensagem.Caption := Rotulo;

  result := wl_mensagem.ShowModal;
end;

function  ZeroEsquerda(str : string; tam:integer): string;
var
  i   : integer;
  aux : string;
begin
  //esta fun��o completa de zeros a esquerda da string
  aux := '';
  for i:=1 to tam-length(str) do begin
    aux := aux + '0';
  end;
  Result := aux+str;
end;

function  ContarCasasDecimais(value: Extended):Integer;
Var
  x: String;
begin
  x      := FloatToStr( Value - Trunc( Value ) );
  Result := Length( Copy( x, Pos( ',', x ) +1, Length( x ) ) );
end;

function  CalculaVencimento(CampoData: TDateTime; DiaVencimento: String; CampoVencimento: TJvDateEdit):TDateTime;
begin
  if DayOf(CampoData) < 25 then begin
    if MonthOf(CampoData) <> 12 then
      CampoVencimento.Date := StrToDate(DiaVencimento+'/'+Copy(DateToStr(IncMonth(CampoData,1)),4,2)+'/'+Copy(DateToStr(CampoData),7,4))
    else
      CampoVencimento.Date := StrToDate(DiaVencimento+'/'+Copy(DateToStr(IncMonth(CampoData,1)),4,2)+'/'+Copy(DateToStr(IncYear(CampoData,1)),7,4));
  end else begin
    if MonthOf(CampoData) <> 12 then begin
      if MonthOf(CampoData) = 11 then
        CampoVencimento.Date := StrToDate(DiaVencimento+'/'+Copy(DateToStr(IncMonth(CampoData,2)),4,2)+'/'+Copy(DateToStr(IncYear(CampoData,1)),7,4))
      else
        CampoVencimento.Date := StrToDate(DiaVencimento+'/'+Copy(DateToStr(IncMonth(CampoData,2)),4,2)+'/'+Copy(DateToStr(CampoData),7,4));
    end else
      CampoVencimento.Date := StrToDate(DiaVencimento+'/'+Copy(DateToStr(IncMonth(CampoData,2)),4,2)+'/'+Copy(DateToStr(IncYear(CampoData,1)),7,4));
  end;
end;

function  TiraPontoNumero(frase :string):string;
var
  a: string;
  i  : integer;
begin
  result := '';
  if trim(frase)='' then
    exit;
  for i:=1 to length(frase) do begin
    a := copy(frase,i,1);
    if (a<>'.') and (a<>'_') and (a<>'-') and (a<>'/') and (a<>':') and (a<>',')
    and (a<>'1') and (a<>'2') and (a<>'3') and (a<>'4') and (a<>'5') and (a<>'6')
    and (a<>'7') and (a<>'8') and (a<>'9') and (a<>'0') then
    Result := result + a;
  end;
end;

function CopyReverse(S: String; Index, Count : Integer) : String;
begin
  Result := ReverseString(S);
  Result := Copy(Result, Index, Count);
  Result := ReverseString(Result);
end;

function  TiraLetra(numero : string): string;
var
  a: string;
  i  : integer;
begin
  result := '';
  if trim(numero)='' then
    exit;
  for i:=1 to length(numero) do begin
    a := copy(numero,i,1);
    if  (a<>'a') and (a<>'b') and (a<>'c') and (a<>'d') and (a<>'e') and (a<>'f')
    and (a<>'g') and (a<>'h') and (a<>'i') and (a<>'j') and (a<>'l') and (a<>'m')
    and (a<>'n') and (a<>'o') and (a<>'p') and (a<>'q') and (a<>'r') and (a<>'s')
    and (a<>'t') and (a<>'u') and (a<>'v') and (a<>'x') and (a<>'z') and (a<>'w')
    and (a<>'y') and (a<>'k')
    and (a<>'A') and (a<>'B') and (a<>'C') and (a<>'D') and (a<>'E') and (a<>'F')
    and (a<>'G') and (a<>'H') and (a<>'I') and (a<>'J') and (a<>'L') and (a<>'M')
    and (a<>'N') and (a<>'O') and (a<>'P') and (a<>'Q') and (a<>'R') and (a<>'S')
    and (a<>'T') and (a<>'U') and (a<>'V') and (a<>'X') and (a<>'Z') and (a<>'W')
    and (a<>'Y') and (a<>'K') and (a<>'$') then
    Result := result + a;
  end;
end;

function  TiraVirgulaBarraNumero(frase :string):string;
var
  a: string;
  i  : integer;
begin
  result := '';
  if trim(frase)='' then
    exit;
  for i:=1 to length(frase) do begin
    a := copy(frase,i,1);
    if (a<>'/') and (a<>',')
    and (a<>'1') and (a<>'2') and (a<>'3') and (a<>'4') and (a<>'5') and (a<>'6')
    and (a<>'7') and (a<>'8') and (a<>'9') and (a<>'0') then
    Result := result + a;
  end;
end;

function  TiraLetraPonto(numero : string): string;
var
  a: string;
  i  : integer;
begin
  result := '';
  if trim(numero)='' then
    exit;
  for i:=1 to length(numero) do begin
    a := copy(numero,i,1);
    if  (a<>'a') and (a<>'b') and (a<>'c') and (a<>'d') and (a<>'e') and (a<>'f')
    and (a<>'g') and (a<>'h') and (a<>'i') and (a<>'j') and (a<>'l') and (a<>'m')
    and (a<>'n') and (a<>'o') and (a<>'p') and (a<>'q') and (a<>'r') and (a<>'s')
    and (a<>'t') and (a<>'u') and (a<>'v') and (a<>'x') and (a<>'z') and (a<>'w')
    and (a<>'y') and (a<>'k')
    and (a<>'A') and (a<>'B') and (a<>'C') and (a<>'D') and (a<>'E') and (a<>'F')
    and (a<>'G') and (a<>'H') and (a<>'I') and (a<>'J') and (a<>'L') and (a<>'M')
    and (a<>'N') and (a<>'O') and (a<>'P') and (a<>'Q') and (a<>'R') and (a<>'S')
    and (a<>'T') and (a<>'U') and (a<>'V') and (a<>'X') and (a<>'Z') and (a<>'W')
    and (a<>'Y') and (a<>'K') and (a<>'.') then
    Result := result + a;
  end;
end;

function  Formata_Inteiro(Numero:integer):string;
var
  aux : string;
begin
  aux := FormatFloat('###,###,##0',numero);
  aux := stringofchar(' ',11-length(aux))+aux;
  result := aux;
end;

function  Substitui_virgula_por_ponto(numero : string): string;
var
  a: string;
  i  : integer;
begin
  result := '';
  for i:=1 to length(numero) do begin
    a := copy(numero,i,1);
    if (a<>',') then
      Result := result + trim(a)
    else
      Result := result + '.';
  end;
  Result := trim(result);
end;

function LerIniConexao(sSecao, sVariavel: String): String;
var
  sNomeArq: String;
  iniArq: TIniFile;
begin
  Result   := '';
  sNomeArq := extractfiledir(application.ExeName);
  sNomeArq := sNomeArq + '\banco.ini';
  if FileExists(sNomeArq) then begin
    iniArq := TIniFile.Create(sNomeArq);
    Result := iniArq.ReadString(sSecao, sVariavel, '');     //le a string
    iniArq.Free;
  end;
  // verifica se o arquivo ini existe;
  if not FileExists(sNomeArq) then begin
    msgPerson('Banco n�o encontrado!',[mbok],'Conex�o com Banco',mtConfirmation);
    Application.Terminate;
  end;
end;

function  LerContas(var lista:TCheckListBox):String;
var
  aux : String;
  i   : Integer;
begin
  aux := '';
  for i:=0 to lista.Items.Count-1 do begin
    if lista.Checked[i] then begin
      if aux = '' then
        aux := '('+char(39)+copy(lista.Items[i],1,2)+char(39)
      else
        aux := aux + ','+char(39)+copy(lista.Items[i],1,2)+char(39);
    end;
  end;
  if aux <> '' then
    aux := aux + ')';
  Result := aux;
end;

function  LerItemIndex(var lista:TCheckListBox):String;
var
  aux : String;
  i   : Integer;
begin
  aux := '';
  for i:=0 to lista.Items.Count-1 do begin
    if lista.Checked[i] then begin
      if aux = '' then
        aux := '('+char(39)+IntToStr(i)+char(39)
      else
        aux := aux + ','+char(39)+IntToStr(i)+char(39);
    end;
  end;
  if aux <> '' then
    aux := aux + ')';
  Result := aux;
end;

function Criptografia(mStr, mChave: String): String;
var
  i, TamanhoString, pos, PosLetra, TamanhoChave: Integer;
begin
  Result := mStr;
  TamanhoString := Length(mStr);
  TamanhoChave := Length(mChave);
  for i := 1 to TamanhoString do begin
    pos := (i mod TamanhoChave);
    if pos = 0 then
      pos := TamanhoChave;
    posLetra := ord(Result[i]) xor ord(mChave[pos]);
    if posLetra = 0 then
      posLetra := ord(Result[i]);
    Result[i] := chr(posLetra);
  end;
end;

function Retorna_Serial_HD:String;
Var
  Serial:DWord;
  DirLen,Flags: DWord;
  DLabel : Array[0..11] of Char;
begin
  GetVolumeInformation(PChar('c:\'),dLabel,12,@Serial,DirLen,Flags,nil,0);
  Result := IntToHex(Serial,8);
end;

function  Retorna_Nome_Computador:String;
var
  I: DWord;
begin
  I := MAX_COMPUTERNAME_LENGTH + 1;
  SetLength(Result, I);
  Windows.GetComputerName(PChar(Result), I);
  Result := string(PChar(Result));
end;

function  Retorna_IP_Computador:String;
var
  r : TIdIPWatch;
begin
 r := TIdIPWatch.Create(nil);
 Result := r.LocalIP;
 r.Free;
end;

function Retorna_MacAddress:String;
var
  Lib: Cardinal;
  Func: function(GUID: PGUID): Longint; stdcall;
  GUID1, GUID2: TGUID;
begin
  Result := '';
  Lib := LoadLibrary('rpcrt4.dll');
  if Lib <> 0 then
  begin
    @Func := GetProcAddress(Lib, 'UuidCreateSequential');
    if Assigned(Func) then
    begin
      if (Func(@GUID1) = 0) and
         (Func(@GUID2) = 0) and
         (GUID1.D4[2] = GUID2.D4[2]) and
         (GUID1.D4[3] = GUID2.D4[3]) and
         (GUID1.D4[4] = GUID2.D4[4]) and
         (GUID1.D4[5] = GUID2.D4[5]) and
         (GUID1.D4[6] = GUID2.D4[6]) and
         (GUID1.D4[7] = GUID2.D4[7]) then
      begin
        Result :=
          IntToHex(GUID1.D4[2], 2) + '-' +
          IntToHex(GUID1.D4[3], 2) + '-' +
          IntToHex(GUID1.D4[4], 2) + '-' +
          IntToHex(GUID1.D4[5], 2) + '-' +
          IntToHex(GUID1.D4[6], 2) + '-' +
          IntToHex(GUID1.D4[7], 2);
      end;
    end;
  end;
end;

function  EspacoEsquerda(str : string; tam:integer): string;
var
  i   : integer;
  aux : string;
begin
  //esta fun��o completa de espacos a esquerda da string
  aux := '';
  for i:=1 to tam-length(str) do begin
    aux := aux + ' ';
  end;
  Result := aux+str;
end;

function  Formata_valor6(valor:Double):string;
var
  aux : string;
begin
  aux := FormatFloat('##0.00',valor);
  aux := stringofchar(' ',6-length(aux))+aux;
  result := aux;
end;

function  Formata_valor8(valor:Double):string;
var
  aux : string;
begin
  aux := FormatFloat('#,##0.00',valor);
  aux := stringofchar(' ',8-length(aux))+aux;
  result := aux;
end;

function MesExtenso( Mes:integer ) : string;
const meses : array[0..11] of PChar = ('Janeiro', 'Fevereiro', 'Mar�o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro','Outubro', 'Novembro', 'Dezembro');
begin
  result := meses[mes-1];
End;

function  TiraZeroEsquerda(numero : string): string;
var
  a: string;
  i  : integer;
  achou : boolean;
begin
  result := '';
  achou := false;
  for i:=1 to length(numero) do begin
    a := copy(numero,i,1);
    if (a<>'0') then
      achou := true;
    if achou=true then
      Result := result + a;
  end;
end;

function  SomenteNumero(str: String): String;
var
  i : Integer;
  a : Char;
begin
  result := '';
  for i:=1 to length(Str) do begin
    a := Str[i];

    if (a in ['0','1','2','3','4','5','6','7','8','9',#8]) then
      Result := Result + a

  end;

end;

function  ValidaCPF(num:string): boolean;
var
  n1,n2,n3,n4,n5,n6,n7,n8,n9: integer;
  d1,d2: integer;
  digitado, calculado: string;
begin
  if trim(num)='' then begin
    result := true;
    exit;
  end;
  if length(trim(num))<>11 then begin
    result:= false;
    exit;
  end;
  n1:=StrToInt(num[1]);
  n2:=StrToInt(num[2]);
  n3:=StrToInt(num[3]);
  n4:=StrToInt(num[4]);
  n5:=StrToInt(num[5]);
  n6:=StrToInt(num[6]);
  n7:=StrToInt(num[7]);
  n8:=StrToInt(num[8]);
  n9:=StrToInt(num[9]);
  d1:=n9*2+n8*3+n7*4+n6*5+n5*6+n4*7+n3*8+n2*9+n1*10;
  d1:=11-(d1 mod 11);
  if d1>=10 then d1:=0;
  d2:=d1*2+n9*3+n8*4+n7*5+n6*6+n5*7+n4*8+n3*9+n2*10+n1*11;
  d2:=11-(d2 mod 11);
  if d2>=10 then d2:=0;
  calculado:=inttostr(d1)+inttostr(d2);
  digitado:=num[10]+num[11];
  if calculado=digitado then
    Result:=true
  else
    Result:=false;
end;

function  RemoveAcento(texto : string): string;
Const
  ComAcento = '����������������������������';
  SemAcento = 'aaeouaoaeioucuAAEOUAOAEIOUCU';
Var
  x : Integer;
Begin
  For x := 1 to Length(texto) do
    if Pos(texto[x],ComAcento)<>0 Then
      texto[x] := SemAcento[Pos(texto[x],ComAcento)];
  Result := texto;
end;

function  RtfToHtml(RICH: TJvRichEdit): string;
var
  I,J,max_array: integer;
  html: string;
  F,FO: array [0..5] of string;
  str,lgt: integer;
  size,breaklines: integer;
  cor : string;
begin
  html := '';
  str := rich.SelStart;
  lgt := rich.SelLength;
  MAX_ARRAY := 5;
  breaklines:=0;



 for J := 0 to MAX_ARRAY do
  begin
    F[J] := '';
    FO[J] := '';
  end;



for I := 0 to length(RICH.text) do
  begin
    RICH.SelStart := I-BREAKLINES;
    RICH.SelLength := 1;
for J := 0 to MAX_ARRAY do
  FO[J] := F[J];

if (RICH.SelAttributes.Style - [fsItalic] - [fsUnderline] - [fsStrikeOut] = [fsBold]) then
  F[0] := '<b>'
else
  F[0] := '</b>';

if (RICH.SelAttributes.Style - [fsItalic] - [fsBold] - [fsStrikeOut] = [fsUnderline]) then
  F[1] := '<u>'
else
  F[1] := '</u>';

if (RICH.SelAttributes.Style - [fsUnderline] - [fsBold] - [fsStrikeOut] = [fsItalic]) then
  F[2] := '<i>'
else
  F[2] := '</i>';

if RICH.SelAttributes.size < 10 then
  SIZE := 1
else if RICH.SelAttributes.size < 12 then
  SIZE := 2
else if RICH.SelAttributes.size < 14 then
  SIZE := 3
else if RICH.SelAttributes.size < 18 then
  SIZE := 4
else if RICH.SelAttributes.size < 22 then
  SIZE := 5
else if RICH.SelAttributes.size < 32 then
  SIZE := 6
else
  SIZE := 7;
COR := ColorToString(RICH.SelAttributes.Color);
if (COR = 'clWindowText') or (COR = 'clBlack') then
  COR := '#000000'
else if COR = 'clWite' then
  COR := '#FFFFFF'
else if COR = 'clAqua' then
  COR := '#00FFFF'
else if COR = 'clFuchsia' then
  COR := '#FF00FF'
else if COR = 'clBlue' then
  COR := '#0000FF'
else if COR = 'clYellow' then
  COR := '#FFFF00'
else if COR = 'clLime' then
  COR := '#00FF00'
else if COR = 'clRed' then
  COR := '#FF0000'
else if COR = 'clSilver' then
  COR := '#C0C0C0'
else if COR = 'clGray' then
  COR := '#808080'
else if COR = 'clTeal' then
  COR := '#008080'
else if COR = 'clPurple' then
  COR := '#800080'
else if COR = 'clNavy' then
  COR := '#000080'
else if COR = 'clOlive' then
  COR := '#808000'
else if COR = 'clGreen' then
  COR := '#008000'
else if COR = 'clMaroon' then
  COR := '#800000'
else if copy(COR,1,1) = '$' then
  COR := '#'+copy(COR,length(COR)-1,2)
            +copy(COR,length(COR)-3,2)
            +copy(COR,length(COR)-5,2)
else
  COR := '#000000';
F[3] := '</font><font face="'+RICH.SelAttributes.Name+'" size='+inttostr(SIZE)+' color="'+COR+'">';

if RICH.Paragraph.Alignment = TParaAlignment(2) then
  F[4] := '<center>'
else
  F[4] := '</center>';

if RICH.Paragraph.Alignment = TParaAlignment(1) then
  F[5] := '<div align="right">'
else
  F[5] := '</div>';

for J := 0 to MAX_ARRAY do
  if FO[J] <> F[J] then
    HTML := HTML + F[J];

if copy(RICH.text,I+1,1) = #13 then
begin
  inc(BREAKLINES);
  HTML := HTML + '<br>';
end;

HTML := HTML + copy(RICH.text,I+1,1);
  end;
  RICH.SelStart := STR;
  RICH.SelLength := LGT;

  result := HTML;
end;

function  LerPalavras(var lista:TCheckListBox):String;
var
  aux : String;
  i   : Integer;
begin
  aux := '';
  for i:=0 to lista.Items.Count-1 do begin
    if lista.Checked[i] then begin
      if aux = '' then begin
        lista.ItemIndex := i;
        aux := '('+char(39)+lista.Items[lista.ItemIndex]+char(39)
      end else begin
        lista.ItemIndex := i;
        aux := aux + ','+char(39)+lista.Items[lista.ItemIndex]+char(39);
      end;
    end;
  end;
  if aux <> '' then
    aux := aux + ')';
  Result := aux;
end;

function  LerSelecionados(var lista:TClientDataSet; campo1, campo2, selecionado:String):String;
var
  aux : String;
  i   : Integer;
begin
  aux := '';
  lista.First;
  while not lista.Eof do begin
    if lista.FieldByName(selecionado).AsBoolean then begin
      if aux = '' then
        aux := char(39)+ZeroEsquerda(IntToStr(lista.FieldByName(campo1).AsInteger),3)+ZeroEsquerda(IntToStr(lista.FieldByName(campo2).AsInteger),3)+char(39)
      else
        aux := aux + ','+char(39)+ZeroEsquerda(IntToStr(lista.FieldByName(campo1).AsInteger),3)+ZeroEsquerda(IntToStr(lista.FieldByName(campo2).AsInteger),3)+char(39);
    end;
    lista.Next;
  end;

  Result := aux;
end;

function  LerPalavrasMemo(var lista:TMemo):String;
var
  aux : String;
  i   : Integer;
begin
  aux := '';
  for i:=0 to lista.Lines.Count-1 do begin
    if aux = '' then begin
      aux := '('+char(39)+lista.Lines[i]+char(39)
    end else begin
      aux := aux + ','+char(39)+lista.Lines[i]+char(39);
    end;
  end;
  if aux <> '' then
    aux := aux + ')';
  Result := aux;
end;

function  Mascara_CNPJ(cnpj:String):String;
begin
  Result := copy(cnpj,1,2)+'.'+copy(cnpj,3,3)+'.'+copy(cnpj,6,3)+'/'+copy(cnpj,9,4)+'-'+copy(cnpj,13,2);
end;

function  RetornaUsuarioLogadoRede: string;
Var
  NetUserNameLength: DWord;
Begin
  NetUserNameLength := 50;
  SetLength(Result, NetUserNameLength);
  GetUserName(pChar(Result),NetUserNameLength);
  SetLength(Result, StrLen(pChar(Result)));
end;

procedure EnviaErroPorEmail(Form: TForm; Erro, Usuario: String);
var
  i                    : integer;
  // vari�veis e objetos necess�rios para o envio
  IdSSLIOHandlerSocket : TIdSSLIOHandlerSocketOpenSSL;
  IdSMTP               : TIdSMTP;
  IdMessage            : TIdMessage;
  IdText               : TIdText;
  Bitmap: TBitmap;
  JPEG: TJpegImage;
begin
  // instancia��o dos objetos
  IdSSLIOHandlerSocket := TIdSSLIOHandlerSocketOpenSSL.Create(Form);
  IdSMTP               := TIdSMTP.Create(Form);
  IdMessage            := TIdMessage.Create(Form);

  try
    // Configura��o do protocolo SSL (TIdSSLIOHandlerSocketOpenSSL)
    IdSSLIOHandlerSocket.SSLOptions.Method := sslvTLSv1_2; //sslvSSLv23
    IdSSLIOHandlerSocket.SSLOptions.Mode   := sslmClient;

    // Configura��o do servidor SMTP (TIdSMTP)
    IdSMTP.IOHandler := IdSSLIOHandlerSocket;
    IdSMTP.UseTLS    := utUseExplicitTLS;
    IdSMTP.AuthType  := satDefault;
    IdSMTP.Port      := 587;
    IdSMTP.Host      := 'smtp.gmail.com';
    IdSMTP.Username  := 'xandyhg@gmail.com';
    IdSMTP.Password  := 'kvnphodlhutbgqzu'; //'xandy101981';

    // Configura��o da mensagem (TIdMessage)
    IdMessage.From.Address              := 'xandyhg@gmail.com';
    IdMessage.From.Name                 := 'xandyhg@gmail.com';
//    IdMessage.ReplyTo.EMailAddresses    := IdMessage.From.Address;
    IdMessage.Recipients.EMailAddresses := 'xandyhg@gmail.com';
//    IdMessage.BccList.EMailAddresses    := mCco.Text;
//    IdMessage.CCList.EMailAddresses     := mCC.Text;
    IdMessage.Subject                   := 'Erro no Sistema Lanchonete Coelh�o';
    IdMessage.Encoding                  := meMIME;

    // Configura��o do corpo do email (TIdText)
    IdText := TIdText.Create(IdMessage.MessageParts);
    IdText.ContentType := 'text/html; charset=iso-8859-1';
    IdText.Body.Add('Ocorreu um erro na m�quina : '+Retorna_Nome_Computador+#13+#13+
                    'Esta m�quina possui o IP: '+Retorna_IP_Computador+#13+#13+
                    'Usu�rio logado: '+Usuario+#13+#13+
                    'A mensagem de erro � a seguinte: '+Erro+#13);

    JPEG := TJpegImage.Create;
    Bitmap := Form.GetFormImage;
    JPEG.Assign(Bitmap);
    JPEG.SaveToFile(Format('%s\%s.jpg', [GetCurrentDir, 'Erro']));
    TIdAttachmentFile.Create(IdMessage.MessageParts, Format('%s\%s.jpg', [GetCurrentDir, 'Erro']));
    JPEG.Free;
    Bitmap.Free;
    // Conex�o e autentica��o
    try
      IdSMTP.Connect;
      IdSMTP.Authenticate;
    except
      on E:Exception do begin
        MessageDlg('Erro na conex�o ou autentica��o: ' +
          E.Message, mtWarning, [mbOK], 0);
        Exit;
      end;
    end;

    // Envio da mensagem
//    try
    IdSMTP.Send(IdMessage);
//      MessageDlg('Mensagem enviada com sucesso!', mtInformation, [mbOK], 0);
//    except
//      On E:Exception do
//      begin
//        MessageDlg('Erro ao enviar a mensagem: ' +
//          E.Message, mtWarning, [mbOK], 0);
//      end;
//    end;
  finally
    // desconecta do servidor
    IdSMTP.Disconnect;
//    icoEnviar.Show;
//    pnEnviar.Show;
//    Panel2.Enabled := True;
//    Panel3.Enabled := True;
//    // libera��o da DLL
    UnLoadOpenSSLLibrary;
    // libera��o dos objetos da mem�ria
    FreeAndNil(IdMessage);
    FreeAndNil(IdSSLIOHandlerSocket);
    FreeAndNil(IdSMTP);
  end;
end;

procedure EnviaTarefaPorEmail(Form : TForm; PPort:Integer; PHost, PUsername, PPassword, PDestinatario, PUsuario, PTarefa, PObs: String; PDataTarefa: TDateTime);
var
  i                    : integer;
  // vari�veis e objetos necess�rios para o envio
  IdSSLIOHandlerSocket : TIdSSLIOHandlerSocketOpenSSL;
  IdSMTP               : TIdSMTP;
  IdMessage            : TIdMessage;
  IdText               : TIdText;
  Bitmap: TBitmap;
  JPEG: TJpegImage;
begin
  // instancia��o dos objetos
  IdSSLIOHandlerSocket := TIdSSLIOHandlerSocketOpenSSL.Create(Form);
  IdSMTP               := TIdSMTP.Create(Form);
  IdMessage            := TIdMessage.Create(Form);

  try
    // Configura��o do protocolo SSL (TIdSSLIOHandlerSocketOpenSSL)
    IdSSLIOHandlerSocket.SSLOptions.Method := sslvSSLv23;
    IdSSLIOHandlerSocket.SSLOptions.Mode   := sslmClient;

    // Configura��o do servidor SMTP (TIdSMTP)
    IdSMTP.IOHandler := IdSSLIOHandlerSocket;
    IdSMTP.UseTLS    := utUseExplicitTLS;
    IdSMTP.AuthType  := satDefault;
    IdSMTP.Port      := PPort;
    IdSMTP.Host      := PHost;
    IdSMTP.Username  := PUsername;
    IdSMTP.Password  := PPassword;

    // Configura��o da mensagem (TIdMessage)
    IdMessage.From.Address              := PUsername;
    IdMessage.From.Name                 := PUsername;
//    IdMessage.ReplyTo.EMailAddresses    := IdMessage.From.Address;
    IdMessage.Recipients.EMailAddresses := PDestinatario;
//    IdMessage.BccList.EMailAddresses    := mCco.Text;
//    IdMessage.CCList.EMailAddresses     := mCC.Text;
    IdMessage.Subject                   := 'T�rmino de tarefa';
    IdMessage.Encoding                  := meMIME;

    // Configura��o do corpo do email (TIdText)
    IdText := TIdText.Create(IdMessage.MessageParts);
    IdText.ContentType := 'text/html; charset=iso-8859-1';
    IdText.Body.Add('Tarefa finalizada: '+PTarefa+#13+#13+#13+
                    ' - Data da tarefa: '+DateTimeToStr(PDataTarefa)+#13+#13+#13+
                    ' - Usu�rio: '+PUsuario+#13+#13+#13+
                    ' - Obs: '+PObs+#13+#13+#13);
    // Conex�o e autentica��o
    try
      IdSMTP.Connect;
      IdSMTP.Authenticate;
    except
      on E:Exception do begin
        MessageDlg('Erro na conex�o ou autentica��o: ' +
          E.Message, mtWarning, [mbOK], 0);
        Exit;
      end;
    end;

    // Envio da mensagem
    IdSMTP.Send(IdMessage);
  finally
    // desconecta do servidor
    IdSMTP.Disconnect;
    UnLoadOpenSSLLibrary;
    // libera��o dos objetos da mem�ria
    FreeAndNil(IdMessage);
    FreeAndNil(IdSSLIOHandlerSocket);
    FreeAndNil(IdSMTP);
  end;
end;

function  Diferenca_duas_horas(Inicio,Fim:String):String;
var
  FIni,FFim : TDateTime;
  aux : string;
begin
  Fini := StrTotime(copy(Inicio,1,8));
  FFim := StrToTime(copy(Fim,1,8));
  If (Inicio > Fim) then
    Result := copy(TimeToStr((StrTotime('23:59:59')-Fini)+FFim),1,8)
  else
    Result := copy(TimeToStr(FFim-Fini),1,8);
  aux := result;
end;

function SomaHoras(Hora, HoraSoma :String):String;
var hh, mm :integer;
    vHora, vMinuto :integer;
begin
  hh:= StrToInt(Copy(Hora, 1, 2)) + StrToInt(Copy(HoraSoma, 1, 2));
  mm:= StrToInt(Copy(Hora, 4, 2)) + StrToInt(Copy(HoraSoma, 4, 2));

  vMinuto:= mm mod 60;
  vHora:=   hh + (mm div 60);

  Result:= FormatFloat('00:', vHora) + FormatFloat('00', vMinuto);
end;

function SegundosToTime( Segundos : Cardinal ) : String;
var
  Seg, Min, Hora: Cardinal;
begin
  Hora := Segundos div 3600;
  Seg := Segundos mod 3600;
  Min := Seg div 60;
  Seg := Seg mod 60;

  Result := FormatFloat(',00', Hora) + ':' +
  FormatFloat('00', Min) + ':' +
  FormatFloat('00', Seg);
end;

procedure CriaFormPopup(Form : TComponent);
var
  I: Integer;
  fAlertaMsn  : TForm;
  BarraIniciar: HWND; {Barra Iniciar}
  tmAltura: Integer;
  tmRect: TRect;
  xTop : Integer;
  xIniTop : Integer;
  OLabel  : TLabel;
begin
  //localiza o Handle da janela iniciar
  BarraIniciar := FindWindow('Shell_TrayWnd', nil);
  //Pega o "ret�ngulo" que envolve a barra e sua altura
  GetWindowRect(BarraIniciar, tmRect);
  tmAltura := tmRect.Bottom - tmRect.Top;
  fAlertaMsn := TForm.Create(Form);
  with fAlertaMsn do begin
    Left := Screen.Width - ClientWidth;
    if tmRect.Top = -2 then
      tmAltura := 30;
    //Pega o top final
    xTop := Screen.Height - ClientHeight - tmAltura;
    //Pega o top inicial
    xIniTop := Screen.Height + ClientHeight + tmAltura;
    Top := xIniTop;
    OLabel := TLabel.create(application);
    OLabel.Parent := fAlertaMsn;
    OLabel.Name := 'LABEL1';
    OLabel.Left := 10;
    OLabel.Top := 10;
    OLabel.font.Size := 20;
    OLabel.Caption := 'Acabou de entrar um Lead para voc�!';
    for I := xIniTop downto xTop do begin
      Top := Top - 1;
      Show;
      Update;
      Application.ProcessMessages;
      Sleep(10);
    end;
    Sleep(3000);
    Close;
    FreeAndNil(fAlertaMsn);
  end;
end;

function  Mascara_CPF(cpf:string):string;
begin
  Result := copy(cpf,1,3)+'.'+copy(cpf,4,3)+'.'+copy(cpf,7,3)+'-'+copy(cpf,10,2);
end;

function TiraSomenteEspaco;
var
  a: string;
  i  : integer;
begin
  result := '';
  for i:=1 to length(texto) do begin
    a := copy(texto,i,1);
    if a<>' ' then
      Result := result + a;
  end;
end;

function BrowseURL(const URL: string) : boolean;
var
   Browser: string;
begin
   Result := True;
   Browser := '';
   with TRegistry.Create do
   try
     RootKey := HKEY_CLASSES_ROOT;
     Access := KEY_QUERY_VALUE;
     if OpenKey('\ChromeHTML\shell\open\command', False) then //Comando para abrir no navegador Google Chrome. ChromeHTML � o registro dentro do RegEdit
       Browser := ReadString('') ;
     CloseKey;
   finally
     Free;
   end;
   if Browser = '' then
   begin
     Result := False;
     Exit;
   end;
   Browser := Copy(Browser, Pos('"', Browser) + 1, Length(Browser)) ;
   Browser := Copy(Browser, 1, Pos('"', Browser) - 1) ;
   ShellExecute(0, 'open', PChar(Browser), PChar(URL), nil, SW_SHOWMAXIMIZED) ;
end;

function  RetornaDiaDaSemana(PData:TDateTime):String;
begin
  if (DayOfWeek(PData))=1 then
    Result := 'Domingo'
  else
    if (DayOfWeek(PData))=2 then
      Result := 'Segunda'
    else
      if (DayOfWeek(PData))=3 then
        Result := 'Ter�a'
      else
        if (DayOfWeek(PData))=4 then
          Result := 'Quarta'
        else
          if (DayOfWeek(PData))=5 then
            Result := 'Quinta'
          else
            if (DayOfWeek(PData))=6 then
              Result := 'Sexta'
            else
              if (DayOfWeek(PData))=7 then
                Result := 'S�bado';
end;

function  RetornaMes(PData:TDateTime):String;
begin
  if (MonthOf(PData))=1 then
    Result := 'Janeiro'
  else
    if (MonthOf(PData))=2 then
      Result := 'Fevereiro'
    else
      if (MonthOf(PData))=3 then
        Result := 'Mar�o'
      else
        if (MonthOf(PData))=4 then
          Result := 'Abril'
        else
          if (MonthOf(PData))=5 then
            Result := 'Maio'
          else
            if (MonthOf(PData))=6 then
              Result := 'Junho'
            else
              if (MonthOf(PData))=7 then
                Result := 'Julho'
              else
                if (MonthOf(PData))=8 then
                  Result := 'Agosto'
                else
                  if (MonthOf(PData))=9 then
                    Result := 'Setembro'
                  else
                    if (MonthOf(PData))=10 then
                      Result := 'Outubro'
                    else
                      if (MonthOf(PData))=11 then
                        Result := 'Novembro'
                      else
                        if (MonthOf(PData))=12 then
                          Result := 'Dezembro';
end;

function  PercentualValorFinal(Valor,Percentual:Double):Double;
begin
  Result := Valor - ((Valor*Percentual)/100);
end;

function  AumentaMes(Data:TDateTime;NumMes:Integer):TDateTime;
var
  i          : Integer;
  xDataFinal : TDateTime;
begin
  xDataFinal := Data;
  for i := 1 to NumMes do
    xDataFinal := IncMonth(xDataFinal);
  Result := xDataFinal;
end;

function  Substitui_barra_por_ponto(data : string): string;
var
  a: string;
  i  : integer;
begin
  result := '';
  for i:=1 to length(data) do begin
    a := copy(data,i,1);
    if (a<>'/') then
      Result := result + trim(a)
    else
      Result := result + '.';
  end;
  Result := trim(result);
end;

function  LerDatas(var lista:TCheckListBox):String;
var
  aux : String;
  i   : Integer;
begin
  aux := '';
  for i:=0 to lista.Items.Count-1 do begin
    if lista.Checked[i] then begin
      if aux = '' then begin
        lista.ItemIndex := i;
        aux := '('+char(39)+Substitui_barra_por_ponto(lista.Items[lista.ItemIndex])+char(39)
      end else begin
        lista.ItemIndex := i;
        aux := aux + ','+char(39)+Substitui_barra_por_ponto(lista.Items[lista.ItemIndex])+char(39);
      end;
    end;
  end;
  if aux <> '' then
    aux := aux + ')';
  Result := aux;
end;

function  LerPalavrasParteCheckList(var lista:TCheckListBox;inicio,fim:Integer):String;
var
  aux : String;
  i   : Integer;
begin
  aux := '';
  for i:=0 to lista.Items.Count-1 do begin
    if lista.Checked[i] then begin
      if aux = '' then begin
        lista.ItemIndex := i;
        aux := '('+char(39)+Copy(lista.Items[lista.ItemIndex],inicio,fim)+char(39)
      end else begin
        lista.ItemIndex := i;
        aux := aux + ','+char(39)+Copy(lista.Items[lista.ItemIndex],inicio,fim)+char(39);
      end;
    end;
  end;
  if aux <> '' then
    aux := aux + ')';
  Result := aux;
end;

function  LerQuery3Numeros(Campo:String;tq:TFDQuery):String;
var
  aux : String;
begin
  aux := '';
  while not tq.Eof do begin
    if aux = '' then
      aux := '('+char(39)+ZeroEsquerda(IntToStr(tq.FieldByName(Campo).AsInteger),2)+char(39)
    else
      aux := aux + ','+char(39)+ZeroEsquerda(IntToStr(tq.FieldByName(Campo).AsInteger),2)+char(39);
    tq.Next;
  end;
  if aux <> '' then
    aux := aux + ')';
  Result := aux;
end;

function  Diferenca_duas_data(d_ini,d_fim:tdatetime):integer;
Var
  Data: TDateTime;
  dia, mes, ano: Word;
begin
  data := d_fim-d_ini;
  DecodeDate( Data, ano, mes, dia);

  if d_ini = d_fim then
    Result := 1
  else
    Result := strtoint(FloatToStr(Data));
end;

function RetornaTextoEntre2CaracIguais(Texto:String; Caracter:Char):String;
var
  I,Posicao1,Posicao2 :Integer;
  TextoInvertido      :String;
begin
  Result := '';
  for I  := Length(Texto) downto 1 do begin
    TextoInvertido := TextoInvertido+Texto[I];
  end;
  Posicao1 := Pos(Caracter,Texto)+1;
  Posicao2 := Pos(Caracter,TextoInvertido)-1;
  Result   := Copy(Texto,Posicao1,Length(Texto)-(Posicao1+Posicao2));
end;

function  RetornaTextoEntre2CaracDiferente(Texto,Delimitador1,Delimitador2:String):String;
begin
  Result := Trim(Copy(Texto, Pos(Delimitador1, Texto)+1, (Pos(Delimitador2, Texto)-1) - (Pos(Delimitador1, Texto)+1)));
end;

function  RetornaTextoDeUmCaracAteFinal(Texto,Delimitador1:String):String;
begin
  Result := Trim(Copy(Texto, Pos(Delimitador1, Texto) +1, Length(Texto)));
end;

function  TiraCodigoPais(numero:String):String;
var
  a : String;
begin
  result := '';
  if trim(numero)='' then
    exit;
  a := numero;
  if Copy(a,1,2) = '55' then
    Result := Copy(a,3,Length(a))
  else if Copy(a,1,2) = '+55' then
    Result := Copy(a,4,Length(a))
  else
    Result := a;
end;

function RetiraEnter(aText : string): string;
begin
  { Retirando as quebras de linha em campos blob }
  Result := StringReplace(aText, #$D#$A, '', [rfReplaceAll]);

  { Retirando as quebras de linha em campos blob }
  Result := StringReplace(Result, #13#10, '', [rfReplaceAll]);
end;

function  Mascara_CEP(cep:String):String;
begin
  Result := copy(cep,1,2)+'.'+copy(cep,3,3)+'-'+copy(cep,6,3);
end;

function  RetIdxUltimaColVisivel(DBGrid: TDBGrid): Integer;
var
  x: integer;
begin
  Result := -1;

  for x := DBGrid.Columns.Count-1 downto 0 do begin
    if  DBGrid.Columns.Items[x].Visible then begin
      Result := x;
      Break;
    end;
  end;
end;

function  NaoInsereDBGrid(DBGrid: TDBGrid; DataSet: TDataSet; Key: Word): Word;
begin
  { N�o deixa inserir registros no DBGrid caso:
    1- estiver em modo de edi��o: E
    1.1- se clicar na tecla "Insert"; OU
    1.2- se clicar na tecla "Seta para baixo" E estiver no �ltimo registro; OU
    1.2- se clicar na tecla "Tab" E estiver no �ltimo registro E estiver na
         �ltima coluna vis�vel. }
  Result := Key;

  if dgEditing in DBGrid.Options then begin
    if (Key = VK_INSERT) or ((Key = VK_DOWN) and (DataSet.RecNo = DataSet.RecordCount)) then
      Result := 0
    else if (Key = VK_TAB) and (DataSet.RecNo = DataSet.RecordCount) then begin
      if (DBGrid.SelectedIndex = RetIdxUltimaColVisivel(DBGrid)) then
        Result := 0;
    end;
  end;
end;

function  InverterPalavras(Texto:String):String;
begin
  Result := ReverseString(Texto);
end;

procedure GetImageByUrl(URL: string; APicture: TPicture;vIdHTTP : TIdHTTP);
var
  Jpeg: TJPEGImage;
  Strm: TMemoryStream;
begin
  Screen.Cursor := crHourGlass;
  Jpeg := TJPEGImage.Create;
  Strm := TMemoryStream.Create;
  try
    vIdHTTP.Get(URL, Strm);
    if (Strm.Size > 0) then
    begin
      Strm.Position := 0;
      Jpeg.LoadFromStream(Strm);
      APicture.Assign(Jpeg);
    end;
  finally
    Strm.Free;
    Jpeg.Free;
    Screen.Cursor := crDefault;
  end;
end;

function  FormataNome(sNome: String): String;
const
  excecao: Array[0..5] of String = (' da ',' de ',' do ',' das ',' dos ',' e ');
var
  tamanho, j: integer;
  i: byte;
begin
  Result := AnsiLowerCase(sNome);
  tamanho := Length(Result);

  for j := 1 to tamanho do
    // Se � a primeira letra ou se o caracter anterior � um espa�o
    if (j = 1) or ((j>1) and (Result[j-1]=Chr(32))) then
      Result[j] := AnsiUpperCase(Result[j])[1];
  for i := 0 to Length(excecao)-1 do
    result:= StringReplace(result,excecao[i],excecao[i],[rfReplaceAll, rfIgnoreCase]);
end;

function  PalavraExiste(PPalavra,PTexto:String):Boolean;
begin
  if Pos(PPalavra,PTexto) <> 0 then
    Result := True
  else
    Result := False;
end;

procedure GetImageByUrl2(URL: string; APicture: TPicture);
var
  Jpeg: TJPEGImage;
  Strm: TMemoryStream;
  vIdHTTP : TIdHTTP;
begin
  Screen.Cursor := crHourGlass;
  Jpeg := TJPEGImage.Create;
  Strm := TMemoryStream.Create;
  vIdHTTP := TIdHTTP.Create(nil);
  try
    vIdHTTP.Get(URL, Strm);
    if (Strm.Size > 0) then
    begin
      Strm.Position := 0;
      Jpeg.LoadFromStream(Strm);
      APicture.Assign(Jpeg);
    end;
  finally
    Strm.Free;
    Jpeg.Free;
    vIdHTTP.Free;
    Screen.Cursor := crDefault;
  end;
end;

end.
