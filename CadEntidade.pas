unit CadEntidade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, CadPadrao, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.StdCtrls, Vcl.Mask, JvExMask, JvToolEdit,
  JvBaseEdits, Vcl.ComCtrls, FireDAC.Comp.Client, Vcl.Buttons,
  Soap.InvokeRegistry, Soap.Rio, Soap.SOAPHTTPClient, WinInet;

type
  TF_CadEntidade = class(TF_CadPadrao)
    edCodigo: TJvCalcEdit;
    Label1: TLabel;
    edEnt_DataCadastro: TJvDateEdit;
    Label3: TLabel;
    Panel1: TPanel;
    rgEnt_Tipo: TRadioGroup;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label24: TLabel;
    sbCidade: TSpeedButton;
    Label70: TLabel;
    edEnd_Logradouro: TEdit;
    edEnd_Bairro: TEdit;
    edEnd_Numero: TEdit;
    edEnd_Complemento: TEdit;
    edCid_ID: TJvCalcEdit;
    edDescCidade: TEdit;
    edDescUF: TEdit;
    edEnd_Cep: TEdit;
    edEnd_Referencia: TEdit;
    HTTPRIO1: THTTPRIO;
    rgEnt_Tipo_Pessoa: TRadioGroup;
    Panel9: TPanel;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    Label12: TLabel;
    rgEnt_Tipo_Cliente: TRadioGroup;
    rgEnt_liberado: TRadioGroup;
    edEnt_limite: TJvCalcEdit;
    TabSheet1: TTabSheet;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    edEnt_DataNasc: TJvDateEdit;
    edEnt_RG: TEdit;
    edEnt_CPF: TEdit;
    rgEnt_Sexo: TRadioGroup;
    edEnt_EstadoCivil: TComboBox;
    TabSheet2: TTabSheet;
    Panel8: TPanel;
    Label36: TLabel;
    Label42: TLabel;
    edEnt_cnpj: TEdit;
    edEnt_nome_fantasia: TEdit;
    Panel7: TPanel;
    Label2: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label7: TLabel;
    Label26: TLabel;
    Label17: TLabel;
    edEnt_nome: TEdit;
    edEnt_Telefone: TEdit;
    edEnt_Celular: TEdit;
    edEnt_email: TEdit;
    edEnt_Contato: TEdit;
    edEnt_Celular_contato: TEdit;
    edEnt_Obs: TMemo;
    rgEnt_Ativo: TRadioGroup;
    Panel10: TPanel;
    edFun_id: TJvCalcEdit;
    sbFuncao: TSpeedButton;
    edFun_descricao: TEdit;
    Label13: TLabel;
    Label59: TLabel;
    edSet_id: TJvCalcEdit;
    sbSetor: TSpeedButton;
    edSet_Descricao: TEdit;
    procedure FormActivate(Sender: TObject);
    procedure icoAlterarClick(Sender: TObject);
    procedure icoCancelarClick(Sender: TObject);
    procedure icoExcluirClick(Sender: TObject);
    procedure icoSalvarClick(Sender: TObject);
    procedure edEnt_CPFEnter(Sender: TObject);
    procedure edEnt_CPFExit(Sender: TObject);
    procedure edEnt_CelularEnter(Sender: TObject);
    procedure edEnt_CelularExit(Sender: TObject);
    procedure edEnt_TelefoneEnter(Sender: TObject);
    procedure edEnt_TelefoneExit(Sender: TObject);
    procedure edEnt_Celular_contatoEnter(Sender: TObject);
    procedure edEnt_Celular_contatoExit(Sender: TObject);
    procedure edEnt_emailExit(Sender: TObject);
    procedure edEnd_CepEnter(Sender: TObject);
    procedure edEnd_CepExit(Sender: TObject);
    procedure edEnt_cnpjEnter(Sender: TObject);
    procedure edEnt_cnpjExit(Sender: TObject);
    procedure rgEnt_TipoClick(Sender: TObject);
    procedure rgEnt_Tipo_PessoaClick(Sender: TObject);
    procedure sbCidadeClick(Sender: TObject);
    procedure edCid_IDExit(Sender: TObject);
    procedure edCid_IDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbSetorClick(Sender: TObject);
    procedure edSet_idExit(Sender: TObject);
    procedure edSet_idKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edFun_idExit(Sender: TObject);
    procedure edFun_idKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbFuncaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ConsultaCorreio;
  end;

var
  F_CadEntidade         : TF_CadEntidade;

implementation

{$R *.dfm}

uses DMC, clsEndereco, clsEntidade, ConEntidade, X, U_Principal, WsCorreios,
  clsCidade, ConCidade, ConSetor, ConFuncao, clsSetor, clsFuncao;

var
  Entidade : TEntidade;
  Endereco : TEndereco;
  Cidade   : TCidade;
  Setor    : TSetor;
  Funcao   : TFuncao;

procedure TF_CadEntidade.edEnt_Celular_contatoEnter(Sender: TObject);
begin
  inherited;
  edEnt_Celular_contato.Text := SomenteNumero(edEnt_Celular_contato.Text);
  edEnt_Celular_contato.SelectAll;
end;

procedure TF_CadEntidade.edEnt_Celular_contatoExit(Sender: TObject);
var
  xFone : String;
begin
  inherited;
  xFone := Trim(TiraZeroEsquerda(SomenteNumero(edEnt_Celular_contato.Text))); //Celular do Cliente
  if Length(xFone) = 8 then
    edEnt_Celular_contato.Text := Copy(xFone,1,4)+'-'+Copy(xFone,5,4) //Telefone normal ou celular sem o d�gito 9, sem o DDD
  else if Length(xFone) = 9 then
    edEnt_Celular_contato.Text := Copy(xFone,1,5)+'-'+Copy(xFone,6,4) //Telefone Celular, com o d�gito 9 a mais, sem DDD
  else if Length(xFone) = 10 then
    edEnt_Celular_contato.Text := '('+Copy(xFone,1,2)+')'+Copy(xFone,3,4)+'-'+Copy(xFone,7,4) //Telefone normal ou celular sem o d�gito 9, com o DDD
  else if Length(xFone) = 11 then
    edEnt_Celular_contato.Text := '('+Copy(xFone,1,2)+')'+Copy(xFone,3,5)+'-'+Copy(xFone,8,4) //Telefone Celular, com o d�gito 9 a mais, com DDD
  else
    edEnt_Celular_contato.Text := Copy(StringOfChar(' ',15),1,15);
  if Trim(edEnt_Celular_contato.Text) <> '' then begin
    Entidade := TEntidade.Create;
    if Entidade.RecuperarPorCelular(edEnt_Celular_contato.Text) then begin
      if Entidade.ent_id <> edCodigo.AsInteger then begin
        msgPerson('J� existe um cadastro com o mesmo celular!'+#13+'C�digo: '+IntToStr(Entidade.ent_id)+#13+'Cliente: '+Entidade.ent_nome+#13+'CPF: '+Entidade.ent_cpf,[mbok],'Cliente',mtWarning);
        Exit;
      end;
    end;
    FreeAndNil(Entidade);
  end;
end;

procedure TF_CadEntidade.edEnt_cnpjEnter(Sender: TObject);
begin
  inherited;
  edEnt_cnpj.Text := SomenteNumero(edEnt_cnpj.Text);
  edEnt_cnpj.SelectAll;
end;

procedure TF_CadEntidade.edEnt_cnpjExit(Sender: TObject);
var
  xCNPJ : String;
begin
  inherited;
  xCNPJ := Trim(SomenteNumero(edEnt_cnpj.Text));
  if Trim(xCNPJ) <> '' then begin
    edEnt_cnpj.Text := Mascara_CNPJ(xCNPJ);

    Entidade := TEntidade.Create;
    if Entidade.RecuperarPorCNPJ(edEnt_cnpj.Text) then begin
      if Entidade.ent_id <> edCodigo.AsInteger then begin
        msgPerson('J� existe um cadastro com o mesmo CNPJ!'+#13+'C�digo: '+IntToStr(Entidade.ent_id)+#13+'Cliente: '+Entidade.ent_nome+#13+'CNPJ: '+Entidade.ent_cnpj,[mbok],'Cliente',mtWarning);
        Exit;
      end;
    end;
    FreeAndNil(Entidade);
  end;
end;

procedure TF_CadEntidade.edEnt_emailExit(Sender: TObject);
begin
  inherited;
  if Trim(edEnt_email.Text) <> '' then begin
    Entidade := TEntidade.Create;
    if Entidade.RecuperarPorEmail(edEnt_email.Text) then begin
      if Entidade.ent_id <> edCodigo.AsInteger then begin
        msgPerson('J� existe um cadastro com o mesmo email!'+#13+'C�digo: '+IntToStr(Entidade.ent_id)+#13+'Cliente: '+Entidade.ent_nome+#13+'CPF: '+Entidade.ent_cpf,[mbok],'Cliente',mtWarning);
        Exit;
      end;
    end;
    FreeAndNil(Entidade);
  end;
end;

procedure TF_CadEntidade.ConsultaCorreio;
var
  wsAtendeCliente   : AtendeCliente;
  wsConsultaCepResp : enderecoERP;
  xCidade           : String;
begin
  try
    try
      wsConsultaCepResp := enderecoERP.Create;
      Cidade := TCidade.Create;

      HTTPRIO1.HTTPWebNode.UseUTF8InHeader := True;
      wsAtendeCliente := GetAtendeCliente(False, '', HTTPRIO1);

      try
        wsConsultaCepResp := wsAtendeCliente.consultaCEP(edEnd_Cep.Text);
//      except
//        on E: ERemotableException do
//        begin
//          if (UpperCase(Trim(E.message)) = 'CEP NAO ENCONTRADO') or (UpperCase(Trim(E.message)) = 'CEP NAO INFORMADO') then
//          begin
//            Application.MessageBox(PChar(Format('Aten��o! Cep %s n�o informado/encontrado!', [edEnd_Cep.Text])), PChar('Mensagem'), MB_OK + MB_ICONINFORMATION);
//            Exit;
//          end
//          else
//          begin
//            raise Exception.Create( E.message );
//          end;
//        end;
//      end;

      //Preenche os campos com as informa��es do endere�o
        edEnd_Logradouro.Text  := wsConsultaCepResp.end_;
        edEnd_Bairro.Text      := wsConsultaCepResp.bairro;
        if Cidade.RecuperarPorCidadeUF(AnsiUpperCase(wsConsultaCepResp.cidade),wsConsultaCepResp.uf) then begin
          edCid_ID.AsInteger   := Cidade.Cid_id;
          edDescCidade.Text    := Cidade.Cid_Cidade;
          edDescUF.Text        := Cidade.Cid_UF;
        end;
        edEnd_Complemento.Text := wsConsultaCepResp.complemento2;
        edEnd_Referencia.Clear;
        edEnd_Numero.SetFocus;
      except
        edEnd_Logradouro.Clear;
        edEnd_Bairro.Clear;
        edEnd_Numero.Clear;
        edEnd_Complemento.Clear;
        edCid_ID.Clear;
        edDescCidade.Clear;
        edDescUF.Clear;
        edEnd_Referencia.Clear;
        edEnd_Logradouro.SetFocus;
      end;
    finally
      FreeAndNil(wsConsultaCepResp);
      FreeAndNil(Cidade);
//      wsAtendeCliente._Release;
    end
  except
    on E: Exception do begin
      Application.MessageBox(PChar(Format('Aten��o! %s%s ', [#13#13, E.message])), PChar('Erro'), MB_OK + MB_ICONERROR);
    end;
  end;
end;

procedure TF_CadEntidade.edCid_IDExit(Sender: TObject);
begin
  inherited;
  if edCid_ID.AsInteger > 0 then begin
    Cidade := TCidade.Create;
    if Cidade.Recuperar(edCid_ID.AsInteger) then begin
      edDescCidade.Text := Cidade.Cid_Cidade;
      edDescUF.Text     := Cidade.Cid_UF;
    end else begin
      msgPerson('Cidade n�o cadastrada!',[mbok],'Entidade',mtError);
      edCid_ID.SetFocus;
      edCid_ID.SelectAll;
    end;
    FreeAndNil(Cidade);
  end else begin
    edDescCidade.Clear;
    edDescUF.Clear;
  end;
end;

procedure TF_CadEntidade.edCid_IDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = 113 then
    sbCidadeClick(Sender);
end;

procedure TF_CadEntidade.edEnd_CepEnter(Sender: TObject);
begin
  inherited;
  edEnd_Cep.Text := SomenteNumero(edEnd_Cep.Text);
  edEnd_Cep.SelectAll;
end;

procedure TF_CadEntidade.edEnd_CepExit(Sender: TObject);
var
  xCep  : String;
  flags : DWORD;
begin
  inherited;
  if (Trim(edEnd_Cep.Text) <> '') then begin
    xCep := ZeroEsquerda(SomenteNumero(edEnd_Cep.Text),8);
    edEnd_Cep.Text := Copy(xCep,1,2)+'.'+Copy(xCep,3,3)+'-'+Copy(xCep,6,3);
    if (Trim(edEnd_Logradouro.Text) = '') then begin
      if not InternetGetConnectedState(@flags, 0) then
        msgPerson('Sem conex�o com a internet!',[mbok],'Aluno',mtError)
      else
        ConsultaCorreio;
    end;
  end;
end;

procedure TF_CadEntidade.edEnt_CelularEnter(Sender: TObject);
begin
  inherited;
  edEnt_Celular.Text := SomenteNumero(edEnt_Celular.Text);
  edEnt_Celular.SelectAll;
end;

procedure TF_CadEntidade.edEnt_CelularExit(Sender: TObject);
var
  xFone : String;
begin
  inherited;
  xFone := Trim(TiraZeroEsquerda(SomenteNumero(edEnt_Celular.Text))); //Celular do Cliente
  if Length(xFone) = 8 then
    edEnt_Celular.Text := Copy(xFone,1,4)+'-'+Copy(xFone,5,4) //Telefone normal ou celular sem o d�gito 9, sem o DDD
  else if Length(xFone) = 9 then
    edEnt_Celular.Text := Copy(xFone,1,5)+'-'+Copy(xFone,6,4) //Telefone Celular, com o d�gito 9 a mais, sem DDD
  else if Length(xFone) = 10 then
    edEnt_Celular.Text := '('+Copy(xFone,1,2)+')'+Copy(xFone,3,4)+'-'+Copy(xFone,7,4) //Telefone normal ou celular sem o d�gito 9, com o DDD
  else if Length(xFone) = 11 then
    edEnt_Celular.Text := '('+Copy(xFone,1,2)+')'+Copy(xFone,3,5)+'-'+Copy(xFone,8,4) //Telefone Celular, com o d�gito 9 a mais, com DDD
  else
    edEnt_Celular.Text := Copy(StringOfChar(' ',15),1,15);
  if Trim(edEnt_Celular.Text) <> '' then begin
    Entidade := TEntidade.Create;
    if Entidade.RecuperarPorCelular(edEnt_Celular.Text) then begin
      if Entidade.ent_id <> edCodigo.AsInteger then begin
        msgPerson('J� existe um cadastro com o mesmo celular!'+#13+'C�digo: '+IntToStr(Entidade.ent_id)+#13+'Cliente: '+Entidade.ent_nome+#13+'CPF: '+Entidade.ent_cpf,[mbok],'Cliente',mtWarning);
        Exit;
      end;
    end;
    FreeAndNil(Entidade);
  end;
end;

procedure TF_CadEntidade.edEnt_CPFEnter(Sender: TObject);
begin
  inherited;
  edEnt_CPF.Text := SomenteNumero(edEnt_CPF.Text);
  edEnt_CPF.SelectAll;
end;

procedure TF_CadEntidade.edEnt_CPFExit(Sender: TObject);
var
  xCPF : String;
begin
  inherited;
  xCPF := Trim(SomenteNumero(edEnt_CPF.Text));
  if Trim(xCPF) <> '' then begin
    if ValidaCPF(xCPF) = False then begin
      msgPerson('CPF inv�lido!',[mbok],'Entidade',mtError);
      edEnt_CPF.SetFocus;
      edEnt_CPF.SelectAll;
      Exit;
    end;
    edEnt_CPF.Text := Copy(xCPF,1,3)+'.'+Copy(xCPF,4,3)+'.'+Copy(xCPF,7,3)+'-'+Copy(xCPF,10,2);

    Entidade := TEntidade.Create;
    if Entidade.RecuperarPorCPF(edEnt_CPF.Text) then begin
      if Entidade.ent_id <> edCodigo.AsInteger then begin
        msgPerson('J� existe um cadastro com o mesmo CPF!'+#13+'C�digo: '+IntToStr(Entidade.ent_id)+#13+'Cliente: '+Entidade.ent_nome+#13+'CPF: '+Entidade.ent_cpf,[mbok],'Cliente',mtWarning);
        Exit;
      end;
    end;
    FreeAndNil(Entidade);
  end;
end;

procedure TF_CadEntidade.edEnt_TelefoneEnter(Sender: TObject);
begin
  inherited;
  edEnt_Telefone.Text := SomenteNumero(edEnt_Telefone.Text);
  edEnt_Telefone.SelectAll;
end;

procedure TF_CadEntidade.edEnt_TelefoneExit(Sender: TObject);
var
  xFone : String;
begin
  inherited;
  xFone := Trim(TiraZeroEsquerda(SomenteNumero(edEnt_Telefone.Text))); //Fixo do Cliente
  if Length(xFone) = 8 then
    edEnt_Telefone.Text := Copy(xFone,1,4)+'-'+Copy(xFone,5,4) //Telefone normal ou celular sem o d�gito 9, sem o DDD
  else if Length(xFone) = 9 then
    edEnt_Telefone.Text := Copy(xFone,1,5)+'-'+Copy(xFone,6,4) //Telefone Celular, com o d�gito 9 a mais, sem DDD
  else if Length(xFone) = 10 then
    edEnt_Telefone.Text := '('+Copy(xFone,1,2)+')'+Copy(xFone,3,4)+'-'+Copy(xFone,7,4) //Telefone normal ou celular sem o d�gito 9, com o DDD
  else if Length(xFone) = 11 then
    edEnt_Telefone.Text := '('+Copy(xFone,1,2)+')'+Copy(xFone,3,5)+'-'+Copy(xFone,8,4) //Telefone Celular, com o d�gito 9 a mais, com DDD
  else
    edEnt_Telefone.Text := Copy(StringOfChar(' ',15),1,15);
  if Trim(edEnt_Telefone.Text) <> '' then begin
    Entidade := TEntidade.Create;
    if Entidade.RecuperarPorTelefone(edEnt_Telefone.Text) then begin
      if Entidade.ent_id <> edCodigo.AsInteger then begin
        msgPerson('J� existe um cadastro com o mesmo telefone!'+#13+'C�digo: '+IntToStr(Entidade.ent_id)+#13+'Cliente: '+Entidade.ent_nome+#13+'CPF: '+Entidade.ent_cpf,[mbok],'Cliente',mtWarning);
        Exit;
      end;
    end;
    FreeAndNil(Entidade);
  end;
end;

procedure TF_CadEntidade.edFun_idExit(Sender: TObject);
begin
  inherited;
  if edFun_id.AsInteger > 0 then begin
    Funcao := TFuncao.Create;
    if Funcao.Recuperar(edFun_id.AsInteger) then begin
      edFun_descricao.Text := Funcao.fun_descricao;
    end else begin
      msgPerson('Fun��o n�o cadastrada!',[mbok],'Entidade',mtError);
      edFun_id.SetFocus;
      edFun_id.SelectAll;
    end;
    FreeAndNil(Funcao);
  end else begin
    edFun_descricao.Clear;
  end;
end;

procedure TF_CadEntidade.edFun_idKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = 113 then
    sbFuncaoClick(Sender);
end;

procedure TF_CadEntidade.edSet_idExit(Sender: TObject);
begin
  inherited;
  if edSet_id.AsInteger > 0 then begin
    Setor := TSetor.Create;
    if Setor.Recuperar(edSet_id.AsInteger) then begin
      edSet_Descricao.Text := Setor.set_descricao;
    end else begin
      msgPerson('Setor n�o cadastrado!',[mbok],'Entidade',mtError);
      edSet_id.SetFocus;
      edSet_id.SelectAll;
    end;
    FreeAndNil(Setor);
  end else begin
    edSet_Descricao.Clear;
  end;
end;

procedure TF_CadEntidade.edSet_idKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if key = 113 then
    sbSetorClick(Sender);
end;

procedure TF_CadEntidade.FormActivate(Sender: TObject);
var
  x : Integer;
begin
  inherited;
  if xAux_ConEntidade_Ent_ID > 0 then begin
    Entidade := TEntidade.Create;
    Endereco := TEndereco.Create;
    if Entidade.Recuperar(xAux_ConEntidade_Ent_ID) then begin
      edCodigo.AsInteger           := xAux_ConEntidade_Ent_ID;
      edEnt_nome.Text              := Entidade.ent_nome;
      edEnt_DataCadastro.Date      := Entidade.ent_datacadastro;
      edEnt_DataNasc.Date          := Entidade.ent_datanasc;
      edEnt_RG.Text                := Entidade.ent_rg;
      edEnt_CPF.Text               := Entidade.ent_cpf;
      edEnt_EstadoCivil.ItemIndex  := Entidade.ent_estadocivil;
      edEnt_Celular.Text           := Entidade.ent_celular;
      edEnt_Telefone.Text          := Entidade.ent_telefone;
      edEnt_email.Text             := Entidade.ent_email;
      rgEnt_Ativo.ItemIndex        := Entidade.ent_ativo;
      edEnt_Obs.Text               := Entidade.ent_obs;
      rgEnt_Sexo.ItemIndex         := Entidade.ent_sexo;
      rgEnt_Tipo.ItemIndex         := Entidade.ent_tipo;
      rgEnt_TipoClick(Sender);
      edEnt_Contato.Text           := Entidade.ent_contato;
      edEnt_Celular_contato.Text   := Entidade.ent_celular_contato;
      edEnt_cnpj.Text              := Entidade.ent_cnpj;
      edEnt_nome_fantasia.Text     := Entidade.ent_nome_fantasia;
      rgEnt_Tipo_Pessoa.ItemIndex  := Entidade.ent_tipo_pessoa;
      rgEnt_Tipo_PessoaClick(Sender);
      edEnt_limite.Value           := Entidade.ent_limite;
      rgEnt_liberado.ItemIndex     := Entidade.ent_liberado;
      edSet_id.AsInteger           := Entidade.set_id;
      edSet_idExit(Sender);
      edFun_id.AsInteger           := Entidade.fun_id;
      edFun_idExit(Sender);
      rgEnt_Tipo_Cliente.ItemIndex := Entidade.ent_tipo_cliente;

      //Endereco
      if Endereco.Recuperar(1,xAux_ConEntidade_Ent_ID) then begin
        edEnd_Cep.Text                := Endereco.End_CEP;
        edEnd_Logradouro.Text         := Endereco.End_Logradouro;
        edEnd_Numero.Text             := Endereco.End_Numero;
        edEnd_Bairro.Text             := Endereco.End_Bairro;
        Cidade := TCidade.Create;
        if Cidade.Recuperar(Endereco.Cid_ID) then begin
          edCid_ID.AsInteger          := Endereco.Cid_ID;
          edDescCidade.Text           := Cidade.Cid_Cidade;
          edDescUF.Text               := Cidade.Cid_UF;
        end else begin
          edCid_ID.Clear;
          edDescCidade.Clear;
          edDescUF.Clear;
        end;
        FreeAndNil(Cidade);
        edEnd_Complemento.Text        := Endereco.End_Complemento;
        edEnd_Referencia.Text         := Endereco.end_referencia;
      end;

      Panel3.Enabled := False;
      ControleBotoes(1);
      Panel1.Enabled := False;
    end;
    FreeAndNil(Entidade);
    FreeAndNil(Endereco);
  end else begin
    edEnt_nome.Clear;
    edEnt_DataCadastro.Date      := Date;
    edEnt_DataNasc.Date          := 0;
    edEnt_RG.Clear;
    edEnt_CPF.Clear;
    edEnt_EstadoCivil.ItemIndex  := -1;
    edEnt_Celular.Clear;
    edEnt_Telefone.Clear;
    edEnt_email.Clear;
    rgEnt_Ativo.ItemIndex        := 0;
    edEnt_Obs.Clear;
    rgEnt_Sexo.ItemIndex         := 1;
    rgEnt_Tipo.ItemIndex         := 0;
    edEnt_Contato.Clear;
    edEnt_Celular_contato.Clear;
    edEnt_cnpj.Clear;
    edEnt_nome_fantasia.Clear;
    rgEnt_Tipo_Pessoa.ItemIndex  := 0;
    edEnt_limite.Clear;
    rgEnt_liberado.ItemIndex     := 0;
    edSet_id.Clear;
    edFun_id.Clear;
    rgEnt_Tipo_Cliente.ItemIndex := 1;
    TabSheet2.TabVisible         := False;

    //Endere�o
    edEnd_Cep.Clear;
    edEnd_Logradouro.Clear;
    edEnd_Bairro.Clear;
    edEnd_Numero.Clear;
    edEnd_Complemento.Clear;
    edCid_ID.Clear;
    edDescCidade.Clear;
    edDescUF.Clear;
    edEnd_Referencia.Clear;

    Panel3.Enabled := True;
    SelectFirst;
    ControleBotoes(2);

    if xAux_ConEntidade_Tela = 'COMPRA' then begin
      rgEnt_Tipo.ItemIndex := 1;
      rgEnt_TipoClick(Sender);
      rgEnt_Tipo.Enabled := False;
      rgEnt_Tipo_Pessoa.ItemIndex := 1;
      rgEnt_Tipo_PessoaClick(Sender);
      rgEnt_Tipo_Pessoa.Enabled := False;
      SelectFirst;
    end;
  end;
end;

procedure TF_CadEntidade.icoAlterarClick(Sender: TObject);
begin
  inherited;
  Panel3.Enabled := True;
  SelectFirst;
  ControleBotoes(2);
end;

procedure TF_CadEntidade.icoCancelarClick(Sender: TObject);
begin
  inherited;
  if xAux_ConEntidade_Ent_ID > 0 then begin
    Panel3.Enabled := False;
    ControleBotoes(1);
    FormActivate(Sender);
  end else begin
    Close;
  end;
end;

procedure TF_CadEntidade.icoExcluirClick(Sender: TObject);
begin
  inherited;
  if msgPerson('Tem certeza que deseja excluir o registro?',[mbYes,mbNo],'Entidade',mtWarning) = IDNO then
    Exit;

  try
    DM.FDCon.StartTransaction;
    Entidade := TEntidade.Create;
    Endereco := TEndereco.Create;

    Endereco.Eliminar(1,edCodigo.AsInteger);
    Entidade.Eliminar(edCodigo.AsInteger);

//    xAux_AuxJustificativa_Codigo   := edCodigo.AsInteger;
//    xAux_AuxJustificativa_NomeForm := UpperCase(Copy(Name,6,Length(Name)));
//    xAux_AuxJustificativa_Descricao:= edCid_Cidade.Text;
//    AbreForm(TF_AuxJustificativa,Self);
//    xAux_AuxJustificativa_Codigo   := 0;
//    xAux_AuxJustificativa_NomeForm := '';
//    xAux_AuxJustificativa_Descricao:= '';

    msgPerson('Registro eliminado com sucesso!',[mbok],'Entidade',mtConfirmation);

    FreeAndNil(Entidade);
    FreeAndNil(Endereco);
    DM.FDCon.ReleaseClients(rmFetchAll);
    DM.FDCon.Commit;
    icoFecharClick(Sender);
  except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  end;
end;

procedure TF_CadEntidade.icoSalvarClick(Sender: TObject);
begin
  inherited;
  Try
    DM.FDCon.StartTransaction;
    Entidade := TEntidade.Create;
    Endereco := TEndereco.Create;

    Entidade.ent_nome            := edEnt_nome.Text;
    Entidade.ent_datacadastro    := edEnt_DataCadastro.Date;
    Entidade.ent_celular         := edEnt_Celular.Text;
    Entidade.ent_telefone        := edEnt_Telefone.Text;
    Entidade.ent_email           := edEnt_email.Text;
    Entidade.ent_ativo           := rgEnt_Ativo.ItemIndex;
    Entidade.ent_obs             := edEnt_Obs.Text;
    Entidade.ent_tipo            := rgEnt_Tipo.ItemIndex;
    Entidade.ent_contato         := edEnt_Contato.Text;
    Entidade.ent_celular_contato := edEnt_Celular_contato.Text;
    if rgEnt_Tipo_Pessoa.ItemIndex = 0 then begin
      Entidade.ent_tipo_pessoa   := 0;
      Entidade.ent_datanasc      := edEnt_DataNasc.Date;
      Entidade.ent_rg            := edEnt_RG.Text;
      Entidade.ent_cpf           := edEnt_CPF.Text;
      Entidade.ent_estadocivil   := edEnt_EstadoCivil.ItemIndex;
      Entidade.ent_sexo          := rgEnt_Sexo.ItemIndex;
      Entidade.ent_cnpj          := '';
      Entidade.ent_nome_fantasia := '';
    end else begin
      Entidade.ent_tipo_pessoa   := 1;
      Entidade.ent_datanasc      := 0;
      Entidade.ent_rg            := '';
      Entidade.ent_cpf           := '';
      Entidade.ent_estadocivil   := -1;
      Entidade.ent_sexo          := -1;
      Entidade.ent_cnpj          := edEnt_cnpj.Text;
      Entidade.ent_nome_fantasia := edEnt_nome_fantasia.Text;
    end;
    if rgEnt_Tipo.ItemIndex = 0 then begin
      Entidade.ent_limite          := edEnt_limite.Value;
      Entidade.ent_liberado        := rgEnt_liberado.ItemIndex;
      Entidade.set_id              := edSet_id.AsInteger;
      Entidade.fun_id              := edFun_id.AsInteger;
      Entidade.ent_tipo_cliente    := rgEnt_Tipo_Cliente.ItemIndex;
    end else begin
      Entidade.ent_limite          := 0;
      Entidade.ent_liberado        := -1;
      Entidade.set_id              := 0;
      Entidade.fun_id              := 0;
      Entidade.ent_tipo_cliente    := -1;
    end;

    if edCodigo.AsInteger = 0 then begin
      xAux_ConEntidade_Ent_ID := Entidade.Ultima;
      Entidade.Gravar(xAux_ConEntidade_Ent_ID);
    end else begin
      Entidade.Gravar(xAux_ConEntidade_Ent_ID);
    end;

    //Endereco
    Endereco.End_CEP         := edEnd_Cep.Text        ;
    Endereco.End_Logradouro  := edEnd_Logradouro.Text ;
    Endereco.End_Numero      := edEnd_Numero.Text     ;
    Endereco.End_Bairro      := edEnd_Bairro.Text     ;
    Endereco.End_Complemento := edEnd_Complemento.Text;
    Endereco.end_referencia  := edEnd_Referencia.Text;
    if edCid_ID.AsInteger > 0 then
      Endereco.Cid_ID        := edCid_ID.AsInteger;
    Endereco.Gravar(1,xAux_ConEntidade_Ent_ID);

    FreeAndNil(Entidade);
    FreeAndNil(Endereco);

//    if edCodigo.AsInteger = 0 then begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 1;
//      log.log_tabela      := 'CIDADE';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'GRAVADO C�DIGO: '+IntToStr(xAux_ConCidade_Cid_ID)+' - '+edCid_Cidade.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end else begin
//      //Ir� gravar o log de acesso
//      Log                 := TLog.create;
//      log.log_tipo        := 2;
//      log.log_tabela      := 'CIDADE';
//      log.log_data        := Date;
//      log.log_hora        := TimeToStr(Time);
//      log.log_idUsuario   := xAux_IDUsuario;
//      log.log_nomeUsuario := xAux_NomeUsuario;
//      log.log_valor       := 0;
//      log.log_obs         := 'ALTERADO C�DIGO: '+IntToStr(xAux_ConCidade_Cid_ID)+' - '+edCid_Cidade.Text;
//      log.log_motivo      := '';
//      log.Gravar(log.Ultima);
//      FreeAndNil(Log);
//      //final do log
//    end;

    DM.FDCon.ReleaseClients(rmFetchAll); DM.FDCon.Commit;

    if edCodigo.AsInteger = 0 then
      msgPerson('Entidade '+IntToStr(xAux_ConEntidade_Ent_ID)+' cadastrada com sucesso!',[mbOK], 'Entidade', mtConfirmation)
    else
      msgPerson('Entidade '+IntToStr(xAux_ConEntidade_Ent_ID)+' alterada com sucesso!',[mbOK], 'Entidade', mtConfirmation);
    Close;
  Except
    on e:Exception do begin
      EnviaErroPorEmail(Self,e.Message,xAux_NomeUsuario);
      DM.FDCon.Rollback;
      ShowMessage('Erro na grava��o! ('+e.message+' )');
    end;
  End;
end;

procedure TF_CadEntidade.rgEnt_TipoClick(Sender: TObject);
begin
  inherited;
  if rgEnt_Tipo.ItemIndex = 0 then begin
    TabSheet3.TabVisible    := True;
    PageControl1.ActivePage := TabSheet3;
  end else if rgEnt_Tipo.ItemIndex = 1 then begin
    TabSheet3.TabVisible := False;
  end else if rgEnt_Tipo.ItemIndex = 2 then begin
    TabSheet3.TabVisible := False;
  end;
end;

procedure TF_CadEntidade.rgEnt_Tipo_PessoaClick(Sender: TObject);
begin
  inherited;
  if rgEnt_Tipo_Pessoa.ItemIndex = 0 then begin
    TabSheet1.TabVisible := True;
    TabSheet2.TabVisible := False;
    Label2.Caption       := 'Nome';
  end else begin
    TabSheet1.TabVisible := False;
    TabSheet2.TabVisible := True;
    Label2.Caption       := 'Raz�o Social';
  end;
end;

procedure TF_CadEntidade.sbCidadeClick(Sender: TObject);
begin
  inherited;
  AbreForm(TF_ConCidade,Self);
  edCid_ID.AsInteger := xAux_ConCidade_Cid_ID;
  edCid_IDExit(Sender);
  edCid_ID.SetFocus;
end;

procedure TF_CadEntidade.sbFuncaoClick(Sender: TObject);
begin
  inherited;
  xAux_ConFuncao_AtivaEnter := True;
  AbreForm(TF_ConFuncao,Self);
  xAux_ConFuncao_AtivaEnter := False;
  edFun_id.AsInteger := xAux_ConFuncao_Fun_ID;
  edFun_idExit(Sender);
  edFun_id.SetFocus;
end;

procedure TF_CadEntidade.sbSetorClick(Sender: TObject);
begin
  inherited;
  xAux_ConSetor_AtivaEnter := True;
  AbreForm(TF_ConSetor,Self);
  xAux_ConSetor_AtivaEnter := False;
  edSet_id.AsInteger := xAux_ConSetor_Set_ID;
  edSet_idExit(Sender);
  edSet_id.SetFocus;
end;

end.
