unit ConCredito;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ConPadrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid, Vcl.ComCtrls,
  Vcl.Imaging.pngimage, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TF_ConCredito = class(TF_ConPadrao)
    FDCredito: TFDQuery;
    ProviderCredito: TDataSetProvider;
    CdsCredito: TClientDataSet;
    DsCredito: TDataSource;
    FDCreditoENT_NOME: TStringField;
    FDCreditoENT_ID: TIntegerField;
    FDCreditoCRED_ID: TIntegerField;
    FDCreditoCRED_VALOR: TFloatField;
    FDCreditoCRED_DATA: TSQLTimeStampField;
    FDCreditoCRED_TIPO: TStringField;
    FDCreditoCR_ID: TIntegerField;
    FDCreditoCRP_SEQ: TIntegerField;
    CdsCreditoENT_NOME: TStringField;
    CdsCreditoENT_ID: TIntegerField;
    CdsCreditoCRED_ID: TIntegerField;
    CdsCreditoCRED_VALOR: TFloatField;
    CdsCreditoCRED_DATA: TSQLTimeStampField;
    CdsCreditoCRED_TIPO: TStringField;
    CdsCreditoCR_ID: TIntegerField;
    CdsCreditoCRP_SEQ: TIntegerField;
    procedure FormActivate(Sender: TObject);
    procedure icoFecharClick(Sender: TObject);
    procedure icoNovoClick(Sender: TObject);
    procedure icoPesquisarClick(Sender: TObject);
    procedure JvDBGrid1DblClick(Sender: TObject);
    procedure JvDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_ConCredito             : TF_ConCredito;
  xAux_ConCredito_Ent_ID   : Integer;
  xAux_ConCredito_Cred_ID  : Integer;
  xAux_ConCredito_Ent_Nome : String;

implementation

{$R *.dfm}

uses DMC, X, CadCredito;

procedure TF_ConCredito.FormActivate(Sender: TObject);
begin
  inherited;
  icoPesquisarClick(Sender);
end;

procedure TF_ConCredito.icoFecharClick(Sender: TObject);
begin
  xAux_ConCredito_Ent_ID  := CdsCredito.FieldByName('ent_id').AsInteger;
  xAux_ConCredito_Cred_ID := CdsCredito.FieldByName('cred_id').AsInteger;
  inherited;

end;

procedure TF_ConCredito.icoNovoClick(Sender: TObject);
begin
  inherited;
  xAux_ConCredito_Ent_ID  := 0;
  xAux_ConCredito_Cred_ID := 0;
  AbreForm(TF_CadCredito,Self);
  icoPesquisarClick(Sender);
  if xAux_ConCredito_Cred_ID <> 0 then
    CdsCredito.Locate('ent_id;cred_id',VarArrayOf([xAux_ConCredito_Ent_ID,xAux_ConCredito_Cred_ID]),[]);
end;

procedure TF_ConCredito.icoPesquisarClick(Sender: TObject);
begin
  inherited;
  CdsCredito.Close;

  FDCredito.Close;
  FDCredito.Params.Clear;
  FDCredito.SQL.Clear;
  FDCredito.SQL.Add('select e.ent_nome, ec.*                       '+
                    'from entidade_credito ec                      '+
                    'inner join entidade e on ec.ent_id = e.ent_id '+
                    'order by e.ent_nome, ec.cred_id               ');
  CdsCredito.Open;

  if not CdsCredito.Eof then
    lblRegTotal.Caption := 'N� de Registros: '+IntToStr(CdsCredito.RecordCount)
  else
    lblRegTotal.Caption := 'N� de Registros: 0';
end;

procedure TF_ConCredito.JvDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  xAux_ConCredito_Ent_ID  := CdsCredito.FieldByName('ent_id').AsInteger;
  xAux_ConCredito_Cred_ID := CdsCredito.FieldByName('cred_id').AsInteger;
  AbreForm(TF_CadCredito, Self);
  icoPesquisarClick(Sender);
  if xAux_ConCredito_Cred_ID <> 0 then
    CdsCredito.Locate('ent_id;cred_id',VarArrayOf([xAux_ConCredito_Ent_ID,xAux_ConCredito_Cred_ID]),[]);
end;

procedure TF_ConCredito.JvDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if Column.FieldName = 'CRED_TIPO' then begin
    if CdsCredito.FieldByName('CRED_TIPO').AsString = 'C' then begin
      JvDBGrid1.Canvas.Brush.Color := clGreen;
      JvDBGrid1.Canvas.Font.Color  := clWhite;
    end else if CdsCredito.FieldByName('CRED_TIPO').AsString = 'D' then begin
      JvDBGrid1.Canvas.Brush.Color := clRed;
      JvDBGrid1.Canvas.Font.Color  := clWhite;
    end else begin
      JvDBGrid1.Canvas.Brush.Color := clWhite;
      JvDBGrid1.Canvas.Font.Color  := clBlack;
    end;
  end;

  JvDBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

end.
