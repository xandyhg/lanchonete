inherited F_ConVendas: TF_ConVendas
  Caption = ' Consulta de Vendas'
  ClientHeight = 476
  ClientWidth = 995
  ExplicitWidth = 1011
  ExplicitHeight = 515
  PixelsPerInch = 96
  TextHeight = 17
  inherited Panel2: TPanel
    Top = 363
    Width = 995
    ExplicitTop = 363
    ExplicitWidth = 995
    inherited icoPesquisar: TImage
      Left = 575
      ExplicitLeft = 575
    end
    inherited icoNovo: TImage
      Left = 680
      ExplicitLeft = 680
    end
    inherited icoImprimir: TImage
      Left = 785
      ExplicitLeft = 785
    end
    inherited icoFechar: TImage
      Left = 890
      ExplicitLeft = 890
    end
  end
  inherited StatusBar1: TStatusBar
    Top = 440
    Width = 995
    ExplicitTop = 440
    ExplicitWidth = 995
  end
  inherited Panel3: TPanel
    Width = 995
    Height = 322
    ExplicitWidth = 995
    ExplicitHeight = 322
    inherited JvDBGrid1: TJvDBGrid
      Width = 995
      Height = 322
      DataSource = DsVendas
      OnDblClick = JvDBGrid1DblClick
      TitleRowHeight = 21
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'VENDA_DATA'
          Title.Alignment = taCenter
          Title.Caption = 'Data Venda'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 98
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'VENDA_HORA'
          Title.Alignment = taCenter
          Title.Caption = 'Hora'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 68
          Visible = True
        end
        item
          Alignment = taLeftJustify
          Expanded = False
          FieldName = 'VENDA_ID'
          Title.Caption = 'C'#243'd. Venda'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENT_NOME'
          Title.Caption = 'Cliente'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 295
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VENDA_DESCONTO'
          Title.Caption = 'Desconto'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 81
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VENDA_TOTAL'
          Title.Caption = 'Total'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 79
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'USUA_LOGIN'
          Title.Alignment = taCenter
          Title.Caption = 'Usu'#225'rio Venda'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -13
          Title.Font.Name = 'Segoe UI'
          Title.Font.Style = []
          Width = 126
          Visible = True
        end>
    end
  end
  inherited Panel4: TPanel
    Top = 408
    Width = 995
    ExplicitTop = 408
    ExplicitWidth = 995
    inherited pnPesquisar: TPanel
      Left = 575
      ExplicitLeft = 575
      ExplicitHeight = 32
    end
    inherited pnNovo: TPanel
      Left = 680
      ExplicitLeft = 680
      ExplicitHeight = 32
    end
    inherited pnImprimir: TPanel
      Left = 785
      ExplicitLeft = 785
      ExplicitHeight = 32
    end
    inherited pnFechar: TPanel
      Left = 890
      ExplicitLeft = 890
      ExplicitHeight = 32
    end
  end
  inherited Panel1: TPanel
    Width = 995
    ExplicitWidth = 995
  end
  object FDVendas: TFDQuery
    Connection = DM.FDCon
    SQL.Strings = (
      
        'select v.venda_id, v.ent_id, e.ent_cpf, e.ent_nome, v.venda_tota' +
        'l,'
      
        'v.venda_data, v.venda_hora, v.venda_desconto, v.usua_id, u.usua_' +
        'login'
      'from vendas v'
      'left outer join entidade e on v.ent_id=e.ent_id'
      'left outer join usuario u on v.usua_id=u.usua_id'
      'order by v.venda_data, v.venda_hora, e.ent_nome')
    Left = 744
    Top = 8
    object FDVendasVENDA_ID: TIntegerField
      FieldName = 'VENDA_ID'
      Origin = 'VENDA_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDVendasENT_ID: TIntegerField
      FieldName = 'ENT_ID'
      Origin = 'ENT_ID'
    end
    object FDVendasENT_CPF: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ENT_CPF'
      Origin = 'ENT_CPF'
      ProviderFlags = []
      ReadOnly = True
      Size = 14
    end
    object FDVendasENT_NOME: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ENT_NOME'
      Origin = 'ENT_NOME'
      ProviderFlags = []
      ReadOnly = True
      Size = 250
    end
    object FDVendasVENDA_TOTAL: TFloatField
      FieldName = 'VENDA_TOTAL'
      Origin = 'VENDA_TOTAL'
    end
    object FDVendasVENDA_DATA: TSQLTimeStampField
      FieldName = 'VENDA_DATA'
      Origin = 'VENDA_DATA'
    end
    object FDVendasVENDA_HORA: TStringField
      FieldName = 'VENDA_HORA'
      Origin = 'VENDA_HORA'
      FixedChar = True
      Size = 8
    end
    object FDVendasVENDA_DESCONTO: TFloatField
      FieldName = 'VENDA_DESCONTO'
      Origin = 'VENDA_DESCONTO'
    end
    object FDVendasUSUA_ID: TIntegerField
      FieldName = 'USUA_ID'
      Origin = 'USUA_ID'
    end
    object FDVendasUSUA_LOGIN: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'USUA_LOGIN'
      Origin = 'USUA_LOGIN'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
  end
  object ProviderVendas: TDataSetProvider
    DataSet = FDVendas
    Left = 776
    Top = 8
  end
  object CdsVendas: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderVendas'
    Left = 808
    Top = 8
    object CdsVendasVENDA_ID: TIntegerField
      FieldName = 'VENDA_ID'
      Required = True
    end
    object CdsVendasENT_ID: TIntegerField
      FieldName = 'ENT_ID'
    end
    object CdsVendasENT_CPF: TStringField
      FieldName = 'ENT_CPF'
      ReadOnly = True
      Size = 14
    end
    object CdsVendasENT_NOME: TStringField
      FieldName = 'ENT_NOME'
      ReadOnly = True
      Size = 250
    end
    object CdsVendasVENDA_TOTAL: TFloatField
      FieldName = 'VENDA_TOTAL'
      DisplayFormat = '#,##0.00'
    end
    object CdsVendasVENDA_DATA: TSQLTimeStampField
      FieldName = 'VENDA_DATA'
    end
    object CdsVendasVENDA_HORA: TStringField
      FieldName = 'VENDA_HORA'
      FixedChar = True
      Size = 8
    end
    object CdsVendasVENDA_DESCONTO: TFloatField
      FieldName = 'VENDA_DESCONTO'
      DisplayFormat = '#,##0.00'
    end
    object CdsVendasUSUA_ID: TIntegerField
      FieldName = 'USUA_ID'
    end
    object CdsVendasUSUA_LOGIN: TStringField
      FieldName = 'USUA_LOGIN'
      ReadOnly = True
      Size = 50
    end
  end
  object DsVendas: TDataSource
    DataSet = CdsVendas
    Left = 840
    Top = 8
  end
end
