program Lanchonete;

uses
  Vcl.Forms,
  U_Principal in 'U_Principal.pas' {Principal},
  DMC in 'DMC.pas' {DM: TDataModule},
  X in 'X.pas',
  U_Login in 'U_Login.pas' {F_Login},
  clsUsuario in 'clsUsuario.pas',
  AuxPadrao in 'AuxPadrao.pas' {F_AuxPadrao},
  CadPadrao in 'CadPadrao.pas' {F_CadPadrao},
  ConPadrao in 'ConPadrao.pas' {F_ConPadrao},
  WsCorreios in 'WsCorreios.pas',
  clsEmpresa in 'clsEmpresa.pas',
  ConUsuario in 'ConUsuario.pas' {F_ConUsuario},
  CadUsuario in 'CadUsuario.pas' {F_CadUsuario},
  AuxTrocaSenha in 'AuxTrocaSenha.pas' {F_AuxTrocaSenha},
  clsConfigEmail in 'clsConfigEmail.pas',
  ConConfigEmail in 'ConConfigEmail.pas' {F_ConConfigEmail},
  CadConfigEmail in 'CadConfigEmail.pas' {F_CadConfigEmail},
  ConCidade in 'ConCidade.pas' {F_ConCidade},
  CadCidade in 'CadCidade.pas' {F_CadCidade},
  clsCidade in 'clsCidade.pas',
  ConFuncao in 'ConFuncao.pas' {F_ConFuncao},
  CadFuncao in 'CadFuncao.pas' {F_CadFuncao},
  clsFuncao in 'clsFuncao.pas',
  ConSetor in 'ConSetor.pas' {F_ConSetor},
  CadSetor in 'CadSetor.pas' {F_CadSetor},
  clsSetor in 'clsSetor.pas',
  clsEndereco in 'clsEndereco.pas',
  clsEntidade in 'clsEntidade.pas',
  ConEntidade in 'ConEntidade.pas' {F_ConEntidade},
  CadEntidade in 'CadEntidade.pas' {F_CadEntidade},
  clsProduto in 'clsProduto.pas',
  ConProduto in 'ConProduto.pas' {F_ConProduto},
  CadProduto in 'CadProduto.pas' {F_CadProduto},
  clsCompra in 'clsCompra.pas',
  clsItens_Compra in 'clsItens_Compra.pas',
  ConCompra in 'ConCompra.pas' {F_ConCompra},
  CadCompra in 'CadCompra.pas' {F_CadCompra},
  clsConfiguracoes in 'clsConfiguracoes.pas',
  Configuracoes in 'Configuracoes.pas' {F_Configuracoes},
  AuxCompra in 'AuxCompra.pas' {F_AuxCompra},
  clsContasPagar in 'clsContasPagar.pas',
  CadContasPagar in 'CadContasPagar.pas' {F_CadContasPagar},
  ConContasPagar in 'ConContasPagar.pas' {F_ConContasPagar},
  clsVendas in 'clsVendas.pas',
  clsItens_Venda in 'clsItens_Venda.pas',
  ConVendas in 'ConVendas.pas' {F_ConVendas},
  CadVendas in 'CadVendas.pas' {F_CadVendas},
  CadContasReceber in 'CadContasReceber.pas' {F_CadContasReceber},
  ConContasReceber in 'ConContasReceber.pas' {F_ConContasReceber},
  clsContasReceber in 'clsContasReceber.pas',
  clsContasReceber_Pgto in 'clsContasReceber_Pgto.pas',
  clsItens_Venda_Pgto in 'clsItens_Venda_Pgto.pas',
  clsEntidade_Credito in 'clsEntidade_Credito.pas',
  AuxFormaPgto in 'AuxFormaPgto.pas' {F_AuxFormaPgto},
  ConCredito in 'ConCredito.pas' {F_ConCredito},
  CadCredito in 'CadCredito.pas' {F_CadCredito};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TPrincipal, Principal);
  Application.Run;
end.
