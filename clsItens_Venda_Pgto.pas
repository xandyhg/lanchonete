unit clsItens_Venda_Pgto;

interface

uses FireDAC.Comp.Client,Sysutils,X;

type TItens_Venda_Pgto = class
  public
    cr_id     : Integer;
    crp_seq   : Integer;
    venda_id  : Integer;
    pro_id    : Integer;
    ivp_valor : Double;

    function  Recuperar(Pcr_id,Pcrp_seq,Pvenda_id,Ppro_id:Integer):Boolean;
    procedure Gravar(Pcr_id,Pcrp_seq,Pvenda_id,Ppro_id:Integer);
    procedure Eliminar(Pcr_id,Pcrp_seq,Pvenda_id,Ppro_id:Integer);
    procedure EliminarTodosItens(Pcr_id,Pcrp_seq:Integer);
  end;

implementation

{ TItens_Venda_Pgto }

uses DMC;

procedure TItens_Venda_Pgto.Eliminar(Pcr_id,Pcrp_seq,Pvenda_id,Ppro_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Itens_Venda_Pgto where cr_id = :k_cr_id and crp_seq = :k_crp_seq and venda_id = :K_venda_id and pro_id = :k_pro_id');
  tq.ParamByName('k_cr_id').AsInteger    := Pcr_id;
  tq.ParamByName('k_crp_seq').AsInteger  := Pcrp_seq;
  tq.ParamByName('K_venda_id').AsInteger := Pvenda_id;
  tq.ParamByName('K_pro_id').AsInteger   := Ppro_id;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TItens_Venda_Pgto.EliminarTodosItens(Pcr_id,Pcrp_seq:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('delete from Itens_Venda_Pgto where cr_id = :k_cr_id and crp_seq = :k_crp_seq');
  tq.ParamByName('k_cr_id').AsInteger    := Pcr_id;
  tq.ParamByName('k_crp_seq').AsInteger  := Pcrp_seq;
  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

procedure TItens_Venda_Pgto.Gravar(Pcr_id,Pcrp_seq,Pvenda_id,Ppro_id:Integer);
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Itens_Venda_Pgto where cr_id = :k_cr_id and crp_seq = :k_crp_seq and venda_id = :k_venda_id and pro_id = :k_pro_id');
  tq.ParamByName('k_cr_id').AsInteger    := Pcr_id;
  tq.ParamByName('k_crp_seq').AsInteger  := Pcrp_seq;
  tq.ParamByName('k_venda_id').AsInteger := Pvenda_id;
  tq.ParamByName('k_pro_id').AsInteger   := Ppro_id;
  tq.Open;
  if tq.Eof then begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('Insert into Itens_Venda_Pgto values('+
    ':k_cr_id,'+
    ':k_crp_seq,'+
    ':k_venda_id,'+
    ':k_pro_id,'+
    ':k_ivp_valor)');
  end else begin
    tq.Close; tq.SQL.Clear; tq.Params.Clear;
    tq.SQL.Add('update Itens_Venda_Pgto set '+
    'ivp_valor = :k_ivp_valor ');
    tq.SQL.Add('where cr_id = :k_cr_id and crp_seq = :k_crp_seq and venda_id = :K_venda_id and pro_id = :k_pro_id ');
  end;

  tq.ParamByName('k_cr_id').AsInteger    := Pcr_id;
  tq.ParamByName('k_crp_seq').AsInteger  := Pcrp_seq;
  tq.ParamByName('k_venda_id').AsInteger := Pvenda_id;
  tq.ParamByName('k_pro_id').AsInteger   := Ppro_id;
  tq.ParamByName('k_ivp_valor').AsFloat  := ivp_valor;

  tq.ExecSQL;
  tq.Close;
  tq.Free;
end;

function  TItens_Venda_Pgto.Recuperar(Pcr_id,Pcrp_seq,Pvenda_id,Ppro_id:Integer):Boolean;
var
  tq : TFDQuery;
begin
  tq := TFDQuery.Create(nil);
  tq.Connection := DM.FDCon;
  tq.SQL.Clear;
  tq.Params.Clear;
  tq.SQL.Add('Select * from Itens_Venda_Pgto where cr_id = :k_cr_id and crp_seq = :k_crp_seq and venda_id = :K_venda_id and pro_id = :k_pro_id');
  tq.ParamByName('k_cr_id').AsInteger    := Pcr_id;
  tq.ParamByName('k_crp_seq').AsInteger  := Pcrp_seq;
  tq.ParamByName('k_venda_id').AsInteger := Pvenda_id;
  tq.ParamByName('k_pro_id').AsInteger   := Ppro_id;
  tq.Open;

  if tq.Eof then
    Result := False
  else
    Result := True;

  cr_id     := tq.FieldByName('cr_id').AsInteger;
  crp_seq   := tq.FieldByName('crp_seq').AsInteger;
  venda_id  := tq.FieldByName('venda_id').AsInteger;
  pro_id    := tq.FieldByName('pro_id').AsInteger;
  ivp_valor := tq.FieldByName('ivp_valor').AsFloat;

  tq.Close;
  tq.Free;
end;

end.
